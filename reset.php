<!DOCTYPE html>
<html lang="en">
<head>
    <title>Saycec</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, shrink-to-fit=no">

    <link rel="icon" type="image/png" href="../asset/images/shortcut.png"/>
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap-4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../font/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../font/fontawesome/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="../font/font-lato/latofonts.css">
    <link rel="stylesheet" type="text/css" href="../font/font-lato/latostyle.css">
    <link rel="stylesheet" type="text/css" href="../asset/css/login.css">

    <style type="text/css">
        label.error{
            color: #E64B9A;
            font-size: .75rem;
            margin-top: 5px;
            padding-left: 24px;
        }
        .alert-guaramo {
            border-color: transparent;
            border-radius: 0;
            background: #77415d;
            font-family: LatoWeb;
            font-size: .85rem;
            color: #fff;
            padding: 10px 26px;
        }
        .logo-login{
            width: 50%;
        }
    </style>

</head>
<body>
    
    <div id="home" name="home" class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form id="t_user_form" name="t_user_form" class="login100-form validate-form" onsubmit="return false" autocomplete="off">
                    <input type="hidden" id="t_user_id" name="t_user_id" maxlength="22" value="<?php echo $tfs->getUserId(); ?>">
                    <span class="login100-form-title p-b-4" >
                        <img src="../asset/images/logologin.png" alt="Guaramo." class="img-fluid logo-login">
                    </span>
                    <span class="login100-form-subtitle mb-5" >
                    </span>

                    <div  id="t_user_form_alert" name="t_user_form_alert" class="alert tf-alert" role="alert" style="display: none">
                    </div>
                    <div class="wrap-input100 validate-input">
                        <input class="input100 t-user-password" type="password" id="t_user[password]" name="t_user[password]">
                        <span class="focus-input100"></span>
                        <span class="label-input100">Ingresa tu contrase&ntilde;a</span>
                    </div>
                    <div><label for="t_user[password]" class="error"></label></div>

                    <div class="wrap-input100 validate-input">
                        <input class="input100 t-user-password-r" type="password" id="t_user[password_r]" name="t_user[password_r]">
                        <span class="focus-input100"></span>
                        <span class="label-input100">Repite tu contrase&ntilde;a</span>
                    </div>
                    <div><label for="t_user[password_r]" class="error"></label></div>
                   
                    <div class="container-login100-form-btn">
                        <button id="login_btn" name="login_btn" class="login100-form-btn">
                            Cambiar
                        </button>
                    </div>
                </form>
                <div class="login100-more" style="background-image: url('../asset/images/bg-forgot.jpg');">
                </div>
            </div>
        </div>
    </div>
    

    <script type="text/javascript" src="../vendor/jquery-3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="../vendor/popper-1.16.0/popper.min.js"></script>
    <script type="text/javascript" src="../vendor/bootstrap-4.5.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../vendor/validation-1.19.2/dist/jquery.validate.js"></script>
    <script type="text/javascript" src="../vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
    <script type="text/javascript" src="../vendor/jsencrypt/jsencrypt.min.js"></script>
    <script type="text/javascript" src="../vendor/serializeObject/jquery.serializeObject.min.js"></script>
    <script type="text/javascript" src="../vendor/serializeJSON/jquery.serializeJSON.min.js"></script>
    <script type="text/javascript" src="../vendor/crypto-js/crypto-js.js"></script>
    <script type="text/javascript" src="../vendor/Encryption/js/Encryption.js"></script> 
    <script type="text/javascript" src="../asset/js/main.js"></script>
    <script type="text/javascript" src="../tuich/core/js/TfCryptography.js"></script>
    <script type="text/javascript" src="../tuich/core/js/TfRequest.js"></script>
    <script type="text/javascript" src="../tuich/core/js/TfValidate.js"></script>
    <script type="text/javascript" src="../localization/datatables_es.js"></script>
    <script type="text/javascript" src="../localization/messages_es.js"></script>

    <script type="text/javascript">
        

         $("#login_btn").on('click', function (e) {            
            if ($("#t_user_form").valid()){
                let tfPost = $('#t_user_form').serializeJSON();
        
                if (TfRequest.reset(tfPost)=='Y'){
                    history.pushState({ step: 0 },null,sessionStorage.getItem('TF_URL_ROOT')+"/0");
                    window.location.href=sessionStorage.getItem('TF_URL_ROOT')+'/0';
                }else{
                    $("#t_user_form_alert").html('Cambio de Contrase&ntilde;a Erroneo');
                    $("#t_user_form_alert").show(); 
                }
            }                
        });


        
        
        

        $(document).ready(function() {
            
            $("#t_user_form").validate();
      

            $(".t-user-password").rules("add", {
                required: true,
                minlength: 8,
                maxlength: 40,
                atLeastOneUppercaseLetter: true,
                atLeastOneNumber: true,
                atLeastOneSymbol: true        
            });

            $(".t-user-password-r").rules("add", {
                required: true,
                minlength: 8,
                maxlength: 40,
                equalTo: ".t-user-password"
            });
     
        }); 

    </script>

</body>
</html>

