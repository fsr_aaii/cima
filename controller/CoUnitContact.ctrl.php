<?php
  if ($tfRequestAction=="AL"){
     $coUnitContactList=CoUnitContact::dataList($tfs);
  }else{
     $coUnitContact = new CoUnitContact($tfs);
     $coUnitContact->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coUnitContact->setCreatedBy($tfs->getUserId());
      $coUnitContact->setCreatedDate(date("Y-m-d H:i:s"));
      $coUnitContact->setValidations();
      $coUnitContact->create();
      if ($coUnitContact->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coUnitContact->update();
      if ($coUnitContact->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoUnitContact","AE",tfRequest::encrypt(array("co_unit_contact_id" => $coUnitContact->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coUnitContact->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoUnitContact.rvw.php");
    }else{
      require("view/CoUnitContact.vw.php");
    } 
  }
?>
