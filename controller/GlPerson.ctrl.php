<?php
  if ($tfRequestAction=="AL"){
     $glPersonList=GlPerson::dataList($tfs);
  }else{
     $glPerson = new GlPerson($tfs);
     $glPerson->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $glPerson->setCreatedBy($tfs->getUserId());
      $glPerson->setCreatedDate(date("Y-m-d H:i:s"));
      $glPerson->setValidations();
      $glPerson->create();
      if ($glPerson->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("GlPerson","AE",tfRequest::encrypt(array("gl_person_id" => $glPerson->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $glPerson->update();
      if ($glPerson->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("GlPerson","AE",tfRequest::encrypt(array("gl_person_id" => $glPerson->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $glPerson->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/GlPerson.rvw.php");
    }else{
      require("view/GlPerson.vw.php");
    } 
  }
?>
