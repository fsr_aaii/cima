<?php
  if ($tfRequestAction=="AL"){
     $tUserRoleList=TUserRole::dataList($tfs);
  }else{
     $tUserRole = new TUserRole($tfs);
     $tUserRole->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $tUserRole->setCreatedBy($tfs->getUserId());
      $tUserRole->setCreatedDate(date("Y-m-d H:i:s"));
      $tUserRole->setValidations();
      $tUserRole->create();
      if ($tUserRole->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $tUserRole->update();
      if ($tUserRole->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("TUserRole","AE",tfRequest::encrypt(array("t_user_role_id" => $tUserRole->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $tUserRole->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/TUserRole.rvw.php");
    }else{
      require("view/TUserRole.vw.php");
    } 
  }
?>
