<?php
  if ($tfRequestAction=="AL"){
     $hrEvaluationCriteriaOptionList=HrEvaluationCriteriaOption::dataList($tfs);
  }else{
     $hrEvaluationCriteriaOption = new HrEvaluationCriteriaOption($tfs);
     $hrEvaluationCriteriaOption->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrEvaluationCriteriaOption->setCreatedBy($tfs->getUserId());
      $hrEvaluationCriteriaOption->setCreatedDate(date("Y-m-d H:i:s"));
      $hrEvaluationCriteriaOption->setValidations();
      $hrEvaluationCriteriaOption->create();
      if ($hrEvaluationCriteriaOption->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $hrEvaluationCriteriaOption->update();
      if ($hrEvaluationCriteriaOption->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrEvaluationCriteriaOption","AE",tfRequest::encrypt(array("hr_evaluation_criteria_option_id" => $hrEvaluationCriteriaOption->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrEvaluationCriteriaOption->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrEvaluationCriteriaOption.rvw.php");
    }else{
      require("view/HrEvaluationCriteriaOption.vw.php");
    } 
  }
?>
