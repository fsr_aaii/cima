<?php
  if ($tfRequestAction=="AL"){
     $opDestinationList=OpDestination::dataList($tfs);
  }else{
     $opDestination = new OpDestination($tfs);
     $opDestination->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $opDestination->setRow($opDestination::qtyByReport($tfs,$opDestination->getIdInspectionReport())+1);
      $opDestination->setActive('Y');
      $opDestination->setProcess('N');
      $opDestination->setCreatedBy($tfs->getUserId());
      $opDestination->setCreatedDate(date("Y-m-d H:i:s"));
      $opDestination->setValidations();
      $opDestination->create();
      if ($opDestination->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $opDestination->update();
      if ($opDestination->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("OpDestination","AE",tfRequest::encrypt(array("op_destination_id" => $opDestination->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $opDestination->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/OpDestination.rvw.php");
    }else{
      require("view/OpDestination.vw.php");
    } 
  }
?>
