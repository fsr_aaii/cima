<?php
  if ($tfRequestAction=="AL"){
     $coBillDetailList=CoBillDetail::dataList($tfs);
  }else{
     $coBillDetail = new CoBillDetail($tfs);
     $coBillDetail->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coBillDetail->setCreatedBy($tfs->getUserId());
      $coBillDetail->setCreatedDate(date("Y-m-d H:i:s"));
      $coBillDetail->setValidations();
      $coBillDetail->create();
      if ($coBillDetail->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coBillDetail->update();
      if ($coBillDetail->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoBillDetail","AE",tfRequest::encrypt(array("co_bill_detail_id" => $coBillDetail->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coBillDetail->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoBillDetail.rvw.php");
    }else{
      require("view/CoBillDetail.vw.php");
    } 
  }
?>
