<?php
  if ($tfRequestAction=="AL"){
     $mmPolymerClosureTypeList=MmPolymerClosureType::dataList($tfs);
  }else{
     $mmPolymerClosureType = new MmPolymerClosureType($tfs);
     $mmPolymerClosureType->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $mmPolymerClosureType->setCreatedBy($tfs->getUserId());
      $mmPolymerClosureType->setCreatedDate(date("Y-m-d H:i:s"));
      $mmPolymerClosureType->setValidations();
      $mmPolymerClosureType->create();
      if ($mmPolymerClosureType->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("MmPolymerClosureType","AE",tfRequest::encrypt(array("mm_polymer_closure_type_id" => $mmPolymerClosureType->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $mmPolymerClosureType->update();
      if ($mmPolymerClosureType->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("MmPolymerClosureType","AE",tfRequest::encrypt(array("mm_polymer_closure_type_id" => $mmPolymerClosureType->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $mmPolymerClosureType->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/MmPolymerClosureType.rvw.php");
    }else{
      require("view/MmPolymerClosureType.vw.php");
    } 
  }
?>
