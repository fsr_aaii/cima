<?php
  if ($tfRequestAction=="AL"){
     $glPersonRelationshipList=GlPersonRelationship::dataList($tfs);
  }else{
     $glPersonRelationship = new GlPersonRelationship($tfs);
     $glPersonRelationship->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $glPersonRelationship->setCreatedBy($tfs->getUserId());
      $glPersonRelationship->setCreatedDate(date("Y-m-d H:i:s"));
      $glPersonRelationship->setValidations();
      $glPersonRelationship->create();
      if ($glPersonRelationship->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("GlPersonRelationship","AE",tfRequest::encrypt(array("gl_person_relationship_id" => $glPersonRelationship->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $glPersonRelationship->update();
      if ($glPersonRelationship->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("GlPersonRelationship","AE",tfRequest::encrypt(array("gl_person_relationship_id" => $glPersonRelationship->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $glPersonRelationship->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/GlPersonRelationship.rvw.php");
    }else{
      require("view/GlPersonRelationship.vw.php");
    } 
  }
?>
