<?php

  if ($tfRequestAction=="AL"){
     //$coAccountingList=CoAccounting::dataList($tfs);
     $glPersonRelationshipList=GlPersonRelationship::customerList($tfs);
  }elseif ($tfRequestAction=="AC"){
    $glPersonRelationship = GlPersonRelationship::condominium($tfs,$tfRequest->gl_person_id);
    $coAccountingList=CoAccounting::dLByPersonYear($tfs,$tfRequest->gl_person_id,date("Y"));
  }elseif ($tfRequestAction=="AV"){
     $coAccounting = new CoAccounting($tfs);
     $coAccounting->populate($tfRequest,in_array($tfRequestAction,array("AC","ACC","AE", "GR"))); 

     $coAccountingDetailList=CoAccountingDetail::dLByAccounting($tfs,$coAccounting->getId());
     $coAccountingUnitList=CoAccountingUnit::dLByAccountingU($tfs,$coAccounting->getId(),$tfRequest->co_accounting_id_unit);

     $coAccountingSumary=$coAccounting->sumaryByAccountingU($tfRequest->co_accounting_id_unit);

     $coExchangeRate=CoExchange::rate($tfs,$coAccounting->getIdPerson());
  }else{
     $coAccounting = new CoAccounting($tfs);
     $coAccounting->populate($tfRequest,in_array($tfRequestAction,array("AC","ACC","AE", "GR"))); 

     $coAccountingDetailList=CoAccountingDetail::dLByAccounting($tfs,$coAccounting->getId());
     $coAccountingUnitList=CoAccountingUnit::dLByAccounting($tfs,$coAccounting->getId());

     $coAccountingSumary=$coAccounting->sumaryByAccounting();

     //$coExchangeRate=CoExchange::rate($tfs,$coAccounting->getIdPerson()); 

    if ($coAccounting->getRate()==''){
     $coExchangeRate=CoExchange::rate($tfs,$coAccounting->getIdPerson(),$coAccounting->getPaymentDate()); 
    }else{
     $coExchangeRate=$coAccounting->getRate();
    }

  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
      break; 
    case "AE":
      break; 
    case "AI":
      $coAccounting->setCreatedBy($tfs->getUserId());
      $coAccounting->setCreatedDate(date("Y-m-d H:i:s"));
      $coAccounting->setValidations();
      $coAccounting->create();
      if ($coAccounting->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("CoAccounting","AE",$tfResponse->encrypt(array("co_accounting_id" => $coAccounting->getId())),2);
        $tfRequestAction="AE";
      }
      //echo '<br>SWAP<br>';
      //$tfs->printTrail();
      break;
    case "AA":
      $coAccounting->update();
      if ($coAccounting->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoAccounting","AE",$tfResponse->encrypt(array("co_accounting_id" => $coAccounting->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coAccounting->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
    case "GR": //Generar Recibo
    
       $coAccounting->setProcessedBy($tfs->getUserId());
       $coAccounting->setProcessedDate(date("Y-m-d H:i:s"));
     
      $coAccounting->generate();
      if ($coAccounting->isValid()){  
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoAccounting.rvw.php");
    }elseif ($tfRequestAction=="AC"){
     require("view/CoAccounting.rovw.php");     
    }elseif ($tfRequestAction=="ACC"){
     require("view/CoAccountingACC.rovw.php");     
    }elseif ($tfRequestAction=="AV"){
     require("view/CoAccountingAV.rvw.php");     
    }else{ 
      require("view/CoAccounting.vw.php");
    } 
  }
?>
