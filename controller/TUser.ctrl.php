<?php

        use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

  if ($tfRequestAction=="AL"){
     $tUserList=TUser::dataList($tfs);
  }elseif ($tuichSessionAction=="AF"){
     $tUserAlert=$tfRequest->t_user_alert;
  }elseif ($tuichSessionAction=="AS"){
     //get user info
     //definir que hacer
  }else{
     $tUser = new TUser($tfs);
     $tUser->populate($tfRequest,in_array($tfRequestAction,array("AC","AE","ACT","INA"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $tUser->setCreatedBy($tfs->getUserId());
      $tUser->setCreatedDate(date("Y-m-d H:i:s"));
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $tUser->setActive('Y');
      $tUser->setPassword('Y');
      $tUser->setPasswordDate(date("Y-m-d"));
      $tUser->setEmailAccount($tUser->getLogin());
      $tUser->setNotified('N');
      $tUser->setCreatedBy($tfs->getUserId());
      $tUser->setCreatedDate(date("Y-m-d H:i:s"));
      $tUser->setValidations();
      $tUser->create();
      if ($tUser->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("TUser","AE",'{"t_user_id":"'.$tUser->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $tUser->setEmailAccount($tUser->getLogin()); 
      $tUser->update();
      if ($tUser->isValid()){ 
        $tfs->checkTrans();
      }
      $coUnitUserList=CoUnitUser::dlByUser($tfs,$tUser->getId());
        $tfs->swapTrail("TUser","AE",'{"t_user_id":"'.$tUser->getId().'"}');
        $tfRequestAction="AE";
      break;    
    case "AB":
      $tUser->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
    case "AS":
        $tUser = new TUser($tfs);
        if (TUser::exist($tfs,$tfRequest->t_user_email_account)){
           $tUser->populateLogin($tfRequest->t_user_email_account);
           $tfRequestAction="AE";

           $coUnitUserList=CoUnitUser::dlByUser($tfs,$tUser->getId());
        }else{
           $tUser->setLogin($tfRequest->t_user_email_account);
           $tfRequestAction="AN";
        }       
      break;
    case "AM":
       
        
       $token = array('tsul'=>$tUser->getLogin(),
                   'tsui'=>$tUser->getId(),
                   'tsup'=>$tUser->getPassword(),
                   'tsupd'=>$tUser->getPasswordDate(),
                   'tsum'=>$tUser->getEmailAccount());

      
       $encryption = new Encryption();  
    
      $jwt = $encryption->encrypt(json_encode($token,TRUE),TUICH_SECRET);


      $body = file_get_contents("template/forgot.html");
      $body = str_replace('{%JWT%}',TF_ROOT_PATH.'/forgot/'.$jwt,$body);
      $body = str_replace('{%NAME%}',$tUser->getName(),$body);
      $body = str_replace('{%LOGIN%}',$tUser->getEmailAccount(),$body);
    

       $mail = new PHPMailer(true);

       try {
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;

        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPDebug  = 0;
        $mail->Host       = 'mail.saycec.com';
        $mail->Port       = 465;
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPAuth   = true;
        $mail->Username = TF_MAIL_ACCOUNT;
        $mail->Password = TF_MAIL_PASSWORD;
        $mail->AddAddress($tUser->getEmailAccount(),$tUser->getName());
        $mail->From = TF_MAIL_ACCOUNT;
        $mail->SetFrom(TF_MAIL_ACCOUNT, TF_MAIL_FROM);
        $mail->Subject = utf8_decode("Bienvenido a Saycec App - crea tu contraseña");
        $mail->MsgHTML($body);
        $mail->Body = $body;

        $mail->send();
        $tUser->setObjMsg("Notificaci&oacute;n enviada satisfactoriamente");
         $coUnitUserList=CoUnitUser::dlByUser($tfs,$tUser->getId());
      } catch (Exception $e) {
        $tUser->setObjError("Error al enviar notificaci&oacute;n (".$e->getMessage().")");
      } 
       break;  
    
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/TUser.rvw.php");
    }elseif  ($tfRequestAction=="AF"){
      require("view/TUserAf.vw.php");
    }else{
      require("view/TUser.vw.php");
    } 
  }
?>
