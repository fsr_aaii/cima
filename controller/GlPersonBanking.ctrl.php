<?php
  if ($tfRequestAction=="AL"){
     $glPersonBankingList=GlPersonBanking::dataList($tfs);
  }else{
     $glPersonBanking = new GlPersonBanking($tfs);
     $glPersonBanking->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $glPersonBanking->setActive("Y");
      $glPersonBanking->setCreatedBy($tfs->getUserId());
      $glPersonBanking->setCreatedDate(date("Y-m-d H:i:s"));
      $glPersonBanking->setValidations();
      $glPersonBanking->create();
      if ($glPersonBanking->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $glPersonBanking->update();
      if ($glPersonBanking->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("GlPersonBanking","AE",tfRequest::encrypt(array("gl_person_banking_id" => $glPersonBanking->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $glPersonBanking->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/GlPersonBanking.rvw.php");
    }else{
      require("view/GlPersonBanking.vw.php");
    } 
  }
?>
