<?php
  if ($tfRequestAction=="AL"){
     $coBillList=CoBill::dataList($tfs);
  }elseif ($tfRequestAction=="BL"){
     //$coBillList=CoBill::dataList($tfs);
    $glPersonRelationshipList=GlPersonRelationship::customerList($tfs);
  }elseif ($tfRequestAction=="BR"){
    $coBillList=CoBill::BalanceDetailByCondominium($tfs,$tfRequest->gl_person_id);
    $balancexx = CoBill::BalanceXX($tfs,$tfRequest->gl_person_id);
  }elseif ($tfRequestAction=="MR"){
    $glPersonRelationshipList=GlPersonRelationship::customerList($tfs);
  }elseif ($tfRequestAction=="ML"){
    $coBillList=CoBill::DefaulterDetailByCondominium($tfs,$tfRequest->gl_person_id);
    $balancexx = CoBill::BalanceXX($tfs,$tfRequest->gl_person_id);
  }else{
     $coBill = new CoBill($tfs);
     $coBill->populate($tfRequest,in_array($tfRequestAction,array("AC","AE","PRT"))); 

     $coBillDetailList=CoBillDetail::sumaryByBill($tfs,$coBill->getId());
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      break; 
    case "AE":
      break; 
    case "AI":
      $coBill->setCreatedBy($tfs->getUserId());
      $coBill->setCreatedDate(date("Y-m-d H:i:s"));
      $coBill->setValidations();
      $coBill->create();
      if ($coBill->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coBill->update();
      if ($coBill->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoBill","AE",tfRequest::encrypt(array("co_bill_id" => $coBill->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coBill->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
    case "PRT":
      $coBillInfo = $coBill->info();
      $coBillDetailInfo = CoBillDetail::infoByBill($tfs,$coBill->getId());
      break;  
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoBill.rvw.php");
    }elseif ($tfRequestAction=="BL") {
      require("view/CoBillBL.rvw.php");
    }elseif ($tfRequestAction=="BR") {
      require("view/CoBillBR.rvw.php");
    }elseif ($tfRequestAction=="MR") {
      require("view/CoBillMR.rvw.php");
    }elseif ($tfRequestAction=="ML") {
      require("view/CoBillML.rvw.php");
    }elseif ($tfRequestAction=="PRT") {
      require("view/CoBill.prt.php");
    }elseif($tfRequestAction=="AC"){
      require("view/CoBill.rovw.php");
    } else{
      require("view/CoBill.vw.php");
    } 
  }
?>
