<?php
  if ($tfRequestAction=="AL"){
     $mmColorList=MmColor::dataList($tfs);
  }else{
     $mmColor = new MmColor($tfs);
     $mmColor->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $mmColor->setCreatedBy($tfs->getUserId());
      $mmColor->setCreatedDate(date("Y-m-d H:i:s"));
      $mmColor->setValidations();
      $mmColor->create();
      if ($mmColor->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("MmColor","AE",tfRequest::encrypt(array("mm_color_id" => $mmColor->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $mmColor->update();
      if ($mmColor->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("MmColor","AE",tfRequest::encrypt(array("mm_color_id" => $mmColor->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $mmColor->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/MmColor.rvw.php");
    }else{
      require("view/MmColor.vw.php");
    } 
  }
?>
