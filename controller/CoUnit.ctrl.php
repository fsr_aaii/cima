<?php
  if ($tfRequestAction=="AL"){
     $coUnitList=CoUnit::dataList($tfs);
  }else{
     $coUnit = new CoUnit($tfs);
     $coUnit->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 

     $coExchangeRate=CoExchange::rate($tfs,$coUnit->getIdPerson()); 

     $balancePrevious = CoBill::balancePrevious($tfs,$coUnit->getId());
     $accountStatus = CoBill::accountStatus($tfs,$coUnit->getId());
     
     $coPaymentList=CoPayment::dLByUnitYear($tfs,$coUnit->getId(),date("Y"));
     $coBillList=CoBill::dLByUnitYear($tfs,$coUnit->getId(),date("Y"));
  }


  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coUnit->setActive('Y');
      $coUnit->setCreatedBy($tfs->getUserId());
      $coUnit->setCreatedDate(date("Y-m-d H:i:s"));
      $coUnit->setValidations();
      $coUnit->create();
      if ($coUnit->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coUnit->update();
      if ($coUnit->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoUnit","AE",tfRequest::encrypt(array("co_unit_id" => $coUnit->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coUnit->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoUnit.rvw.php");
    }else{
      require("view/CoUnit.vw.php");
    } 
  }
?>
