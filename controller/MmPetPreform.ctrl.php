<?php
  if ($tfRequestAction=="AL"){
     $mmPetPreformList=MmPetPreform::dataList($tfs);
  }else{
     $fiTransactionElement = new FiTransactionElement($tfs);
     $fiTransactionElement->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 

     $mmPetPreform = new MmPetPreform($tfs);
     $mmPetPreform->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
       break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $fiTransactionElement->setIdTransactionElementRank(3);
      $fiTransactionElement->setIdElementType(3);
      $fiTransactionElement->setIdTransactionType(3);
      $fiTransactionElement->setActive('Y');
      $fiTransactionElement->setType('P');
      $fiTransactionElement->setCreatedBy($tfs->getUserId());
      $fiTransactionElement->setCreatedDate(date("Y-m-d H:i:s"));
      $fiTransactionElement->setValidations();
      $fiTransactionElement->create();  

      if ($fiTransactionElement->isValid()){
        $mmPetPreform->setId($fiTransactionElement->getId());
        $mmPetPreform->setIdTransactionElement($fiTransactionElement->getId());
        $mmPetPreform->setActive('Y');
        $mmPetPreform->setCreatedBy($tfs->getUserId());
        $mmPetPreform->setCreatedDate(date("Y-m-d H:i:s"));
        $mmPetPreform->setValidations();
        $mmPetPreform->create();
        if ($mmPetPreform->isValid()){ 
          $tfs->checkTrans();
          $tfs->swapTrail("MmPetPreform","AE",tfRequest::encrypt(array("mm_pet_preform_id" => $mmPetPreform->getId())),2);
          $tfRequestAction="AE";
        }
      }


      break;
    case "AA":
      $fiTransactionElement->update();
      if ($fiTransactionElement->isValid()){ 
        $mmPetPreform->update();
        if ($mmPetPreform->isValid()){ 
          $tfs->checkTrans();
        }
      }        
      $tfs->swapTrail("MmPetPreform","AE",tfRequest::encrypt(array("mm_pet_preform_id" => $mmPetPreform->getId())),2);
       $tfRequestAction="AE";
      break;
    case "AB":
      $mmPetPreform->delete();
      $fiTransactionElement->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/MmPetPreform.rvw.php");
    }elseif (in_array($tfRequestAction,array("AN","AI"))){
      require("view/MmPetPreform.vw.php");
    }else{
      $fiTransactionElementUmList=FiTransactionElementUm::dataListByTE($tfs,$fiTransactionElement->getId());
      $sdPricingList=SdPricing::dataListByTE($tfs,$fiTransactionElement->getId());
      $sdDiscountList=SdDiscount::dataListByTE($tfs,$fiTransactionElement->getId());      
      require("view/MmPetPreform.vw.php");
    }
  }
?>
