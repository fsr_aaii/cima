<?php 
switch ($tfRequestAction){
  case "AT":
   $hrUserEvaluation = new HrUserEvaluation($tfs);
   $hrUserEvaluation->populate($tfRequest,TRUE); 
   if ($hrUserEvaluation->getStartTimestamp()==""){
   	 $hrUserEvaluation->setStartTimestamp(date("Y-m-d H:i:s"));
     $hrUserEvaluation->setSetTimer(true);
     $hrUserEvaluation->update();
   }  
   if ($hrUserEvaluation->isValid()){ 
    $tfs->checkTrans();
     if ($hrUserEvaluation->getActive()=="Y"){
	     	if ($hrUserEvaluation->getStartTimestamp()!=""){
	          if ($hrUserEvaluation->getSetTimer()){
	          	 $minutes=0;
	          }else{
	            $minutes=TfWidget::minutesElapsed(date("Y-m-d H:i:s"),$hrUserEvaluation->getStartTimestamp())+1;
	            if ($minutes<0){
	              $minutes=0;	
	            }
              }

              //echo "INI ".$hrUserEvaluation->getStartTimestamp()." FIN ".date("Y-m-d H:i:s");
	          if ($minutes>APP_EVALUATION_TIME){
	          	 $hrUserEvaluation->setActive("N");
                 $hrUserEvaluation->update();
                 $tfs->checkTrans();
                 $tfs->backTrail(); 
	          }else{ 
	          	$minutes=APP_EVALUATION_TIME-$minutes;
                $hrUserEvaluationCriteriaList=HrUserEvaluationCriteria::dLbyEvaluation($tfs,$hrUserEvaluation->getId());
                require("view/HrUserEvaluationCriteria.rvw.php");
	          }

	          
	     	}else{
	     	  require("view/HrUserEvaluationA.rvw.php");
	     	}	        
	   }else{
	   	    if ($hrUserEvaluation->getEndTimestamp()!=''){
               require("view/HrUserEvaluationB.rvw.php");
	   	    }else{
	   	    	$tfs->backTrail();
	   	    }
	     	
	     } 



 	}else{
 	  //print_r($hrUserEvaluation->getObjError());	
 	  //echo "string";
 	  require("view/HrUserEvaluationA.rvw.php");
 	}	
   break;
  case "AR":
	  if (date("d")>=APP_EVALUATION_DAY){

	  $hrUserEvaluation = new HrUserEvaluation($tfs);
	  $hrUserEvaluation->populate4ThisMonth();

	  if ($hrUserEvaluation->isValid()){ 
	      
	     if ($hrUserEvaluation->getActive()=="Y"){
	     	if ($hrUserEvaluation->getStartTimestamp()!=""){
	          if ($hrUserEvaluation->getSetTimer()){
	          	echo "stringaki";
	          	 $minutes=0;
	          }else{
	             $minutes=TfWidget::minutesElapsed(date("Y-m-d H:i:s"),$hrUserEvaluation->getStartTimestamp())+1 ;
	             if ($minutes<0){
	              $minutes=0;	
	            }
              }
              //echo "INI ".$hrUserEvaluation->getStartTimestamp()." FIN ".date("Y-m-d H:i:s");
              if ($minutes>APP_EVALUATION_TIME){
                 $hrUserEvaluation->setActive("N");
                 $hrUserEvaluation->update();
                 $tfs->checkTrans();
                 $tfs->refresh(); 
	          }else{
	          	$minutes=APP_EVALUATION_TIME-$minutes;
                $hrUserEvaluationCriteriaList=HrUserEvaluationCriteria::dLbyEvaluation($tfs,$hrUserEvaluation->getId());
                require("view/HrUserEvaluationCriteria.rvw.php");
	          }

	          
	     	}else{
	     	  require("view/HrUserEvaluationA.rvw.php");
	     	}	        
	     }else{
	     	 if ($hrUserEvaluation->getEndTimestamp()!=''){
               require("view/HrUserEvaluationB.rvw.php");
	   	    }
	     } 
	  }else{
	  	//no hay tarea previa
	    require("view/HrUserEvaluationC.rvw.php");
	  }

	 }else{
       
       $hrUserEvaluation = new HrUserEvaluation($tfs);
	   $hrUserEvaluation->populate4ThisMonth();
       
       if ($hrUserEvaluation->getActive()=='N'){
          if ($hrUserEvaluation->getEndTimestamp()!=''){
               require("view/HrUserEvaluationB.rvw.php");
	   	    }
       }else{
         $hrUserHomework = new HrUserHomework($tfs);
	     $hrUserHomework->populate4ThisMonth();

	     $hrUserHomeworkCriteriaList=HrUserHomeworkCriteria::dLbyHomework($tfs,$hrUserHomework->getId());
 	     require("view/HrUserHomeworkCriteria.rvw.php");
       } 
	  


	 }
   break;
}
?>