<?php
  if ($tfRequestAction=="AL"){
     $coPromptPaymentList=CoPromptPayment::dataList($tfs);
  }else{
     $coPromptPayment = new CoPromptPayment($tfs);
     $coPromptPayment->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coPromptPayment->setCreatedBy($tfs->getUserId());
      $coPromptPayment->setCreatedDate(date("Y-m-d H:i:s"));
      $coPromptPayment->setValidations();
      $coPromptPayment->create();
      if ($coPromptPayment->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coPromptPayment->update();
      if ($coPromptPayment->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoPromptPayment","AE",tfRequest::encrypt(array("co_prompt_payment_id" => $coPromptPayment->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coPromptPayment->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoPromptPayment.rvw.php");
    }else{
      require("view/CoPromptPayment.vw.php");
    } 
  }
?>
