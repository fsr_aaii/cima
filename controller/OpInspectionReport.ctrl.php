<?php
  //use PhpOffice\PhpSpreadsheet\Spreadsheet;
  //use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
  //use PhpOffice\PhpSpreadsheet\IOFactory;

  if ($tfRequestAction=="AL"){
     $opInspectionReportList=OpInspectionReport::dataList($tfs);
  }else{
     $opInspectionReport = new OpInspectionReport($tfs);
     $opInspectionReport->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 


     $opDestinationList=OpDestination::dLByReport($tfs,$opInspectionReport->getId());
     $opNotificationList=OpNotification::dLByReport($tfs,$opInspectionReport->getId());
  }
  switch ($tfRequestAction){
    
    case "AN":

 break;

    case "ANSSS":
     
    $client = OpInspectionReport::getClient();

    $drive = new Google_Service_Drive($client); 
      
    //$file=$opInspectionReport->leer($client);
    //$opInspectionReport->downloadFile($client);  
    
    OpInspectionReport::sync($tfs,$drive,$client,'1llssrd48gOZ_pKYAX0xlvcggK9m3fq_R');

    $tfs->checkTrans();


    //$file=$opInspectionReport->createFile($drive,$client);

   //print_r($file);

//$spreadsheet = new Spreadsheet();
//$sheet = $spreadsheet->getActiveSheet();
//$sheet->setCellValue('A1', 'Hello World !');
//
//$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
//$spreadsheet = $reader->load("template/InspectionReport.xlsx");
//$s = $spreadsheet->Copy();

//$writer = new Xlsx($s);
//$writer->save('aq.xlsx');


/*if (!copy('template/InspectionReport.xlsx', 'upload/xlsx/aq11.xlsx')) {
    echo "Error al copiar $fichero...\n";
}*/


/*$spreadsheet = IOFactory::load('upload/xlsx/aq11.xlsx');
$worksheet = $spreadsheet->getSheetByName("Portada");
$worksheet->getCell('F8')->setValue(15);
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('upload/xlsx/aq1133.xlsx');*/


//load spreadsheet
//$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("upload/xlsx/aq11.xlsx");

//change it
//$sheet = $spreadsheet->getActiveSheet();
//$sheet->setCellValue('F8', 'Dionny');

//write it again to Filesystem with the same name (=replace)
//$writer = new Xlsx($spreadsheet);

//$writer->save('upload/xlsx/aq11.xlsx');


/*$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$spreadsheet = $reader->load("upload/xlsx/aq11.xlsx");

//$spreadsheet = new Spreadsheet();
//$sheet = $spreadsheet->getActiveSheet();
//$sheet->setCellValue('A1', 'Hello World !');

$spreadsheet->getSheetByName("Portada")->setCellValue('F8', 'DIONNY');

$writer = new Xlsx($spreadsheet);

$writer->save();


//echo $spreadsheet->getSheetByName("Portada")->getCell('C9')->getValue();

*/





      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":

     $pos = strpos(OpProduct::description($tfs,$opInspectionReport->getIdProduct1()),' ');

     if ($pos === false) {
        $opInspectionReport->setName($opInspectionReport->getVessel().'-'.OpProduct::description($tfs,$opInspectionReport->getIdProduct1()).'-'.OpPortPier::description($tfs,$opInspectionReport->getIdPortPier()).'-'.date("YmdHis"));
    } else {
        $opInspectionReport->setName($opInspectionReport->getVessel().'-'.strstr(OpProduct::description($tfs,$opInspectionReport->getIdProduct1()), ' ', true).'-'.OpPortPier::description($tfs,$opInspectionReport->getIdPortPier()).'-'.date("YmdHis"));
    }

      $opInspectionReport->setProcess('N');
      $opInspectionReport->setLastProcessing(date("Y-m-d H:i:s"));      
      $opInspectionReport->setCreatedDate(date("Y-m-d H:i:s"));
      $opInspectionReport->setCreatedBy($tfs->getUserId());
      $opInspectionReport->setCreatedDate(date("Y-m-d H:i:s"));
      $opInspectionReport->setValidations();
      $opInspectionReport->create();
      if ($opInspectionReport->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }else{
         print_r($opInspectionReport->getAttrErrors());
      }
      break;
    case "AA":

      $opInspectionReport->update();
      if ($opInspectionReport->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("OpInspectionReport","AE",tfRequest::encrypt(array("op_inspection_report_id" => $opInspectionReport->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $opInspectionReport->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/OpInspectionReport.rvw.php");
    }else{
      require("view/OpInspectionReport.vw.php");
    } 
  }
?>
