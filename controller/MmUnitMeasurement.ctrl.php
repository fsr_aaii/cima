<?php
  if ($tfRequestAction=="AL"){
     $mmUnitMeasurementList=MmUnitMeasurement::dataList($tfs);
  }else{
     $mmUnitMeasurement = new MmUnitMeasurement($tfs);
     $mmUnitMeasurement->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $mmUnitMeasurement->setCreatedBy($tfs->getUserId());
      $mmUnitMeasurement->setCreatedDate(date("Y-m-d H:i:s"));
      $mmUnitMeasurement->setValidations();
      $mmUnitMeasurement->create();
      if ($mmUnitMeasurement->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("MmUnitMeasurement","AE",tfRequest::encrypt(array("mm_unit_measurement_id" => $mmUnitMeasurement->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $mmUnitMeasurement->update();
      if ($mmUnitMeasurement->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("MmUnitMeasurement","AE",tfRequest::encrypt(array("mm_unit_measurement_id" => $mmUnitMeasurement->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $mmUnitMeasurement->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/MmUnitMeasurement.rvw.php");
    }else{
      require("view/MmUnitMeasurement.vw.php");
    } 
  }
?>
