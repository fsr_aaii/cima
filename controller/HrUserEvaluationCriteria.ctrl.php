<?php
  if ($tfRequestAction=="AL"){
     $hrUserEvaluationCriteriaList=HrUserEvaluationCriteria::dataList($tfs);
  }else{
     $hrUserEvaluationCriteria = new HrUserEvaluationCriteria($tfs);
     $hrUserEvaluationCriteria->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrUserEvaluationCriteria->setCreatedBy($tfs->getUserId());
      $hrUserEvaluationCriteria->setCreatedDate(date("Y-m-d H:i:s"));
      $hrUserEvaluationCriteria->setValidations();
      $hrUserEvaluationCriteria->create();
      if ($hrUserEvaluationCriteria->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $hrUserEvaluationCriteria->update();
      if ($hrUserEvaluationCriteria->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrUserEvaluationCriteria","AE",tfRequest::encrypt(array("hr_user_evaluation_criteria_id" => $hrUserEvaluationCriteria->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrUserEvaluationCriteria->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrUserEvaluationCriteria.rvw.php");
    }else{
      require("view/HrUserEvaluationCriteria.vw.php");
    } 
  }
?>
