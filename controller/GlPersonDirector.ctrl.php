<?php
  if ($tfRequestAction=="AL"){
     $glPersonDirectorList=GlPersonDirector::dataList($tfs);
  }else{
     $glPersonDirector = new GlPersonDirector($tfs);
     $glPersonDirector->populate($tfRequest,in_array($tfRequestAction,array("AC","AE","ACT","INA"))); 

     $all = json_encode(CoUnitUser::userByCondominium($tfs,$glPersonDirector->getIdPerson()),true); 
  }

  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $glPersonDirector->setActive('Y');
      $glPersonDirector->setCreatedBy($tfs->getUserId());
      $glPersonDirector->setCreatedDate(date("Y-m-d H:i:s"));
      $glPersonDirector->setValidations();
      $glPersonDirector->create();
      if ($glPersonDirector->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $glPersonDirector->update();
      if ($glPersonDirector->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("GlPersonDirector","AE",tfRequest::encrypt(array("gl_person_director_id" => $glPersonDirector->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $glPersonDirector->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
    case "INA":
      $glPersonDirector->inactivate();
      if ($glPersonDirector->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "ACT":
      $glPersonDirector->activate();
      if ($glPersonDirector->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }  
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/GlPersonDirector.rvw.php");
    }else{
      require("view/GlPersonDirector.vw.php");
    } 
  }
?>
