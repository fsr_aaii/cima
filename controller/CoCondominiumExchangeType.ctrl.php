<?php
  if ($tfRequestAction=="AL"){

     $coCondominiumExchangeTypeList=CoCondominiumExchangeType::dataList($tfs);
  }else{
     $coCondominiumExchangeType = new CoCondominiumExchangeType($tfs);
     $coCondominiumExchangeType->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coCondominiumExchangeType->setActive('Y');
      $coCondominiumExchangeType->setCreatedBy($tfs->getUserId());
      $coCondominiumExchangeType->setCreatedDate(date("Y-m-d H:i:s"));
      $coCondominiumExchangeType->setValidations();
      $coCondominiumExchangeType->create();
      if ($coCondominiumExchangeType->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coCondominiumExchangeType->update();
      if ($coCondominiumExchangeType->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoCondominiumExchangeType","AE",tfRequest::encrypt(array("co_condominium_exchange_type_id" => $coCondominiumExchangeType->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coCondominiumExchangeType->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoCondominiumExchangeType.rvw.php");
    }else{
      require("view/CoCondominiumExchangeType.vw.php");
    } 
  }
?>
