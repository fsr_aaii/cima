UPDATE `t_role` SET `name` = 'Contacto', `description` = 'Contacto' WHERE `t_role`.`id` = 19;
UPDATE `t_sequence` SET `current_value` = '31' WHERE `t_sequence`.`Id` = 1;
UPDATE `t_sequence` SET `current_value` = '31' WHERE `t_sequence`.`Id` = 50;
DELETE FROM `t_process_task_role` WHERE `t_process_task_role`.`id` = 112;

INSERT INTO `t_task` (`id`, `name`, `description`, `id_task_function`, `active`, `sorting`, `created_by`, `created_date`) VALUES ('83', 'Inspecciones', 'Inspecciones', 'R', 'Y', '999', '1', '2021-06-06 13:57:19');
INSERT INTO `t_task_component` (`id`, `id_task`, `id_component`, `action`, `is_main`, `active`, `created_by`, `created_date`) VALUES ('153', '83', '263', 'AU', 'Y', 'Y', '1', '2021-06-06 15:53:05');
INSERT INTO `t_process_task` (`id`, `id_process`, `id_task`, `active`, `created_by`, `created_date`) VALUES ('69', '12', '83', 'Y', '1', '2021-06-06 13:59:52');
INSERT INTO `t_process_task_role` (`id`, `id_process_task`, `id_role`, `default`, `active`, `created_by`, `created_date`) VALUES ('166', '69', '19', 'Y', 'Y', '1', '2021-06-06 14:00:03');

UPDATE `t_process_task_role` SET `default` = 'N' WHERE `t_process_task_role`.`id` = 85;
UPDATE `t_process_task_role` SET `default` = 'Y' WHERE `t_process_task_role`.`id` = 86;




INSERT INTO `t_component` (`id`, `id_component_type`, `name`, `description`, `active`, `created_by`, `created_date`) VALUES ('300', '1', 'OpNotification', 'Controller for entity: op_notification', 'Y', '7', '2021-06-10 07:18:47');
INSERT INTO `t_task_component` (`id`, `id_task`, `id_component`, `action`, `is_main`, `active`, `created_by`, `created_date`) VALUES ('154', '68', '300', 'AN', 'N', 'Y', '1', '2021-06-10 13:20:07');