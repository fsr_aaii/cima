<?php
  if ($tfRequestAction=="AL"){
     $coAccountingUnitList=CoAccountingUnit::dataList($tfs);
  }else{
     $coAccountingUnit = new CoAccountingUnit($tfs);
     $coAccountingUnit->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coAccountingUnit->setCreatedBy($tfs->getUserId());
      $coAccountingUnit->setCreatedDate(date("Y-m-d H:i:s"));
      $coAccountingUnit->setValidations();
      $coAccountingUnit->create();
      if ($coAccountingUnit->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coAccountingUnit->update();
      if ($coAccountingUnit->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoAccountingUnit","AE",tfRequest::encrypt(array("co_accounting_unit_id" => $coAccountingUnit->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coAccountingUnit->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoAccountingUnit.rvw.php");
    }else{
      require("view/CoAccountingUnit.vw.php");
    } 
  }
?>
