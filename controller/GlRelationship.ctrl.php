<?php
  if ($tfRequestAction=="AL"){
     $glRelationshipList=GlRelationship::dataList($tfs);
  }else{
     $glRelationship = new GlRelationship($tfs);
     $glRelationship->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $glRelationship->setCreatedBy($tfs->getUserId());
      $glRelationship->setCreatedDate(date("Y-m-d H:i:s"));
      $glRelationship->setValidations();
      $glRelationship->create();
      if ($glRelationship->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("GlRelationship","AE",tfRequest::encrypt(array("gl_relationship_id" => $glRelationship->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $glRelationship->update();
      if ($glRelationship->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("GlRelationship","AE",tfRequest::encrypt(array("gl_relationship_id" => $glRelationship->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $glRelationship->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/GlRelationship.rvw.php");
    }else{
      require("view/GlRelationship.vw.php");
    } 
  }
?>
