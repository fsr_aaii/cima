<?php
  if ($tfRequestAction=="AL"){
     $hrUserHomeworkList=HrUserHomework::dataList($tfs);
  }else{
     $hrUserHomework = new HrUserHomework($tfs);
     $hrUserHomework->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrUserHomework->setCreatedBy($tfs->getUserId());
      $hrUserHomework->setCreatedDate(date("Y-m-d H:i:s"));
      $hrUserHomework->setValidations();
      $hrUserHomework->create();
      if ($hrUserHomework->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $hrUserHomework->update();
      if ($hrUserHomework->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrUserHomework","AE",tfRequest::encrypt(array("hr_user_homework_id" => $hrUserHomework->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrUserHomework->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrUserHomework.rvw.php");
    }else{
      require("view/HrUserHomework.vw.php");
    } 
  }
?>
