<?php
  if ($tfRequestAction=="AL"){
     $fiTransactionElementUmList=FiTransactionElementUm::dataList($tfs);
  }else{
     $fiTransactionElementUm = new FiTransactionElementUm($tfs);
     $fiTransactionElementUm->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $fiTransactionElementUm->setCreatedBy($tfs->getUserId());
      $fiTransactionElementUm->setCreatedDate(date("Y-m-d H:i:s"));
      $fiTransactionElementUm->setValidations();
      $fiTransactionElementUm->create();
      if ($fiTransactionElementUm->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("FiTransactionElementUm","AE",tfRequest::encrypt(array("fi_transaction_element_um_id" => $fiTransactionElementUm->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $fiTransactionElementUm->update();
      if ($fiTransactionElementUm->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("FiTransactionElementUm","AE",tfRequest::encrypt(array("fi_transaction_element_um_id" => $fiTransactionElementUm->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $fiTransactionElementUm->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/FiTransactionElementUm.rvw.php");
    }else{
      require("view/FiTransactionElementUm.vw.php");
    } 
  }
?>
