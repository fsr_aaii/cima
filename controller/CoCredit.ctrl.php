<?php
  if ($tfRequestAction=="AL"){
     $coCreditList=CoCredit::dataList($tfs);
  }else{
     $coCredit = new CoCredit($tfs);
     $coCredit->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coCredit->setCreatedBy($tfs->getUserId());
      $coCredit->setCreatedDate(date("Y-m-d H:i:s"));
      $coCredit->setValidations();
      $coCredit->create();
      if ($coCredit->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coCredit->update();
      if ($coCredit->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoCredit","AE",tfRequest::encrypt(array("co_credit_id" => $coCredit->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coCredit->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoCredit.rvw.php");
    }else{
      require("view/CoCredit.vw.php");
    } 
  }
?>
