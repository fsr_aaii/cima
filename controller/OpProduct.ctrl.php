<?php
  if ($tfRequestAction=="AL"){
     $opProductList=OpProduct::dataList($tfs);
  }else{
     $opProduct = new OpProduct($tfs);
     $opProduct->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $opProduct->setCreatedBy($tfs->getUserId());
      $opProduct->setCreatedDate(date("Y-m-d H:i:s"));
      $opProduct->setValidations();
      $opProduct->create();
      if ($opProduct->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $opProduct->update();
      if ($opProduct->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("OpProduct","AE",tfRequest::encrypt(array("op_product_id" => $opProduct->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $opProduct->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/OpProduct.rvw.php");
    }else{
      require("view/OpProduct.vw.php");
    } 
  }
?>
