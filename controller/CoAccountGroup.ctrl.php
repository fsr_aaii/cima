<?php
  if ($tfRequestAction=="AL"){
     $coAccountGroupList=CoAccountGroup::dataList($tfs);
  }else{
     $coAccountGroup = new CoAccountGroup($tfs);
     $coAccountGroup->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coAccountGroup->setCreatedBy($tfs->getUserId());
      $coAccountGroup->setCreatedDate(date("Y-m-d H:i:s"));
      $coAccountGroup->setValidations();
      $coAccountGroup->create();
      if ($coAccountGroup->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coAccountGroup->update();
      if ($coAccountGroup->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoAccountGroup","AE",tfRequest::encrypt(array("co_account_group_id" => $coAccountGroup->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coAccountGroup->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoAccountGroup.rvw.php");
    }else{
      require("view/CoAccountGroup.vw.php");
    } 
  }
?>
