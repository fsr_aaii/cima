<?php
  if ($tfRequestAction=="AL"){
     $sdPricingList=SdPricing::dataList($tfs);
  }else{
     $sdPricing = new SdPricing($tfs);
     $sdPricing->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $sdPricing->setCreatedBy($tfs->getUserId());
      $sdPricing->setCreatedDate(date("Y-m-d H:i:s"));
      $sdPricing->setValidations();
      $sdPricing->create();
      if ($sdPricing->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("SdPricing","AE",tfRequest::encrypt(array("sd_pricing_id" => $sdPricing->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $sdPricing->update();
      if ($sdPricing->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("SdPricing","AE",tfRequest::encrypt(array("sd_pricing_id" => $sdPricing->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $sdPricing->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/SdPricing.rvw.php");
    }else{
      require("view/SdPricing.vw.php");
    } 
  }
?>
