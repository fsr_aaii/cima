<?php
 //  $tfs->printTrail();
 //echo '<br>Action'.$tfRequestAction.'<br>';
 //print_r($tfRequest);
  if ($tfRequestAction=="AL"){
     $coAccountingDetailList=CoAccountingDetail::dataList($tfs);
  }else{
     $coAccountingDetail = new CoAccountingDetail($tfs);
     $coAccountingDetail->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coAccountingDetail->setCreatedBy($tfs->getUserId());
      $coAccountingDetail->setCreatedDate(date("Y-m-d H:i:s"));
      $coAccountingDetail->setValidations();
      $coAccountingDetail->create();
      if ($coAccountingDetail->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coAccountingDetail->update();
      if ($coAccountingDetail->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoAccountingDetail","AE",tfRequest::encrypt(array("co_accounting_detail_id" => $coAccountingDetail->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coAccountingDetail->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoAccountingDetail.rvw.php");
    }else{
      require("view/CoAccountingDetail.vw.php");
    } 
  }
?>
