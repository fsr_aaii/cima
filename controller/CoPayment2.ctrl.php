<?php
  if ($tfRequestAction=="AL"){

    if ($tfRequest->co_payment_id_unit!=''){

      $coExchangeRate=CoExchange::rate($tfs,$co_payment_id_person); 

      //$coBillDue=CoBill::totalDueByUnit($tfs,$co_payment_id_unit);
      //$credit=CoCredit::balanceByUnit($tfs,$co_payment_id_unit);  
    
      $balancePrevious = CoBill::balancePrevious($tfs,$tfRequest->co_payment_id_unit);
      $accountStatus = CoBill::accountStatus($tfs,$tfRequest->co_payment_id_unit);

      $coPaymentList=CoPayment::dLByUnitYear($tfs,$tfRequest->co_payment_id_unit,date("Y"));
      $co_payment_id_unit = $tfRequest->co_payment_id_unit;

      $coBillList=CoBill::dLByUnitYear($tfs,$co_payment_id_unit,date("Y"));
      $coUnitList = false;
    }else{
      $coUnitUserList=CoUnitUser::dlByUser($tfs,$tfs->getUserId());     

      if (count($coUnitUserList)>1){
        $coUnitList = true;
      }else{         

        $coExchangeRate=CoExchange::rate($tfs,$coUnitUserList[0]["id_person"]); 

        //$coBillDue=CoBill::totalDueByUnit($tfs,$coUnitUserList[0]["id_unit"]);
        //$credit=CoCredit::balanceByUnit($tfs,$coUnitUserList[0]["id_unit"]);  
        
        $balancePrevious = CoBill::balancePrevious($tfs,$coUnitUserList[0]["id_unit"]);
        $accountStatus = CoBill::accountStatus($tfs,$coUnitUserList[0]["id_unit"]);

        $coPaymentList=CoPayment::dLByUnitYear($tfs,$coUnitUserList[0]["id_unit"],date("Y"));
        $co_payment_id_unit = $coUnitUserList[0]["id_unit"];

        $coBillList=CoBill::dLByUnitYear($tfs,$co_payment_id_unit,date("Y"));
        $coUnitList = false;
      }
    }

  }elseif ($tfRequestAction=="ALL"){

    $glPersonRelationshipList=CoPayment::customerList($tfs);
    
  }elseif ($tfRequestAction=="AP"){

    $coPaymentList=CoPayment::dLByCondominium($tfs,$tfRequest->co_payment_id_condominium);
    
  }elseif ($tfRequestAction=="APN"){

    $glJuridicalPerson = new GlJuridicalPerson($tfs);
    $glJuridicalPerson->populate($tfRequest,TRUE); 
    $coUnitList=CoUnit::dLByPerson($tfs,$tfRequest->gl_juridical_person_id);
    
  }else{
     $coPayment = new CoPayment($tfs);
     $coPayment->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 

  }
  switch ($tfRequestAction){
    case "AN":
      $saldo=CoBill::totalDueByUnit($tfs,$coPayment->getIdUnit());      
      $coExchangeRate=CoExchange::rateUnit($tfs,$coPayment->getIdUnit());
      $promptPayment = CoCondominiumPromptPayment::infoByUnit($tfs,$coPayment->getIdUnit());

      if ($saldo>0){
        
        $since = CoBill::since($tfs,$coPayment->getIdUnit());
        

        if ($since["days"]>-1){
          if ($since["days"] <= $promptPayment["days"]){
             if ($since["rate"]==0){
              $coExchangeRate=CoExchange::rateUnitDate($tfs,$coPayment->getIdUnit(),date("Y-m-d",strtotime(date("d-m-Y")."- ".$since["days"] ." days")));
             }else{
              $coExchangeRate=$since["rate"];
             }
             $due='<p>Usted tiene un deuda de <b>'.TfWidget::amount($saldo).' USD</b> que a la tasa de cambio por pronto pago de (<b>'.TfWidget::amount($coExchangeRate).' VEB</b>) serian <b>'.TfWidget::amount($saldo*$coExchangeRate).' VEB </b></p>';
          }else{
             $due='<p>Usted tiene un deuda de <b>'.TfWidget::amount($saldo).' USD</b> que a la tasa de cambio actual (<b>'.TfWidget::amount($coExchangeRate).' VEB</b>) serian <b>'.TfWidget::amount($saldo*$coExchangeRate).' VEB </b></p>';
          }
        }else{
          $due='<p>Usted tiene un deuda de <b>'.TfWidget::amount($saldo).' USD</b> que a la tasa de cambio actual (<b>'.TfWidget::amount($coExchangeRate).' VEB</b>) serian <b>'.TfWidget::amount($saldo*$coExchangeRate).' VEB </b></p>';
        }
      } 
      break;
    case "AT":
      $saldo=CoBill::totalDueByUnit($tfs,$coPayment->getIdUnit());
      if ($saldo>0){
        $coExchangeRate=CoExchange::rateUnit($tfs,$coPayment->getIdUnit());
        $promptPayment = CoCondominiumPromptPayment::infoByUnit($tfs,$coPayment->getIdUnit());
        $since = CoBill::since($tfs,$coPayment->getIdUnit());
        if ($since["days"]>-1){
          if ($since["days"] <= $promptPayment["days"]){
             
             if ($since["rate"]==0){
              $coExchangeRate=CoExchange::rateUnitDate($tfs,$coPayment->getIdUnit(),date("Y-m-d",strtotime(date("d-m-Y")."- ".$since["days"] ." days")));
             }else{
              $coExchangeRate=$since["rate"];
             }

             $due='<p>Usted tiene un deuda de <b>'.TfWidget::amount($saldo).' USD</b> que a la tasa de cambio por pronto pago de (<b>'.TfWidget::amount($coExchangeRate).' VEB</b>) serian <b>'.TfWidget::amount($saldo*$coExchangeRate).' VEB </b></p>';
          }else{
             $due='<p>Usted tiene un deuda de <b>'.TfWidget::amount($saldo).' USD</b> que a la tasa de cambio actual (<b>'.TfWidget::amount($coExchangeRate).' VEB</b>) serian <b>'.TfWidget::amount($saldo*$coExchangeRate).' VEB </b></p>';
          }
        }else{
          $due='<p>Usted tiene un deuda de <b>'.TfWidget::amount($saldo).' USD</b> que a la tasa de cambio actual (<b>'.TfWidget::amount($coExchangeRate).' VEB</b>) serian <b>'.TfWidget::amount($saldo*$coExchangeRate).' VEB </b></p>';
        }
      } 
      break;  
    case "AL":
      break;
    case "AC":
      break; 
    case "AE":
      break;
    case "CEA":
      $coPayment->confirm();
      if ($coPayment->isValid()){ 
       $tfs->checkTrans();
       $tfs->backTrail(TRUE);
      }
      break;  
    case "CER":
      $coPayment->setStatus("R");
      $coPayment->setValidations();
      $coPayment->update();
      if ($coPayment->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;     
    case "AI":
     $coPayment->setStatus('I');
     //$coPayment->setRate(CoExchange::rateByDate($tfs,CoUnit::idPerson($tfs,$coPayment->getIdUnit()),$coPayment->getPaymentDate()));
     $coPayment->setCreatedBy($tfs->getUserId());
     $coPayment->setCreatedDate(date("Y-m-d H:i:s"));

      $filename=$_FILES['co_payment_photo_img']['name'];
     
        if (isset($filename)) {
          
          $ext = pathinfo($filename, PATHINFO_EXTENSION);
          if ($_FILES['co_payment_photo_img']['error']==0) {
            $allowed =  array('gif','png' ,'jpg','jpeg','bmp');           
            if(!in_array(strtolower($ext),$allowed) ) {
              $objAlerts.=TfWidget::alertDangerTemplate("Image extension not allowed,only are allowed: gif, png, jpg, jpeg, bmp");
            }else{
              $new_filename=date("mdY")."-".TfWidget::strRandom(4)."-".date("His").".".$ext;
              move_uploaded_file($_FILES['co_payment_photo_img']['tmp_name'],"upload/payment/".$new_filename);
              $coPayment->setImagePath("../../../upload/payment/".$new_filename);
              $coPayment->setValidations();
              $coPayment->create();
            }                   
          }
        }

      
      if ($coPayment->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }else{
        //print_r($coPayment->getAttrErrors());
      }
      break;

    case "AA":

    $go=true;

      $filename=$_FILES['co_payment_photo_img']['name'];
     
        if (isset($filename)) {
          
          $ext = pathinfo($filename, PATHINFO_EXTENSION);
          if ($_FILES['co_payment_photo_img']['error']==0) {
            $allowed =  array('gif','png' ,'jpg','jpeg','bmp');           
            if(!in_array(strtolower($ext),$allowed) ) {
              $objAlerts.=TfWidget::alertDangerTemplate("Image extension not allowed,only are allowed: gif, png, jpg, jpeg, bmp");
              $go=false;
            }else{
              $new_filename=date("mdY")."-".TfWidget::strRandom(4)."-".date("His").".".$ext;
              move_uploaded_file($_FILES['co_payment_photo_img']['tmp_name'],"upload/payment/".$new_filename);
              $coPayment->setImagePath("../../../upload/payment/".$new_filename);
             
            }                   
          }
        }

       $coPayment->setValidations();
      $coPayment->update();
      if ($coPayment->isValid()){ 
        $tfs->checkTrans();
      }
      if ($go){
       $tfs->backTrail(TRUE);
      } 
      
      break;
    case "AB":
      $coPayment->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      if ($coUnitList){
        require("view/CoPaymentUnits.rvw.php");
      }else{
        require("view/CoPayment.rvw.php");  
      }
    }elseif ($tfRequestAction=="ALL") {
      require("view/CoPaymentCondominium.rvw.php");
    }elseif ($tfRequestAction=="AP") {
      require("view/CoPaymentAP.rvw.php");
    }elseif (in_array($tfRequestAction,array("CEE","CEA","CER"))) {
      require("view/CoPaymentCE.vw.php");
    }elseif ($tfRequestAction=="AC") {
      require("view/CoPayment.rovw.php");
    }elseif ($tfRequestAction=="APN") {
      require("view/CoPaymentAPN.rvw.php");
    }else{
      require("view/CoPayment.vw.php");
    } 
  }
?>
