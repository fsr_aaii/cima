<?php
  if ($tfRequestAction=="AL"){
     $coExchangeList=CoExchange::dLByPerson($tfs,$tfRequest->co_exchange_id_person);

  }else if ($tfRequestAction=="ALL"){
     $customerList=CoExchange::customerList($tfs);
  }else{
     $coExchange = new CoExchange($tfs);
     $coExchange->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coExchange->setDate(date("Y-m-d"));
      $coExchange->setActive('Y');
      $coExchange->setCreatedBy($tfs->getUserId());
      $coExchange->setCreatedDate(date("Y-m-d H:i:s"));
      $coExchange->setValidations();
      $coExchange->create();
      if ($coExchange->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }else{
        echo "string";
        print_r($coExchange->getAttrErrors());
      }
      break;
    case "AA":
      $coExchange->update();
      if ($coExchange->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoExchange","AE",tfRequest::encrypt(array("co_exchange_id" => $coExchange->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coExchange->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoExchange.rvw.php");
    }else if  ($tfRequestAction=="ALL"){
      require("view/CoExchangeALL.rvw.php");
    }else{
      require("view/CoExchange.vw.php");
    } 
  }
?>
