<?php
  if ($tfRequestAction=="AL"){
     $sdDiscountList=SdDiscount::dataList($tfs);
  }else{
     $sdDiscount = new SdDiscount($tfs);
     $sdDiscount->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $sdDiscount->setCreatedBy($tfs->getUserId());
      $sdDiscount->setCreatedDate(date("Y-m-d H:i:s"));
      $sdDiscount->setValidations();
      $sdDiscount->create();
      if ($sdDiscount->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("SdDiscount","AE",tfRequest::encrypt(array("sd_discount_id" => $sdDiscount->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $sdDiscount->update();
      if ($sdDiscount->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("SdDiscount","AE",tfRequest::encrypt(array("sd_discount_id" => $sdDiscount->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $sdDiscount->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/SdDiscount.rvw.php");
    }else{
      require("view/SdDiscount.vw.php");
    } 
  }
?>
