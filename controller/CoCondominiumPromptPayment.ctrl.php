<?php
  if ($tfRequestAction=="AL"){
     $coCondominiumPromptPaymentList=CoCondominiumPromptPayment::dataList($tfs);
  }else{
     $coCondominiumPromptPayment = new CoCondominiumPromptPayment($tfs);
     $coCondominiumPromptPayment->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
    $coCondominiumPromptPayment->setCreditPercentage('0'); 
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coCondominiumPromptPayment->setActive('Y'); 
      $coCondominiumPromptPayment->setCreatedBy($tfs->getUserId());
      $coCondominiumPromptPayment->setCreatedDate(date("Y-m-d H:i:s"));
      $coCondominiumPromptPayment->setValidations();
      $coCondominiumPromptPayment->create();
      if ($coCondominiumPromptPayment->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coCondominiumPromptPayment->update();
      if ($coCondominiumPromptPayment->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoCondominiumPromptPayment","AE",tfRequest::encrypt(array("co_condominium_prompt_payment_id" => $coCondominiumPromptPayment->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coCondominiumPromptPayment->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoCondominiumPromptPayment.rvw.php");
    }else{
      require("view/CoCondominiumPromptPayment.vw.php");
    } 
  }
?>
