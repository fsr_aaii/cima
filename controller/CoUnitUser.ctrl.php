<?php
  if ($tfRequestAction=="AL"){
     $coUnitUserList=CoUnitUser::dataList($tfs);
  }else{
     $coUnitUser = new CoUnitUser($tfs);
     $coUnitUser->populate($tfRequest,in_array($tfRequestAction,array("AC","AE","ACT","INA"))); 

     $all = json_encode(CoUnit::all($tfs),true); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coUnitUser->setActive('Y');
      $coUnitUser->setCreatedBy($tfs->getUserId());
      $coUnitUser->setCreatedDate(date("Y-m-d H:i:s"));
      $coUnitUser->setValidations();
      $coUnitUser->create();
      if ($coUnitUser->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coUnitUser->update();
      if ($coUnitUser->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoUnitUser","AE",tfRequest::encrypt(array("co_unit_user_id" => $coUnitUser->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coUnitUser->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
    case "INA":
      $coUnitUser->inactivate();
      if ($coUnitUser->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "ACT":
      $coUnitUser->activate();
      if ($coUnitUser->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      
      break;  
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoUnitUser.rvw.php");
    }else{
      require("view/CoUnitUser.vw.php");
    } 
  }
?>
