<?php
  if ($tfRequestAction=="AL"){
     $mmPacketingList=MmPacketing::dataList($tfs);
  }else{
     $mmPacketing = new MmPacketing($tfs);
     $mmPacketing->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $mmPacketing->setCreatedBy($tfs->getUserId());
      $mmPacketing->setCreatedDate(date("Y-m-d H:i:s"));
      $mmPacketing->setValidations();
      $mmPacketing->create();
      if ($mmPacketing->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("MmPacketing","AE",tfRequest::encrypt(array("mm_packeting_id" => $mmPacketing->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $mmPacketing->update();
      if ($mmPacketing->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("MmPacketing","AE",tfRequest::encrypt(array("mm_packeting_id" => $mmPacketing->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $mmPacketing->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/MmPacketing.rvw.php");
    }else{
      require("view/MmPacketing.vw.php");
    } 
  }
?>
