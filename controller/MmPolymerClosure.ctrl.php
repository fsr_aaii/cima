<?php
  if ($tfRequestAction=="AL"){
     $mmPolymerClosureList=MmPolymerClosure::dataList($tfs);
  }else{
     $fiTransactionElement = new FiTransactionElement($tfs);
     $fiTransactionElement->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 

     $mmPolymerClosure = new MmPolymerClosure($tfs);
     $mmPolymerClosure->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":

      $fiTransactionElement->setIdTransactionElementRank(3);
      $fiTransactionElement->setIdElementType(3);
      $fiTransactionElement->setIdTransactionType(3);
      $fiTransactionElement->setActive('Y');
      $fiTransactionElement->setType('C');
      $fiTransactionElement->setCreatedBy($tfs->getUserId());
      $fiTransactionElement->setCreatedDate(date("Y-m-d H:i:s"));
      $fiTransactionElement->setValidations();
      $fiTransactionElement->create();  

      if ($fiTransactionElement->isValid()){
        $mmPolymerClosure->setId($fiTransactionElement->getId());
        $mmPolymerClosure->setIdTransactionElement($fiTransactionElement->getId());
        $mmPolymerClosure->setActive('Y');
        $mmPolymerClosure->setCreatedBy($tfs->getUserId());
        $mmPolymerClosure->setCreatedDate(date("Y-m-d H:i:s"));
        $mmPolymerClosure->setValidations();
        $mmPolymerClosure->create();
        if ($mmPetPreform->isValid()){ 
          $tfs->checkTrans();
          $tfs->swapTrail("MmPolymerClosure","AE",tfRequest::encrypt(array("mm_polymer_closure_id" => $mmPolymerClosure->getId())),2);
          $tfRequestAction="AE";
        }
      }

      break;
    case "AA":
      $fiTransactionElement->update();
      if ($fiTransactionElement->isValid()){ 
        $mmPolymerClosure->update();
        if ($mmPolymerClosure->isValid()){ 
          $tfs->checkTrans();
        }
      }        
      $tfs->swapTrail("MmPolymerClosure","AE",tfRequest::encrypt(array("mm_polymer_closure_id" => $mmPolymerClosure->getId())),2);
       $tfRequestAction="AE";
      break;
    case "AB":
      $fiTransactionElement->delete();
      $mmPolymerClosure->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/MmPolymerClosure.rvw.php");
    }else{
      $fiTransactionElementUmList=FiTransactionElementUm::dataListByTE($tfs,$fiTransactionElement->getId());
      $sdPricingList=SdPricing::dataListByTE($tfs,$fiTransactionElement->getId());
      $sdDiscountList=SdDiscount::dataListByTE($tfs,$fiTransactionElement->getId());     
      require("view/MmPolymerClosure.vw.php");
    } 
  }
?>
