<?php
  if ($tfRequestAction=="AL"){
     $fiElementTypeList=FiElementType::dataList($tfs);
  }else{
     $fiElementType = new FiElementType($tfs);
     $fiElementType->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $fiElementType->setCreatedBy($tfs->getUserId());
      $fiElementType->setCreatedDate(date("Y-m-d H:i:s"));
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $fiElementType->create();
      if ($fiElementType->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("FiElementType","AE",'{"fi_element_type_id":"'.$fiElementType->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $fiElementType->update();
      if ($fiElementType->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("FiElementType","AE",'{"fi_element_type_id":"'.$fiElementType->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $fiElementType->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/FiElementType.rvw.php");
    }else{
      require("view/FiElementType.vw.php");
    } 
  }
?>
