<?php
  if ($tfRequestAction=="AL"){
     $coAccountList=CoAccount::dataList($tfs);
  }else{
     $coAccount = new CoAccount($tfs);
     $coAccount->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $coAccount->setActive('Y');
      $coAccount->setCreatedBy($tfs->getUserId());
      $coAccount->setCreatedDate(date("Y-m-d H:i:s"));
      $coAccount->setValidations();
      $coAccount->create();
      if ($coAccount->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coAccount->update();
      if ($coAccount->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoAccount","AE",tfRequest::encrypt(array("co_account_id" => $coAccount->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coAccount->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CoAccount.rvw.php");
    }else{
      require("view/CoAccount.vw.php");
    } 
  }
?>
