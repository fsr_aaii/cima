<?php
  if ($tfRequestAction=="AL"){
     $opInspectionReportDetailList=OpInspectionReportDetail::dataList($tfs);
  }else{
     $opInspectionReportDetail = new OpInspectionReportDetail($tfs);
     $opInspectionReportDetail->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $opInspectionReportDetail->setCreatedBy($tfs->getUserId());
      $opInspectionReportDetail->setCreatedDate(date("Y-m-d H:i:s"));
      $opInspectionReportDetail->setValidations();
      $opInspectionReportDetail->create();
      if ($opInspectionReportDetail->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $opInspectionReportDetail->update();
      if ($opInspectionReportDetail->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("OpInspectionReportDetail","AE",tfRequest::encrypt(array("op_inspection_report_detail_id" => $opInspectionReportDetail->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $opInspectionReportDetail->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/OpInspectionReportDetail.rvw.php");
    }else{
      require("view/OpInspectionReportDetail.vw.php");
    } 
  }
?>
