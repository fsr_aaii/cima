<?php
  if ($tfRequestAction=="ALL"){
     $glPersonRelationshipList=GlPersonRelationship::customerList($tfs);
  }elseif ($tfRequestAction=="AL") {
      $coPreviousDueList=CoPreviousDue::dLByPerson($tfs,$tfRequest->co_previous_due_id_person);
  }else{
     $coPreviousDue = new CoPreviousDue($tfs);
     $coPreviousDue->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 

     
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      break; 
    case "AE":
      break; 
    case "AI":
      $coPreviousDue->setCreatedBy($tfs->getUserId());
      $coPreviousDue->setCreatedDate(date("Y-m-d H:i:s"));
      $coPreviousDue->setValidations();
      $coPreviousDue->create();
      if ($coPreviousDue->isValid()){ 
        $tfs->checkTrans();
        $tfs->backTrail(TRUE);
      }
      break;
    case "AA":
      $coPreviousDue->update();
      if ($coPreviousDue->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CoPreviousDue","AE",tfRequest::encrypt(array("co_previous_due_id" => $coPreviousDue->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $coPreviousDue->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="ALL"){
      require("view/CoPreviousDueALL.rvw.php");
    }elseif  ($tfRequestAction=="AL"){
      require("view/CoPreviousDue.rvw.php");
    }elseif  ($tfRequestAction=="AC"){
      require("view/CoPreviousDue.rovw.php");
    }else{
      require("view/CoPreviousDue.vw.php");
    } 
  }
?>
