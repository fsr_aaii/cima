<?php
  if ($tfRequestAction=="AL"){
     $fiTransactionElementList=FiTransactionElement::dataList($tfs);
  }else{
     $fiTransactionElement = new FiTransactionElement($tfs);
     $fiTransactionElement->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $fiTransactionElement->setCreatedBy($tfs->getUserId());
      $fiTransactionElement->setCreatedDate(date("Y-m-d H:i:s"));
      $fiTransactionElement->setValidations();
      $fiTransactionElement->create();
      if ($fiTransactionElement->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("FiTransactionElement","AE",tfRequest::encrypt(array("fi_transaction_element_id" => $fiTransactionElement->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $fiTransactionElement->update();
      if ($fiTransactionElement->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("FiTransactionElement","AE",tfRequest::encrypt(array("fi_transaction_element_id" => $fiTransactionElement->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $fiTransactionElement->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/FiTransactionElement.rvw.php");
    }else{
      require("view/FiTransactionElement.vw.php");
    } 
  }
?>
