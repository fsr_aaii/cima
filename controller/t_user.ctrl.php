<?php
  require_once ("modelExtend/t_user.mdlx.php");
  require_once ("modelExtend/t_functional_area.mdlx.php");
  require_once ("modelExtend/t_user_role.mdlx.php");
  require_once ("modelExtend/hr_employee.mdlx.php");
  require_once ("modelExtend/t_role.mdlx.php");

  if ($tuichSessionAction=="AL"){
     $t_user_list=t_user::listByRole($tus,$request->get("t_role_id"));
  }elseif ($tuichSessionAction=="AR"){
    //select role
  }elseif ($tuichSessionAction=="AF"){
     $t_user_alert=$request->get("t_user_alert");
  }elseif ($tuichSessionAction=="AS"){
     //get user info
     //definir que hacer
  }else{
     $t_user = new t_user($tus);
     $t_user->populate($request,in_array($tuichSessionAction,array("AC","AE","AAU","AIU","ANU"))); 
  }
  $viewTitle = "User Audit";
  switch ($tuichSessionAction){
    case "AN":
      $t_user->setActive("Y");
      $t_user->setPassword(tuich_widget::strRandom(12));
      $t_user->setPassword_Date(date("Y-m-d"));
      $t_user->setCreated_By($tus->getSessionUserId());
      $t_user->setCreated_Date(date("Y-m-d H:i:s"));
      break;
    case "AL":
      break;
    case "AC":
      $tus->setSessionReadonly(true);
      break; 
    case "AE":
      break; 
    case "AI":
      $t_user->create();
      if ($t_user->isValid()){ 
        $tus->checkTrans();
        $param=tuich_request::encode('{"t_user_id":"'.$t_user->getId().'"}');
        $tus->redirectPage($SessionURLBase."t_user/AE",$param);
      }
      break;
    case "AA":
      $t_user->setPassword_Date(date("Y-m-d"));
      $t_user->update();
      if ($t_user->isValid()){ 
        $tus->checkTrans();
      }
      $tus->removePage();
      $tuichSessionAction="AE";
      break;
     case "AS":
        $account = strpos($request->get("t_user_email_account"), '@');
        if (is_numeric($account)){
          $param=tuich_request::encode('{"t_user_alert":"Enter the uber email account (without @uber.com)"}');
          $tus->redirectPage($SessionURLBase."t_user/AF",$param);  
        }else{
           $info=t_user::info($tus,$request->get("t_user_email_account")."@uber.com");
           if (count($info)==0){
             //Is the employee registered? NO
             //Does the employee have a registered user? NO
             //Create User Unaudited
             $confirmation = 1;
             $unaudited='<b>UNAUDITED</b>';
             $message='This email account is <b>NOT</b> associated with an employee registered in the system.';
             $action='ANU';
          }elseif($info["id_employee"]!=''){
            $shortInfo=hr_employee::shortInfo($tus,$info["id_employee"]);
            if ($info["id"]!=''){
              //Is the employee registered? YES
              //Does the employee have a registered user? YES
              $param=tuich_request::encode('{"t_user_id":"'.$info["id"].'"}');
              $tus->goPage($SessionURLBase."t_user/AE",$param); 
            }else{
              if ($info["termination_date"]!=''){
                //Is the employee registered? YES
                //Does the employee have a registered user? NO
                //Is the employee active? NO
                //Create User Unaudited
                $confirmation = 2;
                $unaudited='<b>UNAUDITED</b>';
                $message='This email account is associated with an employee who is <b>NOT</b> included in the database since '.$info["termination_date"];
                $action='ANT';
              }else{
                //Is the employee registered? YES
                //Does the employee have a registered user? NO
                //Is the employee active? YES
                //Create User
                $confirmation = 3; 
                $message=' This email account has <b>NOT</b> been granted permissions to <b>ANY ROLE</b>';
                $action='AN';
              } 
            }
          }else{
            if ($info["id"]!=''){
              //Is the employee registered? NO
              //Does the employee have a registered user? YES
              $param=tuich_request::encode('{"t_user_id":"'.$info["id"].'"}');
              $tus->goPage($SessionURLBase."t_user/AE",$param); 
            }else{
                //Is the employee registered? NO
                //Does the employee have a registered user? NO
                //Is the employee active? NO
                //Create User Unaudited
                $confirmation = 4;
                $unaudited='<b>UNAUDITED</b>';
                $message='This email account is <b>NOT</b> associated with an employee registered in the system.';
                $action='ANU';
            }
          }  
        }
    break;    
    case "AB":
      try{
        $t_user->delete();
        $tus->checkTrans();
        $tus->goBack();
      }catch (Throwable $t){
        $t_user->setObjError("This user can not be deleted as it has a referential integrity");
      }
      break;
    case "AAU":
      $t_user->setActive("Y");
      $t_user->update();
      if ($t_user->isValid()){ 
        $tus->checkTrans();
      }
      $tus->removePage();
      break;
    case "AIU":
      $t_user->setActive("N");
      $t_user->update();
      if ($t_user->isValid()){ 
        $tus->checkTrans();
      }
      $tus->removePage();
      break;
    case "ANU":
        $body = file_get_contents("core/tuich-2.0/template/user.html");
        $body = str_replace('{%JWT%}',TUICH_URL_ROOT.'/'.$SessionLocationCode.'/change/'.$t_user->getJWT(),$body);
        $body = str_replace('{%USUARIO%}',$t_user->getName(),$body);
        $body = str_replace('{%LOGIN%}',$t_user->getLogin(),$body);

        $tuich_mail= new tuich_mail();

        $tuich_mail->setUserName(TUICH_MAIL_ACCOUNT);
        $tuich_mail->setPassword(TUICH_MAIL_PASSWORD);
        $tuich_mail->setFrom(TUICH_MAIL_FROM);
        $tuich_mail->setSubject("Uber Talent MX System Account");
        $tuich_mail->setBody($body);
        $sent = false;
        $sent = $tuich_mail->sendMail($t_user->getEmail_Account(),$t_user->getName());
      
        if(!$sent){
         $t_user->setObjError("Sending of Notification Mail Failed");
        } 
      break;    
  }
  if ($tus->toRedirect()){
    $tus->redirect();
  }else{
    if  ($tuichSessionAction=="AL"){
      require("view/t_user.rvw.php");
    }elseif ($tuichSessionAction=="AR"){
      require("view/t_user_ar.vw.php");
    }elseif  ($tuichSessionAction=="AF"){
      require("view/t_user_af.vw.php");
    }elseif  ($tuichSessionAction=="AS"){
      require("view/t_user_as.vw.php");
    }else{
      if (!in_array($tuichSessionAction,array("AN","AI"))){
         $permission=t_user_role::permission2($tus,$t_user->getId()); 
         $shortInfo=hr_employee::shortInfo($tus,$t_user->getId());
      }

      require("view/t_user.vw.php");
    } 
  }
?>
