<?php
  class FiTransactionElementBase extends TfEntity {
    protected $id;
    protected $name;
    protected $code;
    protected $type;
    protected $id_transaction_element_rank;
    protected $id_transaction_type;
    protected $id_element_type;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="fi_transaction_element";
  }

  private function getAll(){

    $q="SELECT id,
               name,
               code,
               type,
               id_transaction_element_rank,
               id_transaction_type,
               id_element_type,
               active,
               created_by,
               created_date
          FROM fi_transaction_element
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->name=$rs["name"];
    $this->code=$rs["code"];
    $this->type=$rs["type"];
    $this->id_transaction_element_rank=$rs["id_transaction_element_rank"];
    $this->id_transaction_type=$rs["id_transaction_type"];
    $this->id_element_type=$rs["id_element_type"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->fi_transaction_element_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_fi_transaction_element){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_fi_transaction_element; 
    }

    if ($tfRequest->exist("fi_transaction_element_name")){
      $this->name=$tfRequest->fi_transaction_element_name;
    }
    if ($tfRequest->exist("fi_transaction_element_code")){
      $this->code=$tfRequest->fi_transaction_element_code;
    }
    if ($tfRequest->exist("fi_transaction_element_type")){
      $this->type=$tfRequest->fi_transaction_element_type;
    }
    if ($tfRequest->exist("fi_transaction_element_id_transaction_element_rank")){
      $this->id_transaction_element_rank=$tfRequest->fi_transaction_element_id_transaction_element_rank;
    }
    if ($tfRequest->exist("fi_transaction_element_id_transaction_type")){
      $this->id_transaction_type=$tfRequest->fi_transaction_element_id_transaction_type;
    }
    if ($tfRequest->exist("fi_transaction_element_id_element_type")){
      $this->id_element_type=$tfRequest->fi_transaction_element_id_element_type;
    }
    if ($tfRequest->exist("fi_transaction_element_active")){
      $this->active=$tfRequest->fi_transaction_element_active;
    }
    if ($tfRequest->exist("fi_transaction_element_created_by")){
      $this->created_by=$tfRequest->fi_transaction_element_created_by;
    }
    if ($tfRequest->exist("fi_transaction_element_created_date")){
      $this->created_date=$tfRequest->fi_transaction_element_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["name"]=array("type"=>"string",
                                  "value"=>$this->name,
                                  "length"=>200,
                                  "required"=>true);
    $this->validation["code"]=array("type"=>"string",
                                  "value"=>$this->code,
                                  "length"=>15,
                                  "required"=>true);
    $this->validation["type"]=array("type"=>"string",
                                  "value"=>$this->type,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["id_transaction_element_rank"]=array("type"=>"number",
                                  "value"=>$this->id_transaction_element_rank,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_transaction_type"]=array("type"=>"number",
                                  "value"=>$this->id_transaction_type,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_element_type"]=array("type"=>"number",
                                  "value"=>$this->id_element_type,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setName($value){
  $this->name=$value;
  }
  public function getName(){
  return $this->name;
  }
  public function setCode($value){
  $this->code=$value;
  }
  public function getCode(){
  return $this->code;
  }
  public function setType($value){
  $this->type=$value;
  }
  public function getType(){
  return $this->type;
  }
  public function setIdTransactionElementRank($value){
  $this->id_transaction_element_rank=$value;
  }
  public function getIdTransactionElementRank(){
  return $this->id_transaction_element_rank;
  }
  public function setIdTransactionType($value){
  $this->id_transaction_type=$value;
  }
  public function getIdTransactionType(){
  return $this->id_transaction_type;
  }
  public function setIdElementType($value){
  $this->id_element_type=$value;
  }
  public function getIdElementType(){
  return $this->id_element_type;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO fi_transaction_element(id,
                               name,
                               code,
                               type,
                               id_transaction_element_rank,
                               id_transaction_type,
                               id_element_type,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->name==''?NULL:$this->name,
                     $this->code==''?NULL:$this->code,
                     $this->type==''?NULL:$this->type,
                     $this->id_transaction_element_rank==''?NULL:$this->id_transaction_element_rank,
                     $this->id_transaction_type==''?NULL:$this->id_transaction_type,
                     $this->id_element_type==''?NULL:$this->id_element_type,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->name!= $rs["name"]){
          if ($this->updateable["name"]){
            $set.=$set_aux."name=?";
            $set_aux=",";
            $param[]=$this->name==''?NULL:$this->name;
          }else{
            $this->objError[]="The field (name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->code!= $rs["code"]){
          if ($this->updateable["code"]){
            $set.=$set_aux."code=?";
            $set_aux=",";
            $param[]=$this->code==''?NULL:$this->code;
          }else{
            $this->objError[]="The field (code) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->type!= $rs["type"]){
          if ($this->updateable["type"]){
            $set.=$set_aux."type=?";
            $set_aux=",";
            $param[]=$this->type==''?NULL:$this->type;
          }else{
            $this->objError[]="The field (type) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_transaction_element_rank!= $rs["id_transaction_element_rank"]){
          if ($this->updateable["id_transaction_element_rank"]){
            $set.=$set_aux."id_transaction_element_rank=?";
            $set_aux=",";
            $param[]=$this->id_transaction_element_rank==''?NULL:$this->id_transaction_element_rank;
          }else{
            $this->objError[]="The field (id_transaction_element_rank) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_transaction_type!= $rs["id_transaction_type"]){
          if ($this->updateable["id_transaction_type"]){
            $set.=$set_aux."id_transaction_type=?";
            $set_aux=",";
            $param[]=$this->id_transaction_type==''?NULL:$this->id_transaction_type;
          }else{
            $this->objError[]="The field (id_transaction_type) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_element_type!= $rs["id_element_type"]){
          if ($this->updateable["id_element_type"]){
            $set.=$set_aux."id_element_type=?";
            $set_aux=",";
            $param[]=$this->id_element_type==''?NULL:$this->id_element_type;
          }else{
            $this->objError[]="The field (id_element_type) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE fi_transaction_element ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM fi_transaction_element
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
