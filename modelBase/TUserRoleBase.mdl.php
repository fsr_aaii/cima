<?php
  class TUserRoleBase extends TfEntity {
    protected $id;
    protected $id_user;
    protected $id_functional_area_role;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="t_user_role";
  }

  private function getAll(){

    $q="SELECT id,
               id_user,
               id_functional_area_role,
               active,
               created_by,
               created_date
          FROM t_user_role
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_user=$rs["id_user"];
    $this->id_functional_area_role=$rs["id_functional_area_role"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->t_user_role_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_t_user_role){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_t_user_role; 
    }

    if ($tfRequest->exist("t_user_role_id_user")){
      $this->id_user=$tfRequest->t_user_role_id_user;
    }
    if ($tfRequest->exist("t_user_role_id_functional_area_role")){
      $this->id_functional_area_role=$tfRequest->t_user_role_id_functional_area_role;
    }
    if ($tfRequest->exist("t_user_role_active")){
      $this->active=$tfRequest->t_user_role_active;
    }
    if ($tfRequest->exist("t_user_role_created_by")){
      $this->created_by=$tfRequest->t_user_role_created_by;
    }
    if ($tfRequest->exist("t_user_role_created_date")){
      $this->created_date=$tfRequest->t_user_role_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_user"]=array("type"=>"number",
                                  "value"=>$this->id_user,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_functional_area_role"]=array("type"=>"number",
                                  "value"=>$this->id_functional_area_role,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdUser($value){
  $this->id_user=$value;
  }
  public function getIdUser(){
  return $this->id_user;
  }
  public function setIdFunctionalAreaRole($value){
  $this->id_functional_area_role=$value;
  }
  public function getIdFunctionalAreaRole(){
  return $this->id_functional_area_role;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO t_user_role(id,
                               id_user,
                               id_functional_area_role,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_user==''?NULL:$this->id_user,
                     $this->id_functional_area_role==''?NULL:$this->id_functional_area_role,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_user!= $rs["id_user"]){
          if ($this->updateable["id_user"]){
            $set.=$set_aux."id_user=?";
            $set_aux=",";
            $param[]=$this->id_user==''?NULL:$this->id_user;
          }else{
            $this->objError[]="The field (id_user) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_functional_area_role!= $rs["id_functional_area_role"]){
          if ($this->updateable["id_functional_area_role"]){
            $set.=$set_aux."id_functional_area_role=?";
            $set_aux=",";
            $param[]=$this->id_functional_area_role==''?NULL:$this->id_functional_area_role;
          }else{
            $this->objError[]="The field (id_functional_area_role) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE t_user_role ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM t_user_role
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
