<?php
  class CoCreditBase extends TfEntity {
    protected $id;
    protected $id_unit;
    protected $amount;
    protected $balance;
    protected $rate;
    protected $status;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="co_credit";
  }

  private function getAll(){

    $q="SELECT id,
               id_unit,
               amount,
               balance,
               rate,
               status,
               created_by,
               created_date
          FROM co_credit
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_unit=$rs["id_unit"];
    $this->amount=$rs["amount"];
    $this->balance=$rs["balance"];
    $this->rate=$rs["rate"];
    $this->status=$rs["status"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->co_credit_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_co_credit){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_co_credit; 
    }

    if ($tfRequest->exist("co_credit_id_unit")){
      $this->id_unit=$tfRequest->co_credit_id_unit;
    }
    if ($tfRequest->exist("co_credit_amount")){
      $this->amount=$tfRequest->co_credit_amount;
    }
    if ($tfRequest->exist("co_credit_balance")){
      $this->balance=$tfRequest->co_credit_balance;
    }
    if ($tfRequest->exist("co_credit_rate")){
      $this->rate=$tfRequest->co_credit_rate;
    }
    if ($tfRequest->exist("co_credit_status")){
      $this->status=$tfRequest->co_credit_status;
    }
    if ($tfRequest->exist("co_credit_created_by")){
      $this->created_by=$tfRequest->co_credit_created_by;
    }
    if ($tfRequest->exist("co_credit_created_date")){
      $this->created_date=$tfRequest->co_credit_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_unit"]=array("type"=>"number",
                                  "value"=>$this->id_unit,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["amount"]=array("type"=>"number",
                                  "value"=>$this->amount,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["balance"]=array("type"=>"number",
                                  "value"=>$this->balance,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["rate"]=array("type"=>"number",
                                  "value"=>$this->rate,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["status"]=array("type"=>"string",
                                  "value"=>$this->status,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdUnit($value){
  $this->id_unit=$value;
  }
  public function getIdUnit(){
  return $this->id_unit;
  }
  public function setAmount($value){
  $this->amount=$value;
  }
  public function getAmount(){
  return $this->amount;
  }
  public function setBalance($value){
  $this->balance=$value;
  }
  public function getBalance(){
  return $this->balance;
  }
  public function setRate($value){
  $this->rate=$value;
  }
  public function getRate(){
  return $this->rate;
  }
  public function setStatus($value){
  $this->status=$value;
  }
  public function getStatus(){
  return $this->status;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO co_credit(id,
                               id_unit,
                               amount,
                               balance,
                               rate,
                               status,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_unit==''?NULL:$this->id_unit,
                     $this->amount==''?NULL:$this->amount,
                     $this->balance==''?NULL:$this->balance,
                     $this->rate==''?NULL:$this->rate,
                     $this->status==''?NULL:$this->status,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_unit!= $rs["id_unit"]){
          if ($this->updateable["id_unit"]){
            $set.=$set_aux."id_unit=?";
            $set_aux=",";
            $param[]=$this->id_unit==''?NULL:$this->id_unit;
          }else{
            $this->objError[]="The field (id_unit) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->amount!= $rs["amount"]){
          if ($this->updateable["amount"]){
            $set.=$set_aux."amount=?";
            $set_aux=",";
            $param[]=$this->amount==''?NULL:$this->amount;
          }else{
            $this->objError[]="The field (amount) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->balance!= $rs["balance"]){
          if ($this->updateable["balance"]){
            $set.=$set_aux."balance=?";
            $set_aux=",";
            $param[]=$this->balance==''?NULL:$this->balance;
          }else{
            $this->objError[]="The field (balance) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->rate!= $rs["rate"]){
          if ($this->updateable["rate"]){
            $set.=$set_aux."rate=?";
            $set_aux=",";
            $param[]=$this->rate==''?NULL:$this->rate;
          }else{
            $this->objError[]="The field (rate) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->status!= $rs["status"]){
          if ($this->updateable["status"]){
            $set.=$set_aux."status=?";
            $set_aux=",";
            $param[]=$this->status==''?NULL:$this->status;
          }else{
            $this->objError[]="The field (status) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE co_credit ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM co_credit
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
