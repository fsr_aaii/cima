<?php
  class GlJuridicalPersonBase extends TfEntity {
    protected $id;
    protected $nin_type;
    protected $nin;
    protected $nin_control_digit;
    protected $email_account;
    protected $legal_name;
    protected $trade_name;
    protected $legal_address;
    protected $phone_number;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="gl_juridical_person";
  }

  private function getAll(){

    $q="SELECT id,
               nin_type,
               nin,
               nin_control_digit,
               email_account,
               legal_name,
               trade_name,
               legal_address,
               phone_number,
               active,
               created_by,
               created_date
          FROM gl_juridical_person
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->nin_type=$rs["nin_type"];
    $this->nin=$rs["nin"];
    $this->nin_control_digit=$rs["nin_control_digit"];
    $this->email_account=$rs["email_account"];
    $this->legal_name=$rs["legal_name"];
    $this->trade_name=$rs["trade_name"];
    $this->legal_address=$rs["legal_address"];
    $this->phone_number=$rs["phone_number"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->gl_juridical_person_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_gl_juridical_person){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_gl_juridical_person; 
    }

    if ($tfRequest->exist("gl_juridical_person_nin_type")){
      $this->nin_type=$tfRequest->gl_juridical_person_nin_type;
    }
    if ($tfRequest->exist("gl_juridical_person_nin")){
      $this->nin=$tfRequest->gl_juridical_person_nin;
    }
    if ($tfRequest->exist("gl_juridical_person_nin_control_digit")){
      $this->nin_control_digit=$tfRequest->gl_juridical_person_nin_control_digit;
    }
    if ($tfRequest->exist("gl_juridical_person_email_account")){
      $this->email_account=$tfRequest->gl_juridical_person_email_account;
    }
    if ($tfRequest->exist("gl_juridical_person_legal_name")){
      $this->legal_name=$tfRequest->gl_juridical_person_legal_name;
    }
    if ($tfRequest->exist("gl_juridical_person_trade_name")){
      $this->trade_name=$tfRequest->gl_juridical_person_trade_name;
    }
    if ($tfRequest->exist("gl_juridical_person_legal_address")){
      $this->legal_address=$tfRequest->gl_juridical_person_legal_address;
    }
    if ($tfRequest->exist("gl_juridical_person_phone_number")){
      $this->phone_number=$tfRequest->gl_juridical_person_phone_number;
    }
    if ($tfRequest->exist("gl_juridical_person_active")){
      $this->active=$tfRequest->gl_juridical_person_active;
    }
    if ($tfRequest->exist("gl_juridical_person_created_by")){
      $this->created_by=$tfRequest->gl_juridical_person_created_by;
    }
    if ($tfRequest->exist("gl_juridical_person_created_date")){
      $this->created_date=$tfRequest->gl_juridical_person_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["nin_type"]=array("type"=>"string",
                                  "value"=>$this->nin_type,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["nin"]=array("type"=>"string",
                                  "value"=>$this->nin,
                                  "length"=>9,
                                  "required"=>true);
    $this->validation["nin_control_digit"]=array("type"=>"number",
                                  "value"=>$this->nin_control_digit,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["email_account"]=array("type"=>"string",
                                  "value"=>$this->email_account,
                                  "length"=>500,
                                  "required"=>false);
    $this->validation["legal_name"]=array("type"=>"string",
                                  "value"=>$this->legal_name,
                                  "length"=>200,
                                  "required"=>true);
    $this->validation["trade_name"]=array("type"=>"string",
                                  "value"=>$this->trade_name,
                                  "length"=>200,
                                  "required"=>true);
    $this->validation["legal_address"]=array("type"=>"string",
                                  "value"=>$this->legal_address,
                                  "length"=>1000,
                                  "required"=>true);
    $this->validation["phone_number"]=array("type"=>"string",
                                  "value"=>$this->phone_number,
                                  "length"=>20,
                                  "required"=>false);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setNinType($value){
  $this->nin_type=$value;
  }
  public function getNinType(){
  return $this->nin_type;
  }
  public function setNin($value){
  $this->nin=$value;
  }
  public function getNin(){
  return $this->nin;
  }
  public function setNinControlDigit($value){
  $this->nin_control_digit=$value;
  }
  public function getNinControlDigit(){
  return $this->nin_control_digit;
  }
  public function setEmailAccount($value){
  $this->email_account=$value;
  }
  public function getEmailAccount(){
  return $this->email_account;
  }
  public function setLegalName($value){
  $this->legal_name=$value;
  }
  public function getLegalName(){
  return $this->legal_name;
  }
  public function setTradeName($value){
  $this->trade_name=$value;
  }
  public function getTradeName(){
  return $this->trade_name;
  }
  public function setLegalAddress($value){
  $this->legal_address=$value;
  }
  public function getLegalAddress(){
  return $this->legal_address;
  }
  public function setPhoneNumber($value){
  $this->phone_number=$value;
  }
  public function getPhoneNumber(){
  return $this->phone_number;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO gl_juridical_person(id,
                               nin_type,
                               nin,
                               nin_control_digit,
                               email_account,
                               legal_name,
                               trade_name,
                               legal_address,
                               phone_number,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->nin_type==''?NULL:$this->nin_type,
                     $this->nin==''?NULL:$this->nin,
                     $this->nin_control_digit==''?NULL:$this->nin_control_digit,
                     $this->email_account==''?NULL:$this->email_account,
                     $this->legal_name==''?NULL:$this->legal_name,
                     $this->trade_name==''?NULL:$this->trade_name,
                     $this->legal_address==''?NULL:$this->legal_address,
                     $this->phone_number==''?NULL:$this->phone_number,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->nin_type!= $rs["nin_type"]){
          if ($this->updateable["nin_type"]){
            $set.=$set_aux."nin_type=?";
            $set_aux=",";
            $param[]=$this->nin_type==''?NULL:$this->nin_type;
          }else{
            $this->objError[]="The field (nin_type) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->nin!= $rs["nin"]){
          if ($this->updateable["nin"]){
            $set.=$set_aux."nin=?";
            $set_aux=",";
            $param[]=$this->nin==''?NULL:$this->nin;
          }else{
            $this->objError[]="The field (nin) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->nin_control_digit!= $rs["nin_control_digit"]){
          if ($this->updateable["nin_control_digit"]){
            $set.=$set_aux."nin_control_digit=?";
            $set_aux=",";
            $param[]=$this->nin_control_digit==''?NULL:$this->nin_control_digit;
          }else{
            $this->objError[]="The field (nin_control_digit) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->email_account!= $rs["email_account"]){
          if ($this->updateable["email_account"]){
            $set.=$set_aux."email_account=?";
            $set_aux=",";
            $param[]=$this->email_account==''?NULL:$this->email_account;
          }else{
            $this->objError[]="The field (email_account) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->legal_name!= $rs["legal_name"]){
          if ($this->updateable["legal_name"]){
            $set.=$set_aux."legal_name=?";
            $set_aux=",";
            $param[]=$this->legal_name==''?NULL:$this->legal_name;
          }else{
            $this->objError[]="The field (legal_name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->trade_name!= $rs["trade_name"]){
          if ($this->updateable["trade_name"]){
            $set.=$set_aux."trade_name=?";
            $set_aux=",";
            $param[]=$this->trade_name==''?NULL:$this->trade_name;
          }else{
            $this->objError[]="The field (trade_name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->legal_address!= $rs["legal_address"]){
          if ($this->updateable["legal_address"]){
            $set.=$set_aux."legal_address=?";
            $set_aux=",";
            $param[]=$this->legal_address==''?NULL:$this->legal_address;
          }else{
            $this->objError[]="The field (legal_address) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->phone_number!= $rs["phone_number"]){
          if ($this->updateable["phone_number"]){
            $set.=$set_aux."phone_number=?";
            $set_aux=",";
            $param[]=$this->phone_number==''?NULL:$this->phone_number;
          }else{
            $this->objError[]="The field (phone_number) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE gl_juridical_person ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM gl_juridical_person
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
