<?php
  class CoExchangeBase extends TfEntity {
    protected $id;
    protected $id_person;
    protected $date;
    protected $rate;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="co_exchange";
  }

  private function getAll(){

    $q="SELECT id,
               id_person,
               date,
               rate,
               active,
               created_by,
               created_date
          FROM co_exchange
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_person=$rs["id_person"];
    $this->date=$rs["date"];
    $this->rate=$rs["rate"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->co_exchange_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_co_exchange){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_co_exchange; 
    }

    if ($tfRequest->exist("co_exchange_id_person")){
      $this->id_person=$tfRequest->co_exchange_id_person;
    }
    if ($tfRequest->exist("co_exchange_date")){
      $this->date=$tfRequest->co_exchange_date;
    }
    if ($tfRequest->exist("co_exchange_rate")){
      $this->rate=$tfRequest->co_exchange_rate;
    }
    if ($tfRequest->exist("co_exchange_active")){
      $this->active=$tfRequest->co_exchange_active;
    }
    if ($tfRequest->exist("co_exchange_created_by")){
      $this->created_by=$tfRequest->co_exchange_created_by;
    }
    if ($tfRequest->exist("co_exchange_created_date")){
      $this->created_date=$tfRequest->co_exchange_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_person"]=array("type"=>"number",
                                  "value"=>$this->id_person,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["date"]=array("type"=>"date",
                                  "value"=>$this->date,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["rate"]=array("type"=>"number",
                                  "value"=>$this->rate,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdPerson($value){
  $this->id_person=$value;
  }
  public function getIdPerson(){
  return $this->id_person;
  }
  public function setDate($value){
  $this->date=$value;
  }
  public function getDate(){
  return $this->date;
  }
  public function setRate($value){
  $this->rate=$value;
  }
  public function getRate(){
  return $this->rate;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO co_exchange(id,
                               id_person,
                               date,
                               rate,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_person==''?NULL:$this->id_person,
                     $this->date==''?NULL:$this->date,
                     $this->rate==''?NULL:$this->rate,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_person!= $rs["id_person"]){
          if ($this->updateable["id_person"]){
            $set.=$set_aux."id_person=?";
            $set_aux=",";
            $param[]=$this->id_person==''?NULL:$this->id_person;
          }else{
            $this->objError[]="The field (id_person) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->date!= $rs["date"]){
          if ($this->updateable["date"]){
            $set.=$set_aux."date=?";
            $set_aux=",";
            $param[]=$this->date==''?NULL:$this->date;
          }else{
            $this->objError[]="The field (date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->rate!= $rs["rate"]){
          if ($this->updateable["rate"]){
            $set.=$set_aux."rate=?";
            $set_aux=",";
            $param[]=$this->rate==''?NULL:$this->rate;
          }else{
            $this->objError[]="The field (rate) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE co_exchange ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM co_exchange
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
