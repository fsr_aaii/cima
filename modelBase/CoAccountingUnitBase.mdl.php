<?php
  class CoAccountingUnitBase extends TfEntity {
    protected $id;
    protected $id_accounting;
    protected $id_unit;
    protected $id_account;
    protected $description;
    protected $observation;
    protected $amount;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="co_accounting_unit";
  }

  private function getAll(){

    $q="SELECT id,
               id_accounting,
               id_unit,
               id_account,
               description,
               observation,
               amount,
               created_by,
               created_date
          FROM co_accounting_unit
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_accounting=$rs["id_accounting"];
    $this->id_unit=$rs["id_unit"];
    $this->id_account=$rs["id_account"];
    $this->description=$rs["description"];
    $this->observation=$rs["observation"];
    $this->amount=$rs["amount"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->co_accounting_unit_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_co_accounting_unit){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_co_accounting_unit; 
    }

    if ($tfRequest->exist("co_accounting_unit_id_accounting")){
      $this->id_accounting=$tfRequest->co_accounting_unit_id_accounting;
    }
    if ($tfRequest->exist("co_accounting_unit_id_unit")){
      $this->id_unit=$tfRequest->co_accounting_unit_id_unit;
    }
    if ($tfRequest->exist("co_accounting_unit_id_account")){
      $this->id_account=$tfRequest->co_accounting_unit_id_account;
    }
    if ($tfRequest->exist("co_accounting_unit_description")){
      $this->description=$tfRequest->co_accounting_unit_description;
    }
    if ($tfRequest->exist("co_accounting_unit_observation")){
      $this->observation=$tfRequest->co_accounting_unit_observation;
    }
    if ($tfRequest->exist("co_accounting_unit_amount")){
      $this->amount=$tfRequest->co_accounting_unit_amount;
    }
    if ($tfRequest->exist("co_accounting_unit_created_by")){
      $this->created_by=$tfRequest->co_accounting_unit_created_by;
    }
    if ($tfRequest->exist("co_accounting_unit_created_date")){
      $this->created_date=$tfRequest->co_accounting_unit_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_accounting"]=array("type"=>"number",
                                  "value"=>$this->id_accounting,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_unit"]=array("type"=>"number",
                                  "value"=>$this->id_unit,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_account"]=array("type"=>"number",
                                  "value"=>$this->id_account,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["description"]=array("type"=>"string",
                                  "value"=>$this->description,
                                  "length"=>5000,
                                  "required"=>true);
    $this->validation["observation"]=array("type"=>"string",
                                  "value"=>$this->observation,
                                  "length"=>5000,
                                  "required"=>false);
    $this->validation["amount"]=array("type"=>"number",
                                  "value"=>$this->amount,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdAccounting($value){
  $this->id_accounting=$value;
  }
  public function getIdAccounting(){
  return $this->id_accounting;
  }
  public function setIdUnit($value){
  $this->id_unit=$value;
  }
  public function getIdUnit(){
  return $this->id_unit;
  }
  public function setIdAccount($value){
  $this->id_account=$value;
  }
  public function getIdAccount(){
  return $this->id_account;
  }
  public function setDescription($value){
  $this->description=$value;
  }
  public function getDescription(){
  return $this->description;
  }
  public function setObservation($value){
  $this->observation=$value;
  }
  public function getObservation(){
  return $this->observation;
  }
  public function setAmount($value){
  $this->amount=$value;
  }
  public function getAmount(){
  return $this->amount;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO co_accounting_unit(id,
                               id_accounting,
                               id_unit,
                               id_account,
                               description,
                               observation,
                               amount,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_accounting==''?NULL:$this->id_accounting,
                     $this->id_unit==''?NULL:$this->id_unit,
                     $this->id_account==''?NULL:$this->id_account,
                     $this->description==''?NULL:$this->description,
                     $this->observation==''?NULL:$this->observation,
                     $this->amount==''?NULL:$this->amount,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_accounting!= $rs["id_accounting"]){
          if ($this->updateable["id_accounting"]){
            $set.=$set_aux."id_accounting=?";
            $set_aux=",";
            $param[]=$this->id_accounting==''?NULL:$this->id_accounting;
          }else{
            $this->objError[]="The field (id_accounting) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_unit!= $rs["id_unit"]){
          if ($this->updateable["id_unit"]){
            $set.=$set_aux."id_unit=?";
            $set_aux=",";
            $param[]=$this->id_unit==''?NULL:$this->id_unit;
          }else{
            $this->objError[]="The field (id_unit) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_account!= $rs["id_account"]){
          if ($this->updateable["id_account"]){
            $set.=$set_aux."id_account=?";
            $set_aux=",";
            $param[]=$this->id_account==''?NULL:$this->id_account;
          }else{
            $this->objError[]="The field (id_account) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->description!= $rs["description"]){
          if ($this->updateable["description"]){
            $set.=$set_aux."description=?";
            $set_aux=",";
            $param[]=$this->description==''?NULL:$this->description;
          }else{
            $this->objError[]="The field (description) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->observation!= $rs["observation"]){
          if ($this->updateable["observation"]){
            $set.=$set_aux."observation=?";
            $set_aux=",";
            $param[]=$this->observation==''?NULL:$this->observation;
          }else{
            $this->objError[]="The field (observation) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->amount!= $rs["amount"]){
          if ($this->updateable["amount"]){
            $set.=$set_aux."amount=?";
            $set_aux=",";
            $param[]=$this->amount==''?NULL:$this->amount;
          }else{
            $this->objError[]="The field (amount) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE co_accounting_unit ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM co_accounting_unit
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
