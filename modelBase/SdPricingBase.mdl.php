<?php
  class SdPricingBase extends TfEntity {
    protected $id;
    protected $id_transaction_element;
    protected $price;
    protected $calculation_factor;
    protected $date_from;
    protected $date_to;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="sd_pricing";
  }

  private function getAll(){

    $q="SELECT id,
               id_transaction_element,
               price,
               calculation_factor,
               date_from,
               date_to,
               created_by,
               created_date
          FROM sd_pricing
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_transaction_element=$rs["id_transaction_element"];
    $this->price=$rs["price"];
    $this->calculation_factor=$rs["calculation_factor"];
    $this->date_from=$rs["date_from"];
    $this->date_to=$rs["date_to"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->sd_pricing_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_sd_pricing){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_sd_pricing; 
    }

    if ($tfRequest->exist("sd_pricing_id_transaction_element")){
      $this->id_transaction_element=$tfRequest->sd_pricing_id_transaction_element;
    }
    if ($tfRequest->exist("sd_pricing_price")){
      $this->price=$tfRequest->sd_pricing_price;
    }
    if ($tfRequest->exist("sd_pricing_calculation_factor")){
      $this->calculation_factor=$tfRequest->sd_pricing_calculation_factor;
    }
    if ($tfRequest->exist("sd_pricing_date_from")){
      $this->date_from=$tfRequest->sd_pricing_date_from;
    }
    if ($tfRequest->exist("sd_pricing_date_to")){
      $this->date_to=$tfRequest->sd_pricing_date_to;
    }
    if ($tfRequest->exist("sd_pricing_created_by")){
      $this->created_by=$tfRequest->sd_pricing_created_by;
    }
    if ($tfRequest->exist("sd_pricing_created_date")){
      $this->created_date=$tfRequest->sd_pricing_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_transaction_element"]=array("type"=>"number",
                                  "value"=>$this->id_transaction_element,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["price"]=array("type"=>"number",
                                  "value"=>$this->price,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["calculation_factor"]=array("type"=>"number",
                                  "value"=>$this->calculation_factor,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["date_from"]=array("type"=>"date",
                                  "value"=>$this->date_from,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["date_to"]=array("type"=>"date",
                                  "value"=>$this->date_to,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdTransactionElement($value){
  $this->id_transaction_element=$value;
  }
  public function getIdTransactionElement(){
  return $this->id_transaction_element;
  }
  public function setPrice($value){
  $this->price=$value;
  }
  public function getPrice(){
  return $this->price;
  }
  public function setCalculationFactor($value){
  $this->calculation_factor=$value;
  }
  public function getCalculationFactor(){
  return $this->calculation_factor;
  }
  public function setDateFrom($value){
  $this->date_from=$value;
  }
  public function getDateFrom(){
  return $this->date_from;
  }
  public function setDateTo($value){
  $this->date_to=$value;
  }
  public function getDateTo(){
  return $this->date_to;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO sd_pricing(id,
                               id_transaction_element,
                               price,
                               calculation_factor,
                               date_from,
                               date_to,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_transaction_element==''?NULL:$this->id_transaction_element,
                     $this->price==''?NULL:$this->price,
                     $this->calculation_factor==''?NULL:$this->calculation_factor,
                     $this->date_from==''?NULL:$this->date_from,
                     $this->date_to==''?NULL:$this->date_to,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_transaction_element!= $rs["id_transaction_element"]){
          if ($this->updateable["id_transaction_element"]){
            $set.=$set_aux."id_transaction_element=?";
            $set_aux=",";
            $param[]=$this->id_transaction_element==''?NULL:$this->id_transaction_element;
          }else{
            $this->objError[]="The field (id_transaction_element) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->price!= $rs["price"]){
          if ($this->updateable["price"]){
            $set.=$set_aux."price=?";
            $set_aux=",";
            $param[]=$this->price==''?NULL:$this->price;
          }else{
            $this->objError[]="The field (price) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->calculation_factor!= $rs["calculation_factor"]){
          if ($this->updateable["calculation_factor"]){
            $set.=$set_aux."calculation_factor=?";
            $set_aux=",";
            $param[]=$this->calculation_factor==''?NULL:$this->calculation_factor;
          }else{
            $this->objError[]="The field (calculation_factor) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->date_from!= $rs["date_from"]){
          if ($this->updateable["date_from"]){
            $set.=$set_aux."date_from=?";
            $set_aux=",";
            $param[]=$this->date_from==''?NULL:$this->date_from;
          }else{
            $this->objError[]="The field (date_from) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->date_to!= $rs["date_to"]){
          if ($this->updateable["date_to"]){
            $set.=$set_aux."date_to=?";
            $set_aux=",";
            $param[]=$this->date_to==''?NULL:$this->date_to;
          }else{
            $this->objError[]="The field (date_to) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE sd_pricing ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM sd_pricing
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
