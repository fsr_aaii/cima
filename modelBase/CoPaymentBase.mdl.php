<?php
  class CoPaymentBase extends TfEntity {
    protected $id;
    protected $id_unit;
    protected $currency;
    protected $currency_confirmed;
    protected $payment_date;
    protected $payment_date_confirmed;
    protected $rate;
    protected $rate_confirmed;
    protected $amount;
    protected $amount_confirmed;
    protected $image_path;
    protected $observation;
    protected $status;
    protected $confirmed_by;
    protected $confirmed_date;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="co_payment";
  }

  private function getAll(){

    $q="SELECT id,
               id_unit,
               currency,
               currency_confirmed,
               payment_date,
               payment_date_confirmed,
               rate,
               rate_confirmed,
               amount,
               amount_confirmed,
               image_path,
               observation,
               status,
               confirmed_by,
               confirmed_date,
               created_by,
               created_date
          FROM co_payment
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_unit=$rs["id_unit"];
    $this->currency=$rs["currency"];
    $this->currency_confirmed=$rs["currency_confirmed"];
    $this->payment_date=$rs["payment_date"];
    $this->payment_date_confirmed=$rs["payment_date_confirmed"];
    $this->rate=$rs["rate"];
    $this->rate_confirmed=$rs["rate_confirmed"];
    $this->amount=$rs["amount"];
    $this->amount_confirmed=$rs["amount_confirmed"];
    $this->image_path=$rs["image_path"];
    $this->observation=$rs["observation"];
    $this->status=$rs["status"];
    $this->confirmed_by=$rs["confirmed_by"];
    $this->confirmed_date=$rs["confirmed_date"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->co_payment_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_co_payment){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_co_payment; 
    }

    if ($tfRequest->exist("co_payment_id_unit")){
      $this->id_unit=$tfRequest->co_payment_id_unit;
    }
    if ($tfRequest->exist("co_payment_currency")){
      $this->currency=$tfRequest->co_payment_currency;
    }
    if ($tfRequest->exist("co_payment_currency_confirmed")){
      $this->currency_confirmed=$tfRequest->co_payment_currency_confirmed;
    }
    if ($tfRequest->exist("co_payment_payment_date")){
      $this->payment_date=$tfRequest->co_payment_payment_date;
    }
    if ($tfRequest->exist("co_payment_payment_date_confirmed")){
      $this->payment_date_confirmed=$tfRequest->co_payment_payment_date_confirmed;
    }
    if ($tfRequest->exist("co_payment_rate")){
      $this->rate=$tfRequest->co_payment_rate;
    }
    if ($tfRequest->exist("co_payment_rate_confirmed")){
      $this->rate_confirmed=$tfRequest->co_payment_rate_confirmed;
    }
    if ($tfRequest->exist("co_payment_amount")){
      $this->amount=$tfRequest->co_payment_amount;
    }
    if ($tfRequest->exist("co_payment_amount_confirmed")){
      $this->amount_confirmed=$tfRequest->co_payment_amount_confirmed;
    }
    if ($tfRequest->exist("co_payment_image_path")){
      $this->image_path=$tfRequest->co_payment_image_path;
    }
    if ($tfRequest->exist("co_payment_observation")){
      $this->observation=$tfRequest->co_payment_observation;
    }
    if ($tfRequest->exist("co_payment_status")){
      $this->status=$tfRequest->co_payment_status;
    }
    if ($tfRequest->exist("co_payment_confirmed_by")){
      $this->confirmed_by=$tfRequest->co_payment_confirmed_by;
    }
    if ($tfRequest->exist("co_payment_confirmed_date")){
      $this->confirmed_date=$tfRequest->co_payment_confirmed_date;
    }
    if ($tfRequest->exist("co_payment_created_by")){
      $this->created_by=$tfRequest->co_payment_created_by;
    }
    if ($tfRequest->exist("co_payment_created_date")){
      $this->created_date=$tfRequest->co_payment_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_unit"]=array("type"=>"number",
                                  "value"=>$this->id_unit,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["currency"]=array("type"=>"string",
                                  "value"=>$this->currency,
                                  "length"=>3,
                                  "required"=>true);
    $this->validation["currency_confirmed"]=array("type"=>"string",
                                  "value"=>$this->currency_confirmed,
                                  "length"=>3,
                                  "required"=>false);
    $this->validation["payment_date"]=array("type"=>"date",
                                  "value"=>$this->payment_date,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["payment_date_confirmed"]=array("type"=>"date",
                                  "value"=>$this->payment_date_confirmed,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["rate"]=array("type"=>"number",
                                  "value"=>$this->rate,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["rate_confirmed"]=array("type"=>"number",
                                  "value"=>$this->rate_confirmed,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["amount"]=array("type"=>"number",
                                  "value"=>$this->amount,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["amount_confirmed"]=array("type"=>"number",
                                  "value"=>$this->amount_confirmed,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["image_path"]=array("type"=>"string",
                                  "value"=>$this->image_path,
                                  "length"=>500,
                                  "required"=>true);
    $this->validation["observation"]=array("type"=>"string",
                                  "value"=>$this->observation,
                                  "length"=>4294967295,
                                  "required"=>false);
    $this->validation["status"]=array("type"=>"string",
                                  "value"=>$this->status,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["confirmed_by"]=array("type"=>"number",
                                  "value"=>$this->confirmed_by,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["confirmed_date"]=array("type"=>"datetime",
                                  "value"=>$this->confirmed_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdUnit($value){
  $this->id_unit=$value;
  }
  public function getIdUnit(){
  return $this->id_unit;
  }
  public function setCurrency($value){
  $this->currency=$value;
  }
  public function getCurrency(){
  return $this->currency;
  }
  public function setCurrencyConfirmed($value){
  $this->currency_confirmed=$value;
  }
  public function getCurrencyConfirmed(){
  return $this->currency_confirmed;
  }
  public function setPaymentDate($value){
  $this->payment_date=$value;
  }
  public function getPaymentDate(){
  return $this->payment_date;
  }
  public function setPaymentDateConfirmed($value){
  $this->payment_date_confirmed=$value;
  }
  public function getPaymentDateConfirmed(){
  return $this->payment_date_confirmed;
  }
  public function setRate($value){
  $this->rate=$value;
  }
  public function getRate(){
  return $this->rate;
  }
  public function setRateConfirmed($value){
  $this->rate_confirmed=$value;
  }
  public function getRateConfirmed(){
  return $this->rate_confirmed;
  }
  public function setAmount($value){
  $this->amount=$value;
  }
  public function getAmount(){
  return $this->amount;
  }
  public function setAmountConfirmed($value){
  $this->amount_confirmed=$value;
  }
  public function getAmountConfirmed(){
  return $this->amount_confirmed;
  }
  public function setImagePath($value){
  $this->image_path=$value;
  }
  public function getImagePath(){
  return $this->image_path;
  }
  public function setObservation($value){
  $this->observation=$value;
  }
  public function getObservation(){
  return $this->observation;
  }
  public function setStatus($value){
  $this->status=$value;
  }
  public function getStatus(){
  return $this->status;
  }
  public function setConfirmedBy($value){
  $this->confirmed_by=$value;
  }
  public function getConfirmedBy(){
  return $this->confirmed_by;
  }
  public function setConfirmedDate($value){
  $this->confirmed_date=$value;
  }
  public function getConfirmedDate(){
  return $this->confirmed_date;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO co_payment(id,
                               id_unit,
                               currency,
                               currency_confirmed,
                               payment_date,
                               payment_date_confirmed,
                               rate,
                               rate_confirmed,
                               amount,
                               amount_confirmed,
                               image_path,
                               observation,
                               status,
                               confirmed_by,
                               confirmed_date,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_unit==''?NULL:$this->id_unit,
                     $this->currency==''?NULL:$this->currency,
                     $this->currency_confirmed==''?NULL:$this->currency_confirmed,
                     $this->payment_date==''?NULL:$this->payment_date,
                     $this->payment_date_confirmed==''?NULL:$this->payment_date_confirmed,
                     $this->rate==''?NULL:$this->rate,
                     $this->rate_confirmed==''?NULL:$this->rate_confirmed,
                     $this->amount==''?NULL:$this->amount,
                     $this->amount_confirmed==''?NULL:$this->amount_confirmed,
                     $this->image_path==''?NULL:$this->image_path,
                     $this->observation==''?NULL:$this->observation,
                     $this->status==''?NULL:$this->status,
                     $this->confirmed_by==''?NULL:$this->confirmed_by,
                     $this->confirmed_date==''?NULL:$this->confirmed_date,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_unit!= $rs["id_unit"]){
          if ($this->updateable["id_unit"]){
            $set.=$set_aux."id_unit=?";
            $set_aux=",";
            $param[]=$this->id_unit==''?NULL:$this->id_unit;
          }else{
            $this->objError[]="The field (id_unit) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->currency!= $rs["currency"]){
          if ($this->updateable["currency"]){
            $set.=$set_aux."currency=?";
            $set_aux=",";
            $param[]=$this->currency==''?NULL:$this->currency;
          }else{
            $this->objError[]="The field (currency) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->currency_confirmed!= $rs["currency_confirmed"]){
          if ($this->updateable["currency_confirmed"]){
            $set.=$set_aux."currency_confirmed=?";
            $set_aux=",";
            $param[]=$this->currency_confirmed==''?NULL:$this->currency_confirmed;
          }else{
            $this->objError[]="The field (currency_confirmed) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->payment_date!= $rs["payment_date"]){
          if ($this->updateable["payment_date"]){
            $set.=$set_aux."payment_date=?";
            $set_aux=",";
            $param[]=$this->payment_date==''?NULL:$this->payment_date;
          }else{
            $this->objError[]="The field (payment_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->payment_date_confirmed!= $rs["payment_date_confirmed"]){
          if ($this->updateable["payment_date_confirmed"]){
            $set.=$set_aux."payment_date_confirmed=?";
            $set_aux=",";
            $param[]=$this->payment_date_confirmed==''?NULL:$this->payment_date_confirmed;
          }else{
            $this->objError[]="The field (payment_date_confirmed) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->rate!= $rs["rate"]){
          if ($this->updateable["rate"]){
            $set.=$set_aux."rate=?";
            $set_aux=",";
            $param[]=$this->rate==''?NULL:$this->rate;
          }else{
            $this->objError[]="The field (rate) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->rate_confirmed!= $rs["rate_confirmed"]){
          if ($this->updateable["rate_confirmed"]){
            $set.=$set_aux."rate_confirmed=?";
            $set_aux=",";
            $param[]=$this->rate_confirmed==''?NULL:$this->rate_confirmed;
          }else{
            $this->objError[]="The field (rate_confirmed) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->amount!= $rs["amount"]){
          if ($this->updateable["amount"]){
            $set.=$set_aux."amount=?";
            $set_aux=",";
            $param[]=$this->amount==''?NULL:$this->amount;
          }else{
            $this->objError[]="The field (amount) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->amount_confirmed!= $rs["amount_confirmed"]){
          if ($this->updateable["amount_confirmed"]){
            $set.=$set_aux."amount_confirmed=?";
            $set_aux=",";
            $param[]=$this->amount_confirmed==''?NULL:$this->amount_confirmed;
          }else{
            $this->objError[]="The field (amount_confirmed) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->image_path!= $rs["image_path"]){
          if ($this->updateable["image_path"]){
            $set.=$set_aux."image_path=?";
            $set_aux=",";
            $param[]=$this->image_path==''?NULL:$this->image_path;
          }else{
            $this->objError[]="The field (image_path) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->observation!= $rs["observation"]){
          if ($this->updateable["observation"]){
            $set.=$set_aux."observation=?";
            $set_aux=",";
            $param[]=$this->observation==''?NULL:$this->observation;
          }else{
            $this->objError[]="The field (observation) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->status!= $rs["status"]){
          if ($this->updateable["status"]){
            $set.=$set_aux."status=?";
            $set_aux=",";
            $param[]=$this->status==''?NULL:$this->status;
          }else{
            $this->objError[]="The field (status) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->confirmed_by!= $rs["confirmed_by"]){
          if ($this->updateable["confirmed_by"]){
            $set.=$set_aux."confirmed_by=?";
            $set_aux=",";
            $param[]=$this->confirmed_by==''?NULL:$this->confirmed_by;
          }else{
            $this->objError[]="The field (confirmed_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->confirmed_date!= $rs["confirmed_date"]){
          if ($this->updateable["confirmed_date"]){
            $set.=$set_aux."confirmed_date=?";
            $set_aux=",";
            $param[]=$this->confirmed_date==''?NULL:$this->confirmed_date;
          }else{
            $this->objError[]="The field (confirmed_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE co_payment ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM co_payment
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
