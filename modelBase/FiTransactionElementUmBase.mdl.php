<?php
  class FiTransactionElementUmBase extends TfEntity {
    protected $id;
    protected $id_transaction_element;
    protected $id_packeting;
    protected $id_unit_measurement;
    protected $qty;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="fi_transaction_element_um";
  }

  private function getAll(){

    $q="SELECT id,
               id_transaction_element,
               id_packeting,
               id_unit_measurement,
               qty,
               active,
               created_by,
               created_date
          FROM fi_transaction_element_um
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_transaction_element=$rs["id_transaction_element"];
    $this->id_packeting=$rs["id_packeting"];
    $this->id_unit_measurement=$rs["id_unit_measurement"];
    $this->qty=$rs["qty"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->fi_transaction_element_um_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_fi_transaction_element_um){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_fi_transaction_element_um; 
    }

    if ($tfRequest->exist("fi_transaction_element_um_id_transaction_element")){
      $this->id_transaction_element=$tfRequest->fi_transaction_element_um_id_transaction_element;
    }
    if ($tfRequest->exist("fi_transaction_element_um_id_packeting")){
      $this->id_packeting=$tfRequest->fi_transaction_element_um_id_packeting;
    }
    if ($tfRequest->exist("fi_transaction_element_um_id_unit_measurement")){
      $this->id_unit_measurement=$tfRequest->fi_transaction_element_um_id_unit_measurement;
    }
    if ($tfRequest->exist("fi_transaction_element_um_qty")){
      $this->qty=$tfRequest->fi_transaction_element_um_qty;
    }
    if ($tfRequest->exist("fi_transaction_element_um_active")){
      $this->active=$tfRequest->fi_transaction_element_um_active;
    }
    if ($tfRequest->exist("fi_transaction_element_um_created_by")){
      $this->created_by=$tfRequest->fi_transaction_element_um_created_by;
    }
    if ($tfRequest->exist("fi_transaction_element_um_created_date")){
      $this->created_date=$tfRequest->fi_transaction_element_um_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_transaction_element"]=array("type"=>"number",
                                  "value"=>$this->id_transaction_element,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_packeting"]=array("type"=>"number",
                                  "value"=>$this->id_packeting,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_unit_measurement"]=array("type"=>"number",
                                  "value"=>$this->id_unit_measurement,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["qty"]=array("type"=>"number",
                                  "value"=>$this->qty,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdTransactionElement($value){
  $this->id_transaction_element=$value;
  }
  public function getIdTransactionElement(){
  return $this->id_transaction_element;
  }
  public function setIdPacketing($value){
  $this->id_packeting=$value;
  }
  public function getIdPacketing(){
  return $this->id_packeting;
  }
  public function setIdUnitMeasurement($value){
  $this->id_unit_measurement=$value;
  }
  public function getIdUnitMeasurement(){
  return $this->id_unit_measurement;
  }
  public function setQty($value){
  $this->qty=$value;
  }
  public function getQty(){
  return $this->qty;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO fi_transaction_element_um(id,
                               id_transaction_element,
                               id_packeting,
                               id_unit_measurement,
                               qty,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_transaction_element==''?NULL:$this->id_transaction_element,
                     $this->id_packeting==''?NULL:$this->id_packeting,
                     $this->id_unit_measurement==''?NULL:$this->id_unit_measurement,
                     $this->qty==''?NULL:$this->qty,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_transaction_element!= $rs["id_transaction_element"]){
          if ($this->updateable["id_transaction_element"]){
            $set.=$set_aux."id_transaction_element=?";
            $set_aux=",";
            $param[]=$this->id_transaction_element==''?NULL:$this->id_transaction_element;
          }else{
            $this->objError[]="The field (id_transaction_element) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_packeting!= $rs["id_packeting"]){
          if ($this->updateable["id_packeting"]){
            $set.=$set_aux."id_packeting=?";
            $set_aux=",";
            $param[]=$this->id_packeting==''?NULL:$this->id_packeting;
          }else{
            $this->objError[]="The field (id_packeting) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_unit_measurement!= $rs["id_unit_measurement"]){
          if ($this->updateable["id_unit_measurement"]){
            $set.=$set_aux."id_unit_measurement=?";
            $set_aux=",";
            $param[]=$this->id_unit_measurement==''?NULL:$this->id_unit_measurement;
          }else{
            $this->objError[]="The field (id_unit_measurement) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->qty!= $rs["qty"]){
          if ($this->updateable["qty"]){
            $set.=$set_aux."qty=?";
            $set_aux=",";
            $param[]=$this->qty==''?NULL:$this->qty;
          }else{
            $this->objError[]="The field (qty) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE fi_transaction_element_um ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM fi_transaction_element_um
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
