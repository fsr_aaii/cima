<?php
  class CoAccountBase extends TfEntity {
    protected $id;
    protected $id_account_group;
    protected $name;
    protected $seat;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="co_account";
  }

  private function getAll(){

    $q="SELECT id,
               id_account_group,
               name,
               seat,
               active,
               created_by,
               created_date
          FROM co_account
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_account_group=$rs["id_account_group"];
    $this->name=$rs["name"];
    $this->seat=$rs["seat"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->co_account_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_co_account){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_co_account; 
    }

    if ($tfRequest->exist("co_account_id_account_group")){
      $this->id_account_group=$tfRequest->co_account_id_account_group;
    }
    if ($tfRequest->exist("co_account_name")){
      $this->name=$tfRequest->co_account_name;
    }
    if ($tfRequest->exist("co_account_seat")){
      $this->seat=$tfRequest->co_account_seat;
    }
    if ($tfRequest->exist("co_account_active")){
      $this->active=$tfRequest->co_account_active;
    }
    if ($tfRequest->exist("co_account_created_by")){
      $this->created_by=$tfRequest->co_account_created_by;
    }
    if ($tfRequest->exist("co_account_created_date")){
      $this->created_date=$tfRequest->co_account_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_account_group"]=array("type"=>"number",
                                  "value"=>$this->id_account_group,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["name"]=array("type"=>"string",
                                  "value"=>$this->name,
                                  "length"=>200,
                                  "required"=>true);
    $this->validation["seat"]=array("type"=>"string",
                                  "value"=>$this->seat,
                                  "length"=>1,
                                  "required"=>false);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdAccountGroup($value){
  $this->id_account_group=$value;
  }
  public function getIdAccountGroup(){
  return $this->id_account_group;
  }
  public function setName($value){
  $this->name=$value;
  }
  public function getName(){
  return $this->name;
  }
  public function setSeat($value){
  $this->seat=$value;
  }
  public function getSeat(){
  return $this->seat;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO co_account(id,
                               id_account_group,
                               name,
                               seat,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_account_group==''?NULL:$this->id_account_group,
                     $this->name==''?NULL:$this->name,
                     $this->seat==''?NULL:$this->seat,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_account_group!= $rs["id_account_group"]){
          if ($this->updateable["id_account_group"]){
            $set.=$set_aux."id_account_group=?";
            $set_aux=",";
            $param[]=$this->id_account_group==''?NULL:$this->id_account_group;
          }else{
            $this->objError[]="The field (id_account_group) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->name!= $rs["name"]){
          if ($this->updateable["name"]){
            $set.=$set_aux."name=?";
            $set_aux=",";
            $param[]=$this->name==''?NULL:$this->name;
          }else{
            $this->objError[]="The field (name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->seat!= $rs["seat"]){
          if ($this->updateable["seat"]){
            $set.=$set_aux."seat=?";
            $set_aux=",";
            $param[]=$this->seat==''?NULL:$this->seat;
          }else{
            $this->objError[]="The field (seat) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE co_account ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM co_account
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
