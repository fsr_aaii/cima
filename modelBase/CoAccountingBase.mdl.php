<?php
  class CoAccountingBase extends TfEntity {
    protected $id;
    protected $id_person;
    protected $rate;
    protected $payment_date;
    protected $processed_by;
    protected $processed_date;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="co_accounting";
  }

  private function getAll(){

    $q="SELECT id,
               id_person,
               rate,
               payment_date,
               processed_by,
               processed_date,
               created_by,
               created_date
          FROM co_accounting
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_person=$rs["id_person"];
    $this->rate=$rs["rate"];
    $this->payment_date=$rs["payment_date"];
    $this->processed_by=$rs["processed_by"];
    $this->processed_date=$rs["processed_date"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->co_accounting_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_co_accounting){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_co_accounting; 
    }

    if ($tfRequest->exist("co_accounting_id_person")){
      $this->id_person=$tfRequest->co_accounting_id_person;
    }
    if ($tfRequest->exist("co_accounting_rate")){
      $this->rate=$tfRequest->co_accounting_rate;
    }
    if ($tfRequest->exist("co_accounting_payment_date")){
      $this->payment_date=$tfRequest->co_accounting_payment_date;
    }
    if ($tfRequest->exist("co_accounting_processed_by")){
      $this->processed_by=$tfRequest->co_accounting_processed_by;
    }
    if ($tfRequest->exist("co_accounting_processed_date")){
      $this->processed_date=$tfRequest->co_accounting_processed_date;
    }
    if ($tfRequest->exist("co_accounting_created_by")){
      $this->created_by=$tfRequest->co_accounting_created_by;
    }
    if ($tfRequest->exist("co_accounting_created_date")){
      $this->created_date=$tfRequest->co_accounting_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_person"]=array("type"=>"number",
                                  "value"=>$this->id_person,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["rate"]=array("type"=>"number",
                                  "value"=>$this->rate,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["payment_date"]=array("type"=>"date",
                                  "value"=>$this->payment_date,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["processed_by"]=array("type"=>"number",
                                  "value"=>$this->processed_by,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["processed_date"]=array("type"=>"datetime",
                                  "value"=>$this->processed_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdPerson($value){
  $this->id_person=$value;
  }
  public function getIdPerson(){
  return $this->id_person;
  }
  public function setRate($value){
  $this->rate=$value;
  }
  public function getRate(){
  return $this->rate;
  }
  public function setPaymentDate($value){
  $this->payment_date=$value;
  }
  public function getPaymentDate(){
  return $this->payment_date;
  }
  public function setProcessedBy($value){
  $this->processed_by=$value;
  }
  public function getProcessedBy(){
  return $this->processed_by;
  }
  public function setProcessedDate($value){
  $this->processed_date=$value;
  }
  public function getProcessedDate(){
  return $this->processed_date;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO co_accounting(id,
                               id_person,
                               rate,
                               payment_date,
                               processed_by,
                               processed_date,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_person==''?NULL:$this->id_person,
                     $this->rate==''?NULL:$this->rate,
                     $this->payment_date==''?NULL:$this->payment_date,
                     $this->processed_by==''?NULL:$this->processed_by,
                     $this->processed_date==''?NULL:$this->processed_date,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_person!= $rs["id_person"]){
          if ($this->updateable["id_person"]){
            $set.=$set_aux."id_person=?";
            $set_aux=",";
            $param[]=$this->id_person==''?NULL:$this->id_person;
          }else{
            $this->objError[]="The field (id_person) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->rate!= $rs["rate"]){
          if ($this->updateable["rate"]){
            $set.=$set_aux."rate=?";
            $set_aux=",";
            $param[]=$this->rate==''?NULL:$this->rate;
          }else{
            $this->objError[]="The field (rate) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->payment_date!= $rs["payment_date"]){
          if ($this->updateable["payment_date"]){
            $set.=$set_aux."payment_date=?";
            $set_aux=",";
            $param[]=$this->payment_date==''?NULL:$this->payment_date;
          }else{
            $this->objError[]="The field (payment_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->processed_by!= $rs["processed_by"]){
          if ($this->updateable["processed_by"]){
            $set.=$set_aux."processed_by=?";
            $set_aux=",";
            $param[]=$this->processed_by==''?NULL:$this->processed_by;
          }else{
            $this->objError[]="The field (processed_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->processed_date!= $rs["processed_date"]){
          if ($this->updateable["processed_date"]){
            $set.=$set_aux."processed_date=?";
            $set_aux=",";
            $param[]=$this->processed_date==''?NULL:$this->processed_date;
          }else{
            $this->objError[]="The field (processed_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE co_accounting ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM co_accounting
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
