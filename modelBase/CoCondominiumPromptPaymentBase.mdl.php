<?php
  class CoCondominiumPromptPaymentBase extends TfEntity {
    protected $id;
    protected $id_person;
    protected $days;
    protected $credit_percentage;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="co_condominium_prompt_payment";
  }

  private function getAll(){

    $q="SELECT id,
               id_person,
               days,
               credit_percentage,
               active,
               created_by,
               created_date
          FROM co_condominium_prompt_payment
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_person=$rs["id_person"];
    $this->days=$rs["days"];
    $this->credit_percentage=$rs["credit_percentage"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->co_condominium_prompt_payment_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_co_condominium_prompt_payment){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_co_condominium_prompt_payment; 
    }

    if ($tfRequest->exist("co_condominium_prompt_payment_id_person")){
      $this->id_person=$tfRequest->co_condominium_prompt_payment_id_person;
    }
    if ($tfRequest->exist("co_condominium_prompt_payment_days")){
      $this->days=$tfRequest->co_condominium_prompt_payment_days;
    }
    if ($tfRequest->exist("co_condominium_prompt_payment_credit_percentage")){
      $this->credit_percentage=$tfRequest->co_condominium_prompt_payment_credit_percentage;
    }
    if ($tfRequest->exist("co_condominium_prompt_payment_active")){
      $this->active=$tfRequest->co_condominium_prompt_payment_active;
    }
    if ($tfRequest->exist("co_condominium_prompt_payment_created_by")){
      $this->created_by=$tfRequest->co_condominium_prompt_payment_created_by;
    }
    if ($tfRequest->exist("co_condominium_prompt_payment_created_date")){
      $this->created_date=$tfRequest->co_condominium_prompt_payment_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_person"]=array("type"=>"number",
                                  "value"=>$this->id_person,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["days"]=array("type"=>"number",
                                  "value"=>$this->days,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["credit_percentage"]=array("type"=>"number",
                                  "value"=>$this->credit_percentage,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdPerson($value){
  $this->id_person=$value;
  }
  public function getIdPerson(){
  return $this->id_person;
  }
  public function setDays($value){
  $this->days=$value;
  }
  public function getDays(){
  return $this->days;
  }
  public function setCreditPercentage($value){
  $this->credit_percentage=$value;
  }
  public function getCreditPercentage(){
  return $this->credit_percentage;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO co_condominium_prompt_payment(id,
                               id_person,
                               days,
                               credit_percentage,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_person==''?NULL:$this->id_person,
                     $this->days==''?NULL:$this->days,
                     $this->credit_percentage==''?NULL:$this->credit_percentage,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_person!= $rs["id_person"]){
          if ($this->updateable["id_person"]){
            $set.=$set_aux."id_person=?";
            $set_aux=",";
            $param[]=$this->id_person==''?NULL:$this->id_person;
          }else{
            $this->objError[]="The field (id_person) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->days!= $rs["days"]){
          if ($this->updateable["days"]){
            $set.=$set_aux."days=?";
            $set_aux=",";
            $param[]=$this->days==''?NULL:$this->days;
          }else{
            $this->objError[]="The field (days) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->credit_percentage!= $rs["credit_percentage"]){
          if ($this->updateable["credit_percentage"]){
            $set.=$set_aux."credit_percentage=?";
            $set_aux=",";
            $param[]=$this->credit_percentage==''?NULL:$this->credit_percentage;
          }else{
            $this->objError[]="The field (credit_percentage) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE co_condominium_prompt_payment ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM co_condominium_prompt_payment
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
