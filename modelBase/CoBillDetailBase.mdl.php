<?php
  class CoBillDetailBase extends TfEntity {
    protected $id;
    protected $id_bill;
    protected $id_account;
    protected $amount;
    protected $type;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="co_bill_detail";
  }

  private function getAll(){

    $q="SELECT id,
               id_bill,
               id_account,
               amount,
               type,
               created_by,
               created_date
          FROM co_bill_detail
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_bill=$rs["id_bill"];
    $this->id_account=$rs["id_account"];
    $this->amount=$rs["amount"];
    $this->type=$rs["type"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->co_bill_detail_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_co_bill_detail){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_co_bill_detail; 
    }

    if ($tfRequest->exist("co_bill_detail_id_bill")){
      $this->id_bill=$tfRequest->co_bill_detail_id_bill;
    }
    if ($tfRequest->exist("co_bill_detail_id_account")){
      $this->id_account=$tfRequest->co_bill_detail_id_account;
    }
    if ($tfRequest->exist("co_bill_detail_amount")){
      $this->amount=$tfRequest->co_bill_detail_amount;
    }
    if ($tfRequest->exist("co_bill_detail_type")){
      $this->type=$tfRequest->co_bill_detail_type;
    }
    if ($tfRequest->exist("co_bill_detail_created_by")){
      $this->created_by=$tfRequest->co_bill_detail_created_by;
    }
    if ($tfRequest->exist("co_bill_detail_created_date")){
      $this->created_date=$tfRequest->co_bill_detail_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_bill"]=array("type"=>"number",
                                  "value"=>$this->id_bill,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_account"]=array("type"=>"number",
                                  "value"=>$this->id_account,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["amount"]=array("type"=>"number",
                                  "value"=>$this->amount,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["type"]=array("type"=>"string",
                                  "value"=>$this->type,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdBill($value){
  $this->id_bill=$value;
  }
  public function getIdBill(){
  return $this->id_bill;
  }
  public function setIdAccount($value){
  $this->id_account=$value;
  }
  public function getIdAccount(){
  return $this->id_account;
  }
  public function setAmount($value){
  $this->amount=$value;
  }
  public function getAmount(){
  return $this->amount;
  }
  public function setType($value){
  $this->type=$value;
  }
  public function getType(){
  return $this->type;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO co_bill_detail(id,
                               id_bill,
                               id_account,
                               amount,
                               type,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_bill==''?NULL:$this->id_bill,
                     $this->id_account==''?NULL:$this->id_account,
                     $this->amount==''?NULL:$this->amount,
                     $this->type==''?NULL:$this->type,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_bill!= $rs["id_bill"]){
          if ($this->updateable["id_bill"]){
            $set.=$set_aux."id_bill=?";
            $set_aux=",";
            $param[]=$this->id_bill==''?NULL:$this->id_bill;
          }else{
            $this->objError[]="The field (id_bill) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_account!= $rs["id_account"]){
          if ($this->updateable["id_account"]){
            $set.=$set_aux."id_account=?";
            $set_aux=",";
            $param[]=$this->id_account==''?NULL:$this->id_account;
          }else{
            $this->objError[]="The field (id_account) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->amount!= $rs["amount"]){
          if ($this->updateable["amount"]){
            $set.=$set_aux."amount=?";
            $set_aux=",";
            $param[]=$this->amount==''?NULL:$this->amount;
          }else{
            $this->objError[]="The field (amount) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->type!= $rs["type"]){
          if ($this->updateable["type"]){
            $set.=$set_aux."type=?";
            $set_aux=",";
            $param[]=$this->type==''?NULL:$this->type;
          }else{
            $this->objError[]="The field (type) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE co_bill_detail ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM co_bill_detail
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
