<?php
  class OpInspectionReportDetailBase extends TfEntity {
    protected $id;
    protected $row;
    protected $hash;
    protected $id_travel;
    protected $SICA_guide;
    protected $id_list;
    protected $transport;
    protected $name;
    protected $nin;
    protected $chuto;
    protected $id_destination;
    protected $BL;
    protected $admission_date;
    protected $admission_time;
    protected $tara_number;
    protected $status_1;
    protected $operation_day;
    protected $survey_guide;
    protected $ocamar_guide;
    protected $store;
    protected $gross_weight;
    protected $tara_weight;
    protected $net_weight;
    protected $aggregate;
    protected $ROB;
    protected $status_2;
    protected $id_travel_ocamar;
    protected $departure_date;
    protected $departure_time;
    protected $seal;
    protected $observation;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="op_inspection_report_detail";
  }

  private function getAll(){

    $q="SELECT id,
               row,
               hash,
               id_travel,
               SICA_guide,
               id_list,
               transport,
               name,
               nin,
               chuto,
               id_destination,
               BL,
               admission_date,
               admission_time,
               tara_number,
               status_1,
               operation_day,
               survey_guide,
               ocamar_guide,
               store,
               gross_weight,
               tara_weight,
               net_weight,
               aggregate,
               ROB,
               status_2,
               id_travel_ocamar,
               departure_date,
               departure_time,
               seal,
               observation,
               active,
               created_by,
               created_date
          FROM op_inspection_report_detail
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->row=$rs["row"];
    $this->hash=$rs["hash"];
    $this->id_travel=$rs["id_travel"];
    $this->SICA_guide=$rs["SICA_guide"];
    $this->id_list=$rs["id_list"];
    $this->transport=$rs["transport"];
    $this->name=$rs["name"];
    $this->nin=$rs["nin"];
    $this->chuto=$rs["chuto"];
    $this->id_destination=$rs["id_destination"];
    $this->BL=$rs["BL"];
    $this->admission_date=$rs["admission_date"];
    $this->admission_time=$rs["admission_time"];
    $this->tara_number=$rs["tara_number"];
    $this->status_1=$rs["status_1"];
    $this->operation_day=$rs["operation_day"];
    $this->survey_guide=$rs["survey_guide"];
    $this->ocamar_guide=$rs["ocamar_guide"];
    $this->store=$rs["store"];
    $this->gross_weight=$rs["gross_weight"];
    $this->tara_weight=$rs["tara_weight"];
    $this->net_weight=$rs["net_weight"];
    $this->aggregate=$rs["aggregate"];
    $this->ROB=$rs["ROB"];
    $this->status_2=$rs["status_2"];
    $this->id_travel_ocamar=$rs["id_travel_ocamar"];
    $this->departure_date=$rs["departure_date"];
    $this->departure_time=$rs["departure_time"];
    $this->seal=$rs["seal"];
    $this->observation=$rs["observation"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->op_inspection_report_detail_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_op_inspection_report_detail){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_op_inspection_report_detail; 
    }

    if ($tfRequest->exist("op_inspection_report_detail_row")){
      $this->row=$tfRequest->op_inspection_report_detail_row;
    }
    if ($tfRequest->exist("op_inspection_report_detail_hash")){
      $this->hash=$tfRequest->op_inspection_report_detail_hash;
    }
    if ($tfRequest->exist("op_inspection_report_detail_id_travel")){
      $this->id_travel=$tfRequest->op_inspection_report_detail_id_travel;
    }
    if ($tfRequest->exist("op_inspection_report_detail_SICA_guide")){
      $this->SICA_guide=$tfRequest->op_inspection_report_detail_SICA_guide;
    }
    if ($tfRequest->exist("op_inspection_report_detail_id_list")){
      $this->id_list=$tfRequest->op_inspection_report_detail_id_list;
    }
    if ($tfRequest->exist("op_inspection_report_detail_transport")){
      $this->transport=$tfRequest->op_inspection_report_detail_transport;
    }
    if ($tfRequest->exist("op_inspection_report_detail_name")){
      $this->name=$tfRequest->op_inspection_report_detail_name;
    }
    if ($tfRequest->exist("op_inspection_report_detail_nin")){
      $this->nin=$tfRequest->op_inspection_report_detail_nin;
    }
    if ($tfRequest->exist("op_inspection_report_detail_chuto")){
      $this->chuto=$tfRequest->op_inspection_report_detail_chuto;
    }
    if ($tfRequest->exist("op_inspection_report_detail_id_destination")){
      $this->id_destination=$tfRequest->op_inspection_report_detail_id_destination;
    }
    if ($tfRequest->exist("op_inspection_report_detail_BL")){
      $this->BL=$tfRequest->op_inspection_report_detail_BL;
    }
    if ($tfRequest->exist("op_inspection_report_detail_admission_date")){
      $this->admission_date=$tfRequest->op_inspection_report_detail_admission_date;
    }
    if ($tfRequest->exist("op_inspection_report_detail_admission_time")){
      $this->admission_time=$tfRequest->op_inspection_report_detail_admission_time;
    }
    if ($tfRequest->exist("op_inspection_report_detail_tara_number")){
      $this->tara_number=$tfRequest->op_inspection_report_detail_tara_number;
    }
    if ($tfRequest->exist("op_inspection_report_detail_status_1")){
      $this->status_1=$tfRequest->op_inspection_report_detail_status_1;
    }
    if ($tfRequest->exist("op_inspection_report_detail_operation_day")){
      $this->operation_day=$tfRequest->op_inspection_report_detail_operation_day;
    }
    if ($tfRequest->exist("op_inspection_report_detail_survey_guide")){
      $this->survey_guide=$tfRequest->op_inspection_report_detail_survey_guide;
    }
    if ($tfRequest->exist("op_inspection_report_detail_ocamar_guide")){
      $this->ocamar_guide=$tfRequest->op_inspection_report_detail_ocamar_guide;
    }
    if ($tfRequest->exist("op_inspection_report_detail_store")){
      $this->store=$tfRequest->op_inspection_report_detail_store;
    }
    if ($tfRequest->exist("op_inspection_report_detail_gross_weight")){
      $this->gross_weight=$tfRequest->op_inspection_report_detail_gross_weight;
    }
    if ($tfRequest->exist("op_inspection_report_detail_tara_weight")){
      $this->tara_weight=$tfRequest->op_inspection_report_detail_tara_weight;
    }
    if ($tfRequest->exist("op_inspection_report_detail_net_weight")){
      $this->net_weight=$tfRequest->op_inspection_report_detail_net_weight;
    }
    if ($tfRequest->exist("op_inspection_report_detail_aggregate")){
      $this->aggregate=$tfRequest->op_inspection_report_detail_aggregate;
    }
    if ($tfRequest->exist("op_inspection_report_detail_ROB")){
      $this->ROB=$tfRequest->op_inspection_report_detail_ROB;
    }
    if ($tfRequest->exist("op_inspection_report_detail_status_2")){
      $this->status_2=$tfRequest->op_inspection_report_detail_status_2;
    }
    if ($tfRequest->exist("op_inspection_report_detail_id_travel_ocamar")){
      $this->id_travel_ocamar=$tfRequest->op_inspection_report_detail_id_travel_ocamar;
    }
    if ($tfRequest->exist("op_inspection_report_detail_departure_date")){
      $this->departure_date=$tfRequest->op_inspection_report_detail_departure_date;
    }
    if ($tfRequest->exist("op_inspection_report_detail_departure_time")){
      $this->departure_time=$tfRequest->op_inspection_report_detail_departure_time;
    }
    if ($tfRequest->exist("op_inspection_report_detail_seal")){
      $this->seal=$tfRequest->op_inspection_report_detail_seal;
    }
    if ($tfRequest->exist("op_inspection_report_detail_observation")){
      $this->observation=$tfRequest->op_inspection_report_detail_observation;
    }
    if ($tfRequest->exist("op_inspection_report_detail_active")){
      $this->active=$tfRequest->op_inspection_report_detail_active;
    }
    if ($tfRequest->exist("op_inspection_report_detail_created_by")){
      $this->created_by=$tfRequest->op_inspection_report_detail_created_by;
    }
    if ($tfRequest->exist("op_inspection_report_detail_created_date")){
      $this->created_date=$tfRequest->op_inspection_report_detail_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["row"]=array("type"=>"number",
                                  "value"=>$this->row,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["hash"]=array("type"=>"string",
                                  "value"=>$this->hash,
                                  "length"=>4500,
                                  "required"=>false);
    $this->validation["id_travel"]=array("type"=>"string",
                                  "value"=>$this->id_travel,
                                  "length"=>3,
                                  "required"=>false);
    $this->validation["SICA_guide"]=array("type"=>"string",
                                  "value"=>$this->SICA_guide,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["id_list"]=array("type"=>"string",
                                  "value"=>$this->id_list,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["transport"]=array("type"=>"string",
                                  "value"=>$this->transport,
                                  "length"=>450,
                                  "required"=>false);
    $this->validation["name"]=array("type"=>"string",
                                  "value"=>$this->name,
                                  "length"=>200,
                                  "required"=>true);
    $this->validation["nin"]=array("type"=>"string",
                                  "value"=>$this->nin,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["chuto"]=array("type"=>"string",
                                  "value"=>$this->chuto,
                                  "length"=>15,
                                  "required"=>false);
    $this->validation["id_destination"]=array("type"=>"number",
                                  "value"=>$this->id_destination,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["BL"]=array("type"=>"string",
                                  "value"=>$this->BL,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["admission_date"]=array("type"=>"string",
                                  "value"=>$this->admission_date,
                                  "length"=>20,
                                  "required"=>false);
    $this->validation["admission_time"]=array("type"=>"string",
                                  "value"=>$this->admission_time,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["tara_number"]=array("type"=>"string",
                                  "value"=>$this->tara_number,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["status_1"]=array("type"=>"string",
                                  "value"=>$this->status_1,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["operation_day"]=array("type"=>"string",
                                  "value"=>$this->operation_day,
                                  "length"=>5,
                                  "required"=>false);
    $this->validation["survey_guide"]=array("type"=>"string",
                                  "value"=>$this->survey_guide,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["ocamar_guide"]=array("type"=>"string",
                                  "value"=>$this->ocamar_guide,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["store"]=array("type"=>"string",
                                  "value"=>$this->store,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["gross_weight"]=array("type"=>"string",
                                  "value"=>$this->gross_weight,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["tara_weight"]=array("type"=>"string",
                                  "value"=>$this->tara_weight,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["net_weight"]=array("type"=>"string",
                                  "value"=>$this->net_weight,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["aggregate"]=array("type"=>"string",
                                  "value"=>$this->aggregate,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["ROB"]=array("type"=>"string",
                                  "value"=>$this->ROB,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["status_2"]=array("type"=>"string",
                                  "value"=>$this->status_2,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["id_travel_ocamar"]=array("type"=>"string",
                                  "value"=>$this->id_travel_ocamar,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["departure_date"]=array("type"=>"string",
                                  "value"=>$this->departure_date,
                                  "length"=>20,
                                  "required"=>false);
    $this->validation["departure_time"]=array("type"=>"string",
                                  "value"=>$this->departure_time,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["seal"]=array("type"=>"string",
                                  "value"=>$this->seal,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["observation"]=array("type"=>"string",
                                  "value"=>$this->observation,
                                  "length"=>1000,
                                  "required"=>false);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setRow($value){
  $this->row=$value;
  }
  public function getRow(){
  return $this->row;
  }
  public function setHash($value){
  $this->hash=$value;
  }
  public function getHash(){
  return $this->hash;
  }
  public function setIdTravel($value){
  $this->id_travel=$value;
  }
  public function getIdTravel(){
  return $this->id_travel;
  }
  public function setSICAGuide($value){
  $this->SICA_guide=$value;
  }
  public function getSICAGuide(){
  return $this->SICA_guide;
  }
  public function setIdList($value){
  $this->id_list=$value;
  }
  public function getIdList(){
  return $this->id_list;
  }
  public function setTransport($value){
  $this->transport=$value;
  }
  public function getTransport(){
  return $this->transport;
  }
  public function setName($value){
  $this->name=$value;
  }
  public function getName(){
  return $this->name;
  }
  public function setNin($value){
  $this->nin=$value;
  }
  public function getNin(){
  return $this->nin;
  }
  public function setChuto($value){
  $this->chuto=$value;
  }
  public function getChuto(){
  return $this->chuto;
  }
  public function setIdDestination($value){
  $this->id_destination=$value;
  }
  public function getIdDestination(){
  return $this->id_destination;
  }
  public function setBL($value){
  $this->BL=$value;
  }
  public function getBL(){
  return $this->BL;
  }
  public function setAdmissionDate($value){
  $this->admission_date=$value;
  }
  public function getAdmissionDate(){
  return $this->admission_date;
  }
  public function setAdmissionTime($value){
  $this->admission_time=$value;
  }
  public function getAdmissionTime(){
  return $this->admission_time;
  }
  public function setTaraNumber($value){
  $this->tara_number=$value;
  }
  public function getTaraNumber(){
  return $this->tara_number;
  }
  public function setStatus1($value){
  $this->status_1=$value;
  }
  public function getStatus1(){
  return $this->status_1;
  }
  public function setOperationDay($value){
  $this->operation_day=$value;
  }
  public function getOperationDay(){
  return $this->operation_day;
  }
  public function setSurveyGuide($value){
  $this->survey_guide=$value;
  }
  public function getSurveyGuide(){
  return $this->survey_guide;
  }
  public function setOcamarGuide($value){
  $this->ocamar_guide=$value;
  }
  public function getOcamarGuide(){
  return $this->ocamar_guide;
  }
  public function setStore($value){
  $this->store=$value;
  }
  public function getStore(){
  return $this->store;
  }
  public function setGrossWeight($value){
  $this->gross_weight=$value;
  }
  public function getGrossWeight(){
  return $this->gross_weight;
  }
  public function setTaraWeight($value){
  $this->tara_weight=$value;
  }
  public function getTaraWeight(){
  return $this->tara_weight;
  }
  public function setNetWeight($value){
  $this->net_weight=$value;
  }
  public function getNetWeight(){
  return $this->net_weight;
  }
  public function setAggregate($value){
  $this->aggregate=$value;
  }
  public function getAggregate(){
  return $this->aggregate;
  }
  public function setROB($value){
  $this->ROB=$value;
  }
  public function getROB(){
  return $this->ROB;
  }
  public function setStatus2($value){
  $this->status_2=$value;
  }
  public function getStatus2(){
  return $this->status_2;
  }
  public function setIdTravelOcamar($value){
  $this->id_travel_ocamar=$value;
  }
  public function getIdTravelOcamar(){
  return $this->id_travel_ocamar;
  }
  public function setDepartureDate($value){
  $this->departure_date=$value;
  }
  public function getDepartureDate(){
  return $this->departure_date;
  }
  public function setDepartureTime($value){
  $this->departure_time=$value;
  }
  public function getDepartureTime(){
  return $this->departure_time;
  }
  public function setSeal($value){
  $this->seal=$value;
  }
  public function getSeal(){
  return $this->seal;
  }
  public function setObservation($value){
  $this->observation=$value;
  }
  public function getObservation(){
  return $this->observation;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO op_inspection_report_detail(id,
                               row,
                               hash,
                               id_travel,
                               SICA_guide,
                               id_list,
                               transport,
                               name,
                               nin,
                               chuto,
                               id_destination,
                               BL,
                               admission_date,
                               admission_time,
                               tara_number,
                               status_1,
                               operation_day,
                               survey_guide,
                               ocamar_guide,
                               store,
                               gross_weight,
                               tara_weight,
                               net_weight,
                               aggregate,
                               ROB,
                               status_2,
                               id_travel_ocamar,
                               departure_date,
                               departure_time,
                               seal,
                               observation,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->row==''?NULL:$this->row,
                     $this->hash==''?NULL:$this->hash,
                     $this->id_travel==''?NULL:$this->id_travel,
                     $this->SICA_guide==''?NULL:$this->SICA_guide,
                     $this->id_list==''?NULL:$this->id_list,
                     $this->transport==''?NULL:$this->transport,
                     $this->name==''?NULL:$this->name,
                     $this->nin==''?NULL:$this->nin,
                     $this->chuto==''?NULL:$this->chuto,
                     $this->id_destination==''?NULL:$this->id_destination,
                     $this->BL==''?NULL:$this->BL,
                     $this->admission_date==''?NULL:$this->admission_date,
                     $this->admission_time==''?NULL:$this->admission_time,
                     $this->tara_number==''?NULL:$this->tara_number,
                     $this->status_1==''?NULL:$this->status_1,
                     $this->operation_day==''?NULL:$this->operation_day,
                     $this->survey_guide==''?NULL:$this->survey_guide,
                     $this->ocamar_guide==''?NULL:$this->ocamar_guide,
                     $this->store==''?NULL:$this->store,
                     $this->gross_weight==''?NULL:$this->gross_weight,
                     $this->tara_weight==''?NULL:$this->tara_weight,
                     $this->net_weight==''?NULL:$this->net_weight,
                     $this->aggregate==''?NULL:$this->aggregate,
                     $this->ROB==''?NULL:$this->ROB,
                     $this->status_2==''?NULL:$this->status_2,
                     $this->id_travel_ocamar==''?NULL:$this->id_travel_ocamar,
                     $this->departure_date==''?NULL:$this->departure_date,
                     $this->departure_time==''?NULL:$this->departure_time,
                     $this->seal==''?NULL:$this->seal,
                     $this->observation==''?NULL:$this->observation,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->row!= $rs["row"]){
          if ($this->updateable["row"]){
            $set.=$set_aux."row=?";
            $set_aux=",";
            $param[]=$this->row==''?NULL:$this->row;
          }else{
            $this->objError[]="The field (row) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->hash!= $rs["hash"]){
          if ($this->updateable["hash"]){
            $set.=$set_aux."hash=?";
            $set_aux=",";
            $param[]=$this->hash==''?NULL:$this->hash;
          }else{
            $this->objError[]="The field (hash) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_travel!= $rs["id_travel"]){
          if ($this->updateable["id_travel"]){
            $set.=$set_aux."id_travel=?";
            $set_aux=",";
            $param[]=$this->id_travel==''?NULL:$this->id_travel;
          }else{
            $this->objError[]="The field (id_travel) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->SICA_guide!= $rs["SICA_guide"]){
          if ($this->updateable["SICA_guide"]){
            $set.=$set_aux."SICA_guide=?";
            $set_aux=",";
            $param[]=$this->SICA_guide==''?NULL:$this->SICA_guide;
          }else{
            $this->objError[]="The field (SICA_guide) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_list!= $rs["id_list"]){
          if ($this->updateable["id_list"]){
            $set.=$set_aux."id_list=?";
            $set_aux=",";
            $param[]=$this->id_list==''?NULL:$this->id_list;
          }else{
            $this->objError[]="The field (id_list) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->transport!= $rs["transport"]){
          if ($this->updateable["transport"]){
            $set.=$set_aux."transport=?";
            $set_aux=",";
            $param[]=$this->transport==''?NULL:$this->transport;
          }else{
            $this->objError[]="The field (transport) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->name!= $rs["name"]){
          if ($this->updateable["name"]){
            $set.=$set_aux."name=?";
            $set_aux=",";
            $param[]=$this->name==''?NULL:$this->name;
          }else{
            $this->objError[]="The field (name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->nin!= $rs["nin"]){
          if ($this->updateable["nin"]){
            $set.=$set_aux."nin=?";
            $set_aux=",";
            $param[]=$this->nin==''?NULL:$this->nin;
          }else{
            $this->objError[]="The field (nin) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->chuto!= $rs["chuto"]){
          if ($this->updateable["chuto"]){
            $set.=$set_aux."chuto=?";
            $set_aux=",";
            $param[]=$this->chuto==''?NULL:$this->chuto;
          }else{
            $this->objError[]="The field (chuto) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_destination!= $rs["id_destination"]){
          if ($this->updateable["id_destination"]){
            $set.=$set_aux."id_destination=?";
            $set_aux=",";
            $param[]=$this->id_destination==''?NULL:$this->id_destination;
          }else{
            $this->objError[]="The field (id_destination) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->BL!= $rs["BL"]){
          if ($this->updateable["BL"]){
            $set.=$set_aux."BL=?";
            $set_aux=",";
            $param[]=$this->BL==''?NULL:$this->BL;
          }else{
            $this->objError[]="The field (BL) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->admission_date!= $rs["admission_date"]){
          if ($this->updateable["admission_date"]){
            $set.=$set_aux."admission_date=?";
            $set_aux=",";
            $param[]=$this->admission_date==''?NULL:$this->admission_date;
          }else{
            $this->objError[]="The field (admission_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->admission_time!= $rs["admission_time"]){
          if ($this->updateable["admission_time"]){
            $set.=$set_aux."admission_time=?";
            $set_aux=",";
            $param[]=$this->admission_time==''?NULL:$this->admission_time;
          }else{
            $this->objError[]="The field (admission_time) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->tara_number!= $rs["tara_number"]){
          if ($this->updateable["tara_number"]){
            $set.=$set_aux."tara_number=?";
            $set_aux=",";
            $param[]=$this->tara_number==''?NULL:$this->tara_number;
          }else{
            $this->objError[]="The field (tara_number) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->status_1!= $rs["status_1"]){
          if ($this->updateable["status_1"]){
            $set.=$set_aux."status_1=?";
            $set_aux=",";
            $param[]=$this->status_1==''?NULL:$this->status_1;
          }else{
            $this->objError[]="The field (status_1) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->operation_day!= $rs["operation_day"]){
          if ($this->updateable["operation_day"]){
            $set.=$set_aux."operation_day=?";
            $set_aux=",";
            $param[]=$this->operation_day==''?NULL:$this->operation_day;
          }else{
            $this->objError[]="The field (operation_day) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->survey_guide!= $rs["survey_guide"]){
          if ($this->updateable["survey_guide"]){
            $set.=$set_aux."survey_guide=?";
            $set_aux=",";
            $param[]=$this->survey_guide==''?NULL:$this->survey_guide;
          }else{
            $this->objError[]="The field (survey_guide) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->ocamar_guide!= $rs["ocamar_guide"]){
          if ($this->updateable["ocamar_guide"]){
            $set.=$set_aux."ocamar_guide=?";
            $set_aux=",";
            $param[]=$this->ocamar_guide==''?NULL:$this->ocamar_guide;
          }else{
            $this->objError[]="The field (ocamar_guide) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->store!= $rs["store"]){
          if ($this->updateable["store"]){
            $set.=$set_aux."store=?";
            $set_aux=",";
            $param[]=$this->store==''?NULL:$this->store;
          }else{
            $this->objError[]="The field (store) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->gross_weight!= $rs["gross_weight"]){
          if ($this->updateable["gross_weight"]){
            $set.=$set_aux."gross_weight=?";
            $set_aux=",";
            $param[]=$this->gross_weight==''?NULL:$this->gross_weight;
          }else{
            $this->objError[]="The field (gross_weight) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->tara_weight!= $rs["tara_weight"]){
          if ($this->updateable["tara_weight"]){
            $set.=$set_aux."tara_weight=?";
            $set_aux=",";
            $param[]=$this->tara_weight==''?NULL:$this->tara_weight;
          }else{
            $this->objError[]="The field (tara_weight) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->net_weight!= $rs["net_weight"]){
          if ($this->updateable["net_weight"]){
            $set.=$set_aux."net_weight=?";
            $set_aux=",";
            $param[]=$this->net_weight==''?NULL:$this->net_weight;
          }else{
            $this->objError[]="The field (net_weight) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->aggregate!= $rs["aggregate"]){
          if ($this->updateable["aggregate"]){
            $set.=$set_aux."aggregate=?";
            $set_aux=",";
            $param[]=$this->aggregate==''?NULL:$this->aggregate;
          }else{
            $this->objError[]="The field (aggregate) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->ROB!= $rs["ROB"]){
          if ($this->updateable["ROB"]){
            $set.=$set_aux."ROB=?";
            $set_aux=",";
            $param[]=$this->ROB==''?NULL:$this->ROB;
          }else{
            $this->objError[]="The field (ROB) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->status_2!= $rs["status_2"]){
          if ($this->updateable["status_2"]){
            $set.=$set_aux."status_2=?";
            $set_aux=",";
            $param[]=$this->status_2==''?NULL:$this->status_2;
          }else{
            $this->objError[]="The field (status_2) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_travel_ocamar!= $rs["id_travel_ocamar"]){
          if ($this->updateable["id_travel_ocamar"]){
            $set.=$set_aux."id_travel_ocamar=?";
            $set_aux=",";
            $param[]=$this->id_travel_ocamar==''?NULL:$this->id_travel_ocamar;
          }else{
            $this->objError[]="The field (id_travel_ocamar) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->departure_date!= $rs["departure_date"]){
          if ($this->updateable["departure_date"]){
            $set.=$set_aux."departure_date=?";
            $set_aux=",";
            $param[]=$this->departure_date==''?NULL:$this->departure_date;
          }else{
            $this->objError[]="The field (departure_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->departure_time!= $rs["departure_time"]){
          if ($this->updateable["departure_time"]){
            $set.=$set_aux."departure_time=?";
            $set_aux=",";
            $param[]=$this->departure_time==''?NULL:$this->departure_time;
          }else{
            $this->objError[]="The field (departure_time) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->seal!= $rs["seal"]){
          if ($this->updateable["seal"]){
            $set.=$set_aux."seal=?";
            $set_aux=",";
            $param[]=$this->seal==''?NULL:$this->seal;
          }else{
            $this->objError[]="The field (seal) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->observation!= $rs["observation"]){
          if ($this->updateable["observation"]){
            $set.=$set_aux."observation=?";
            $set_aux=",";
            $param[]=$this->observation==''?NULL:$this->observation;
          }else{
            $this->objError[]="The field (observation) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE op_inspection_report_detail ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM op_inspection_report_detail
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
