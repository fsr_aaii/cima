<?php
  class CoBillBase extends TfEntity {
    protected $id;
    protected $id_accounting;
    protected $id_unit;
    protected $aliquot;
    protected $rate;
    protected $rate_payment;
    protected $balance_30;
    protected $balance_60;
    protected $balance_90;
    protected $balance_120;
    protected $balance_previous;
    protected $paymented_by;
    protected $paymented_date;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="co_bill";
  }

  private function getAll(){

    $q="SELECT id,
               id_accounting,
               id_unit,
               aliquot,
               rate,
               rate_payment,
               balance_30,
               balance_60,
               balance_90,
               balance_120,
               balance_previous,
               paymented_by,
               paymented_date,
               created_by,
               created_date
          FROM co_bill
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash('ripemd160',json_encode($rs));
    $this->id_accounting=$rs["id_accounting"];
    $this->id_unit=$rs["id_unit"];
    $this->aliquot=$rs["aliquot"];
    $this->rate=$rs["rate"];
    $this->rate_payment=$rs["rate_payment"];
    $this->balance_30=$rs["balance_30"];
    $this->balance_60=$rs["balance_60"];
    $this->balance_90=$rs["balance_90"];
    $this->balance_120=$rs["balance_120"];
    $this->balance_previous=$rs["balance_previous"];
    $this->paymented_by=$rs["paymented_by"];
    $this->paymented_date=$rs["paymented_date"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->co_bill_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_co_bill){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_co_bill; 
    }

    if ($tfRequest->exist("co_bill_id_accounting")){
      $this->id_accounting=$tfRequest->co_bill_id_accounting;
    }
    if ($tfRequest->exist("co_bill_id_unit")){
      $this->id_unit=$tfRequest->co_bill_id_unit;
    }
    if ($tfRequest->exist("co_bill_aliquot")){
      $this->aliquot=$tfRequest->co_bill_aliquot;
    }
    if ($tfRequest->exist("co_bill_rate")){
      $this->rate=$tfRequest->co_bill_rate;
    }
    if ($tfRequest->exist("co_bill_rate_payment")){
      $this->rate_payment=$tfRequest->co_bill_rate_payment;
    }
    if ($tfRequest->exist("co_bill_balance_30")){
      $this->balance_30=$tfRequest->co_bill_balance_30;
    }
    if ($tfRequest->exist("co_bill_balance_60")){
      $this->balance_60=$tfRequest->co_bill_balance_60;
    }
    if ($tfRequest->exist("co_bill_balance_90")){
      $this->balance_90=$tfRequest->co_bill_balance_90;
    }
    if ($tfRequest->exist("co_bill_balance_120")){
      $this->balance_120=$tfRequest->co_bill_balance_120;
    }
    if ($tfRequest->exist("co_bill_balance_previous")){
      $this->balance_previous=$tfRequest->co_bill_balance_previous;
    }
    if ($tfRequest->exist("co_bill_paymented_by")){
      $this->paymented_by=$tfRequest->co_bill_paymented_by;
    }
    if ($tfRequest->exist("co_bill_paymented_date")){
      $this->paymented_date=$tfRequest->co_bill_paymented_date;
    }
    if ($tfRequest->exist("co_bill_created_by")){
      $this->created_by=$tfRequest->co_bill_created_by;
    }
    if ($tfRequest->exist("co_bill_created_date")){
      $this->created_date=$tfRequest->co_bill_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_accounting"]=array("type"=>"number",
                                  "value"=>$this->id_accounting,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_unit"]=array("type"=>"number",
                                  "value"=>$this->id_unit,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["aliquot"]=array("type"=>"number",
                                  "value"=>$this->aliquot,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["rate"]=array("type"=>"number",
                                  "value"=>$this->rate,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["rate_payment"]=array("type"=>"number",
                                  "value"=>$this->rate_payment,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["balance_30"]=array("type"=>"number",
                                  "value"=>$this->balance_30,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["balance_60"]=array("type"=>"number",
                                  "value"=>$this->balance_60,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["balance_90"]=array("type"=>"number",
                                  "value"=>$this->balance_90,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["balance_120"]=array("type"=>"number",
                                  "value"=>$this->balance_120,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["balance_previous"]=array("type"=>"number",
                                  "value"=>$this->balance_previous,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["paymented_by"]=array("type"=>"number",
                                  "value"=>$this->paymented_by,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["paymented_date"]=array("type"=>"datetime",
                                  "value"=>$this->paymented_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdAccounting($value){
  $this->id_accounting=$value;
  }
  public function getIdAccounting(){
  return $this->id_accounting;
  }
  public function setIdUnit($value){
  $this->id_unit=$value;
  }
  public function getIdUnit(){
  return $this->id_unit;
  }
  public function setAliquot($value){
  $this->aliquot=$value;
  }
  public function getAliquot(){
  return $this->aliquot;
  }
  public function setRate($value){
  $this->rate=$value;
  }
  public function getRate(){
  return $this->rate;
  }
  public function setRatePayment($value){
  $this->rate_payment=$value;
  }
  public function getRatePayment(){
  return $this->rate_payment;
  }
  public function setBalance30($value){
  $this->balance_30=$value;
  }
  public function getBalance30(){
  return $this->balance_30;
  }
  public function setBalance60($value){
  $this->balance_60=$value;
  }
  public function getBalance60(){
  return $this->balance_60;
  }
  public function setBalance90($value){
  $this->balance_90=$value;
  }
  public function getBalance90(){
  return $this->balance_90;
  }
  public function setBalance120($value){
  $this->balance_120=$value;
  }
  public function getBalance120(){
  return $this->balance_120;
  }
  public function setBalancePrevious($value){
  $this->balance_previous=$value;
  }
  public function getBalancePrevious(){
  return $this->balance_previous;
  }
  public function setPaymentedBy($value){
  $this->paymented_by=$value;
  }
  public function getPaymentedBy(){
  return $this->paymented_by;
  }
  public function setPaymentedDate($value){
  $this->paymented_date=$value;
  }
  public function getPaymentedDate(){
  return $this->paymented_date;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO co_bill(id,
                               id_accounting,
                               id_unit,
                               aliquot,
                               rate,
                               rate_payment,
                               balance_30,
                               balance_60,
                               balance_90,
                               balance_120,
                               balance_previous,
                               paymented_by,
                               paymented_date,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_accounting==''?NULL:$this->id_accounting,
                     $this->id_unit==''?NULL:$this->id_unit,
                     $this->aliquot==''?NULL:$this->aliquot,
                     $this->rate==''?NULL:$this->rate,
                     $this->rate_payment==''?NULL:$this->rate_payment,
                     $this->balance_30==''?NULL:$this->balance_30,
                     $this->balance_60==''?NULL:$this->balance_60,
                     $this->balance_90==''?NULL:$this->balance_90,
                     $this->balance_120==''?NULL:$this->balance_120,
                     $this->balance_previous==''?NULL:$this->balance_previous,
                     $this->paymented_by==''?NULL:$this->paymented_by,
                     $this->paymented_date==''?NULL:$this->paymented_date,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash('ripemd160',json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash('ripemd160',json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_accounting!= $rs["id_accounting"]){
          if ($this->updateable["id_accounting"]){
            $set.=$set_aux."id_accounting=?";
            $set_aux=",";
            $param[]=$this->id_accounting==''?NULL:$this->id_accounting;
          }else{
            $this->objError[]="The field (id_accounting) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_unit!= $rs["id_unit"]){
          if ($this->updateable["id_unit"]){
            $set.=$set_aux."id_unit=?";
            $set_aux=",";
            $param[]=$this->id_unit==''?NULL:$this->id_unit;
          }else{
            $this->objError[]="The field (id_unit) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->aliquot!= $rs["aliquot"]){
          if ($this->updateable["aliquot"]){
            $set.=$set_aux."aliquot=?";
            $set_aux=",";
            $param[]=$this->aliquot==''?NULL:$this->aliquot;
          }else{
            $this->objError[]="The field (aliquot) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->rate!= $rs["rate"]){
          if ($this->updateable["rate"]){
            $set.=$set_aux."rate=?";
            $set_aux=",";
            $param[]=$this->rate==''?NULL:$this->rate;
          }else{
            $this->objError[]="The field (rate) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->rate_payment!= $rs["rate_payment"]){
          if ($this->updateable["rate_payment"]){
            $set.=$set_aux."rate_payment=?";
            $set_aux=",";
            $param[]=$this->rate_payment==''?NULL:$this->rate_payment;
          }else{
            $this->objError[]="The field (rate_payment) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->balance_30!= $rs["balance_30"]){
          if ($this->updateable["balance_30"]){
            $set.=$set_aux."balance_30=?";
            $set_aux=",";
            $param[]=$this->balance_30==''?NULL:$this->balance_30;
          }else{
            $this->objError[]="The field (balance_30) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->balance_60!= $rs["balance_60"]){
          if ($this->updateable["balance_60"]){
            $set.=$set_aux."balance_60=?";
            $set_aux=",";
            $param[]=$this->balance_60==''?NULL:$this->balance_60;
          }else{
            $this->objError[]="The field (balance_60) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->balance_90!= $rs["balance_90"]){
          if ($this->updateable["balance_90"]){
            $set.=$set_aux."balance_90=?";
            $set_aux=",";
            $param[]=$this->balance_90==''?NULL:$this->balance_90;
          }else{
            $this->objError[]="The field (balance_90) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->balance_120!= $rs["balance_120"]){
          if ($this->updateable["balance_120"]){
            $set.=$set_aux."balance_120=?";
            $set_aux=",";
            $param[]=$this->balance_120==''?NULL:$this->balance_120;
          }else{
            $this->objError[]="The field (balance_120) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->balance_previous!= $rs["balance_previous"]){
          if ($this->updateable["balance_previous"]){
            $set.=$set_aux."balance_previous=?";
            $set_aux=",";
            $param[]=$this->balance_previous==''?NULL:$this->balance_previous;
          }else{
            $this->objError[]="The field (balance_previous) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->paymented_by!= $rs["paymented_by"]){
          if ($this->updateable["paymented_by"]){
            $set.=$set_aux."paymented_by=?";
            $set_aux=",";
            $param[]=$this->paymented_by==''?NULL:$this->paymented_by;
          }else{
            $this->objError[]="The field (paymented_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->paymented_date!= $rs["paymented_date"]){
          if ($this->updateable["paymented_date"]){
            $set.=$set_aux."paymented_date=?";
            $set_aux=",";
            $param[]=$this->paymented_date==''?NULL:$this->paymented_date;
          }else{
            $this->objError[]="The field (paymented_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE co_bill ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash('ripemd160',json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM co_bill
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
