<?php
  class CoPreviousDueBase extends TfEntity {
    protected $id;
    protected $id_unit;
    protected $payment_date;
    protected $observation;
    protected $amount;
    protected $paymented_by;
    protected $paymented_date;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="co_previous_due";
  }

  private function getAll(){

    $q="SELECT id,
               id_unit,
               payment_date,
               observation,
               amount,
               paymented_by,
               paymented_date,
               created_by,
               created_date
          FROM co_previous_due
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_unit=$rs["id_unit"];
    $this->payment_date=$rs["payment_date"];
    $this->observation=$rs["observation"];
    $this->amount=$rs["amount"];
    $this->paymented_by=$rs["paymented_by"];
    $this->paymented_date=$rs["paymented_date"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->co_previous_due_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_co_previous_due){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_co_previous_due; 
    }

    if ($tfRequest->exist("co_previous_due_id_unit")){
      $this->id_unit=$tfRequest->co_previous_due_id_unit;
    }
    if ($tfRequest->exist("co_previous_due_payment_date")){
      $this->payment_date=$tfRequest->co_previous_due_payment_date;
    }
    if ($tfRequest->exist("co_previous_due_observation")){
      $this->observation=$tfRequest->co_previous_due_observation;
    }
    if ($tfRequest->exist("co_previous_due_amount")){
      $this->amount=$tfRequest->co_previous_due_amount;
    }
    if ($tfRequest->exist("co_previous_due_paymented_by")){
      $this->paymented_by=$tfRequest->co_previous_due_paymented_by;
    }
    if ($tfRequest->exist("co_previous_due_paymented_date")){
      $this->paymented_date=$tfRequest->co_previous_due_paymented_date;
    }
    if ($tfRequest->exist("co_previous_due_created_by")){
      $this->created_by=$tfRequest->co_previous_due_created_by;
    }
    if ($tfRequest->exist("co_previous_due_created_date")){
      $this->created_date=$tfRequest->co_previous_due_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_unit"]=array("type"=>"number",
                                  "value"=>$this->id_unit,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["payment_date"]=array("type"=>"date",
                                  "value"=>$this->payment_date,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["observation"]=array("type"=>"string",
                                  "value"=>$this->observation,
                                  "length"=>5000,
                                  "required"=>false);
    $this->validation["amount"]=array("type"=>"number",
                                  "value"=>$this->amount,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["paymented_by"]=array("type"=>"number",
                                  "value"=>$this->paymented_by,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["paymented_date"]=array("type"=>"datetime",
                                  "value"=>$this->paymented_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdUnit($value){
  $this->id_unit=$value;
  }
  public function getIdUnit(){
  return $this->id_unit;
  }
  public function setPaymentDate($value){
  $this->payment_date=$value;
  }
  public function getPaymentDate(){
  return $this->payment_date;
  }
  public function setObservation($value){
  $this->observation=$value;
  }
  public function getObservation(){
  return $this->observation;
  }
  public function setAmount($value){
  $this->amount=$value;
  }
  public function getAmount(){
  return $this->amount;
  }
  public function setPaymentedBy($value){
  $this->paymented_by=$value;
  }
  public function getPaymentedBy(){
  return $this->paymented_by;
  }
  public function setPaymentedDate($value){
  $this->paymented_date=$value;
  }
  public function getPaymentedDate(){
  return $this->paymented_date;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO co_previous_due(id,
                               id_unit,
                               payment_date,
                               observation,
                               amount,
                               paymented_by,
                               paymented_date,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_unit==''?NULL:$this->id_unit,
                     $this->payment_date==''?NULL:$this->payment_date,
                     $this->observation==''?NULL:$this->observation,
                     $this->amount==''?NULL:$this->amount,
                     $this->paymented_by==''?NULL:$this->paymented_by,
                     $this->paymented_date==''?NULL:$this->paymented_date,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_unit!= $rs["id_unit"]){
          if ($this->updateable["id_unit"]){
            $set.=$set_aux."id_unit=?";
            $set_aux=",";
            $param[]=$this->id_unit==''?NULL:$this->id_unit;
          }else{
            $this->objError[]="The field (id_unit) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->payment_date!= $rs["payment_date"]){
          if ($this->updateable["payment_date"]){
            $set.=$set_aux."payment_date=?";
            $set_aux=",";
            $param[]=$this->payment_date==''?NULL:$this->payment_date;
          }else{
            $this->objError[]="The field (payment_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->observation!= $rs["observation"]){
          if ($this->updateable["observation"]){
            $set.=$set_aux."observation=?";
            $set_aux=",";
            $param[]=$this->observation==''?NULL:$this->observation;
          }else{
            $this->objError[]="The field (observation) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->amount!= $rs["amount"]){
          if ($this->updateable["amount"]){
            $set.=$set_aux."amount=?";
            $set_aux=",";
            $param[]=$this->amount==''?NULL:$this->amount;
          }else{
            $this->objError[]="The field (amount) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->paymented_by!= $rs["paymented_by"]){
          if ($this->updateable["paymented_by"]){
            $set.=$set_aux."paymented_by=?";
            $set_aux=",";
            $param[]=$this->paymented_by==''?NULL:$this->paymented_by;
          }else{
            $this->objError[]="The field (paymented_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->paymented_date!= $rs["paymented_date"]){
          if ($this->updateable["paymented_date"]){
            $set.=$set_aux."paymented_date=?";
            $set_aux=",";
            $param[]=$this->paymented_date==''?NULL:$this->paymented_date;
          }else{
            $this->objError[]="The field (paymented_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE co_previous_due ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM co_previous_due
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
