<?php
  class OpInspectionReportBase extends TfEntity {
    protected $id;
    protected $inspection_report_no;
    protected $id_file;
    protected $name;
    protected $version;
    protected $id_customer;
    protected $vessel;
    protected $id_port_pier;
    protected $id_product_1;
    protected $id_product_2;
    protected $BL;
    protected $quantity;
    protected $inspection_date;
    protected $issued_date;
    protected $process;
    protected $last_processing;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="op_inspection_report";
  }

  private function getAll(){

    $q="SELECT id,
               inspection_report_no,
               id_file,
               name,
               version,
               id_customer,
               vessel,
               id_port_pier,
               id_product_1,
               id_product_2,
               BL,
               quantity,
               inspection_date,
               issued_date,
               process,
               last_processing,
               created_by,
               created_date
          FROM op_inspection_report
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->inspection_report_no=$rs["inspection_report_no"];
    $this->id_file=$rs["id_file"];
    $this->name=$rs["name"];
    $this->version=$rs["version"];
    $this->id_customer=$rs["id_customer"];
    $this->vessel=$rs["vessel"];
    $this->id_port_pier=$rs["id_port_pier"];
    $this->id_product_1=$rs["id_product_1"];
    $this->id_product_2=$rs["id_product_2"];
    $this->BL=$rs["BL"];
    $this->quantity=$rs["quantity"];
    $this->inspection_date=$rs["inspection_date"];
    $this->issued_date=$rs["issued_date"];
    $this->process=$rs["process"];
    $this->last_processing=$rs["last_processing"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->op_inspection_report_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_op_inspection_report){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_op_inspection_report; 
    }

    if ($tfRequest->exist("op_inspection_report_inspection_report_no")){
      $this->inspection_report_no=$tfRequest->op_inspection_report_inspection_report_no;
    }
    if ($tfRequest->exist("op_inspection_report_id_file")){
      $this->id_file=$tfRequest->op_inspection_report_id_file;
    }
    if ($tfRequest->exist("op_inspection_report_name")){
      $this->name=$tfRequest->op_inspection_report_name;
    }
    if ($tfRequest->exist("op_inspection_report_version")){
      $this->version=$tfRequest->op_inspection_report_version;
    }
    if ($tfRequest->exist("op_inspection_report_id_customer")){
      $this->id_customer=$tfRequest->op_inspection_report_id_customer;
    }
    if ($tfRequest->exist("op_inspection_report_vessel")){
      $this->vessel=$tfRequest->op_inspection_report_vessel;
    }
    if ($tfRequest->exist("op_inspection_report_id_port_pier")){
      $this->id_port_pier=$tfRequest->op_inspection_report_id_port_pier;
    }
    if ($tfRequest->exist("op_inspection_report_id_product_1")){
      $this->id_product_1=$tfRequest->op_inspection_report_id_product_1;
    }
    if ($tfRequest->exist("op_inspection_report_id_product_2")){
      $this->id_product_2=$tfRequest->op_inspection_report_id_product_2;
    }
    if ($tfRequest->exist("op_inspection_report_BL")){
      $this->BL=$tfRequest->op_inspection_report_BL;
    }
    if ($tfRequest->exist("op_inspection_report_quantity")){
      $this->quantity=$tfRequest->op_inspection_report_quantity;
    }
    if ($tfRequest->exist("op_inspection_report_inspection_date")){
      $this->inspection_date=$tfRequest->op_inspection_report_inspection_date;
    }
    if ($tfRequest->exist("op_inspection_report_issued_date")){
      $this->issued_date=$tfRequest->op_inspection_report_issued_date;
    }
    if ($tfRequest->exist("op_inspection_report_process")){
      $this->process=$tfRequest->op_inspection_report_process;
    }
    if ($tfRequest->exist("op_inspection_report_last_processing")){
      $this->last_processing=$tfRequest->op_inspection_report_last_processing;
    }
    if ($tfRequest->exist("op_inspection_report_created_by")){
      $this->created_by=$tfRequest->op_inspection_report_created_by;
    }
    if ($tfRequest->exist("op_inspection_report_created_date")){
      $this->created_date=$tfRequest->op_inspection_report_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["inspection_report_no"]=array("type"=>"string",
                                  "value"=>$this->inspection_report_no,
                                  "length"=>11,
                                  "required"=>false);
    $this->validation["id_file"]=array("type"=>"string",
                                  "value"=>$this->id_file,
                                  "length"=>200,
                                  "required"=>false);
    $this->validation["name"]=array("type"=>"string",
                                  "value"=>$this->name,
                                  "length"=>200,
                                  "required"=>true);
    $this->validation["version"]=array("type"=>"number",
                                  "value"=>$this->version,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_customer"]=array("type"=>"number",
                                  "value"=>$this->id_customer,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["vessel"]=array("type"=>"string",
                                  "value"=>$this->vessel,
                                  "length"=>200,
                                  "required"=>true);
    $this->validation["id_port_pier"]=array("type"=>"number",
                                  "value"=>$this->id_port_pier,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_product_1"]=array("type"=>"number",
                                  "value"=>$this->id_product_1,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_product_2"]=array("type"=>"number",
                                  "value"=>$this->id_product_2,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["BL"]=array("type"=>"string",
                                  "value"=>$this->BL,
                                  "length"=>45,
                                  "required"=>false);
    $this->validation["quantity"]=array("type"=>"string",
                                  "value"=>$this->quantity,
                                  "length"=>20,
                                  "required"=>true);
    $this->validation["inspection_date"]=array("type"=>"date",
                                  "value"=>$this->inspection_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["issued_date"]=array("type"=>"date",
                                  "value"=>$this->issued_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["process"]=array("type"=>"string",
                                  "value"=>$this->process,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["last_processing"]=array("type"=>"datetime",
                                  "value"=>$this->last_processing,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setInspectionReportNo($value){
  $this->inspection_report_no=$value;
  }
  public function getInspectionReportNo(){
  return $this->inspection_report_no;
  }
  public function setIdFile($value){
  $this->id_file=$value;
  }
  public function getIdFile(){
  return $this->id_file;
  }
  public function setName($value){
  $this->name=$value;
  }
  public function getName(){
  return $this->name;
  }
  public function setVersion($value){
  $this->version=$value;
  }
  public function getVersion(){
  return $this->version;
  }
  public function setIdCustomer($value){
  $this->id_customer=$value;
  }
  public function getIdCustomer(){
  return $this->id_customer;
  }
  public function setVessel($value){
  $this->vessel=$value;
  }
  public function getVessel(){
  return $this->vessel;
  }
  public function setIdPortPier($value){
  $this->id_port_pier=$value;
  }
  public function getIdPortPier(){
  return $this->id_port_pier;
  }
  public function setIdProduct1($value){
  $this->id_product_1=$value;
  }
  public function getIdProduct1(){
  return $this->id_product_1;
  }
  public function setIdProduct2($value){
  $this->id_product_2=$value;
  }
  public function getIdProduct2(){
  return $this->id_product_2;
  }
  public function setBL($value){
  $this->BL=$value;
  }
  public function getBL(){
  return $this->BL;
  }
  public function setQuantity($value){
  $this->quantity=$value;
  }
  public function getQuantity(){
  return $this->quantity;
  }
  public function setInspectionDate($value){
  $this->inspection_date=$value;
  }
  public function getInspectionDate(){
  return $this->inspection_date;
  }
  public function setIssuedDate($value){
  $this->issued_date=$value;
  }
  public function getIssuedDate(){
  return $this->issued_date;
  }
  public function setProcess($value){
  $this->process=$value;
  }
  public function getProcess(){
  return $this->process;
  }
  public function setLastProcessing($value){
  $this->last_processing=$value;
  }
  public function getLastProcessing(){
  return $this->last_processing;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO op_inspection_report(id,
                               inspection_report_no,
                               id_file,
                               name,
                               version,
                               id_customer,
                               vessel,
                               id_port_pier,
                               id_product_1,
                               id_product_2,
                               BL,
                               quantity,
                               inspection_date,
                               issued_date,
                               process,
                               last_processing,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->inspection_report_no==''?NULL:$this->inspection_report_no,
                     $this->id_file==''?NULL:$this->id_file,
                     $this->name==''?NULL:$this->name,
                     $this->version==''?NULL:$this->version,
                     $this->id_customer==''?NULL:$this->id_customer,
                     $this->vessel==''?NULL:$this->vessel,
                     $this->id_port_pier==''?NULL:$this->id_port_pier,
                     $this->id_product_1==''?NULL:$this->id_product_1,
                     $this->id_product_2==''?NULL:$this->id_product_2,
                     $this->BL==''?NULL:$this->BL,
                     $this->quantity==''?NULL:$this->quantity,
                     $this->inspection_date==''?NULL:$this->inspection_date,
                     $this->issued_date==''?NULL:$this->issued_date,
                     $this->process==''?NULL:$this->process,
                     $this->last_processing==''?NULL:$this->last_processing,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->inspection_report_no!= $rs["inspection_report_no"]){
          if ($this->updateable["inspection_report_no"]){
            $set.=$set_aux."inspection_report_no=?";
            $set_aux=",";
            $param[]=$this->inspection_report_no==''?NULL:$this->inspection_report_no;
          }else{
            $this->objError[]="The field (inspection_report_no) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_file!= $rs["id_file"]){
          if ($this->updateable["id_file"]){
            $set.=$set_aux."id_file=?";
            $set_aux=",";
            $param[]=$this->id_file==''?NULL:$this->id_file;
          }else{
            $this->objError[]="The field (id_file) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->name!= $rs["name"]){
          if ($this->updateable["name"]){
            $set.=$set_aux."name=?";
            $set_aux=",";
            $param[]=$this->name==''?NULL:$this->name;
          }else{
            $this->objError[]="The field (name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->version!= $rs["version"]){
          if ($this->updateable["version"]){
            $set.=$set_aux."version=?";
            $set_aux=",";
            $param[]=$this->version==''?NULL:$this->version;
          }else{
            $this->objError[]="The field (version) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_customer!= $rs["id_customer"]){
          if ($this->updateable["id_customer"]){
            $set.=$set_aux."id_customer=?";
            $set_aux=",";
            $param[]=$this->id_customer==''?NULL:$this->id_customer;
          }else{
            $this->objError[]="The field (id_customer) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->vessel!= $rs["vessel"]){
          if ($this->updateable["vessel"]){
            $set.=$set_aux."vessel=?";
            $set_aux=",";
            $param[]=$this->vessel==''?NULL:$this->vessel;
          }else{
            $this->objError[]="The field (vessel) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_port_pier!= $rs["id_port_pier"]){
          if ($this->updateable["id_port_pier"]){
            $set.=$set_aux."id_port_pier=?";
            $set_aux=",";
            $param[]=$this->id_port_pier==''?NULL:$this->id_port_pier;
          }else{
            $this->objError[]="The field (id_port_pier) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_product_1!= $rs["id_product_1"]){
          if ($this->updateable["id_product_1"]){
            $set.=$set_aux."id_product_1=?";
            $set_aux=",";
            $param[]=$this->id_product_1==''?NULL:$this->id_product_1;
          }else{
            $this->objError[]="The field (id_product_1) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_product_2!= $rs["id_product_2"]){
          if ($this->updateable["id_product_2"]){
            $set.=$set_aux."id_product_2=?";
            $set_aux=",";
            $param[]=$this->id_product_2==''?NULL:$this->id_product_2;
          }else{
            $this->objError[]="The field (id_product_2) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->BL!= $rs["BL"]){
          if ($this->updateable["BL"]){
            $set.=$set_aux."BL=?";
            $set_aux=",";
            $param[]=$this->BL==''?NULL:$this->BL;
          }else{
            $this->objError[]="The field (BL) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->quantity!= $rs["quantity"]){
          if ($this->updateable["quantity"]){
            $set.=$set_aux."quantity=?";
            $set_aux=",";
            $param[]=$this->quantity==''?NULL:$this->quantity;
          }else{
            $this->objError[]="The field (quantity) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->inspection_date!= $rs["inspection_date"]){
          if ($this->updateable["inspection_date"]){
            $set.=$set_aux."inspection_date=?";
            $set_aux=",";
            $param[]=$this->inspection_date==''?NULL:$this->inspection_date;
          }else{
            $this->objError[]="The field (inspection_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->issued_date!= $rs["issued_date"]){
          if ($this->updateable["issued_date"]){
            $set.=$set_aux."issued_date=?";
            $set_aux=",";
            $param[]=$this->issued_date==''?NULL:$this->issued_date;
          }else{
            $this->objError[]="The field (issued_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->process!= $rs["process"]){
          if ($this->updateable["process"]){
            $set.=$set_aux."process=?";
            $set_aux=",";
            $param[]=$this->process==''?NULL:$this->process;
          }else{
            $this->objError[]="The field (process) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->last_processing!= $rs["last_processing"]){
          if ($this->updateable["last_processing"]){
            $set.=$set_aux."last_processing=?";
            $set_aux=",";
            $param[]=$this->last_processing==''?NULL:$this->last_processing;
          }else{
            $this->objError[]="The field (last_processing) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE op_inspection_report ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM op_inspection_report
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
