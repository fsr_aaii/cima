<?php
  class MmPetPreformBase extends TfEntity {
    protected $id;
    protected $id_transaction_element;
    protected $id_color;
    protected $id_polymer_closure_type;
    protected $weight;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="mm_pet_preform";
  }

  private function getAll(){

    $q="SELECT id,
               id_transaction_element,
               id_color,
               id_polymer_closure_type,
               weight,
               active,
               created_by,
               created_date
          FROM mm_pet_preform
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_transaction_element=$rs["id_transaction_element"];
    $this->id_color=$rs["id_color"];
    $this->id_polymer_closure_type=$rs["id_polymer_closure_type"];
    $this->weight=$rs["weight"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->mm_pet_preform_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_mm_pet_preform){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_mm_pet_preform; 
    }

    if ($tfRequest->exist("mm_pet_preform_id_transaction_element")){
      $this->id_transaction_element=$tfRequest->mm_pet_preform_id_transaction_element;
    }
    if ($tfRequest->exist("mm_pet_preform_id_color")){
      $this->id_color=$tfRequest->mm_pet_preform_id_color;
    }
    if ($tfRequest->exist("mm_pet_preform_id_polymer_closure_type")){
      $this->id_polymer_closure_type=$tfRequest->mm_pet_preform_id_polymer_closure_type;
    }
    if ($tfRequest->exist("mm_pet_preform_weight")){
      $this->weight=$tfRequest->mm_pet_preform_weight;
    }
    if ($tfRequest->exist("mm_pet_preform_active")){
      $this->active=$tfRequest->mm_pet_preform_active;
    }
    if ($tfRequest->exist("mm_pet_preform_created_by")){
      $this->created_by=$tfRequest->mm_pet_preform_created_by;
    }
    if ($tfRequest->exist("mm_pet_preform_created_date")){
      $this->created_date=$tfRequest->mm_pet_preform_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_transaction_element"]=array("type"=>"number",
                                  "value"=>$this->id_transaction_element,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_color"]=array("type"=>"number",
                                  "value"=>$this->id_color,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_polymer_closure_type"]=array("type"=>"number",
                                  "value"=>$this->id_polymer_closure_type,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["weight"]=array("type"=>"number",
                                  "value"=>$this->weight,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdTransactionElement($value){
  $this->id_transaction_element=$value;
  }
  public function getIdTransactionElement(){
  return $this->id_transaction_element;
  }
  public function setIdColor($value){
  $this->id_color=$value;
  }
  public function getIdColor(){
  return $this->id_color;
  }
  public function setIdPolymerClosureType($value){
  $this->id_polymer_closure_type=$value;
  }
  public function getIdPolymerClosureType(){
  return $this->id_polymer_closure_type;
  }
  public function setWeight($value){
  $this->weight=$value;
  }
  public function getWeight(){
  return $this->weight;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    if ($this->id == ''){
      $this->id = $this->sequence();
    }
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO mm_pet_preform(id,
                               id_transaction_element,
                               id_color,
                               id_polymer_closure_type,
                               weight,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_transaction_element==''?NULL:$this->id_transaction_element,
                     $this->id_color==''?NULL:$this->id_color,
                     $this->id_polymer_closure_type==''?NULL:$this->id_polymer_closure_type,
                     $this->weight==''?NULL:$this->weight,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_transaction_element!= $rs["id_transaction_element"]){
          if ($this->updateable["id_transaction_element"]){
            $set.=$set_aux."id_transaction_element=?";
            $set_aux=",";
            $param[]=$this->id_transaction_element==''?NULL:$this->id_transaction_element;
          }else{
            $this->objError[]="The field (id_transaction_element) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_color!= $rs["id_color"]){
          if ($this->updateable["id_color"]){
            $set.=$set_aux."id_color=?";
            $set_aux=",";
            $param[]=$this->id_color==''?NULL:$this->id_color;
          }else{
            $this->objError[]="The field (id_color) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_polymer_closure_type!= $rs["id_polymer_closure_type"]){
          if ($this->updateable["id_polymer_closure_type"]){
            $set.=$set_aux."id_polymer_closure_type=?";
            $set_aux=",";
            $param[]=$this->id_polymer_closure_type==''?NULL:$this->id_polymer_closure_type;
          }else{
            $this->objError[]="The field (id_polymer_closure_type) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->weight!= $rs["weight"]){
          if ($this->updateable["weight"]){
            $set.=$set_aux."weight=?";
            $set_aux=",";
            $param[]=$this->weight==''?NULL:$this->weight;
          }else{
            $this->objError[]="The field (weight) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE mm_pet_preform ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM mm_pet_preform
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
