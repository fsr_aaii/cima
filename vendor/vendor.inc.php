<?php 

  require 'autoload.php';

  require_once('adodb5/adodb.inc.php');
  require_once('adodb5/adodb-exceptions.inc.php');
  require_once("Encryption/php/Encryption.php"); 
  require_once("cryptojs-aes-php/src/CryptoJSAES.php"); 
  require_once('jdorn/sql-formatter/lib/SqlFormatter.php');

  require_once('phpmailer/phpmailer/src/Exception.php');
  require_once('phpmailer/phpmailer/src/PHPMailer.php');
  require_once('phpmailer/phpmailer/src/SMTP.php');
  require_once('altorouter/altorouter/AltoRouter.php');
  require_once('google-api-php-client--PHP7.0/vendor/autoload.php');
  //require_once ('mpdf-5.7/mpdf.php');


