<?php

  $tuichValidateMessage[0]='';
  $tuichValidateMessage[1]='La longitud del campo no es correcta'; //checkLength
  $tuichValidateMessage[2]='Los campos deben ser iguales'; //compare
  $tuichValidateMessage[3]='El campo no puede estar vacio'; //hasValue
  $tuichValidateMessage[4]='El campo solo debe contener letras'; //isAlpha
  $tuichValidateMessage[5]='EL campo solo debe contener valores alfanumericos'; //isAlphaNumeric
  $tuichValidateMessage[6]='El campo no contiene una fecha valida'; //isDate
  $tuichValidateMessage[7]='El correo electronico contenido dentro del campo no es correcto'; //isEmail
  $tuichValidateMessage[8]='El campo debe estar vacio'; //isEmpty
  $tuichValidateMessage[9]='No es una URL valida'; //isInternetURL
  $tuichValidateMessage[10]='No es una dirrección IP válida'; //isIPAddress
  $tuichValidateMessage[11]='El campo no es un número'; //isNumber
  $tuichValidateMessage[12]='No es una abreviación válida'; //isStateAbbreviation
  $tuichValidateMessage[13]='El valor del campo excede el máximo permitido'; //isTooLong
  $tuichValidateMessage[14]='El valor del campo excede el mínimo permitido'; //isTooShort
  $tuichValidateMessage[15]='El campo solo puede contener digitos numericos'; //isUnsignedNumber
  
//require_once('vendor/autoload.php');
require_once('vendor/vendor.inc.php');
require_once('environment/databases.inc.php');
require_once('environment/properties.inc.php');
require_once('environment/app.inc.php');
require_once('tuich/autoload.php');

?>