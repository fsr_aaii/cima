DELIMITER ;;
CREATE OR REPLACE TRIGGER OpInspectionReportDetail_aui_trg BEFORE INSERT ON OpInspectionReportDetail FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"row";"hash";"id_travel";"SICA_guide";"id_list";"transport";"name";"nin";"chuto";"id_destination";"BL";"admission_date";"admission_time";"tara_number";"status_1";"operation_day";"survey_guide";"ocamar_guide";"store";"gross_weight";"tara_weight";"net_weight";"aggregate";"ROB";"status_2";"id_travel_ocamar";"departure_date";"departure_time";"seal";"observation";"active";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.row,'";"',NEW.hash,'";"',NEW.id_travel,'";"',NEW.SICA_guide,'";"',NEW.id_list,'";"',NEW.transport,'";"',NEW.name,'";"',NEW.nin,'";"',NEW.chuto,'";"',NEW.id_destination,'";"',NEW.BL,'";"',NEW.admission_date,'";"',NEW.admission_time,'";"',NEW.tara_number,'";"',NEW.status_1,'";"',NEW.operation_day,'";"',NEW.survey_guide,'";"',NEW.ocamar_guide,'";"',NEW.store,'";"',NEW.gross_weight,'";"',NEW.tara_weight,'";"',NEW.net_weight,'";"',NEW.aggregate,'";"',NEW.ROB,'";"',NEW.status_2,'";"',NEW.id_travel_ocamar,'";"',NEW.departure_date,'";"',NEW.departure_time,'";"',NEW.seal,'";"',NEW.observation,'";"',NEW.active,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','OpInspectionReportDetail',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER OpInspectionReportDetail_aud_trg BEFORE DELETE ON OpInspectionReportDetail FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"row";"hash";"id_travel";"SICA_guide";"id_list";"transport";"name";"nin";"chuto";"id_destination";"BL";"admission_date";"admission_time";"tara_number";"status_1";"operation_day";"survey_guide";"ocamar_guide";"store";"gross_weight";"tara_weight";"net_weight";"aggregate";"ROB";"status_2";"id_travel_ocamar";"departure_date";"departure_time";"seal";"observation";"active";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.row,'";"',OLD.hash,'";"',OLD.id_travel,'";"',OLD.SICA_guide,'";"',OLD.id_list,'";"',OLD.transport,'";"',OLD.name,'";"',OLD.nin,'";"',OLD.chuto,'";"',OLD.id_destination,'";"',OLD.BL,'";"',OLD.admission_date,'";"',OLD.admission_time,'";"',OLD.tara_number,'";"',OLD.status_1,'";"',OLD.operation_day,'";"',OLD.survey_guide,'";"',OLD.ocamar_guide,'";"',OLD.store,'";"',OLD.gross_weight,'";"',OLD.tara_weight,'";"',OLD.net_weight,'";"',OLD.aggregate,'";"',OLD.ROB,'";"',OLD.status_2,'";"',OLD.id_travel_ocamar,'";"',OLD.departure_date,'";"',OLD.departure_time,'";"',OLD.seal,'";"',OLD.observation,'";"',OLD.active,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','OpInspectionReportDetail',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER OpInspectionReportDetail_auu_trg BEFORE UPDATE ON OpInspectionReportDetail FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.row!=OLD.row THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"row"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.row,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.row,'"');
     SET v_separator=';';
   END IF;
   IF NEW.hash!=OLD.hash THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"hash"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.hash,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.hash,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_travel!=OLD.id_travel THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_travel"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_travel,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_travel,'"');
     SET v_separator=';';
   END IF;
   IF NEW.SICA_guide!=OLD.SICA_guide THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"SICA_guide"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.SICA_guide,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.SICA_guide,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_list!=OLD.id_list THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_list"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_list,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_list,'"');
     SET v_separator=';';
   END IF;
   IF NEW.transport!=OLD.transport THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"transport"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.transport,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.transport,'"');
     SET v_separator=';';
   END IF;
   IF NEW.name!=OLD.name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.nin!=OLD.nin THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"nin"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.nin,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.nin,'"');
     SET v_separator=';';
   END IF;
   IF NEW.chuto!=OLD.chuto THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"chuto"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.chuto,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.chuto,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_destination!=OLD.id_destination THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_destination"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_destination,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_destination,'"');
     SET v_separator=';';
   END IF;
   IF NEW.BL!=OLD.BL THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"BL"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.BL,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.BL,'"');
     SET v_separator=';';
   END IF;
   IF NEW.admission_date!=OLD.admission_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"admission_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.admission_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.admission_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.admission_time!=OLD.admission_time THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"admission_time"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.admission_time,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.admission_time,'"');
     SET v_separator=';';
   END IF;
   IF NEW.tara_number!=OLD.tara_number THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"tara_number"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.tara_number,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.tara_number,'"');
     SET v_separator=';';
   END IF;
   IF NEW.status_1!=OLD.status_1 THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"status_1"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.status_1,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.status_1,'"');
     SET v_separator=';';
   END IF;
   IF NEW.operation_day!=OLD.operation_day THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"operation_day"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.operation_day,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.operation_day,'"');
     SET v_separator=';';
   END IF;
   IF NEW.survey_guide!=OLD.survey_guide THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"survey_guide"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.survey_guide,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.survey_guide,'"');
     SET v_separator=';';
   END IF;
   IF NEW.ocamar_guide!=OLD.ocamar_guide THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"ocamar_guide"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.ocamar_guide,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.ocamar_guide,'"');
     SET v_separator=';';
   END IF;
   IF NEW.store!=OLD.store THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"store"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.store,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.store,'"');
     SET v_separator=';';
   END IF;
   IF NEW.gross_weight!=OLD.gross_weight THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"gross_weight"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.gross_weight,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.gross_weight,'"');
     SET v_separator=';';
   END IF;
   IF NEW.tara_weight!=OLD.tara_weight THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"tara_weight"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.tara_weight,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.tara_weight,'"');
     SET v_separator=';';
   END IF;
   IF NEW.net_weight!=OLD.net_weight THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"net_weight"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.net_weight,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.net_weight,'"');
     SET v_separator=';';
   END IF;
   IF NEW.aggregate!=OLD.aggregate THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"aggregate"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.aggregate,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.aggregate,'"');
     SET v_separator=';';
   END IF;
   IF NEW.ROB!=OLD.ROB THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"ROB"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.ROB,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.ROB,'"');
     SET v_separator=';';
   END IF;
   IF NEW.status_2!=OLD.status_2 THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"status_2"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.status_2,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.status_2,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_travel_ocamar!=OLD.id_travel_ocamar THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_travel_ocamar"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_travel_ocamar,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_travel_ocamar,'"');
     SET v_separator=';';
   END IF;
   IF NEW.departure_date!=OLD.departure_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"departure_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.departure_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.departure_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.departure_time!=OLD.departure_time THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"departure_time"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.departure_time,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.departure_time,'"');
     SET v_separator=';';
   END IF;
   IF NEW.seal!=OLD.seal THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"seal"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.seal,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.seal,'"');
     SET v_separator=';';
   END IF;
   IF NEW.observation!=OLD.observation THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"observation"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.observation,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.observation,'"');
     SET v_separator=';';
   END IF;
   IF NEW.active!=OLD.active THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"active"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.active,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.active,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','OpInspectionReportDetail',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

