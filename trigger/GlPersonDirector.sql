DELIMITER ;;
CREATE OR REPLACE TRIGGER GlPersonDirector_aui_trg BEFORE INSERT ON GlPersonDirector FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_person";"id_user";"job";"active";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_person,'";"',NEW.id_user,'";"',NEW.job,'";"',NEW.active,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','GlPersonDirector',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER GlPersonDirector_aud_trg BEFORE DELETE ON GlPersonDirector FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_person";"id_user";"job";"active";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_person,'";"',OLD.id_user,'";"',OLD.job,'";"',OLD.active,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','GlPersonDirector',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER GlPersonDirector_auu_trg BEFORE UPDATE ON GlPersonDirector FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_person!=OLD.id_person THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_person"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_person,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_person,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_user!=OLD.id_user THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_user"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_user,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_user,'"');
     SET v_separator=';';
   END IF;
   IF NEW.job!=OLD.job THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"job"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.job,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.job,'"');
     SET v_separator=';';
   END IF;
   IF NEW.active!=OLD.active THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"active"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.active,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.active,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','GlPersonDirector',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

