DELIMITER ;;
CREATE OR REPLACE TRIGGER CoUnitContact_aui_trg BEFORE INSERT ON CoUnitContact FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_unit";"name";"account";"phone_number";"active";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_unit,'";"',NEW.name,'";"',NEW.account,'";"',NEW.phone_number,'";"',NEW.active,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','CoUnitContact',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER CoUnitContact_aud_trg BEFORE DELETE ON CoUnitContact FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_unit";"name";"account";"phone_number";"active";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_unit,'";"',OLD.name,'";"',OLD.account,'";"',OLD.phone_number,'";"',OLD.active,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','CoUnitContact',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER CoUnitContact_auu_trg BEFORE UPDATE ON CoUnitContact FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_unit!=OLD.id_unit THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_unit"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_unit,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_unit,'"');
     SET v_separator=';';
   END IF;
   IF NEW.name!=OLD.name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.account!=OLD.account THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"account"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.account,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.account,'"');
     SET v_separator=';';
   END IF;
   IF NEW.phone_number!=OLD.phone_number THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"phone_number"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.phone_number,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.phone_number,'"');
     SET v_separator=';';
   END IF;
   IF NEW.active!=OLD.active THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"active"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.active,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.active,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','CoUnitContact',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

