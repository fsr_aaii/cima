DELIMITER ;;
CREATE OR REPLACE TRIGGER CoAccounting_aui_trg BEFORE INSERT ON CoAccounting FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_person";"payment_date";"processed_by";"processed_date";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_person,'";"',NEW.payment_date,'";"',NEW.processed_by,'";"',NEW.processed_date,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','CoAccounting',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER CoAccounting_aud_trg BEFORE DELETE ON CoAccounting FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_person";"payment_date";"processed_by";"processed_date";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_person,'";"',OLD.payment_date,'";"',OLD.processed_by,'";"',OLD.processed_date,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','CoAccounting',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER CoAccounting_auu_trg BEFORE UPDATE ON CoAccounting FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_person!=OLD.id_person THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_person"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_person,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_person,'"');
     SET v_separator=';';
   END IF;
   IF NEW.payment_date!=OLD.payment_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"payment_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.payment_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.payment_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.processed_by!=OLD.processed_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"processed_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.processed_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.processed_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.processed_date!=OLD.processed_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"processed_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.processed_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.processed_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','CoAccounting',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

