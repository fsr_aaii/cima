DELIMITER ;;
CREATE OR REPLACE TRIGGER CoAccountingDetail_aui_trg BEFORE INSERT ON CoAccountingDetail FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_accounting";"id_account";"description";"observation";"amount";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_accounting,'";"',NEW.id_account,'";"',NEW.description,'";"',NEW.observation,'";"',NEW.amount,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','CoAccountingDetail',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER CoAccountingDetail_aud_trg BEFORE DELETE ON CoAccountingDetail FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_accounting";"id_account";"description";"observation";"amount";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_accounting,'";"',OLD.id_account,'";"',OLD.description,'";"',OLD.observation,'";"',OLD.amount,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','CoAccountingDetail',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER CoAccountingDetail_auu_trg BEFORE UPDATE ON CoAccountingDetail FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_accounting!=OLD.id_accounting THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_accounting"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_accounting,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_accounting,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_account!=OLD.id_account THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_account"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_account,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_account,'"');
     SET v_separator=';';
   END IF;
   IF NEW.description!=OLD.description THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"description"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.description,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.description,'"');
     SET v_separator=';';
   END IF;
   IF NEW.observation!=OLD.observation THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"observation"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.observation,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.observation,'"');
     SET v_separator=';';
   END IF;
   IF NEW.amount!=OLD.amount THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"amount"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.amount,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.amount,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','CoAccountingDetail',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

