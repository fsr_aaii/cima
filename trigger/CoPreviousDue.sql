DELIMITER ;;
CREATE OR REPLACE TRIGGER CoPreviousDue_aui_trg BEFORE INSERT ON CoPreviousDue FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_unit";"payment_date";"seat";"observation";"amount";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_unit,'";"',NEW.payment_date,'";"',NEW.seat,'";"',NEW.observation,'";"',NEW.amount,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','CoPreviousDue',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER CoPreviousDue_aud_trg BEFORE DELETE ON CoPreviousDue FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_unit";"payment_date";"seat";"observation";"amount";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_unit,'";"',OLD.payment_date,'";"',OLD.seat,'";"',OLD.observation,'";"',OLD.amount,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','CoPreviousDue',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER CoPreviousDue_auu_trg BEFORE UPDATE ON CoPreviousDue FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_unit!=OLD.id_unit THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_unit"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_unit,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_unit,'"');
     SET v_separator=';';
   END IF;
   IF NEW.payment_date!=OLD.payment_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"payment_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.payment_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.payment_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.seat!=OLD.seat THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"seat"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.seat,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.seat,'"');
     SET v_separator=';';
   END IF;
   IF NEW.observation!=OLD.observation THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"observation"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.observation,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.observation,'"');
     SET v_separator=';';
   END IF;
   IF NEW.amount!=OLD.amount THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"amount"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.amount,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.amount,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','CoPreviousDue',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

