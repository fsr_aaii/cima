DELIMITER ;;
CREATE OR REPLACE TRIGGER CoBill_aui_trg BEFORE INSERT ON CoBill FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_accounting";"id_unit";"aliquot";"paymented_by";"paymented_date";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_accounting,'";"',NEW.id_unit,'";"',NEW.aliquot,'";"',NEW.paymented_by,'";"',NEW.paymented_date,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','CoBill',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER CoBill_aud_trg BEFORE DELETE ON CoBill FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_accounting";"id_unit";"aliquot";"paymented_by";"paymented_date";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_accounting,'";"',OLD.id_unit,'";"',OLD.aliquot,'";"',OLD.paymented_by,'";"',OLD.paymented_date,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','CoBill',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER CoBill_auu_trg BEFORE UPDATE ON CoBill FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_accounting!=OLD.id_accounting THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_accounting"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_accounting,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_accounting,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_unit!=OLD.id_unit THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_unit"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_unit,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_unit,'"');
     SET v_separator=';';
   END IF;
   IF NEW.aliquot!=OLD.aliquot THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"aliquot"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.aliquot,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.aliquot,'"');
     SET v_separator=';';
   END IF;
   IF NEW.paymented_by!=OLD.paymented_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"paymented_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.paymented_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.paymented_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.paymented_date!=OLD.paymented_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"paymented_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.paymented_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.paymented_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','CoBill',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

