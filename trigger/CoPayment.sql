DELIMITER ;;
CREATE OR REPLACE TRIGGER CoPayment_aui_trg BEFORE INSERT ON CoPayment FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_unit";"amount";"amount_confirmed";"image_path";"observation";"confirmed_by";"confirmed_date";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_unit,'";"',NEW.amount,'";"',NEW.amount_confirmed,'";"',NEW.image_path,'";"',NEW.observation,'";"',NEW.confirmed_by,'";"',NEW.confirmed_date,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','CoPayment',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER CoPayment_aud_trg BEFORE DELETE ON CoPayment FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_unit";"amount";"amount_confirmed";"image_path";"observation";"confirmed_by";"confirmed_date";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_unit,'";"',OLD.amount,'";"',OLD.amount_confirmed,'";"',OLD.image_path,'";"',OLD.observation,'";"',OLD.confirmed_by,'";"',OLD.confirmed_date,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','CoPayment',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER CoPayment_auu_trg BEFORE UPDATE ON CoPayment FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_unit!=OLD.id_unit THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_unit"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_unit,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_unit,'"');
     SET v_separator=';';
   END IF;
   IF NEW.amount!=OLD.amount THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"amount"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.amount,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.amount,'"');
     SET v_separator=';';
   END IF;
   IF NEW.amount_confirmed!=OLD.amount_confirmed THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"amount_confirmed"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.amount_confirmed,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.amount_confirmed,'"');
     SET v_separator=';';
   END IF;
   IF NEW.image_path!=OLD.image_path THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"image_path"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.image_path,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.image_path,'"');
     SET v_separator=';';
   END IF;
   IF NEW.observation!=OLD.observation THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"observation"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.observation,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.observation,'"');
     SET v_separator=';';
   END IF;
   IF NEW.confirmed_by!=OLD.confirmed_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"confirmed_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.confirmed_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.confirmed_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.confirmed_date!=OLD.confirmed_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"confirmed_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.confirmed_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.confirmed_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','CoPayment',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

