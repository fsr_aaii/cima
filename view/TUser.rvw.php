<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8 info-panel">
            <div class="col-6 container title">T User</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="TUser" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
               <a class="btn-guaramo-text" data-tf-table="#t_user_dt" data-tf-file="T User" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>
       <table id="t_user_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Login</th>
             <th class="all">Photo</th>
             <th class="all">Active</th>
             <th class="all">Password</th>
             <th class="all">Password Date</th>
             <th class="all">Email Account</th>
             <th class="all">Name</th>
             <th class="all">Phone Number</th>
             <th class="all">Source</th>
             <th class="all">Notified</th>
             <th class="none">Creado por</th>
             <th class="none">Creado el</th>
             <th class="all text-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($tUserList as $row){
    $tfData["t_user_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["login"].'</td>
            <td>'.$row["photo"].'</td>
            <td>'.$row["active"].'</td>
            <td>'.$row["password"].'</td>
            <td>'.$row["password_date"].'</td>
            <td>'.$row["email_account"].'</td>
            <td>'.$row["name"].'</td>
            <td>'.$row["phone_number"].'</td>
            <td>'.$row["source"].'</td>
            <td>'.$row["notified"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="text-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="TUser" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#t_user_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>