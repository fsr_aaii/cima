<style type="text/css">
.nice-select.open .list {
    height: 200px; 
    overflow:auto;
 }   
</style>
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_accounting_unit_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccountingUnit" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coAccountingUnit->getCreatedBy()).'  el '.$coAccountingUnit->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_accounting_unit_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccountingUnit" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_accounting_unit_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccountingUnit" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coAccountingUnit->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coAccountingUnit->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_accounting_unit_form" name="co_accounting_unit_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Gasto Individual</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_accounting_unit" name="is_co_accounting_unit" value="'.$coAccountingUnit->getInitialState().'">
         <input type="hidden" id="co_accounting_unit_id" name="co_accounting_unit_id" maxlength="22" value="'.$coAccountingUnit->getId().'">
         <input type="hidden" id="co_accounting_unit_id_accounting" name="co_accounting_unit_id_accounting" maxlength="22" value="'.$coAccountingUnit->getIdAccounting().'">

      </div>
       <div class="col-lg-12 container">
       <label for="co_accounting_unit_id_unit" class="control-label">Unidad:</label>
        <select  id="co_accounting_unit_id_unit" name="co_accounting_unit_id_unit" class="co_accounting_unit_id_unit form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CoUnit::selectOptionsByPerson($tfs,$coAccountingUnit->getIdPerson()),$coAccountingUnit->getIdUnit()).
'      </select>
      <label for="co_accounting_unit_id_unit" class="error">'.$coAccountingUnit->getAttrError("id_unit").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_accounting_unit_description" class="control-label">Descripcion:</label>
        <textarea id="co_accounting_unit_description" name="co_accounting_unit_description" class="co_accounting_unit_description form-control" rows="3" >'.$coAccountingUnit->getDescription().'</textarea>
      <label for="co_accounting_unit_description" class="error">'.$coAccountingUnit->getAttrError("description").'</label>
      </div>
      <div class="col-lg-8 container">
       <label for="co_accounting_unit_id_account" class="control-label">Concepto:</label>
        <select  id="co_accounting_unit_id_account" name="co_accounting_unit_id_account" class="co_accounting_unit_id_account form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CoAccount::selectOptions($tfs),$coAccountingUnit->getIdAccount()).
'      </select>
      <label for="co_accounting_unit_id_account" class="error">'.$coAccountingUnit->getAttrError("id_account").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="co_accounting_unit_amount" class="control-label">Monto:</label>
        <input type="text" id="co_accounting_unit_amount" name="co_accounting_unit_amount" class="co_accounting_unit_amount form-control"  maxlength="22"  value="'.$coAccountingUnit->getAmount().'"  tabindex="3"/>
      <label for="co_accounting_unit_amount" class="error">'.$coAccountingUnit->getAttrError("amount").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_accounting_unit_observation" class="control-label">Observaccion:</label>
        <textarea id="co_accounting_unit_observation" name="co_accounting_unit_observation" class="co_accounting_unit_observation form-control" rows="3" >'.$coAccountingUnit->getObservation().'</textarea>
      <label for="co_accounting_unit_observation" class="error">'.$coAccountingUnit->getAttrError("observation").'</label>
      </div>
      

   <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">

  function coAccountingUnitRules(){

  $("#co_accounting_unit_form").validate();
  $("#co_accounting_unit_id_accounting").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_accounting_unit_id_unit").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_accounting_unit_id_account").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_accounting_unit_description").rules("add", {
    required:true,
    maxlength:5000
  });
  $("#co_accounting_unit_observation").rules("add", {
    maxlength:5000
  });
  $("#co_accounting_unit_amount").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}


$(document).ready(function(){
  coAccountingUnitRules();
  $('select').niceSelect();

})
</script>