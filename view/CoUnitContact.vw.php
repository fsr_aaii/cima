<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_unit_contact_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnitContact" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coUnitContact->getCreatedBy()).'  el '.$coUnitContact->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_unit_contact_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnitContact" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_unit_contact_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnitContact" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coUnitContact->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coUnitContact->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_unit_contact_form" name="co_unit_contact_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Co Unit Contact</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_unit_contact" name="is_co_unit_contact" value="'.$coUnitContact->getInitialState().'">
         <input type="hidden" id="co_unit_contact_id" name="co_unit_contact_id" maxlength="22" value="'.$coUnitContact->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="co_unit_contact_id_unit" class="control-label">Id Unit:</label>
        <select  id="co_unit_contact_id_unit" name="co_unit_contact_id_unit" class="co_unit_contact_id_unit form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CoUnit::selectOptions($tfs),$coUnitContact->getIdUnit()).
'      </select>
      <label for="co_unit_contact_id_unit" class="error">'.$coUnitContact->getAttrError("id_unit").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_unit_contact_name" class="control-label">Name:</label>
        <textarea id="co_unit_contact_name" name="co_unit_contact_name" class="co_unit_contact_name form-control" rows="3" >'.$coUnitContact->getName().'</textarea>
      <label for="co_unit_contact_name" class="error">'.$coUnitContact->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_unit_contact_account" class="control-label">Account:</label>
        <input type="text" id="co_unit_contact_account" name="co_unit_contact_account" class="co_unit_contact_account form-control"  maxlength="250"  value="'.$coUnitContact->getAccount().'"  tabindex="3"/>
      <label for="co_unit_contact_account" class="error">'.$coUnitContact->getAttrError("account").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_unit_contact_phone_number" class="control-label">Phone Number:</label>
        <input type="text" id="co_unit_contact_phone_number" name="co_unit_contact_phone_number" class="co_unit_contact_phone_number form-control"  maxlength="20"  value="'.$coUnitContact->getPhoneNumber().'"  tabindex="4"/>
      <label for="co_unit_contact_phone_number" class="error">'.$coUnitContact->getAttrError("phone_number").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_unit_contact_active" class="control-label">Active:</label>
        <select  id="co_unit_contact_active" name="co_unit_contact_active" class="co_unit_contact_active form-control" tabindex="5">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($coUnitContact->getActive(),'Y').
'      </select>
      <label for="co_unit_contact_active" class="error">'.$coUnitContact->getAttrError("active").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function coUnitContactRules(){
  $("#co_unit_contact_form").validate();
  $("#co_unit_contact_id_unit").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_unit_contact_name").rules("add", {
    required:true,
    maxlength:500
  });
  $("#co_unit_contact_account").rules("add", {
    maxlength:250
  });
  $("#co_unit_contact_phone_number").rules("add", {
    maxlength:20
  });
  $("#co_unit_contact_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  coUnitContactRules();
  $('select').niceSelect();

})
</script>