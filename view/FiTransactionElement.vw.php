<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn" role="button" data-tf-form="#fi_transaction_element_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="fiTransactionElement" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$fiTransactionElement->getCreatedBy()).'  on '.$fiTransactionElement->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn" role="button" data-tf-form="#fi_transaction_element_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="fiTransactionElement" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn" role="button" data-tf-form="#fi_transaction_element_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="fiTransactionElement" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($fiTransactionElement->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($fiTransactionElement->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="fi_transaction_element_form" name="fi_transaction_element_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container epic-title xl mb-5">Fi Transaction Element</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_fi_transaction_element" name="is_fi_transaction_element" value="'.$fiTransactionElement->getInitialState().'">
         <input type="hidden" id="fi_transaction_element_id" name="fi_transaction_element_id" maxlength="22" value="'.$fiTransactionElement->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="fi_transaction_element_name" class="control-label">Name:</label>
        <input type="text" id="fi_transaction_element_name" name="fi_transaction_element_name" class="fi_transaction_element_name form-control"  maxlength="200"  value="'.$fiTransactionElement->getName().'"  tabindex="1"/>
      <label for="fi_transaction_element_name" class="error">'.$fiTransactionElement->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="fi_transaction_element_code" class="control-label">Code:</label>
        <input type="text" id="fi_transaction_element_code" name="fi_transaction_element_code" class="fi_transaction_element_code form-control"  maxlength="15"  value="'.$fiTransactionElement->getCode().'"  tabindex="2"/>
      <label for="fi_transaction_element_code" class="error">'.$fiTransactionElement->getAttrError("code").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="fi_transaction_element_type" class="control-label">Type:</label>
        <input type="text" id="fi_transaction_element_type" name="fi_transaction_element_type" class="fi_transaction_element_type form-control"  maxlength="1"  value="'.$fiTransactionElement->getType().'"  tabindex="3"/>
      <label for="fi_transaction_element_type" class="error">'.$fiTransactionElement->getAttrError("type").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="fi_transaction_element_id_transaction_element_rank" class="control-label">Id Transaction Element Rank:</label>
        <select  id="fi_transaction_element_id_transaction_element_rank" name="fi_transaction_element_id_transaction_element_rank" class="fi_transaction_element_id_transaction_element_rank form-control" tabindex="4">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(FiTransactionElementRank::selectOptions($tfs),$fiTransactionElement->getIdTransactionElementRank()).
'      </select>
      <label for="fi_transaction_element_id_transaction_element_rank" class="error">'.$fiTransactionElement->getAttrError("id_transaction_element_rank").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="fi_transaction_element_id_transaction_type" class="control-label">Id Transaction Type:</label>
        <select  id="fi_transaction_element_id_transaction_type" name="fi_transaction_element_id_transaction_type" class="fi_transaction_element_id_transaction_type form-control" tabindex="5">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(FiTransactionType::selectOptions($tfs),$fiTransactionElement->getIdTransactionType()).
'      </select>
      <label for="fi_transaction_element_id_transaction_type" class="error">'.$fiTransactionElement->getAttrError("id_transaction_type").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="fi_transaction_element_id_element_type" class="control-label">Id Element Type:</label>
        <select  id="fi_transaction_element_id_element_type" name="fi_transaction_element_id_element_type" class="fi_transaction_element_id_element_type form-control" tabindex="6">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(FiElementType::selectOptions($tfs),$fiTransactionElement->getIdElementType()).
'      </select>
      <label for="fi_transaction_element_id_element_type" class="error">'.$fiTransactionElement->getAttrError("id_element_type").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="fi_transaction_element_active" class="control-label">Active:</label>
        <select  id="fi_transaction_element_active" name="fi_transaction_element_active" class="fi_transaction_element_active form-control" tabindex="7">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($fiTransactionElement->getActive(),'Y').
'      </select>
      <label for="fi_transaction_element_active" class="error">'.$fiTransactionElement->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function fiTransactionElementRules(){
  $("#fi_transaction_element_form").validate();
  $("#fi_transaction_element_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#fi_transaction_element_code").rules("add", {
    required:true,
    maxlength:15
  });
  $("#fi_transaction_element_type").rules("add", {
    required:true,
    maxlength:1
  });
  $("#fi_transaction_element_id_transaction_element_rank").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#fi_transaction_element_id_transaction_type").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#fi_transaction_element_id_element_type").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#fi_transaction_element_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  fiTransactionElementRules();
  $('select').niceSelect();

})
</script>