<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            
           <form id="gl_juridical_person_form" name="gl_juridical_person_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="row">
           <div class="mx-auto col-lg-12 p-0">
            <div class="col-12 container title">Gastos</div>
           </div>  

      <div class="col-lg-5 pb-4 container">
       <label for="gl_juridical_person_trade_name" class="control-label">Nombre Comercial:</label>
         <span class="form-control">'.$glPersonRelationship["trade_name"].'</span>
      </div>
      <div class="col-lg-5 pb-4 container">
       <label for="gl_juridical_person_legal_name" class="control-label">Raz&oacute;n Social:</label>
       <span class="form-control">'.$glPersonRelationship["legal_name"].'</span>
      </div>
      <div class="col-lg-2 pb-4 container">
       <label for="gl_juridical_person_nin_type" class="control-label">RIF:</label>
       <span class="form-control">'.$glPersonRelationship["rif"].'</span>
      </div>
      <div class="col-lg-12 pb-4 container">
       <label for="gl_juridical_person_legal_address" class="control-label">Direcci&oacute;n Fiscal:</label>
        <span class="form-control">'.$glPersonRelationship["legal_address"].'</span>
      </div>

   </fieldset>
  </form>
';

$tfNewData["co_accounting_id_person"] = $tfRequest->gl_person_id;

  $html.='<div class="row">
           <div class="mx-auto col-lg-12 p-0">
            <div class="col-6 container title">Gastos</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccounting" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt($tfNewData).'" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
             </div>
           </div>  
           <input type="hidden" id="co_accounting_id_person" name="co_accounting_id_person" maxlength="22" value="'.$tfRequest->gl_person_id.'">
           <div class="col-12 container row justify-content-end mb-3">
            <div class="mr-0 p-0 col-lg-2 container">
              <select  id="co_accounting_year" name="co_accounting_year" class="co_accounting_year form-control" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccounting" data-tf-fn-name="accountingByYear"  tabindex="1">'.
      TfWidget::selectStructure(TfWidget::years($tfs,2020),date("Y")).
'      </select>
            </div>
           </div>
     <div id="co-accounting-all" class="row col-12 p-0">
             <div  id="co-accounting-y-'.date("Y").'" class="co-accounting-list row col-12 p-0"> ';
   foreach ($coAccountingList as $row){
    $tfData["co_accounting_id"] = $row["id"];

    if ($row["processed_date"]!=''){
      $check='<div class="badge-container"> 
                              <span class="badge badge-pill circle-check float-right m-2"><i class="bx bx-check"></i></span>
                            </div>';
      $status = 'PROCESADO';
      $action='ACC';
    }else{
      $check='';
      $action='AE';
      $status = 'POR PROCESADO';
    }

    $html.='  <div class="col-lg-4 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccounting" data-tf-action="'.$action.'" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
            <div class="d-flex align-items-center card-01 p-4" >
            '.$check.'
                <div class="ml-2">
                    <div class="epic c-text-1 text-white m-0">'.$row["month"].'</div>
                    <div class="epic c-text-3 text-white m-0">'.$row["payment_date"].'</div>
                    <div class="epic c-text-5 text-white m-0">'.$status.'</div>
                </div>
            </div>
          </div>';  
   }
   $html.='</div>
        </div>';

   $html.='</div>
        </div>';
 echo $html;
?>

<script type="text/javascript">
  
  $(".filter").on("keyup", function() {
  var input = $(this).val().toUpperCase();

  $(".coaching").each(function() {
    if ($(this).data("string").toUpperCase().indexOf(input) < 0) {
      $(this).hide();
    } else {
      $(this).show();
    }
  })
});


  $(document).ready(function(){

   $("#co_accounting_year").on('change', function(e) {
     let year= $(this).children("option:selected").val();

     if ($("#co-accounting-y-"+year).length){
      $('.co-accounting-list').hide();
      $('#co-accounting-y-'+year).show();
     }else{
       $('.co-accounting-list').hide();
       let JDATA = new Object();
        JDATA.tfTaskId = $(this).data("tf-task-id");
        JDATA.tfController = $(this).data("tf-controller"); 
        JDATA.tfFnName = $(this).data("tf-fn-name");  
        JDATA.tfData = new Object();
        JDATA.tfData.co_accounting_id_person=$("#co_accounting_id_person").val();
        JDATA.tfData.co_accounting_year=year;

         
        let response = TfRequest.fn(JDATA);
        console.log(response);   
        var JRESP=JSON.parse(response);   
        if (JRESP.code=='OK'){
           $('#co-accounting-all').append(JRESP.content);   
        }else{
          $('#co-accounting-all').append('<div id="co-accounting-y-'+year+'" class="co-accounting-list row col-12 p-0">Error al consultar</div>');
        }

       
     } 
  });



})


</script>