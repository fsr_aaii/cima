<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn" role="button" data-tf-form="#gl_relationship_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlRelationship" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$glRelationship->getCreatedBy()).'  on '.$glRelationship->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn" role="button" data-tf-form="#gl_relationship_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlRelationship" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn" role="button" data-tf-form="#gl_relationship_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlRelationship" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($glRelationship->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($glRelationship->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="gl_relationship_form" name="gl_relationship_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title mb-5">Gl Relationship</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_gl_relationship" name="is_gl_relationship" value="'.$glRelationship->getInitialState().'">
         <input type="hidden" id="gl_relationship_id" name="gl_relationship_id" maxlength="22" value="'.$glRelationship->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="gl_relationship_name" class="control-label">Name:</label>
        <input type="text" id="gl_relationship_name" name="gl_relationship_name" class="gl_relationship_name form-control"  maxlength="200"  value="'.$glRelationship->getName().'"  tabindex="1"/>
      <label for="gl_relationship_name" class="error">'.$glRelationship->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_relationship_active" class="control-label">Active:</label>
        <select  id="gl_relationship_active" name="gl_relationship_active" class="gl_relationship_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($glRelationship->getActive(),'Y').
'      </select>
      <label for="gl_relationship_active" class="error">'.$glRelationship->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function glRelationshipRules(){
  $("#gl_relationship_form").validate();
  $("#gl_relationship_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#gl_relationship_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  glRelationshipRules();


})
</script>