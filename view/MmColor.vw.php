<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn" role="button" data-tf-form="#mm_color_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmColor" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$mmColor->getCreatedBy()).'  on '.$mmColor->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn" role="button" data-tf-form="#mm_color_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmColor" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn" role="button" data-tf-form="#mm_color_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmColor" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($mmColor->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($mmColor->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="mm_color_form" name="mm_color_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container epic-title xl mb-5">Mm Color</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_mm_color" name="is_mm_color" value="'.$mmColor->getInitialState().'">
         <input type="hidden" id="mm_color_id" name="mm_color_id" maxlength="22" value="'.$mmColor->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="mm_color_name" class="control-label">Name:</label>
        <input type="text" id="mm_color_name" name="mm_color_name" class="mm_color_name form-control"  maxlength="200"  value="'.$mmColor->getName().'"  tabindex="1"/>
      <label for="mm_color_name" class="error">'.$mmColor->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="mm_color_active" class="control-label">Active:</label>
        <select  id="mm_color_active" name="mm_color_active" class="mm_color_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($mmColor->getActive(),'Y').
'      </select>
      <label for="mm_color_active" class="error">'.$mmColor->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function mmColorRules(){
  $("#mm_color_form").validate();
  $("#mm_color_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#mm_color_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  mmColorRules();


})
</script>