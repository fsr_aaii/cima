<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#op_inspection_report_detail_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="opInspectionReportDetail" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$opInspectionReportDetail->getCreatedBy()).'  el '.$opInspectionReportDetail->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#op_inspection_report_detail_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="opInspectionReportDetail" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#op_inspection_report_detail_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="opInspectionReportDetail" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($opInspectionReportDetail->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($opInspectionReportDetail->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="op_inspection_report_detail_form" name="op_inspection_report_detail_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Op Inspection Report Detail</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_op_inspection_report_detail" name="is_op_inspection_report_detail" value="'.$opInspectionReportDetail->getInitialState().'">
         <input type="hidden" id="op_inspection_report_detail_id" name="op_inspection_report_detail_id" maxlength="22" value="'.$opInspectionReportDetail->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_row" class="control-label">Row:</label>
        <input type="text" id="op_inspection_report_detail_row" name="op_inspection_report_detail_row" class="op_inspection_report_detail_row form-control"  maxlength="22"  value="'.$opInspectionReportDetail->getRow().'"  tabindex="1"/>
      <label for="op_inspection_report_detail_row" class="error">'.$opInspectionReportDetail->getAttrError("row").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_hash" class="control-label">Hash:</label>
        <textarea id="op_inspection_report_detail_hash" name="op_inspection_report_detail_hash" class="op_inspection_report_detail_hash form-control" rows="3" >'.$opInspectionReportDetail->getHash().'</textarea>
      <label for="op_inspection_report_detail_hash" class="error">'.$opInspectionReportDetail->getAttrError("hash").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_id_travel" class="control-label">Id Travel:</label>
        <input type="text" id="op_inspection_report_detail_id_travel" name="op_inspection_report_detail_id_travel" class="op_inspection_report_detail_id_travel form-control"  maxlength="3"  value="'.$opInspectionReportDetail->getIdTravel().'"  tabindex="3"/>
      <label for="op_inspection_report_detail_id_travel" class="error">'.$opInspectionReportDetail->getAttrError("id_travel").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_SICA_guide" class="control-label">SICA Guide:</label>
        <input type="text" id="op_inspection_report_detail_SICA_guide" name="op_inspection_report_detail_SICA_guide" class="op_inspection_report_detail_SICA_guide form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getSICAGuide().'"  tabindex="4"/>
      <label for="op_inspection_report_detail_SICA_guide" class="error">'.$opInspectionReportDetail->getAttrError("SICA_guide").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_id_list" class="control-label">Id List:</label>
        <input type="text" id="op_inspection_report_detail_id_list" name="op_inspection_report_detail_id_list" class="op_inspection_report_detail_id_list form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getIdList().'"  tabindex="5"/>
      <label for="op_inspection_report_detail_id_list" class="error">'.$opInspectionReportDetail->getAttrError("id_list").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_transport" class="control-label">Transport:</label>
        <input type="text" id="op_inspection_report_detail_transport" name="op_inspection_report_detail_transport" class="op_inspection_report_detail_transport form-control"  maxlength="450"  value="'.$opInspectionReportDetail->getTransport().'"  tabindex="6"/>
      <label for="op_inspection_report_detail_transport" class="error">'.$opInspectionReportDetail->getAttrError("transport").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_name" class="control-label">Name:</label>
        <input type="text" id="op_inspection_report_detail_name" name="op_inspection_report_detail_name" class="op_inspection_report_detail_name form-control"  maxlength="200"  value="'.$opInspectionReportDetail->getName().'"  tabindex="7"/>
      <label for="op_inspection_report_detail_name" class="error">'.$opInspectionReportDetail->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_nin" class="control-label">Nin:</label>
        <input type="text" id="op_inspection_report_detail_nin" name="op_inspection_report_detail_nin" class="op_inspection_report_detail_nin form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getNin().'"  tabindex="8"/>
      <label for="op_inspection_report_detail_nin" class="error">'.$opInspectionReportDetail->getAttrError("nin").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_chuto" class="control-label">Chuto:</label>
        <input type="text" id="op_inspection_report_detail_chuto" name="op_inspection_report_detail_chuto" class="op_inspection_report_detail_chuto form-control"  maxlength="15"  value="'.$opInspectionReportDetail->getChuto().'"  tabindex="9"/>
      <label for="op_inspection_report_detail_chuto" class="error">'.$opInspectionReportDetail->getAttrError("chuto").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_id_destination" class="control-label">Id Destination:</label>
        <select  id="op_inspection_report_detail_id_destination" name="op_inspection_report_detail_id_destination" class="op_inspection_report_detail_id_destination form-control" tabindex="10">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(OpDestination::selectOptions($tfs),$opInspectionReportDetail->getIdDestination()).
'      </select>
      <label for="op_inspection_report_detail_id_destination" class="error">'.$opInspectionReportDetail->getAttrError("id_destination").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_BL" class="control-label">BL:</label>
        <input type="text" id="op_inspection_report_detail_BL" name="op_inspection_report_detail_BL" class="op_inspection_report_detail_BL form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getBL().'"  tabindex="11"/>
      <label for="op_inspection_report_detail_BL" class="error">'.$opInspectionReportDetail->getAttrError("BL").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_admission_date" class="control-label">Admission Date:</label>
        <input type="text" id="op_inspection_report_detail_admission_date" name="op_inspection_report_detail_admission_date" class="op_inspection_report_detail_admission_date form-control"  maxlength="20"  value="'.$opInspectionReportDetail->getAdmissionDate().'"  tabindex="12"/>
      <label for="op_inspection_report_detail_admission_date" class="error">'.$opInspectionReportDetail->getAttrError("admission_date").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_admission_time" class="control-label">Admission Time:</label>
        <input type="text" id="op_inspection_report_detail_admission_time" name="op_inspection_report_detail_admission_time" class="op_inspection_report_detail_admission_time form-control"  maxlength="5"  value="'.$opInspectionReportDetail->getAdmissionTime().'"  tabindex="13"/>
      <label for="op_inspection_report_detail_admission_time" class="error">'.$opInspectionReportDetail->getAttrError("admission_time").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_tara_number" class="control-label">Tara Number:</label>
        <input type="text" id="op_inspection_report_detail_tara_number" name="op_inspection_report_detail_tara_number" class="op_inspection_report_detail_tara_number form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getTaraNumber().'"  tabindex="14"/>
      <label for="op_inspection_report_detail_tara_number" class="error">'.$opInspectionReportDetail->getAttrError("tara_number").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_status_1" class="control-label">Status 1:</label>
        <input type="text" id="op_inspection_report_detail_status_1" name="op_inspection_report_detail_status_1" class="op_inspection_report_detail_status_1 form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getStatus1().'"  tabindex="15"/>
      <label for="op_inspection_report_detail_status_1" class="error">'.$opInspectionReportDetail->getAttrError("status_1").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_operation_day" class="control-label">Operation Day:</label>
        <input type="text" id="op_inspection_report_detail_operation_day" name="op_inspection_report_detail_operation_day" class="op_inspection_report_detail_operation_day form-control"  maxlength="5"  value="'.$opInspectionReportDetail->getOperationDay().'"  tabindex="16"/>
      <label for="op_inspection_report_detail_operation_day" class="error">'.$opInspectionReportDetail->getAttrError("operation_day").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_survey_guide" class="control-label">Survey Guide:</label>
        <input type="text" id="op_inspection_report_detail_survey_guide" name="op_inspection_report_detail_survey_guide" class="op_inspection_report_detail_survey_guide form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getSurveyGuide().'"  tabindex="17"/>
      <label for="op_inspection_report_detail_survey_guide" class="error">'.$opInspectionReportDetail->getAttrError("survey_guide").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_ocamar_guide" class="control-label">Ocamar Guide:</label>
        <input type="text" id="op_inspection_report_detail_ocamar_guide" name="op_inspection_report_detail_ocamar_guide" class="op_inspection_report_detail_ocamar_guide form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getOcamarGuide().'"  tabindex="18"/>
      <label for="op_inspection_report_detail_ocamar_guide" class="error">'.$opInspectionReportDetail->getAttrError("ocamar_guide").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_store" class="control-label">Store:</label>
        <input type="text" id="op_inspection_report_detail_store" name="op_inspection_report_detail_store" class="op_inspection_report_detail_store form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getStore().'"  tabindex="19"/>
      <label for="op_inspection_report_detail_store" class="error">'.$opInspectionReportDetail->getAttrError("store").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_gross_weight" class="control-label">Gross Weight:</label>
        <input type="text" id="op_inspection_report_detail_gross_weight" name="op_inspection_report_detail_gross_weight" class="op_inspection_report_detail_gross_weight form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getGrossWeight().'"  tabindex="20"/>
      <label for="op_inspection_report_detail_gross_weight" class="error">'.$opInspectionReportDetail->getAttrError("gross_weight").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_tara_weight" class="control-label">Tara Weight:</label>
        <input type="text" id="op_inspection_report_detail_tara_weight" name="op_inspection_report_detail_tara_weight" class="op_inspection_report_detail_tara_weight form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getTaraWeight().'"  tabindex="21"/>
      <label for="op_inspection_report_detail_tara_weight" class="error">'.$opInspectionReportDetail->getAttrError("tara_weight").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_net_weight" class="control-label">Net Weight:</label>
        <input type="text" id="op_inspection_report_detail_net_weight" name="op_inspection_report_detail_net_weight" class="op_inspection_report_detail_net_weight form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getNetWeight().'"  tabindex="22"/>
      <label for="op_inspection_report_detail_net_weight" class="error">'.$opInspectionReportDetail->getAttrError("net_weight").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_aggregate" class="control-label">Aggregate:</label>
        <input type="text" id="op_inspection_report_detail_aggregate" name="op_inspection_report_detail_aggregate" class="op_inspection_report_detail_aggregate form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getAggregate().'"  tabindex="23"/>
      <label for="op_inspection_report_detail_aggregate" class="error">'.$opInspectionReportDetail->getAttrError("aggregate").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_ROB" class="control-label">ROB:</label>
        <input type="text" id="op_inspection_report_detail_ROB" name="op_inspection_report_detail_ROB" class="op_inspection_report_detail_ROB form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getROB().'"  tabindex="24"/>
      <label for="op_inspection_report_detail_ROB" class="error">'.$opInspectionReportDetail->getAttrError("ROB").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_status_2" class="control-label">Status 2:</label>
        <input type="text" id="op_inspection_report_detail_status_2" name="op_inspection_report_detail_status_2" class="op_inspection_report_detail_status_2 form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getStatus2().'"  tabindex="25"/>
      <label for="op_inspection_report_detail_status_2" class="error">'.$opInspectionReportDetail->getAttrError("status_2").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_id_travel_ocamar" class="control-label">Id Travel Ocamar:</label>
        <input type="text" id="op_inspection_report_detail_id_travel_ocamar" name="op_inspection_report_detail_id_travel_ocamar" class="op_inspection_report_detail_id_travel_ocamar form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getIdTravelOcamar().'"  tabindex="26"/>
      <label for="op_inspection_report_detail_id_travel_ocamar" class="error">'.$opInspectionReportDetail->getAttrError("id_travel_ocamar").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_departure_date" class="control-label">Departure Date:</label>
        <input type="text" id="op_inspection_report_detail_departure_date" name="op_inspection_report_detail_departure_date" class="op_inspection_report_detail_departure_date form-control"  maxlength="20"  value="'.$opInspectionReportDetail->getDepartureDate().'"  tabindex="27"/>
      <label for="op_inspection_report_detail_departure_date" class="error">'.$opInspectionReportDetail->getAttrError("departure_date").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_departure_time" class="control-label">Departure Time:</label>
        <input type="text" id="op_inspection_report_detail_departure_time" name="op_inspection_report_detail_departure_time" class="op_inspection_report_detail_departure_time form-control"  maxlength="5"  value="'.$opInspectionReportDetail->getDepartureTime().'"  tabindex="28"/>
      <label for="op_inspection_report_detail_departure_time" class="error">'.$opInspectionReportDetail->getAttrError("departure_time").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_seal" class="control-label">Seal:</label>
        <input type="text" id="op_inspection_report_detail_seal" name="op_inspection_report_detail_seal" class="op_inspection_report_detail_seal form-control"  maxlength="45"  value="'.$opInspectionReportDetail->getSeal().'"  tabindex="29"/>
      <label for="op_inspection_report_detail_seal" class="error">'.$opInspectionReportDetail->getAttrError("seal").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_observation" class="control-label">Observation:</label>
        <textarea id="op_inspection_report_detail_observation" name="op_inspection_report_detail_observation" class="op_inspection_report_detail_observation form-control" rows="3" >'.$opInspectionReportDetail->getObservation().'</textarea>
      <label for="op_inspection_report_detail_observation" class="error">'.$opInspectionReportDetail->getAttrError("observation").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_inspection_report_detail_active" class="control-label">Active:</label>
        <select  id="op_inspection_report_detail_active" name="op_inspection_report_detail_active" class="op_inspection_report_detail_active form-control" tabindex="31">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($opInspectionReportDetail->getActive(),'Y').
'      </select>
      <label for="op_inspection_report_detail_active" class="error">'.$opInspectionReportDetail->getAttrError("active").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function opInspectionReportDetailRules(){
  $("#op_inspection_report_detail_form").validate();
  $("#op_inspection_report_detail_row").rules("add", {
    number:true,
    maxlength:22
  });
  $("#op_inspection_report_detail_hash").rules("add", {
    maxlength:4500
  });
  $("#op_inspection_report_detail_id_travel").rules("add", {
    maxlength:3
  });
  $("#op_inspection_report_detail_SICA_guide").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_id_list").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_transport").rules("add", {
    maxlength:450
  });
  $("#op_inspection_report_detail_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#op_inspection_report_detail_nin").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_chuto").rules("add", {
    maxlength:15
  });
  $("#op_inspection_report_detail_id_destination").rules("add", {
    number:true,
    maxlength:22
  });
  $("#op_inspection_report_detail_BL").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_admission_date").rules("add", {
    maxlength:20
  });
  $("#op_inspection_report_detail_admission_time").rules("add", {
    maxlength:5
  });
  $("#op_inspection_report_detail_tara_number").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_status_1").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_operation_day").rules("add", {
    maxlength:5
  });
  $("#op_inspection_report_detail_survey_guide").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_ocamar_guide").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_store").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_gross_weight").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_tara_weight").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_net_weight").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_aggregate").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_ROB").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_status_2").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_id_travel_ocamar").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_departure_date").rules("add", {
    maxlength:20
  });
  $("#op_inspection_report_detail_departure_time").rules("add", {
    maxlength:5
  });
  $("#op_inspection_report_detail_seal").rules("add", {
    maxlength:45
  });
  $("#op_inspection_report_detail_observation").rules("add", {
    maxlength:1000
  });
  $("#op_inspection_report_detail_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  opInspectionReportDetailRules();


})
</script>