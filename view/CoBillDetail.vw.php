<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_bill_detail_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBillDetail" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coBillDetail->getCreatedBy()).'  el '.$coBillDetail->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_bill_detail_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBillDetail" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_bill_detail_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBillDetail" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coBillDetail->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coBillDetail->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_bill_detail_form" name="co_bill_detail_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Co Bill Detail</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_bill_detail" name="is_co_bill_detail" value="'.$coBillDetail->getInitialState().'">
         <input type="hidden" id="co_bill_detail_id" name="co_bill_detail_id" maxlength="22" value="'.$coBillDetail->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="co_bill_detail_id_bill" class="control-label">Id Bill:</label>
        <select  id="co_bill_detail_id_bill" name="co_bill_detail_id_bill" class="co_bill_detail_id_bill form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CoBill::selectOptions($tfs),$coBillDetail->getIdBill()).
'      </select>
      <label for="co_bill_detail_id_bill" class="error">'.$coBillDetail->getAttrError("id_bill").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_bill_detail_id_account" class="control-label">Id Account:</label>
        <select  id="co_bill_detail_id_account" name="co_bill_detail_id_account" class="co_bill_detail_id_account form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CoAccount::selectOptions($tfs),$coBillDetail->getIdAccount()).
'      </select>
      <label for="co_bill_detail_id_account" class="error">'.$coBillDetail->getAttrError("id_account").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_bill_detail_amount" class="control-label">Amount:</label>
        <input type="text" id="co_bill_detail_amount" name="co_bill_detail_amount" class="co_bill_detail_amount form-control"  maxlength="22"  value="'.$coBillDetail->getAmount().'"  tabindex="3"/>
      <label for="co_bill_detail_amount" class="error">'.$coBillDetail->getAttrError("amount").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
  function coBillDetailRules(){

  $("#co_bill_detail_form").validate();
  $("#co_bill_detail_id_bill").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_bill_detail_id_account").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_bill_detail_amount").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}


$(document).ready(function(){
  coBillDetailRules();
  $('select').niceSelect();

})
</script>