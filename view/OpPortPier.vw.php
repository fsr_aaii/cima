<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#op_port_pier_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="opPortPier" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$opPortPier->getCreatedBy()).'  el '.$opPortPier->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#op_port_pier_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="opPortPier" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#op_port_pier_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="opPortPier" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($opPortPier->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($opPortPier->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="op_port_pier_form" name="op_port_pier_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Puerto - Muelle</div>
      <div class="col-lg-6 container ">'.$audit.'</div>
      <div class="col-lg-12 container mt-5 ">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_op_port_pier" name="is_op_port_pier" value="'.$opPortPier->getInitialState().'">
         <input type="hidden" id="op_port_pier_id" name="op_port_pier_id" maxlength="22" value="'.$opPortPier->getId().'">

      </div>
      <div class="col-lg-10 container">
       <label for="op_port_pier_name" class="control-label">Nombre:</label>
        <input type="text" id="op_port_pier_name" name="op_port_pier_name" class="op_port_pier_name form-control"  maxlength="200"  value="'.$opPortPier->getName().'"  tabindex="1"/>
      <label for="op_port_pier_name" class="error">'.$opPortPier->getAttrError("name").'</label>
      </div>
      <div class="col-lg-2 container">
       <label for="op_port_pier_active" class="control-label">Habilitado:</label>
        <select  id="op_port_pier_active" name="op_port_pier_active" class="op_port_pier_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($opPortPier->getActive(),'Y').
'      </select>
      <label for="op_port_pier_active" class="error">'.$opPortPier->getAttrError("active").'</label>
      </div>

   <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function opPortPierRules(){
  $("#op_port_pier_form").validate();
  $("#op_port_pier_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#op_port_pier_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  opPortPierRules();


})
</script>