<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_bill_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBill" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coBill->getCreatedBy()).'  el '.$coBill->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_bill_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBill" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_bill_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBill" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coBill->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coBill->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_bill_form" name="co_bill_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Co Bill</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_bill" name="is_co_bill" value="'.$coBill->getInitialState().'">
         <input type="hidden" id="co_bill_id" name="co_bill_id" maxlength="22" value="'.$coBill->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="co_bill_id_accounting" class="control-label">Id Accounting:</label>
        <select  id="co_bill_id_accounting" name="co_bill_id_accounting" class="co_bill_id_accounting form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CoAccounting::selectOptions($tfs),$coBill->getIdAccounting()).
'      </select>
      <label for="co_bill_id_accounting" class="error">'.$coBill->getAttrError("id_accounting").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_bill_id_unit" class="control-label">Id Unit:</label>
        <select  id="co_bill_id_unit" name="co_bill_id_unit" class="co_bill_id_unit form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CoUnit::selectOptions($tfs),$coBill->getIdUnit()).
'      </select>
      <label for="co_bill_id_unit" class="error">'.$coBill->getAttrError("id_unit").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_bill_aliquot" class="control-label">Aliquot:</label>
        <input type="text" id="co_bill_aliquot" name="co_bill_aliquot" class="co_bill_aliquot form-control"  maxlength="22"  value="'.$coBill->getAliquot().'"  tabindex="3"/>
      <label for="co_bill_aliquot" class="error">'.$coBill->getAttrError("aliquot").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_bill_paymented_by" class="control-label">Paymented By:</label>
        <input type="text" id="co_bill_paymented_by" name="co_bill_paymented_by" class="co_bill_paymented_by form-control"  maxlength="22"  value="'.$coBill->getPaymentedBy().'"  tabindex="4"/>
      <label for="co_bill_paymented_by" class="error">'.$coBill->getAttrError("paymented_by").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_bill_paymented_date" class="control-label">Paymented Date:</label>
        <input type="text" id="co_bill_paymented_date" name="co_bill_paymented_date" class="co_bill_paymented_date form-control"  maxlength="22"  value="'.$coBill->getPaymentedDate().'"  tabindex="5"/>
      <label for="co_bill_paymented_date" class="error">'.$coBill->getAttrError("paymented_date").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
  function coBillRules(){

  $("#co_bill_form").validate();
  $("#co_bill_id_accounting").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_bill_id_unit").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_bill_aliquot").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_bill_paymented_by").rules("add", {
    number:true,
    maxlength:22
  });
  $("#co_bill_paymented_date").rules("add", {
    maxlength:22
  });

}


$(document).ready(function(){
  coBillRules();
  $('select').niceSelect();

})
</script>