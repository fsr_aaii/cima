<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#gl_person_banking_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonBanking" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$glPersonBanking->getCreatedBy()).'  el '.$glPersonBanking->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#gl_person_banking_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonBanking" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#gl_person_banking_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonBanking" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($glPersonBanking->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($glPersonBanking->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-5 form-frame shadow mb-4">
    <form id="gl_person_banking_form" name="gl_person_banking_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Gl Person Banking</div>
     <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_gl_person_banking" name="is_gl_person_banking" value="'.$glPersonBanking->getInitialState().'">
         <input type="hidden" id="gl_person_banking_id" name="gl_person_banking_id" maxlength="22" value="'.$glPersonBanking->getId().'">
         <input type="hidden" id="gl_person_banking_id_person" name="gl_person_banking_id_person" maxlength="22" value="'.$glPersonBanking->getIdPerson().'">

      </div>
      <div class="col-lg-12 container">
       <label for="gl_person_banking_bank" class="control-label">Banco:</label>
        <input type="text" id="gl_person_banking_bank" name="gl_person_banking_bank" class="gl_person_banking_bank form-control"  maxlength="200"  value="'.$glPersonBanking->getBank().'"  tabindex="2"/>
      <label for="gl_person_banking_bank" class="error">'.$glPersonBanking->getAttrError("bank").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_person_banking_account" class="control-label">Cta:</label>
        <input type="text" id="gl_person_banking_account" name="gl_person_banking_account" class="gl_person_banking_account form-control"  maxlength="200"  value="'.$glPersonBanking->getAccount().'"  tabindex="3"/>
      <label for="gl_person_banking_account" class="error">'.$glPersonBanking->getAttrError("account").'</label>
      </div>
  
      <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function glPersonBankingRules(){
  $("#gl_person_banking_form").validate();
  $("#gl_person_banking_id_person").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#gl_person_banking_bank").rules("add", {
    required:true,
    maxlength:200
  });
  $("#gl_person_banking_account").rules("add", {
    required:true,
    maxlength:200
  });

}


$(document).ready(function(){
  glPersonBankingRules();


})
</script>