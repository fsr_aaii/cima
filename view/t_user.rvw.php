<?php
  if (!is_object($tus)){
    header("Location: ../403.html");
    exit();
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8">
            <div class="col-6 title '.$tus->getSessionTaskTypeStyle().'">'.t_role::description($tus,$request->get("t_role_id")).' users</div>
             <div class="col-6 text-right action">
             </div>
             <input type="hidden" id="SessionURLBase" name="SessionURLBase" value="'.$SessionURLBase.'">
       <table id="t_user_rep_table" class="display responsive" style="width:100%">
         <thead>
           <tr>
            <th class="all">Name</th>
            <th class="all">Login</th>
            <th class="all">Audit?</th>
            <th class="none">Email</th>
            <th class="none">Last Password</th>
            <th class="none">Created by</th>
            <th class="none">Created date</th>
           </tr>
         </thead>
         <tbody>';
   foreach ($t_user_list as $row){
    $html.='   <tr>
                 <td>'.$row["name"].'</td>
                 <td>'.$row["login"].'</td>
                 <td>'.$row["audit"].'</td>
                 <td>'.$row["email_account"].'</td>
                 <td>'.$row["password_date"].'</td>
                 <td>'.$row["created_by"].'</td>
                 <td>'.$row["created_date"].'</td>
              </tr>';
   }
 $html.='  </tbody>
           </table>
          </div>
         </div>';
 echo $html;
?>
 <script type="text/javascript" src="../javascript/t_user.rep.js"></script>

