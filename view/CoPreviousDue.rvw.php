<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }
  $tfData["co_previous_due_id_person"] = $tfRequest->co_previous_due_id_person;
  $html='<div class="row">
           <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-6 container title">Deuda Previa</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPreviousDue" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
               <a class="btn-guaramo-text" data-tf-table="#co_previous_due_dt" data-tf-file="Co Previous Due" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>
       <table id="co_previous_due_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Unidad</th>
             <th class="all">Fecha</th>
             <th class="all">Pagado el</th>
             <th class="all">Monto</th>
             <th class="all text-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($coPreviousDueList as $row){
    $tfData["co_previous_due_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["unit"].'</td>
            <td>'.$row["payment_date"].'</td>
            <td>'.$row["paymented_date"].'</td>
            <td>'.$row["amount"].'</td>';
    if ($row["paymented_date"]!=''){
      $html.='<td class="text-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPreviousDue" data-tf-action="AC" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Ver
                   </a>
                 </td>'; 
    }else{
      $html.='<td class="text-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPreviousDue" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
    }
    $html.='</tr>';             
   }
   $html.='</tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#co_previous_due_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>