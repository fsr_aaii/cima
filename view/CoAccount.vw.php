
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_account_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccount" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coAccount->getCreatedBy()).'  el '.$coAccount->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_account_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccount" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_account_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccount" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }

  $seat = array();
  $seat[0] = array('value'=>'D','option'=>'Debito');
  $seat[1] = array('value'=>'C','option'=>'Credito');

  foreach ($coAccount->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coAccount->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-6 form-frame shadow mb-4">
    <form id="co_account_form" name="co_account_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Concepto</div>
      <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>     
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_account" name="is_co_account" value="'.$coAccount->getInitialState().'">
         <input type="hidden" id="co_account_id" name="co_account_id" maxlength="22" value="'.$coAccount->getId().'">
      </div>
      <div class="col-lg-12 container">
       <label for="co_account_name" class="control-label">Nombre:</label>
        <input type="text" id="co_account_name" name="co_account_name" class="co_account_name form-control"  maxlength="200"  value="'.$coAccount->getName().'"  tabindex="1"/>
      <label for="co_account_name" class="error">'.$coAccount->getAttrError("name").'</label>
      </div>
       <div class="col-lg-9 container">
       <label for="co_account_id_account_group" class="control-label">Grupo:</label>
        <select  id="co_account_id_account_group" name="co_account_id_account_group" class="co_account_id_account_group form-control" tabindex="2">
      <option value="">Selecciona</option>'.
      TfWidget::selectStructure(CoAccountGroup::selectOptions($tfs),$coAccount->getIdAccountGroup()).
'      </select>
      <label for="co_account_id_account_group" class="error">'.$coAccount->getAttrError("id_account_group").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="co_account_seat" class="control-label">Asiento:</label>
        <select  id="co_account_seat" name="co_account_seat" class="co_account_seat form-control" tabindex="3">
      <option value="">Selecciona</option>'.
      TfWidget::selectStructure($seat,$coAccount->getSeat()).
'      </select>
<label for="co_account_seat" class="error">'.$coAccount->getAttrError("seat").'</label>
      </div>
   <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">

  function coAccountRules(){
    $('select').niceSelect();
  $("#co_account_form").validate();
  $("#co_account_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#co_account_seat").rules("add", {
    required:true,
    maxlength:200
  });

}


$(document).ready(function(){
  coAccountRules();


})
</script>