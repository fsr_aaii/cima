<style type="text/css">
  .form-control.group:focus {
   background-color: #e9ecef !important;
  border: 1px solid #ced4da !important;
  border-right: none !important;
}
.input-group-text{
  font-size: .85rem;
}
</style>
<?php
  if (!is_object($tus)){
    header("Location: ../403.html");
    exit();
  }

  if (isset($t_user_alert)){
    $objAlerts=tuich_widget::alertDangerTemplate($t_user_alert);
  }
 

  
  $html = '<div class="row">
  <div class="mx-auto col-lg-5 form-frame">
    <div class="col-lg-12 title '.$tus->getSessionTaskTypeStyle().'">User by Role</div>
    <div class="col-12 information">
       Please select the role you want to list
             </div> 
    <label for="" class="error"></label>         
    <form id="t_user_form" name="t_user_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container">
        '.$objAlerts.'
      </div>
     
  <div class="col-lg-12 container">
   <label for="t_user_email_account" class="control-label">Role:</label>
     <div class="input-group">

       <select  id="t_role_id" name="t_role_id" class="t_role_id form-control group" tabindex="3">
       <option value="">Select a option</option>'.
        tuich_widget::selectStructure(t_role::selectOptions($tus),"").
     '</select>

       <span class="input-group-btn">
         <button id="t_role_id_find" type="button" class="btn btn-secondary" onclick="tuich_validate(\'#t_user_form\')?tuich_router(\''.$SessionURLBase.'t_user/AL\',\'#t_user_form\'):false;"><span class="far fa-search"></span></button>
       </span>
     </div>
     <label for="t_role_id" class="error"></label>
   </div>
   
 
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript" src="../javascript/t_user_ar.js"></script>
