<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#hr_user_evaluation_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="hrUserEvaluation" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$hrUserEvaluation->getCreatedBy()).'  el '.$hrUserEvaluation->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#hr_user_evaluation_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="hrUserEvaluation" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#hr_user_evaluation_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="hrUserEvaluation" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrUserEvaluation->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrUserEvaluation->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="hr_user_evaluation_form" name="hr_user_evaluation_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr User Evaluation</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_user_evaluation" name="is_hr_user_evaluation" value="'.$hrUserEvaluation->getInitialState().'">
         <input type="hidden" id="hr_user_evaluation_id" name="hr_user_evaluation_id" maxlength="22" value="'.$hrUserEvaluation->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_user_evaluation_id_user" class="control-label">Id User:</label>
        <input type="text" id="hr_user_evaluation_id_user" name="hr_user_evaluation_id_user" class="hr_user_evaluation_id_user form-control"  maxlength="22"  value="'.$hrUserEvaluation->getIdUser().'"  tabindex="1"/>
      <label for="hr_user_evaluation_id_user" class="error">'.$hrUserEvaluation->getAttrError("id_user").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_user_evaluation_date" class="control-label">Date:</label>
        <input type="text" id="hr_user_evaluation_date" name="hr_user_evaluation_date" class="hr_user_evaluation_date form-control"  maxlength="22"  value="'.$hrUserEvaluation->getDate().'"  tabindex="2"/>
      <label for="hr_user_evaluation_date" class="error">'.$hrUserEvaluation->getAttrError("date").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_user_evaluation_active" class="control-label">Active:</label>
        <select  id="hr_user_evaluation_active" name="hr_user_evaluation_active" class="hr_user_evaluation_active form-control" tabindex="3">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($hrUserEvaluation->getActive(),'Y').
'      </select>
      <label for="hr_user_evaluation_active" class="error">'.$hrUserEvaluation->getAttrError("active").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_user_evaluation_id_evaluation_type" class="control-label">Id Evaluation Type:</label>
        <select  id="hr_user_evaluation_id_evaluation_type" name="hr_user_evaluation_id_evaluation_type" class="hr_user_evaluation_id_evaluation_type form-control" tabindex="4">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrEvaluationType::selectOptions($tfs),$hrUserEvaluation->getIdEvaluationType()).
'      </select>
      <label for="hr_user_evaluation_id_evaluation_type" class="error">'.$hrUserEvaluation->getAttrError("id_evaluation_type").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_user_evaluation_evaluator_type" class="control-label">Evaluator Type:</label>
        <input type="text" id="hr_user_evaluation_evaluator_type" name="hr_user_evaluation_evaluator_type" class="hr_user_evaluation_evaluator_type form-control"  maxlength="1"  value="'.$hrUserEvaluation->getEvaluatorType().'"  tabindex="5"/>
      <label for="hr_user_evaluation_evaluator_type" class="error">'.$hrUserEvaluation->getAttrError("evaluator_type").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrUserEvaluationRules(){
  $("#hr_user_evaluation_form").validate();
  $("#hr_user_evaluation_id_user").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_user_evaluation_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#hr_user_evaluation_active").rules("add", {
    required:true,
    maxlength:1
  });
  $("#hr_user_evaluation_id_evaluation_type").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_user_evaluation_evaluator_type").rules("add", {
    required:true,
    maxlength:1
  });

}

$("#hr_user_evaluation_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  hrUserEvaluationRules();

  $("#hr_user_evaluation_date").css('vertical-align','top');
  $("#hr_user_evaluation_date").mask('y999-m9-d9');

})
</script>