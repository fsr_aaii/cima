<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn" role="button" data-tf-form="#mm_packeting_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmPacketing" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$mmPacketing->getCreatedBy()).'  on '.$mmPacketing->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn" role="button" data-tf-form="#mm_packeting_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmPacketing" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn" role="button" data-tf-form="#mm_packeting_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmPacketing" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($mmPacketing->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($mmPacketing->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="mm_packeting_form" name="mm_packeting_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title mb-5">Mm Packeting</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_mm_packeting" name="is_mm_packeting" value="'.$mmPacketing->getInitialState().'">
         <input type="hidden" id="mm_packeting_id" name="mm_packeting_id" maxlength="22" value="'.$mmPacketing->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="mm_packeting_name" class="control-label">Name:</label>
        <input type="text" id="mm_packeting_name" name="mm_packeting_name" class="mm_packeting_name form-control"  maxlength="200"  value="'.$mmPacketing->getName().'"  tabindex="1"/>
      <label for="mm_packeting_name" class="error">'.$mmPacketing->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="mm_packeting_active" class="control-label">Active:</label>
        <select  id="mm_packeting_active" name="mm_packeting_active" class="mm_packeting_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($mmPacketing->getActive(),'Y').
'      </select>
      <label for="mm_packeting_active" class="error">'.$mmPacketing->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function mmPacketingRules(){
  $("#mm_packeting_form").validate();
  $("#mm_packeting_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#mm_packeting_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  mmPacketingRules();


})
</script>