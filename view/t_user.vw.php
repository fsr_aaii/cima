<?php
  if (!is_object($tus)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tuichSessionAction){
   case "AN":
   case "AI":
    $buttons ='<a href="#" class="btn btn-success" onclick="jConfirm(\'Are you sure you want to save this information?\',\'Confirmation to save\',
                  function(r){if(r){tuich_validate(\'#t_user_form\')?tuich_router(\''.$SessionURLBase.'t_user/AI\',\'#t_user_form\'):false;}});"><span class="far fa-save"></span> Save</a>';
    break;
      case "AE":
   case "AB":
   case "AA":
   case "AAU":
   case "AIU":
   case "ANU":
   $audit='<div class="col-lg-12 container">
     <span class="create_by">Created by '.t_user::description($tus,$t_user->getCreated_By()).' on '.$t_user->getCreated_Date().'. Last password change on '.$t_user->getPassword_Date().'</span>
    </div>';

    if ($t_user->getActive()=="Y"){
          $disabled='';
          $status='<label for="" class="control-label" style="padding-top:16px;"><span class="far fa-user-check"></span>Active</label>';
          $buttons.='<a href="#" class="btn btn-danger margin-filter" onclick="javascript:tuich_router(\''.$SessionURLBase.'t_user/AIU\',\''.tuich_request::encode('{"t_user_id":"'.$t_user->getId().'"}').'\');"><span class="far fa-user-times"></span> Inactivate</a>';
          $buttons.='<a href="#" class="btn btn-info margin-filter" onclick="javascript:tuich_router(\''.$SessionURLBase.'t_user/ANU\',\''.tuich_request::encode('{"t_user_id":"'.$t_user->getId().'"}').'\');"><span class=" far fa-bell"></span> Notify</a>';
        }else{
          $disabled='disabled="disabled"';
          $status='<label for="" class="control-label"  style="padding-top:16px;"><span class="far fa-user-times"></span>Inactive</label>';
          $buttons.='<a href="#" class="btn btn-success margin-filter" onclick="javascript:tuich_router(\''.$SessionURLBase.'t_user/AAU\',\''.tuich_request::encode('{"t_user_id":"'.$t_user->getId().'"}').'\');"><span class="far fa-user-check"></span> Activate</a>';
        } 
    $buttons.='<a href="#" class="btn btn-success" onclick="jConfirm(\'Are you sure you want to update this information?\',\'Confirmation to update\',
                  function(r){if(r){tuich_validate(\'#t_user_form\')?tuich_router(\''.$SessionURLBase.'t_user/AA\',\'#t_user_form\'):false;}});"><span class="far fa-save"></span> Save</a>';
    $buttons.='<a href="#" class="btn btn-danger" onclick="jConfirm(\'Are you sure you want to delete this information?\',\'Confirmation to delete\',
                  function(r){if(r){tuich_validate(\'#t_user_form\')?tuich_router(\''.$SessionURLBase.'t_user/AB\',\'#t_user_form\'):false;}});"><span class="far fa-trash-alt"></span> Delete</a>';              
    break;
  }
  foreach ($t_user->getObjError() as $oe) {
    $objAlerts.=tuich_widget::alertDangerTemplate($oe);
  }
  foreach ($t_user->getObjMsg() as $om) {
    $objAlerts.=tuich_widget::alertSuccessTemplate($om);
  }

 if ($shortInfo["photo"]=='' or $shortInfo["photo"]=='..'){
    $photo='../asset/images/blank-user.jpg';
  }else{
    $photo=$shortInfo["photo"];
  }

  if($t_user->getAudit()=="Y"){
    $t_user_audit_check="checked";
  }else{
    $t_user_audit_check="";
  }

  $html = '<div class="row">
  <div class="mx-auto col-lg-8 form-frame">
    <div class="col-lg-12 title '.$tus->getSessionTaskTypeStyle().'">'.$viewTitle.'</div>
    <form id="t_user_form" name="t_user_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container">
        '.$objAlerts.'
      </div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_t_user" name="is_t_user" value="'.$t_user->getInitialState().'">
         <input type="hidden" id="SessionURLBase" name="SessionURLBase" value="'.$SessionURLBase.'">
         <input type="hidden" id="t_user_id" name="t_user_id" class="t_user_id" maxlength="22" value="'.$t_user->getId().'">
      </div>


       <div class="col-lg-12 col-container">
   <div class="col-lg-3 col-container">
     <div class="col-md-12 container">
    <div class="form-group">
        <img class="photo-profile" src="'.$photo.'"/>
    </div>
</div>
   </div>
   <div class="col-lg-9 col-container">
   
      <div class="col-10 container">
      <span class="profile-name">'.$shortInfo["name"].'</span>
      </div>
      <div class="col-2 container text-right">
       '.$status.'
      </div>

  <div class="col-12 container">
   <span class="profile-business-title">'.$shortInfo["business_title"].'</span>
  </div>
  <div class="col-8 container profile">
  <label for=""  class="control-label">Cost Center:</label>
   <span class="">'.$shortInfo["cost_center"].'</span>
  </div>
  <div class="col-4 container profile text-right">
  <label for=""  class="control-label">Direct Employees:</label>
   <span class="">'.$shortInfo["direct_employees"].'</span>
  </div>
  <div class="col-12 container profile">
  <label for=""  class="control-label">Mail:</label>
   <span class="">'.$t_user->getEmail_account().'</span>
  </div>
  <div class="col-lg-5 container profile">
   <label for=""  class="control-label">Hire Date:</label>
   <span class="">'.$shortInfo["hire_date"].'</span>
  </div>
  <div class="col-lg-5 container profile">
  <label for=""  class="control-label">Termination Date:</label>
   <span class="">'.$shortInfo["termination_date"].'</span>
  </div>
   <div class="col-lg-2 container profile text-right">
  <label for=""  class="control-label">'.$shortInfo["level"].'</label>
  </div>

  </div>
</div>
  
    '.$audit.'
   </fieldset>
  </form>
  <form  method="post" onsubmit="return false" class="form-horizontal info-panel detail">
    <fieldset>';
    
   /* $html.='<div class="col-lg-3 container mb-2 mt-2"><input type="checkbox" id="t_user_audit" name="t_user_audit" value="Y" '.$t_user_audit_check.'> User Audit</div>
    <div class="col-12 information">If the user audit is checked, the system will control the activation and deactivation of the user; otherwise,only the administrator could enable or disable this user.</div>';*/

  if (is_array($permission)){
    $html.='<div id="msg" class="col-lg-12 container"></div>';
 
    $id_functional_area=""; 
     
    foreach ($permission as $row){
       if ($id_functional_area!=$row["id_functional_area"]){
          $id_functional_area=$row["id_functional_area"];
          $html= str_replace("{%ROLES%}",$html_roles,$html);
          $html_roles="";
          $html.='<div  class="col-lg-12 container">
                   <div class="title-column">'.t_functional_area::description($tus,$id_functional_area).'</div>
                    {%ROLES%}
                  </div>';    
       }
       if($row["active"]=="Y"){
          $check="checked";
       }else{
          $check="";
       }
       $html_roles.='<div class="col-lg-3 container"><input type="checkbox" value="'.$row["id_functional_area_role"].'" disabled '.$check.'> '.$row["name"].'</div>';
       /*if (in_array($row["id_role"],array(19,21))){
         $html_roles.='<div class="col-lg-3 container"><input type="checkbox" value="'.$row["id_functional_area_role"].'" disabled '.$check.'> '.$row["name"].'</div>';
       }else{
         $html_roles.='<div class="col-lg-3 container"><input type="checkbox" class="user-role-cb" name="check_list[]" value="'.$row["id_functional_area_role"].'" '.$disabled.' '.$check.'> '.$row["name"].'</div>';
       }  */
     
    }  
    $html= str_replace("{%ROLES%}",$html_roles,$html);
  } 
  $html.='   <div class="col-lg-12">&nbsp</div>
                </fieldset>
              </form>      
              </div>
            </div>';
  echo $html;
?>
