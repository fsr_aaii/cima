<style type="text/css">
  .table.clean td, .table.clean th {
    border: none;
}
</style>
<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8 info-panel">
            <div class="col-6 container title">Mi Guia</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-table="#hr_user_homework_criteria_dt" data-tf-file="Hr User Homework Criteria" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>
       <table id="hr_user_homework_criteria_dt" class="table clean display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all text-center">#</th>
             <th class="all">Preguntas</th>
           </tr>
         </thead>
         <tbody>';
   $i=0;      
   foreach ($hrUserHomeworkCriteriaList as $row){
    $i++;
    $html.='   <tr>
           <td class="text-right pr-2">'.$i.'.-</td>
            <td>'.$row["evaluation_criteria"].'</td>
            </tr>';  
   }
   $html.='</tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
