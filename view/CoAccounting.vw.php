<style type="text/css">
  #co_accounting_dt_wrapper{
    padding: 0;
  }
</style>

<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_accounting_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccounting" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coAccounting->getCreatedBy()).'  el '.$coAccounting->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_accounting_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccounting" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_accounting_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccounting" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coAccounting->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coAccounting->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="co_accounting_form" name="co_accounting_form" method="post" onsubmit="return false" class="form-horizontal info-panel p-0">
      <fieldset>
      <div class="col-lg-12 container title">Aviso de Cobro</div>
      <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_accounting" name="is_co_accounting" value="'.$coAccounting->getInitialState().'">
         <input type="hidden" id="co_accounting_id" name="co_accounting_id" maxlength="22" value="'.$coAccounting->getId().'">
         <input type="hidden" id="co_accounting_id_person" name="co_accounting_id_person" maxlength="22" value="'.$coAccounting->getIdPerson().'">
      </div>
      <div class="col-lg-12 container">
       <label for="co_accounting_payment_date" class="control-label">Fecha de Aviso de Cobro:</label>
        <input type="text" id="co_accounting_payment_date" name="co_accounting_payment_date" class="co_accounting_payment_date form-control"  maxlength="22"  value="'.$coAccounting->getPaymentDate().'"  tabindex="2"/>
      <label for="co_accounting_payment_date" class="error">'.$coAccounting->getAttrError("payment_date").'</label>
      </div>
     
   <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>';

if ($coAccounting->getId()!=''){

$tfNewData["co_accounting_detail_id_accounting"] = $coAccounting->getId();
$tfNewData["co_accounting_unit_id_accounting"] = $coAccounting->getId();

  $html.='<div class="row">
           <div class="mx-auto col-lg-12 p-0">
            <div class="col-6 title p-0">Gastos</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccountingDetail" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt($tfNewData).'" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
             </div>
           </div>  ';
   foreach ($coAccountingDetailList as $row){

    $tfData["co_accounting_detail_id"] = $row["id"];
    $html.='  <div class="col-lg-12 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccountingDetail" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
            <div class="d-flex align-items-center card-01 p-4" >
                <div class="col-12 p-0 ml-2">
                    <div class="epic col-12 g-text-1 text-white p-0 m-0">'.$row["account"].'</div>
                    <div class="epic col-5 c-text-4 text-white p-0 m-0">'.$row["description"].'</div>
                    <div class="epic col-4 c-text-4 text-white p-0 m-0 text-right">'.TfWidget::amount($row["amount"]).' VEB</div>
                    <div class="epic col-3 c-text-4 text-white p-0 m-0 text-right">'.TfWidget::amount(round($row["amount"]/$coExchangeRate,2)).' USD</div>
                </div>
            </div>
          </div>';  
   }
   $html.='</div>
      <div class="row">
           <div class="mx-auto col-lg-12 p-0">
            <div class="col-6 title p-0">Gastos Individuales</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccountingUnit" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt($tfNewData).'" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
             </div>
           </div>  ';
   foreach ($coAccountingUnitList as $row){

    $tfData["co_accounting_unit_id"] = $row["id"];
    $html.='  <div class="col-lg-12 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccountingUnit" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
            <div class="d-flex align-items-center card-01 p-4" >
                <div class="col-12 p-0 ml-2">
                   <div class="epic col-12 g-text-1 text-white p-0 m-0">'.$row["unit"].': '.$row["account"].'</div>
                    <div class="epic col-5 c-text-4 text-white p-0 m-0">'.$row["description"].'</div>
                    <div class="epic col-4 c-text-4 text-white p-0 m-0 text-right">'.TfWidget::amount($row["amount"]).' VEB</div>
                    <div class="epic col-3 c-text-4 text-white p-0 m-0 text-right">'.TfWidget::amount(round($row["amount"]/$coExchangeRate,2)).' USD</div>
                </div>
            </div>
          </div>';  
   }
   $html.='</div>

  <div class="row">
    <div class="col-lg-12 p-0">
<div class="col-12 container title p-0">Resumen</div>
          <table id="co_accounting_dt" class="display responsive" style="width:100%">
  <thead>
    <tr>
      <th class="all">Concepto</th>
      <th class="all text-right">VEB</th>
      <th class="all text-right">USD</th>
    </tr>
  </thead>
  <tbody>      
   
';

  $total=0;
   foreach ($coAccountingSumary as $row){
    $html.='<tr>
      <td>'.$row["account"].'</td>
      <td class="text-right">'.TfWidget::amount($row["amount"]).'</td>
      <td class="text-right">'.TfWidget::amount(round($row["amount"]/$coExchangeRate,2)).'</td>
    </tr>'; 

     $total+= $row["amount"];
   }
   $html.='</tbody>
   <tfoot><tr>
      <th>Total</th>
      <th class="text-right">'.TfWidget::amount($total).'</th>
      <th class="text-right">'.TfWidget::amount(round($total/$coExchangeRate,2)).'</th>
    </tr></tfoot>
</table>
</div>
     <div class="col-lg-12 container mb-5 mt-2 text-right p-0">';


 $coPaymentQty = CoPayment::qty2ApproveByCondominium($tfs,$coAccounting->getIdPerson()); 
 if ($coPaymentQty>0){
  $html.='      <div class="jumbotron text-light bg-dark">
  <h1 class="display-4">Informaci&oacute;n</h1>
  <p class="lead">No podr&aacute; generar procesar estos gastos y por consiguiente los avisos por cobrar a los copropietarios hasta que procese todos los pagos pendientes de este condominio.</p>
  <hr class="my-4">
  <p>Por favor procese los '.$coPaymentQty.' pagos pendientes.</p>

</div>';
 }else{

   if ($coExchangeRate!='') {
        $html.='      <a class="btn-guaramo-text" data-tf-form="#co_accounting_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccounting" data-tf-action="GR" onclick="TfRequest.do(this);">
                       Generar Aviso de Cobro
        </a>';
   }else{
     $html.='      <div class="jumbotron text-light bg-dark">
  <h1 class="display-4">Informaci&oacute;n</h1>
  <p class="lead">No podr&aacute; generar ningun aviso de cobro debido que no ha cargado ninguna tasa cambiaria al condominio.</p>

</div>';
   }

}
$html.='</div>';

}  

$html.='</div>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">

  
  function coAccountingRules(){
  $("#co_accounting_form").validate();
  $("#co_accounting_id_person").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_accounting_payment_date").rules("add", {
    isDate:true,
    maxlength:22
  });

}

$("#co_accounting_payment_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
   $('select').niceSelect();
  coAccountingRules();
  $("#co_accounting_dt").DataTable({
   info:false,
    paging:false,
    lengthChange:false,
    dom: 'rtip'
  });
  $("#co_accounting_payment_date").css('vertical-align','top');
  $("#co_accounting_payment_date").mask('y999-m9-d9');

})
</script>