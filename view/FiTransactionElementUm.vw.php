<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-link" role="button" data-tf-form="#fi_transaction_element_um_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="fiTransactionElementUm" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$fiTransactionElementUm->getCreatedBy()).'  on '.$fiTransactionElementUm->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn-link" role="button" data-tf-form="#fi_transaction_element_um_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="fiTransactionElementUm" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-link" role="button" data-tf-form="#fi_transaction_element_um_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="fiTransactionElementUm" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($fiTransactionElementUm->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($fiTransactionElementUm->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="fi_transaction_element_um_form" name="fi_transaction_element_um_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title xl mb-5">Presentacion</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_fi_transaction_element_um" name="is_fi_transaction_element_um" value="'.$fiTransactionElementUm->getInitialState().'">
         <input type="hidden" id="fi_transaction_element_um_id" name="fi_transaction_element_um_id" maxlength="22" value="'.$fiTransactionElementUm->getId().'">
         <input type="hidden" id="fi_transaction_element_um_id_transaction_element" name="fi_transaction_element_um_id_transaction_element" maxlength="22" value="'.$fiTransactionElementUm->getIdTransactionElement().'">

      </div>

      <div class="col-lg-5 container">
       <label for="fi_transaction_element_um_id_packeting" class="control-label">Tipo:</label>
        <select  id="fi_transaction_element_um_id_packeting" name="fi_transaction_element_um_id_packeting" class="fi_transaction_element_um_id_packeting form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(MmPacketing::selectOptions($tfs),$fiTransactionElementUm->getIdPacketing()).
'      </select>
      <label for="fi_transaction_element_um_id_packeting" class="error">'.$fiTransactionElementUm->getAttrError("id_packeting").'</label>
      </div>
      <div class="col-lg-5 container">
       <label for="fi_transaction_element_um_id_unit_measurement" class="control-label">Unidad de Medida:</label>
        <select  id="fi_transaction_element_um_id_unit_measurement" name="fi_transaction_element_um_id_unit_measurement" class="fi_transaction_element_um_id_unit_measurement form-control" tabindex="3">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(MmUnitMeasurement::selectOptions($tfs),$fiTransactionElementUm->getIdUnitMeasurement()).
'      </select>
      <label for="fi_transaction_element_um_id_unit_measurement" class="error">'.$fiTransactionElementUm->getAttrError("id_unit_measurement").'</label>
      </div>
      <div class="col-lg-2 container">
       <label for="fi_transaction_element_um_qty" class="control-label">Cantidad:</label>
        <input type="number" id="fi_transaction_element_um_qty" name="fi_transaction_element_um_qty" class="fi_transaction_element_um_qty form-control"  maxlength="22" min="1" value="'.$fiTransactionElementUm->getQty().'"  tabindex="4"/>
      <label for="fi_transaction_element_um_qty" class="error">'.$fiTransactionElementUm->getAttrError("qty").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function fiTransactionElementUmRules(){
  $("#fi_transaction_element_um_form").validate();
  $("#fi_transaction_element_um_id_transaction_element").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#fi_transaction_element_um_id_packeting").rules("add", {
    number:true,
    maxlength:22
  });
  $("#fi_transaction_element_um_id_unit_measurement").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#fi_transaction_element_um_qty").rules("add", {
    number:true,
    maxlength:22
  });

}


$(document).ready(function(){
  fiTransactionElementUmRules();


})
</script>