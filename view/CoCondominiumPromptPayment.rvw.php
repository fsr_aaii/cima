<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-6 container title">Co Condominium Prompt Payment</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCondominiumPromptPayment" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
               <a class="btn-guaramo-text" data-tf-table="#co_condominium_prompt_payment_dt" data-tf-file="Co Condominium Prompt Payment" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>
       <table id="co_condominium_prompt_payment_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Id Person</th>
             <th class="all">Days</th>
             <th class="all">Active</th>
             <th class="none">Creado por</th>
             <th class="none">Creado el</th>
             <th class="all text-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($coCondominiumPromptPaymentList as $row){
    $tfData["co_condominium_prompt_payment_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["id_person"].'</td>
            <td>'.$row["days"].'</td>
            <td>'.$row["active"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="text-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCondominiumPromptPayment" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#co_condominium_prompt_payment_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>