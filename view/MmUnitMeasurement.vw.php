<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn" role="button" data-tf-form="#mm_unit_measurement_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmUnitMeasurement" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$mmUnitMeasurement->getCreatedBy()).'  on '.$mmUnitMeasurement->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn" role="button" data-tf-form="#mm_unit_measurement_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmUnitMeasurement" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn" role="button" data-tf-form="#mm_unit_measurement_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmUnitMeasurement" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($mmUnitMeasurement->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($mmUnitMeasurement->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="mm_unit_measurement_form" name="mm_unit_measurement_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title mb-5">Mm Unit Measurement</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_mm_unit_measurement" name="is_mm_unit_measurement" value="'.$mmUnitMeasurement->getInitialState().'">
         <input type="hidden" id="mm_unit_measurement_id" name="mm_unit_measurement_id" maxlength="22" value="'.$mmUnitMeasurement->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="mm_unit_measurement_name" class="control-label">Name:</label>
        <input type="text" id="mm_unit_measurement_name" name="mm_unit_measurement_name" class="mm_unit_measurement_name form-control"  maxlength="200"  value="'.$mmUnitMeasurement->getName().'"  tabindex="1"/>
      <label for="mm_unit_measurement_name" class="error">'.$mmUnitMeasurement->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="mm_unit_measurement_abbreviation" class="control-label">Abbreviation:</label>
        <input type="text" id="mm_unit_measurement_abbreviation" name="mm_unit_measurement_abbreviation" class="mm_unit_measurement_abbreviation form-control"  maxlength="5"  value="'.$mmUnitMeasurement->getAbbreviation().'"  tabindex="2"/>
      <label for="mm_unit_measurement_abbreviation" class="error">'.$mmUnitMeasurement->getAttrError("abbreviation").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="mm_unit_measurement_active" class="control-label">Active:</label>
        <select  id="mm_unit_measurement_active" name="mm_unit_measurement_active" class="mm_unit_measurement_active form-control" tabindex="3">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($mmUnitMeasurement->getActive(),'Y').
'      </select>
      <label for="mm_unit_measurement_active" class="error">'.$mmUnitMeasurement->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function mmUnitMeasurementRules(){
  $("#mm_unit_measurement_form").validate();
  $("#mm_unit_measurement_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#mm_unit_measurement_abbreviation").rules("add", {
    maxlength:5
  });
  $("#mm_unit_measurement_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  mmUnitMeasurementRules();


})
</script>