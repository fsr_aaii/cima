<style type="text/css">
  span.form-control{
    border-bottom:none !important;
  }

 .card-02 {
           background: #658E9C;
           height: 90%;
          border-radius: 5px;           

         }


.gTable td, .gTable th {
  font-size: .9rem;
}

@media (max-width: 1200px) {
  .gTable td, .gTable th {
    font-size: calc(.5rem + 1vw);
  }
}
</style>
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }
  
  $tfData["gl_person_id"] = $glPerson->getId();
  $tfData["gl_juridical_person_id"] = $glPerson->getId();

  $audit='<span class="created_by">Creado por '.TUser::description($tfs,$glJuridicalPerson->getCreatedBy()).'  el '.$glJuridicalPerson->getCreatedDate().'</span>';
 
  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="gl_juridical_person_form" name="gl_juridical_person_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="row">
           <div class="mx-auto col-lg-12 p-0">
            <div class="col-6 container title">Condominio</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlCustomer" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                 Editar
               </a>
             </div>
           </div>  

     <div class="col-lg-12 container mb-5">'.$audit.'</div>
     
      <div class="col-lg-5 container">
       <label for="gl_juridical_person_trade_name" class="control-label">Nombre Comercial:</label>
         <span class="form-control">'.$glJuridicalPerson->getTradeName().'</span>
      </div>
      <div class="col-lg-5 container">
       <label for="gl_juridical_person_legal_name" class="control-label">Raz&oacute;n Social:</label>
       <span class="form-control">'.$glJuridicalPerson->getLegalName().'</span>
      </div>
      <div class="col-lg-2 container">
       <label for="gl_juridical_person_nin_type" class="control-label">RIF:</label>
       <span class="form-control">'.$glJuridicalPerson->getNinType().'-'.$glJuridicalPerson->getNin().'-'.$glJuridicalPerson->getNinControlDigit().'</span>
      </div>
        <div class="col-lg-4 container">
       <label for="" class="control-label">Email:</label>
       <span class="form-control">'.$glJuridicalPerson->getEmailAccount().'</span>
      </div>
      <div class="col-lg-8 container">
       <label for="gl_juridical_person_legal_address" class="control-label">Direcci&oacute;n Fiscal:</label>
        <span class="form-control">'.$glJuridicalPerson->getLegalAddress().'</span>
      </div>

   </fieldset>
  </form>';
  
  if ($glJuridicalPerson->getId()!=''){

$tfNewData["co_condominium_prompt_payment_id_person"] = $glJuridicalPerson->getId();
$tfNewData["co_condominium_exchange_type_id_person"] = $glJuridicalPerson->getId(); 
$tfNewData["co_unit_id_person"] = $glJuridicalPerson->getId();
$tfNewData["gl_person_director_id_person"] = $glJuridicalPerson->getId();
$tfNewData["gl_person_banking_id_person"] = $glJuridicalPerson->getId();

$html.='<div class="row">
           <div class="mx-auto col-lg-12 p-0 mt-5 mb-3">
            <div class="col-6 container title">Bancos</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonBanking" data-tf-action="AN"  data-tf-data="'.$tfResponse->encrypt($tfNewData).'" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
             </div>
           </div>  
      <div class="col-12 p-0">
      <table id="gl_person_banking_dt" class="display responsive" style="width:100%">
         <thead>
    <tr>
      <th class="all">Banco</th>
      <th class="all">Cta</th>
    </tr>
  </thead>
  <tbody>';
   foreach ($glPersonBankingList as $row){
    $html.='   <tr>
            <td class="all">'.$row["bank"].'</td>
            <td class="all">'.$row["account"].'</td>
            </tr>';  
   }
   $html.='</tbody>
          </table>
          </div></div>';

$html.='<div class="row">
           <div class="mx-auto col-lg-12 p-0 mt-5 mb-3">
            <div class="col-6 container title">Pronto Pago</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCondominiumPromptPayment" data-tf-action="AN"  data-tf-data="'.$tfResponse->encrypt($tfNewData).'" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
             </div>
           </div>  
      <div class="col-12 p-0">
      <table id="cco_condominium_prompt_payment_dt" class="display responsive" style="width:100%">
         <thead>
    <tr>
      <th class="all">Fecha</th>
      <th class="all text-right" scope="col">Dias</th>
      <th class="all text-right" scope="col">% Credito</th>
    </tr>
  </thead>
  <tbody>';
   foreach ($coCondominiumPromptPaymentList as $row){
    $html.='   <tr>
            <td class="all">'.$row["created_date"].'</td>
            <td class="all text-right">'.$row["days"].'</td>
            <td class="all text-right">'.$row["credit_percentage"].'</td>
            </tr>';  
   }
   $html.='</tbody>
          </table>
          </div></div>';

  $html.='<div class="row">
           <div class="mx-auto col-lg-12 p-0 mt-5 mb-3">
            <div class="col-6 container title">Tipo de Cambio</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCondominiumExchangeType" data-tf-action="AN"  data-tf-data="'.$tfResponse->encrypt($tfNewData).'" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
             </div>
           </div>  
      <div class="col-12 p-0">
      <table id="co_exchange_dt" class="display responsive" style="width:100%">
         <thead>
    <tr>
      <th class="all">Fecha</th>
      <th class="all text-right" scope="col">Tipo</th>
    </tr>
  </thead>
  <tbody>';
   foreach ($coCondominiumExchangeTypeList as $row){
    $html.='   <tr>
            <td class="all">'.$row["created_date"].'</td>
            <td class="all text-right">'.$row["exchange_type"].'</td>
            </tr>';  
   }
   $html.='</tbody>
          </table>
          </div></div>';

   
  $html.='<div class="row">
           <div class="mx-auto col-lg-12 p-0 mt-5 mb-3">
            <div class="col-6 container title">Unidades</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnit" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt($tfNewData).'" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
             </div>
           </div>  
      <div class="row col-12">';
$i=0;
   foreach ($coUnitList as $row){
     $i++;
    $tfData["co_unit_id"] = $row["id"];


    $html.='  <div class="col-lg-4 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnit" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
            <div class="d-flex align-items-center card-01 p-4" >
                <div class="col-12 p-0 m-0">
                    <div class="col-lg-6 epic c-text-1 text-white m-0 p-0">'.$row["name"].'</div>
                    <div class="col-lg-6 epic g-text-5 text-white m-0 p-0 text-right">'.$row["aliquot"].'</div>
                    <div class="col-lg-12 epic c-text-3 text-white m-0 p-0">'.$row["user"].'</div>
                </div>
            </div>
          </div>';  
          if ($i%3==0){
      $html.='</div>
          <div class="row col-12">';
    }  

   }
   $html.='</div></div>';


   $html.='<div class="row">
           <div class="mx-auto col-lg-12 p-0 mt-5 mb-3">
            <div class="col-6 container title">Miembros Junta</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonDirector" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt($tfNewData).'" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
             </div>
           </div>  
      <div class="row col-12">';
$i=0;


   foreach ($glPersonDirectorList as $row){
    $i++;
    $tfData["gl_person_director_id"] = $row["id"];
    $html.='  <div class="col-lg-4 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonDirector" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
            <div class="d-flex align-items-center card-01 p-4" >
                <div class="col-12 p-0 m-0">
                    <div class="epic c-text-2 text-white m-0">'.$row["name"].'</div>
                    <div class="epic c-text-3 text-white m-0">'.$row["job"].'</div>
                    <div class="epic c-text-4 text-white m-0">'.$row["account"].'</div>
                    <div class="epic c-text-4 text-white m-0">'.$row["phone_number"].' </div>
                </div>
            </div>
          </div>';  
           if ($i%3==0){
      $html.='</div>
          <div class="row col-12">';
    }  
   }
   $html.='</div>
   </div>
        </div>';
  }

 $html.='</div>
</div>';
  echo $html;
?>

<script type="text/javascript">
$(document).ready(function() {
  $("#co_exchange_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'rtip',
    order: [[ 0, "desc" ]]
  });
  $("#cco_condominium_prompt_payment_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'rtip',
    order: [[ 0, "desc" ]]
  });
  $("#gl_person_banking_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'rtip',
    order: [[ 0, "desc" ]]
  });

});
</script>
