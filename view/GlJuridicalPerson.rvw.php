<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-6 title">Gl Juridical Person</div>
             <div class="col-6 text-right action">
               <a class="btn" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlJuridicalPerson" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo</a>
               <a class="btn" role="button" data-tf-table="#gl_juridical_person_dt" data-tf-file="Gl Juridical Person" onclick="TfExport.excel(this);">
                 Excel</a>
             </div>
       <table id="gl_juridical_person_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Nin Type</th>
             <th class="all">Nin</th>
             <th class="all">Nin Control Digit</th>
             <th class="all">Legal Name</th>
             <th class="all">Trade Name</th>
             <th class="all">Legal Address</th>
             <th class="all">Id Area Code</th>
             <th class="all">Phone Number</th>
             <th class="all">Active</th>
             <th class="none">Created by</th>
             <th class="none">Created date</th>
             <th class="all dt-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($glJuridicalPersonList as $row){
    $tfData["gl_juridical_person_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["nin_type"].'</td>
            <td>'.$row["nin"].'</td>
            <td>'.$row["nin_control_digit"].'</td>
            <td>'.$row["legal_name"].'</td>
            <td>'.$row["trade_name"].'</td>
            <td>'.$row["legal_address"].'</td>
            <td>'.$row["id_area_code"].'</td>
            <td>'.$row["phone_number"].'</td>
            <td>'.$row["active"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="text-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlJuridicalPerson" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#gl_juridical_person_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>