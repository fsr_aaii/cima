<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn" role="button" data-tf-form="#gl_person_relationship_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonRelationship" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$glPersonRelationship->getCreatedBy()).'  on '.$glPersonRelationship->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn" role="button" data-tf-form="#gl_person_relationship_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonRelationship" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn" role="button" data-tf-form="#gl_person_relationship_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonRelationship" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($glPersonRelationship->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($glPersonRelationship->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="gl_person_relationship_form" name="gl_person_relationship_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title mb-5">Gl Person Relationship</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_gl_person_relationship" name="is_gl_person_relationship" value="'.$glPersonRelationship->getInitialState().'">
         <input type="hidden" id="gl_person_relationship_id" name="gl_person_relationship_id" maxlength="22" value="'.$glPersonRelationship->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="gl_person_relationship_id_person" class="control-label">Id Person:</label>
        <select  id="gl_person_relationship_id_person" name="gl_person_relationship_id_person" class="gl_person_relationship_id_person form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(GlPerson::selectOptions($tfs),$glPersonRelationship->getIdPerson()).
'      </select>
      <label for="gl_person_relationship_id_person" class="error">'.$glPersonRelationship->getAttrError("id_person").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_person_relationship_id_relationship" class="control-label">Id Relationship:</label>
        <select  id="gl_person_relationship_id_relationship" name="gl_person_relationship_id_relationship" class="gl_person_relationship_id_relationship form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(GlRelationship::selectOptions($tfs),$glPersonRelationship->getIdRelationship()).
'      </select>
      <label for="gl_person_relationship_id_relationship" class="error">'.$glPersonRelationship->getAttrError("id_relationship").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_person_relationship_date_from" class="control-label">Date From:</label>
        <input type="text" id="gl_person_relationship_date_from" name="gl_person_relationship_date_from" class="gl_person_relationship_date_from form-control"  maxlength="22"  value="'.$glPersonRelationship->getDateFrom().'"  tabindex="3"/>
      <label for="gl_person_relationship_date_from" class="error">'.$glPersonRelationship->getAttrError("date_from").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_person_relationship_date_to" class="control-label">Date To:</label>
        <input type="text" id="gl_person_relationship_date_to" name="gl_person_relationship_date_to" class="gl_person_relationship_date_to form-control"  maxlength="22"  value="'.$glPersonRelationship->getDateTo().'"  tabindex="4"/>
      <label for="gl_person_relationship_date_to" class="error">'.$glPersonRelationship->getAttrError("date_to").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_person_relationship_active" class="control-label">Active:</label>
        <select  id="gl_person_relationship_active" name="gl_person_relationship_active" class="gl_person_relationship_active form-control" tabindex="5">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($glPersonRelationship->getActive(),'Y').
'      </select>
      <label for="gl_person_relationship_active" class="error">'.$glPersonRelationship->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function glPersonRelationshipRules(){
  $("#gl_person_relationship_form").validate();
  $("#gl_person_relationship_id_person").rules("add", {
    number:true,
    maxlength:22
  });
  $("#gl_person_relationship_id_relationship").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#gl_person_relationship_date_from").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#gl_person_relationship_date_to").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#gl_person_relationship_active").rules("add", {
    required:true,
    maxlength:1
  });

}

$("#gl_person_relationship_date_from").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#gl_person_relationship_date_to").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  glPersonRelationshipRules();
  $('select').niceSelect();
  $("#gl_person_relationship_date_from").css('vertical-align','top');
  $("#gl_person_relationship_date_from").mask('y999-m9-d9');
  $("#gl_person_relationship_date_to").css('vertical-align','top');
  $("#gl_person_relationship_date_to").mask('y999-m9-d9');

})
</script>