<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
            <div class="mx-auto col-lg-8 tf-card shadow mb-4">
            <div class="col-8 title">Informe de Inspecci&oacute;n</div>
             <div class="col-4 text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpInspectionReport" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
             </div>
       <table id="op_inspection_report_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Id Customer</th>
             <th class="all">Vessel</th>
             <th class="all">Id Port Pier</th>
             <th class="all">Product</th>
             <th class="all">Quantity</th>
             <th class="all">Inspection Date</th>
             <th class="all">Issued Date</th>
             <th class="none">Creado por</th>
             <th class="none">Creado el</th>
             <th class="all text-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($opInspectionReportList as $row){
    $tfData["op_inspection_report_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["id_customer"].'</td>
            <td>'.$row["vessel"].'</td>
            <td>'.$row["id_port_pier"].'</td>
            <td>'.$row["product"].'</td>
            <td>'.$row["quantity"].'</td>
            <td>'.$row["inspection_date"].'</td>
            <td>'.$row["issued_date"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="text-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpInspectionReport" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#op_inspection_report_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>