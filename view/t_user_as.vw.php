<?php
  if (!is_object($tus)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tuichSessionAction){
   case "AN":
   case "AI":
    $buttons ='<a href="#" class="btn btn-success" onclick="jConfirm(\'Are you sure you want to save this information?\',\'Confirmation to save\',
                  function(r){if(r){tuich_validate(\'#t_user_form\')?tuich_router(\''.$SessionURLBase.'t_user/AI\',\'#t_user_form\'):false;}});"><span class="far fa-save"></span> Save</a>';
    break;
      case "AE":
  
  }

 if ($shortInfo["photo"]=='' or $shortInfo["photo"]=='..'){
    $photo='../asset/images/blank-user.jpg';
  }else{
    $photo=$shortInfo["photo"];
  }

  $html = '<div class="row">
  <div class="mx-auto col-lg-8 form-frame">
    <div class="col-lg-12 title '.$tus->getSessionTaskTypeStyle().'">User Audit</div>
    <form id="t_user_form" name="t_user_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container">
         <input type="hidden" id="SessionURLBase" name="SessionURLBase" value="'.$SessionURLBase.'">
      </div>';
  if ($shortInfo["name"]!=''){    
  $html.= '<div class="col-lg-12 col-container">
   <div class="col-lg-3 col-container">
     <div class="col-md-12 container">
    <div class="form-group">
        <img class="photo-profile" src="'.$photo.'"/>
    </div>
</div>
   </div>
   <div class="col-lg-9 col-container">
   
      <div class="col-10 container">
      <span class="profile-name">'.$shortInfo["name"].'</span>
      </div>
      <div class="col-2 container text-right">
       '.$status.'
      </div>

  <div class="col-12 container">
   <span class="profile-business-title">'.$shortInfo["business_title"].'</span>
  </div>
  <div class="col-8 container profile">
  <label for=""  class="control-label">Cost Center:</label>
   <span class="">'.$shortInfo["cost_center"].'</span>
  </div>
  <div class="col-4 container profile text-right">
  <label for=""  class="control-label">Direct Employees:</label>
   <span class="">'.$shortInfo["direct_employees"].'</span>
  </div>
  <div class="col-12 container profile">
  <label for=""  class="control-label">Mail:</label>
   <span class="">'.$request->get("t_user_email_account").'@uber.com</span>
  </div>
  <div class="col-lg-5 container profile">
   <label for=""  class="control-label">Hire Date:</label>
   <span class="">'.$shortInfo["hire_date"].'</span>
  </div>
  <div class="col-lg-5 container profile">
  <label for=""  class="control-label">Termination Date:</label>
   <span class="">'.$shortInfo["termination_date"].'</span>
  </div>
   <div class="col-lg-2 container profile text-right">
  <label for=""  class="control-label">'.$shortInfo["level"].'</label>
  </div>

  </div>
</div>';
}


  $html.= '</fieldset>
  </form>
              </div>
            </div>
    <div class="row">
  <div class="mx-auto col-lg-8 form-frame">
  <div class="jumbotron">
  <h1 class="display-4">Alert!</h1>
  <p class="lead">'.$message.'</p>
  <hr class="my-4">
  <p>If you want to verify another uber email account, click on "Back"</p>
  
  <p class="lead">
    <div class="col-lg-12 container text-right keypad">
<a class="btn btn-primary" href="#" onclick="javascript:tuich_router(\''.substr($SessionURLBase, 0, -3).'previous\');"><i class="far fa-arrow-left"></i> Back</a>
    </div>
    
  </p>
</div>
          </div> 
    </div>                  ';
  echo $html;
?>

