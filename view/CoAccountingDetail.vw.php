<style type="text/css">
.nice-select.open .list {
    height: 200px; 
    overflow:auto;
 }   
</style>
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_accounting_detail_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccountingDetail" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coAccountingDetail->getCreatedBy()).'  el '.$coAccountingDetail->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_accounting_detail_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccountingDetail" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_accounting_detail_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccountingDetail" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coAccountingDetail->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coAccountingDetail->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_accounting_detail_form" name="co_accounting_detail_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Gasto Detallado</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_accounting_detail" name="is_co_accounting_detail" value="'.$coAccountingDetail->getInitialState().'">
         <input type="hidden" id="co_accounting_detail_id" name="co_accounting_detail_id" maxlength="22" value="'.$coAccountingDetail->getId().'">
         <input type="hidden" id="co_accounting_detail_id_accounting" name="co_accounting_detail_id_accounting" maxlength="22" value="'.$coAccountingDetail->getIdAccounting().'">

      </div>
      <div class="col-lg-12 container">
       <label for="co_accounting_detail_description" class="control-label">Descripcion:</label>
        <textarea id="co_accounting_detail_description" name="co_accounting_detail_description" class="co_accounting_detail_description form-control" rows="3" >'.$coAccountingDetail->getDescription().'</textarea>
      <label for="co_accounting_detail_description" class="error">'.$coAccountingDetail->getAttrError("description").'</label>
      </div>
      <div class="col-lg-8 container">
       <label for="co_accounting_detail_id_account" class="control-label">Concepto:</label>
        <select  id="co_accounting_detail_id_account" name="co_accounting_detail_id_account" class="co_accounting_detail_id_account form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CoAccount::selectOptions($tfs),$coAccountingDetail->getIdAccount()).
'      </select>
      <label for="co_accounting_detail_id_account" class="error">'.$coAccountingDetail->getAttrError("id_account").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="co_accounting_detail_amount" class="control-label">Monto:</label>
        <input type="text" id="co_accounting_detail_amount" name="co_accounting_detail_amount" class="co_accounting_detail_amount form-control"  maxlength="22"  value="'.$coAccountingDetail->getAmount().'"  tabindex="3"/>
      <label for="co_accounting_detail_amount" class="error">'.$coAccountingDetail->getAttrError("amount").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_accounting_detail_observation" class="control-label">Observaccion:</label>
        <textarea id="co_accounting_detail_observation" name="co_accounting_detail_observation" class="co_accounting_detail_observation form-control" rows="3" >'.$coAccountingDetail->getObservation().'</textarea>
      <label for="co_accounting_detail_observation" class="error">'.$coAccountingDetail->getAttrError("observation").'</label>
      </div>
      

   <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">

  function coAccountingDetailRules(){

  $("#co_accounting_detail_form").validate();
  $("#co_accounting_detail_id_accounting").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_accounting_detail_id_account").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_accounting_detail_description").rules("add", {
    required:true,
    maxlength:5000
  });
  $("#co_accounting_detail_observation").rules("add", {
    maxlength:5000
  });
  $("#co_accounting_detail_amount").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}


$(document).ready(function(){
  coAccountingDetailRules();
  $('select').niceSelect();

})
</script>