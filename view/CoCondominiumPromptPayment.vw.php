<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_condominium_prompt_payment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCondominiumPromptPayment" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coCondominiumPromptPayment->getCreatedBy()).'  el '.$coCondominiumPromptPayment->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_condominium_prompt_payment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCondominiumPromptPayment" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_condominium_prompt_payment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCondominiumPromptPayment" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coCondominiumPromptPayment->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coCondominiumPromptPayment->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-6 form-frame shadow mb-4">
    <form id="co_condominium_prompt_payment_form" name="co_condominium_prompt_payment_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Pronto Pago</div>
     <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_condominium_prompt_payment" name="is_co_condominium_prompt_payment" value="'.$coCondominiumPromptPayment->getInitialState().'">
         <input type="hidden" id="co_condominium_prompt_payment_id" name="co_condominium_prompt_payment_id" maxlength="22" value="'.$coCondominiumPromptPayment->getId().'">
         <input type="hidden" id="co_condominium_prompt_payment_id_person" name="co_condominium_prompt_payment_id_person" maxlength="22" value="'.$coCondominiumPromptPayment->getIdPerson().'">
      </div>
      <div class="col-lg-6 container">
       <label for="co_condominium_prompt_payment_days" class="control-label">Dias:</label>
        <input type="text" id="co_condominium_prompt_payment_days" name="co_condominium_prompt_payment_days" class="co_condominium_prompt_payment_days form-control"  maxlength="22"  value="'.$coCondominiumPromptPayment->getDays().'"  tabindex="2"/>
      <label for="co_condominium_prompt_payment_days" class="error">'.$coCondominiumPromptPayment->getAttrError("days").'</label>
      </div>
      <div class="col-lg-6 container">
       <label for="co_condominium_prompt_payment_credit_percentage" class="control-label">% Credito:</label>
        <input type="text" id="co_condominium_prompt_payment_credit_percentage" name="co_condominium_prompt_payment_credit_percentage" class="co_condominium_prompt_payment_credit_percentage form-control"  maxlength="22"  value="'.$coCondominiumPromptPayment->getCreditPercentage().'"  tabindex="2"/>
      <label for="co_condominium_prompt_payment_credit_percentage" class="error">'.$coCondominiumPromptPayment->getAttrError("credit_percentage").'</label>
      </div>
      <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function coCondominiumPromptPaymentRules(){
  $("#co_condominium_prompt_payment_form").validate();

  $("#co_condominium_prompt_payment_days").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_condominium_prompt_payment_credit_percentage").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });


}


$(document).ready(function(){
  coCondominiumPromptPaymentRules();


})
</script>