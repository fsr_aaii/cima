<style type="text/css">
    
  #cu_forecast_dt .fixed{
    width: 10%; 

  }
  .c-chart .card{
    padding: 0;
    border: none !important;
  }
  .c-chart .card-body{
    padding: 0;
  }
  .c-chart .chart-container{
    padding: 0;
  }
.fixed-height-chart{
  height: 450px;
}
.my-primary-color {
    color: #36b3b9 !important;
}    
</style>
<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
            <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-12 mb-5 title">Consulta de Inspecci&oacute;n</div>  
            <div class="row col-12 mb-2">
                <div class="col-lg-3 col-12 p-0">
                   <div class="col-12 p-0"><b>Cliente:</b></div> 
                   <div class="col-12 p-0">'.$opInspectionReport["customer"].'</div>  
               </div>  
                <div class="col-lg-3 col-12 p-0">
                  <div class="col-12 p-0"><b>Muelle:</b> </div>
                  <div class="col-12 p-0">'.$opInspectionReport["port_pier"].'</div>
                </div>  
                <div class="col-lg-3 col-12 p-0">
                 <div class="col-12 p-0"><b>Buque: </b></div>
                 <div class="col-12 p-0">'.$opInspectionReport["vessel"].'</div>  
                </div> 
                <div class="col-lg-3 col-12 p-0">
                 <div class="col-12 p-0"><b>Cantidad:</b></div>
                 <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["qty"]).' Kgs</div>  
                </div> 
            </div>
            <div class="row col-12 mb-2">
                <div class="col-lg-4 col-12 p-0">
                  <div class="col-12 p-0"><b>BL: </b></div> 
                  <div class="col-12 p-0">'.$opInspectionReport["BL"].'</div> 
                </div>  
                <div class="col-lg-4 col-12 p-0">
                  <div class="col-12 p-0"><b>Producto 1: </b> </div>
                  <div class="col-12 p-0">'.$opInspectionReport["product_1"].'</div>
                </div>  
                <div class="col-lg-4 col-12 p-0">
                  <div class="col-12 p-0"><b>Producto 2: </b> </div>
                  <div class="col-12 p-0">'.$opInspectionReport["product_2"].'</div> 
                </div> 
            </div>  
            <div class="row col-12 mb-2">
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh. Estim.: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["cant_veh_estim"]).'</div> 
                 </div> 
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Prom:</b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["promedio_kgs"]).' Kgs</div>
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh.(SICA): </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["VEH_SICA"]).'</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Aprox.(SICA): </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["KG_APROX_SICA"]).' Kgs</div> 
                </div>   
            </div> 
            <div class="row col-12 mb-2">
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh. Pesado.: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["VEH_PESADO"]).'</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Prom Pesado:</b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["KG_APROX_PESADO"]).' Kgs</div>
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Remanente: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["remanente_unidades"]).'</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Remanente: </b> </div>
                  <div class="col-12 p-0">'.TfWidget::qty($opInspectionReportDetail["remanente_kgs"]).' Kgs</div>  
                </div>  
            </div>

            <div id="accordion-t1" class="col-lg-12 container">
              <div class="my-epic-profile card p-0 border-0">
                <!--begin accordion-t1-header-->
                <div class="col-12 card-header border-0" id="heading-t1">
                  <div class="col-12 container"> 
                    <a id="accordion-a-t1" href="#" class="section active collapsed my-primary-color" data-toggle="collapse" data-target="#collapse-t1" aria-expanded="false" aria-controls="collapse-t1" data-form-action="form-action-t1">Transportes (SICA/PESADOS):</a>
                   </div>
                </div>   
                <!--end accordion-t1-header-->
                <!--begin accordion-t1-content-->
                <div id="collapse-t1" class="collapsed collapse" aria-labelledby="heading-t1" data-parent="#accordion-t1" style="">        
                  <div class="row">';
                 

    foreach ($opInspectionReportDetailTransport as $t){
        $diff = ($t["VEH_SICA"]-$t["VEH_PESADO"]);
        if ($diff!="0"){
          $class='text-danger';
        }else{
          $class='text-primary';
        }
        $html.='<div class="row col-12 mb-2 p-0">
                <div class="col-lg-8 col-9 p-0"><b>'.$t["transport"].'</b></div> 
                <div class="col-lg-4 col-3 p-0 text-right">'.$t["VEH_SICA"].'/'.$t["VEH_PESADO"].' <span class="'.$class.'">('.$diff.')</span></div> 
            </div> ';
    }  
    $html.='   </div>
              </div>
              <!--end accordion-t1-content-->
               </div>                      
              </div>';



    $html.='  <div id="accordion-t2" class="col-lg-12 container">
              <div class="my-epic-profile card p-0 border-0">
                <!--begin accordion-t2-header-->
                <div class="col-12 card-header border-0" id="heading-t2">
                  <div class="col-12 container"> 
                    <a id="accordion-a-t2" href="#" class="section active collapsed my-primary-color" data-toggle="collapse" data-target="#collapse-t2" aria-expanded="false" aria-controls="collapse-t2" data-form-action="form-action-t1">Transportes (Por Cargar):</a>
                   </div>
                </div>   
                <!--end accordion-t2-header-->
                <!--begin accordion-t2-content-->
                <div id="collapse-t2" class="collapsed collapse" aria-labelledby="heading-t1" data-parent="#accordion-t2" style="">        
                  <div class="row">';
                 

    foreach ($opInspectionReportDetailTransport2 as $t){
        $html.='<div class="row col-12 mb-2 p-0">
                <div class="col-lg-3 col-6 p-0"><b>'.$t["transport"].'</b></div> 
                <div class="col-lg-3 col-6 p-0">'.$t["destination"].'</div> 
                <div class="col-lg-3 col-6 p-0">'.$t["SICA_guide"].' </div> 
                <div class="col-lg-3 col-6 p-0">'.$t["name"].'</div> 
            </div>
             ';
    }  
   
    $html.='   </div>
              </div>
              <!--end accordion-t2-content-->
               </div>                      
              </div>

              <div id="accordion-e1" class="col-lg-12 container">
              <div class="my-epic-profile card p-0 border-0">
                <!--begin accordion-e1-header-->
                <div class="col-12 card-header border-0" id="heading-e1">
                  <div class="col-12 container"> 
                    <a id="accordion-a-e1" href="#" class="section active collapsed my-primary-color" data-toggle="collapse" data-target="#collapse-e1" aria-expanded="false" aria-controls="collapse-e1" data-form-action="form-action-e1">Estadisticas:</a>
                   </div>
                </div>   
                <!--end accordion-e1-header-->
                <!--begin accordion-e1-content-->
                <div id="collapse-e1" class="collapsed collapse" aria-labelledby="heading-e1" data-parent="#accordion-e1" style="">        
                  <div class="row">
                   <div class="row col-12 mb-2">
            <div class="col-lg-6">
              <div class="col-12 text-center subtitle mt-4">Vehiculos con Guia</div>
              <div class="col-lg-12 container c-chart">
                <div class="card mb-12">
                  <div class="card-body"> 
                    <div class="chart-container fixed-height-chart">
                      <canvas id="chart-pr1"></canvas>
                    </div>
                  </div>        
                </div>
              </div>
              <div class="col-lg-12 text-center container">
                <span class="created_by">Generado el '.$generateDate.'</span>
              </div>
          </div>    
          <div class="col-lg-6">
              <div class="col-12 text-center subtitle mt-4">Vehiculos Pesados</div>
              <div class="col-lg-12 container c-chart">
                <div class="card mb-12">
                  <div class="card-body"> 
                    <div class="chart-container fixed-height-chart">
                      <canvas id="chart-pr2"></canvas>
                    </div>
                  </div>        
                </div>
              </div>
              <div class="col-lg-12 text-center container">
                <span class="created_by">Generado el '.$generateDate.'</span>
              </div>
            </div>
             </div>
                  </div>
                  <!--end accordion-t2-content-->
                </div>    
              </div> 
              </div> ';

    $html.='<div class="col-12 mt-3 mb-5 title">Destinos</div> ';

   foreach ($opDestinationList as $s){
    $html.='<div class="row col-12 mb-2">
                <div class="col-lg-6 col-12 p-0">
                  <div class="col-12 p-0"><b>Destino: </b></div>
                  <div class="col-12 p-0">'.$s["name"].'</div> 
                </div>  
                <div class="col-lg-3 col-12 p-0">
                  <div class="col-12 p-0"><b>BL: </b></div>
                  <div class="col-12 p-0">'.$s["BL"].'</div>  
                </div>  
                <div class="col-lg-3 col-12 p-0">
                  <div class="col-12 p-0"><b>Cantidad:</b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["qty"]).' Kgs</div>     
                </div>             
            </div> 
            <div class="row col-12 mb-2">
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh. Estim.: </b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["cant_veh_estim"]).'</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Prom:</b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["promedio_kgs"]).' Kgs</div>
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh.(SICA): </b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["VEH_SICA"]).'</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Aprox.(SICA): </b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["KG_APROX_SICA"]).' Kgs</div>  
                </div>  
            </div> 
            <div class="row col-12 mb-2">
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Veh. Pesado.: </b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["VEH_PESADO"]).'</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Prom Pesado:</b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["KG_APROX_PESADO"]).' Kgs</div>
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Remanente: </b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["remanente_unidades"]).'</div> 
                </div>  
                <div class="col-lg-3 col-6 p-0">
                  <div class="col-12 p-0"><b>Remanente: </b></div>
                  <div class="col-12 p-0">'.TfWidget::qty($s["remanente_kgs"]).' Kgs</div>  
                </div>  
            </div>
            <div id="accordion-t3" class="col-lg-12 container">
              <div class="my-epic-profile card p-0 border-0">
                <!--begin accordion-t3-header-->
                <div class="col-12 card-header border-0" id="heading-t3">
                  <div class="col-12 container"> 
                    <a id="accordion-a-t3" href="#" class="section active collapsed my-primary-color" data-toggle="collapse" data-target="#collapse-t3" aria-expanded="false" aria-controls="collapse-t3" data-form-action="form-action-t3">Transportes (SICA/PESADOS):</a>
                   </div>
                </div>   
                <!--end accordion-t3-header-->
                <!--begin accordion-t3-content-->
                <div id="collapse-t3" class="collapsed collapse" aria-labelledby="heading-t3" data-parent="#accordion-t3" style="">        
                  <div class="row">';


             
    $opTransportList = OpDestination::detailTransport($tfs,$s["id"]);   
    

    foreach ($opTransportList as $t){
        $diff = ($t["VEH_SICA"]-$t["VEH_PESADO"]);
        if ($diff!="0"){
          $class='text-danger';
        }else{
          $class='text-primary';
        }
        $html.='<div class="row col-12 mb-2 p-0">
                <div class="col-lg-8 col-9 p-0"><b>'.$t["transport"].'</b></div> 
                <div class="col-lg-4 col-3 p-0 text-right">'.$t["VEH_SICA"].'/'.$t["VEH_PESADO"].' <span class="'.$class.'">('.$diff.')</span></div> 
            </div> ';
    }     
   }


   $html.='   </div>
              </div>
              <!--end accordion-t3-content-->
               </div>                      
              </div>';


     $html.="<script type=\"text/javascript\">
 Chart.defaults.global.defaultFontFamily = \"'Roboto', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif\";
Chart.defaults.global.legend.position = 'bottom';
Chart.defaults.global.legend.labels.usePointStyle = true;
Chart.defaults.global.legend.labels.boxWidth = 15;
Chart.defaults.global.tooltips.backgroundColor = '#000';

var ggColors = [['rgba(192, 57, 43, 1)', 'rgba(192, 57, 43, 0.8)'],
                      ['rgba(234, 67, 53, 1)', 'rgba(234, 67, 53, 0.8)'],
                      ['rgba(17, 103, 177, 1)', 'rgba(17, 103, 177, 0.8)'],
                      ['rgba(24, 123, 205, 1)', 'rgba(24, 123, 205, 0.8)'],
                      ['rgba(52, 152, 219, 1)', 'rgba(52, 152, 219, 0.8)'],
                      ['rgba(42, 157, 244, 1)', 'rgba(42, 157, 244, 0.8)'],
                      ['rgba(22, 160, 133, 1)', 'rgba(22, 160, 133, 0.8)'],
                      ['rgba(39, 174, 96, 1)', 'rgba(39, 174, 96, 0.8)'],
                      ['rgba(46, 204, 113, 1)', 'rgba(46, 204, 113, 0.8)'],
                      ['rgba(241, 196, 15, 1)', 'rgba(241, 196, 15, 0.8)'],
                      ['rgba(243, 156, 18, 1)', 'rgba(243, 156, 18, 0.8)'],
                      ['rgba(230, 126, 34, 1)', 'rgba(230, 126, 34, 0.8)'],
                      ['rgba(211, 84, 0, 1)', 'rgba(211, 84, 0, 0.8)'],
                      ['rgba(255, 99, 88, 1)', 'rgba(255, 99, 88, 0.8)'],
                      ['rgba(128, 0, 128, 1)', 'rgba(128, 0, 128 , 0.8)'],
                      ['rgba(155, 89, 182, 1)', 'rgba(155, 89, 182, 0.8)'],
                      ['rgba(142, 68, 173, 1)', 'rgba(142, 68, 173, 0.8)'],
                      ['rgba(255, 67, 127, 1)', 'rgba(255, 67, 127, 0.8)'],
                      ['rgba(255, 53, 167, 1)', 'rgba(255, 53, 167, 0.8)'],
                      ['rgba(189, 195, 199, 1)', 'rgba(189, 195, 199, 0.8)'],
                      ['rgba(149, 165, 166, 1)', 'rgba(149, 165, 166, 0.8)'],
                      ['rgba(127, 140, 141, 1)', 'rgba(127, 140, 141, 0.8)'],
                      ['rgba(149, 112, 108, 1)', 'rgba(149, 112, 108, 0.8)'],
                      ['rgba(170, 85, 76, 1)', 'rgba(170, 85, 76, 0.8)']];     
    
var chartColors = [ggColors[0][0],ggColors[1][0],ggColors[2][0],ggColors[3][0],
                   ggColors[4][0],ggColors[5][0],ggColors[6][0],ggColors[7][0],
                   ggColors[8][0],ggColors[9][0],ggColors[10][0],ggColors[11][0],
                   ggColors[12][0],ggColors[13][0],ggColors[14][0],ggColors[15][0],
                   ggColors[16][0],ggColors[17][0],ggColors[18][0],ggColors[19][0],
                   ggColors[20][0],ggColors[21][0],ggColors[22][0],ggColors[23][0]];                   
                       
var chartColorsHover = [ggColors[0][1],ggColors[1][1],ggColors[2][1],ggColors[3][1],
                        ggColors[4][1],ggColors[5][1],ggColors[6][1],ggColors[7][1],
                        ggColors[8][1],ggColors[9][1],ggColors[10][1],ggColors[11][0],
                        ggColors[12][1],ggColors[13][1],ggColors[14][1],ggColors[15][1],
                        ggColors[16][1],ggColors[17][1],ggColors[18][1],ggColors[19][1],
                        ggColors[20][1],ggColors[21][1],ggColors[22][1],ggColors[23][1]];

var chartColorsShort = [ggColors[1][0],ggColors[3][0],ggColors[7][0],ggColors[9][0],ggColors[15][0]];
                       
var chartColorsShortHover = [ggColors[1][1],ggColors[3][1],ggColors[7][1],ggColors[9][1],ggColors[15][1]];

var ctx = document.getElementById('chart-pr1').getContext('2d');

  var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ".$chart_pr_label.",
        datasets: [{
           data: ".$chart_pr_data1.",
            borderWidth: 2,
                        hoverBorderWidth: 5,
                        backgroundColor: chartColors,
                        hoverbackgroundColor: chartColorsHover,
                        hoverBorderColor: chartColorsHover,
                        borderColor: chartColors,
        }]
    },
    options: {
                          responsive: true,
                          plugins: {
                              datalabels: {
                                  formatter: (value, ctx) => {
                                      let sum = 0;
                                      let dataArr = ctx.chart.data.datasets[0].data;
                                      dataArr.map(data => {
                                          sum += parseInt(data);
                                      });
                                      let percentage = (value*100 / sum).toFixed(2)+\"%\";
                                      return percentage;
                                  }
                              }
                          },
                          legend: {
                            position: 'bottom',
                          },
                          title: {
                            display: true,
                            text: ' '
                          },
                          animation: {
                            animateScale: true,
                            animateRotate: true
                          },
                          responsive: true,
                          maintainAspectRatio: false,

                        }
});
if ($(document).width()<450){
        myChart.options.legend.display=false;
}

var ctx = document.getElementById('chart-pr2').getContext('2d');

  var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ".$chart_pr_label.",
        datasets: [{
           data: ".$chart_pr_data2.",
            borderWidth: 2,
                        hoverBorderWidth: 5,
                        backgroundColor: chartColors,
                        hoverbackgroundColor: chartColorsHover,
                        hoverBorderColor: chartColorsHover,
                        borderColor: chartColors,
        }]
    },
    options: {
                          responsive: true,
                          plugins: {
                              datalabels: {
                                  formatter: (value, ctx) => {
                                      let sum = 0;
                                      let dataArr = ctx.chart.data.datasets[0].data;
                                      dataArr.map(data => {
                                          sum += parseInt(data);
                                      });
                                      let percentage = (value*100 / sum).toFixed(2)+\"%\";
                                      return percentage;
                                  }
                              }
                          },
                          legend: {
                            position: 'bottom',
                          },
                          title: {
                            display: true,
                            text: ' '
                          },
                          animation: {
                            animateScale: true,
                            animateRotate: true
                          },
                          responsive: true,
                          maintainAspectRatio: false,

                        }
});
if ($(document).width()<450){
        myChart.options.legend.display=false;
}
</script>";         
  echo $html;   
?>


