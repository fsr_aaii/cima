<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_prompt_payment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="coPromptPayment" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coPromptPayment->getCreatedBy()).'  el '.$coPromptPayment->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_prompt_payment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="coPromptPayment" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_prompt_payment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="coPromptPayment" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coPromptPayment->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coPromptPayment->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_prompt_payment_form" name="co_prompt_payment_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Co Prompt Payment</div>
     <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_prompt_payment" name="is_co_prompt_payment" value="'.$coPromptPayment->getInitialState().'">
         <input type="hidden" id="co_prompt_payment_id" name="co_prompt_payment_id" maxlength="22" value="'.$coPromptPayment->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="co_prompt_payment_id_unit" class="control-label">Id Unit:</label>
        <input type="text" id="co_prompt_payment_id_unit" name="co_prompt_payment_id_unit" class="co_prompt_payment_id_unit form-control"  maxlength="22"  value="'.$coPromptPayment->getIdUnit().'"  tabindex="1"/>
      <label for="co_prompt_payment_id_unit" class="error">'.$coPromptPayment->getAttrError("id_unit").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_prompt_payment_rate" class="control-label">Rate:</label>
        <input type="text" id="co_prompt_payment_rate" name="co_prompt_payment_rate" class="co_prompt_payment_rate form-control"  maxlength="22"  value="'.$coPromptPayment->getRate().'"  tabindex="2"/>
      <label for="co_prompt_payment_rate" class="error">'.$coPromptPayment->getAttrError("rate").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_prompt_payment_amount" class="control-label">Amount:</label>
        <input type="text" id="co_prompt_payment_amount" name="co_prompt_payment_amount" class="co_prompt_payment_amount form-control"  maxlength="22"  value="'.$coPromptPayment->getAmount().'"  tabindex="3"/>
      <label for="co_prompt_payment_amount" class="error">'.$coPromptPayment->getAttrError("amount").'</label>
      </div>

      <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function coPromptPaymentRules(){
  $("#co_prompt_payment_form").validate();
  $("#co_prompt_payment_id_unit").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_prompt_payment_rate").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_prompt_payment_amount").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}


$(document).ready(function(){
  coPromptPaymentRules();


})
</script>