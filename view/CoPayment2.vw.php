<style type="text/css">
.button-container {
    width: 100%;
    padding: 0;
}
.img-profile {
    display: block;
    width: 100%;
}
.button-container a {
    position: absolute;
    top: .3em;
    right: .3em;
    color: white;
    text-transform: uppercase;
    padding: .5em .75em .3em .75em;
}
.btn-file {
    position: absolute;
    text-align: left;
    overflow: hidden;
}
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

.img-fluid {
    max-width: 100%;
    height: auto;
    padding: 0;
}
</style>
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }


  $currency = array();
  $currency[0]['value'] = 'VEB';
  $currency[0]['option'] = 'Bolivar';
  $currency[1]['value'] = 'USD';
  $currency[1]['option'] = 'Dolar';

    if ($coPayment->getImagePath()=='..' or $coPayment->getImagePath()==''){
    $photo='../../../asset/images/no-image.png';
  }else{
    $photo=$coPayment->getImagePath();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_payment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPayment" data-tf-action="AI" onclick="CoPaymentDo(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coPayment->getCreatedBy()).'  el '.$coPayment->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_payment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPayment" data-tf-action="AA" onclick="CoPaymentDo(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_payment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPayment" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coPayment->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coPayment->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_payment_form" name="co_payment_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Pago</div>
      <div class="col-lg-12 container">'.$objAlerts.'aaaaaaaaaaaaaaaaaaa</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_payment" name="is_co_payment" value="'.$coPayment->getInitialState().'">
         <input type="hidden" id="co_payment_id" name="co_payment_id" maxlength="22" value="'.$coPayment->getId().'">
         <input type="hidden" id="co_payment_id_unit" name="co_payment_id_unit" maxlength="22" value="'.$coPayment->getIdUnit().'">
         <input type="hidden" id="co_payment_rate" name="co_payment_rate" maxlength="22" value="'.$coExchangeRate.'">
         <input type="hidden" id="co_payment_errors" name="co_payment_errors" maxlength="22" value="'.$coPayment->getErrors().'"> 
         <input type="hidden" id="co_payment_image_path" name="co_payment_image_path"  value="'.$coPayment->getImagePath().'">
      </div>

       <div class="col-lg-12 col-container">
         <label for=""  class="control-label">Image:</label>
         <input id="co_payment_image_path2" name="co_payment_image_path2" type="hidden" value="'.$coPayment->getImagePath().'"  class="form-control" >
         <div class="col-12 button-container">           
           <img id="photo-upload" name="photo-upload" class="col-12  img-fluid " src="'.$photo.'">
           <a class="btn btn-file btn-guaramo xl btn-photo-upload rounded-circle href="#" alt="Upload">
            <i class="bx bx-image"></i>
            <input type="file" id="co_payment_photo_img" name="co_payment_photo_img">
           </a>
         </div>
         <label for="co_payment_image_path" class="error">'.$coPayment->getAttrError("image_path").'</label> 
      </div>
      <div class="col-lg-4 container">
       <label for="co_payment_amount" class="control-label">Moneda:</label>
               <select  id="co_payment_currency" name="co_payment_currency" class="co_payment_currency form-control" tabindex="2">
      <option value="">Selecciona Moneda</option>'.
      TfWidget::selectStructure($currency,$coPayment->getCurrency()).
'      </select>
      <label for="co_payment_currency" class="error">'.$coPayment->getAttrError("currency").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="co_payment_amount" class="control-label">Fecha Pago:</label>
        <input type="text" id="co_payment_payment_date" name="co_payment_payment_date" class="co_payment_payment_date form-control"  maxlength="22"  value="'.$coPayment->getPaymentDate().'"  tabindex="2"/>
      <label for="co_payment_payment_date" class="error">'.$coPayment->getAttrError("payment_date").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="co_payment_amount" class="control-label">Monto:</label>
        <input type="text" id="co_payment_amount" name="co_payment_amount" class="co_payment_amount form-control"  maxlength="22"  value="'.$coPayment->getAmount().'"  tabindex="2"/>
      <label for="co_payment_amount" class="error">'.$coPayment->getAttrError("amount").'</label>
      </div>

      <div class="col-lg-12 container">
       <label for="co_payment_observation" class="control-label">Observacion:</label>
        <textarea id="co_payment_observation" name="co_payment_observation" class="co_payment_observation form-control" rows="3" >'.$coPayment->getObservation().'</textarea>
      <label for="co_payment_observation" class="error">'.$coPayment->getAttrError("observation").'</label>
      </div>
      <div class="col-lg-12 container">
        '.$due.' 
      </div>


   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function coPaymentRules(){
  $("#co_payment_form").validate();
  $("#co_payment_id_unit").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

  $("#co_payment_amount").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_payment_currency").rules("add", {
    required:true,
    maxlength:3
  });
  $("#co_payment_image_path2").rules("add", {
    required:true,
    maxlength:500
  });
  $("#co_payment_observation").rules("add", {
    maxlength:4294967295
  });
  $("#co_payment_payment_date").rules("add", {
    isDate:true,
    maxlength:22
  });

}

$("#co_payment_payment_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

function CoPaymentDo(element){
  jConfirm('¿Está seguro de que desea procesar esta información?','Continue?',function(r){
    if(r){
      if(tfValidate('#co_payment_form')){
        let url=sessionStorage.getItem('TF_URL_ROOT')+"/view/"+element.dataset.tfTaskId+"/"+element.dataset.tfController+"/"+element.dataset.tfAction;
      //  console.log("url copayment2 "+ url );
        let form = element.dataset.tfForm;
        let go = true;     
        let jsonObject = new Object();
        let file_data = $('#co_payment_photo_img').prop('files')[0];
        var img_data = new FormData();
        img_data.append('co_payment_photo_img', file_data);
        
        if (typeof form != 'undefined') {
          if ($(form).valid()){
           jsonObject = $(form).serializeJSON();   
          }else{
            go = false;
          } 
        }
        let cryptography  = new TfCryptography();         
        data = cryptography.encryptJSON(jsonObject,sessionStorage.getItem('RANDOM_PUBLIC_KEY'));
        img_data.append('request-data', data);

        if (go){
          $.ajax({
            data: img_data,
            url: url,    
            type: 'POST',
            dataType: 'text', 
            cache: false,
            contentType: false,
            processData: false,
            async : false,
            success:function(response){ 
              //let JDATA=JSON.parse(response);
              //let cryptography  = new TfCryptography();
              //                   $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, sessionStorage.getItem('RANDOM_PUBLIC_KEY')));
               $("#loading").show();
               $("#screen").hide();
               try{ 
                 let JDATA=JSON.parse(response);
                 let cryptography  = new TfCryptography();
                 if (JDATA.tfContent.encrypt=='true'){
                  $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, sessionStorage.getItem('RANDOM_PUBLIC_KEY')));
                 }else{
                  $('#tf-container').html(JDATA.tfContent.value); 
                 }
                 $("#loading").hide();
                  if ($('#co_payment_errors').val()==0 || (typeof $('#co_payment_errors').val() == 'undefined')){
                    jAlert("Su pago ha sido registrado correctamente", "Información"); 
                 }else{
                    jAlert("Su pago NO ha sido registrado", "Información"); 
                 }
              

                 $("#screen").show();
                 window.scrollTo(0,0);
                }catch (e) {
                  $("#loading").hide();
                   jAlert("Ha ocurrido un error y su pago no se ha registrado", "Información");
                  

                  $("#screen").show();
                 console.log('TfRequestPost');
                 console.log(e.message);
                 console.log(response);
             } 
            }
          });
        }
      }  
    }
  })
}

$(document).ready(function(){
  coPaymentRules();
  $('select').niceSelect();
  $("#co_payment_payment_date").css('vertical-align','top');
  $("#co_payment_payment_date").mask('y999-m9-d9');

  $(document).on('change', '.btn-photo-upload :file', function() {
    var input = $(this),
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [label]);
  });

  $('.btn-photo-upload :file').on('fileselect', function(event, label) {
    var input = $(this).parents('.input-group').find(':text'),
        log = label;
        
    $('#co_payment_image_path2').val(log);
      
  });
  
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#photo-upload').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
  }

    $("#co_payment_photo_img").change(function(){
        readURL(this);
    });
})
</script>