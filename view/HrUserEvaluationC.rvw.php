<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8 info-panel">
           <div class="jumbotron">
  <h1 class="display-4">Lo Siento!</h1>
  <p class="lead">';
foreach ($hrUserEvaluation->getObjError() as $oe) {
    $html.=$oe;
  }
  $html.='</p>
  <hr class="my-4">
  <p>Espera tu proxima guia de estudio.</p>
    <p class="lead">
    <a class="btn btn-primary btn-lg" role="button" onclick="TfRequest.logout();">Salir</a>
  </p>
</div>
         </div>
        </div>';
 echo $html;
?>