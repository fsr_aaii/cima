<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#op_notification_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="opNotification" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$opNotification->getCreatedBy()).'  el '.$opNotification->getCreatedDate().'</span>';
    break;
  }
  foreach ($opNotification->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($opNotification->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="op_notification_form" name="op_notification_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Notificacion</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_op_notification" name="is_op_notification" value="'.$opNotification->getInitialState().'">
         <input type="hidden" id="op_notification_id" name="op_notification_id" maxlength="22" value="'.$opNotification->getId().'">
         <input type="hidden" id="op_notification_id_inspection_report" name="op_notification_id_inspection_report" maxlength="22" value="'.$opNotification->getIdInspectionReport().'">
      </div>
      <div class="col-lg-12 container">
       <label for="op_notification_subject" class="control-label">Asunto:</label>
        <input type="text" id="op_notification_subject" name="op_notification_subject" class="op_notification_subject form-control"  maxlength="450"  value="'.$opNotification->getSubject().'"  tabindex="1"/>
      <label for="op_notification_subject" class="error">'.$opNotification->getAttrError("subject").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="op_notification_body" class="control-label">Mensaje:</label>
        <textarea id="op_notification_body" name="op_notification_body" class="op_notification_body form-control" rows="10" >'.$opNotification->getBody().'</textarea>
      <label for="op_notification_body" class="error">'.$opNotification->getAttrError("body").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function opNotificationRules(){
  $("#op_notification_form").validate();
  $("#op_notification_id_inspection_report").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#op_notification_body").rules("add", {
    required:true,
    maxlength:5000
  });

}


$(document).ready(function(){
  opNotificationRules();


})
</script>