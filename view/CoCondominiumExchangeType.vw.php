<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_condominium_exchange_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCondominiumExchangeType" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coCondominiumExchangeType->getCreatedBy()).'  el '.$coCondominiumExchangeType->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_condominium_exchange_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCondominiumExchangeType" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_condominium_exchange_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCondominiumExchangeType" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coCondominiumExchangeType->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coCondominiumExchangeType->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $tipo = array();
  $tipo[0]['value'] = 'O';
  $tipo[0]['option'] = 'BCV';
  $tipo[1]['value'] = 'F';
  $tipo[1]['option'] = 'LIBRE';
  
  $html = '<div class="row">
  <div class="mx-auto card col-lg-5 form-frame shadow mb-4">
    <form id="co_condominium_exchange_type_form" name="co_condominium_exchange_type_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Tipo de Cambio</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_condominium_exchange_type" name="is_co_condominium_exchange_type" value="'.$coCondominiumExchangeType->getInitialState().'">
         <input type="hidden" id="co_condominium_exchange_type_id" name="co_condominium_exchange_type_id" maxlength="22" value="'.$coCondominiumExchangeType->getId().'">
         <input type="hidden" id="co_condominium_exchange_type_id_person" name="co_condominium_exchange_type_id_person" maxlength="22" value="'.$coCondominiumExchangeType->getIdPerson().'">

      </div>
      <div class="col-lg-12 container">
       <label for="co_condominium_exchange_type_exchange_type" class="control-label">Tipo:</label>

         <select  id="co_condominium_exchange_type_exchange_type" name="co_condominium_exchange_type_exchange_type" class="co_condominium_exchange_type_exchange_type form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure($tipo,$coCondominiumExchangeType->getExchangeType()).
'      </select>
      <label for="co_condominium_exchange_type_exchange_type" class="error">'.$coCondominiumExchangeType->getAttrError("exchange_type").'</label>
      </div>
   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">

  function coCondominiumExchangeTypeRules(){
  $("#co_condominium_exchange_type_form").validate();
  $("#co_condominium_exchange_type_id_person").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_condominium_exchange_type_exchange_type").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  $('select').niceSelect();
  coCondominiumExchangeTypeRules();


})
</script>