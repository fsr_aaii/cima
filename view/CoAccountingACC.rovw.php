<style type="text/css">
  #co_accounting_dt_wrapper{
    padding: 0;
  }
</style>
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coAccounting->getCreatedBy()).'  el '.$coAccounting->getCreatedDate().'</span>';
  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="co_accounting_form" name="co_accounting_form" method="post" onsubmit="return false" class="form-horizontal info-panel p-0">
      <fieldset>
      <div class="col-lg-12 container title">Aviso de Cobro</div>
      <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_accounting" name="is_co_accounting" value="'.$coAccounting->getInitialState().'">
         <input type="hidden" id="co_accounting_id" name="co_accounting_id" maxlength="22" value="'.$coAccounting->getId().'">
         <input type="hidden" id="co_accounting_id_person" name="co_accounting_id_person" maxlength="22" value="'.$coAccounting->getIdPerson().'">
      </div>
      <div class="col-lg-4 container">
       <label for="" class="control-label">Fecha de Aviso de Cobro:</label>
        <span class="form-control">'.$coAccounting->getPaymentDate().'</span>
      </div>
      <div class="col-lg-4 container">
       <label for="" class="control-label">Procesado por:</label>
        <span class="form-control">'.TUser::description($tfs,$coAccounting->getProcessedBy()).'</span>
      </div>
      <div class="col-lg-4 container">
       <label for="" class="control-label">Procesado el:</label>
        <span class="form-control">'.$coAccounting->getProcessedDate().'</span>
      </div>
     

   </fieldset>
  </form>';

$tfNewData["co_accounting_detail_id_accounting"] = $coAccounting->getId();

  $html.='<div class="row">
           <div class="col-12 container title p-0">Gastos</div>
      ';
   foreach ($coAccountingDetailList as $row){

    $html.='  <div class="col-lg-12 coaching" >
            <div class="d-flex align-items-center p-0" >
                <div class="col-12 p-0">
                    <div class="epic col-12 g-text-1 p-0 m-0">'.$row["account"].'</div>
                    <div class="epic col-5 c-text-4 p-0 m-0">'.$row["description"].'</div>
                    <div class="epic col-4 c-text-4 p-0 m-0 text-right">'.TfWidget::amount($row["amount"]).' VEB</div>
                    <div class="epic col-3 c-text-4 p-0 m-0 text-right">'.TfWidget::amount(round($row["amount"]/$coExchangeRate,2)).' USD</div>
                </div>
            </div>
          </div>';  
   }
   $html.='</div>

   <div class="row">
           <div class="col-12 container title p-0">Gastos Individuales</div>
      ';
   foreach ($coAccountingUnitList as $row){

    $html.='  <div class="col-lg-12 coaching" >
            <div class="d-flex align-items-center p-0" >
                <div class="col-12 p-0">
                    <div class="epic col-12 g-text-1 p-0 m-0">'.$row["unit"].': '.$row["account"].'</div>
                    <div class="epic col-5 c-text-4 p-0 m-0">'.$row["description"].'</div>
                    <div class="epic col-4 c-text-4 p-0 m-0 text-right">'.TfWidget::amount($row["amount"]).' VEB</div>
                    <div class="epic col-3 c-text-4 p-0 m-0 text-right">'.TfWidget::amount(round($row["amount"]/$coExchangeRate,2)).' USD</div>
                </div>
            </div>
          </div>';  
   }
   $html.='</div>

  <div class="row">
    <div class="col-lg-12 p-0">
         <div class="col-12 container title p-0">Resumen</div>
          <table id="co_accounting_dt" class="display responsive" style="width:100%">
  <thead>
    <tr>
      <th class="all">Concepto</th>
      <th class="all text-right">VEB</th>
      <th class="all text-right">USD</th>
    </tr>
  </thead>
  <tbody>    
   
';

  $total=0;
   foreach ($coAccountingSumary as $row){
    $html.='<tr>
      <td>'.$row["account"].'</td>
      <td class="text-right">'.TfWidget::amount($row["amount"]).'</td>
      <td class="text-right">'.TfWidget::amount(round($row["amount"]/$coExchangeRate,2)).'</td>
    </tr>'; 

     $total+= $row["amount"];
   }
   $html.='</tbody>
   <tfoot><tr>
      <th>Total</th>
      <th class="text-right">'.TfWidget::amount($total).'</th>
      <th class="text-right">'.TfWidget::amount(round($total/$coExchangeRate,2)).'</th>
    </tr></tfoot>
</table>
</div>

</div>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
  $(document).ready(function() {
  $("#co_accounting_dt").DataTable({
   info:false,
    paging:false,
    lengthChange:false,
    dom: 'rtip'
  });
});
</script>