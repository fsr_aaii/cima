<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8 info-panel">
            <div class="col-6 container title">Unidades</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnitUser" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
               <a class="btn-guaramo-text" data-tf-table="#co_unit_user_dt" data-tf-file="Co Unit User" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>
             <div class="row col-12">';
$i=0;
   foreach ($coUnitUserList as $row){
     $i++;
    $tfData["co_payment_id_unit"] = $row["id_unit"];


    $html.='  <div class="col-lg-6 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPayment" data-tf-action="AL" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
            <div class="d-flex align-items-center card-01 p-4" >
                <div class="col-12 p-0 m-0">
                    <div class="epic c-text-1 text-white m-0">'.$row["condominium"].'</div>
                    <div class="col-lg-6 epic g-text-2 text-white m-0 p-0">'.$row["name"].'</div>
                    <div class="col-lg-6 epic g-text-5 text-white m-0 p-0 text-right">'.$row["aliquot"].'</div>
                    <div class="col-lg-12 epic c-text-3 text-white m-0 p-0">'.$row["user"].'</div>
                </div>
            </div>
          </div>';  
          if ($i%2==0){
            $html.='</div>
                <div class="row col-12">';
          }  

     }
     $html.='</div></div>';
  

   $html.='
         </div>
        </div>';
 echo $html;
?>
