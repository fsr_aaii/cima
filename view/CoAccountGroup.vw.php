<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_account_group_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="coAccountGroup" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coAccountGroup->getCreatedBy()).'  el '.$coAccountGroup->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_account_group_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="coAccountGroup" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_account_group_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="coAccountGroup" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coAccountGroup->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coAccountGroup->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_account_group_form" name="co_account_group_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Grupo Concepto</div>
     <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_account_group" name="is_co_account_group" value="'.$coAccountGroup->getInitialState().'">
         <input type="hidden" id="co_account_group_id" name="co_account_group_id" maxlength="22" value="'.$coAccountGroup->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="co_account_group_name" class="control-label">Nombre:</label>
        <input type="text" id="co_account_group_name" name="co_account_group_name" class="co_account_group_name form-control"  maxlength="200"  value="'.$coAccountGroup->getName().'"  tabindex="1"/>
      <label for="co_account_group_name" class="error">'.$coAccountGroup->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_account_group_active" class="control-label">Activo:</label>
        <select  id="co_account_group_active" name="co_account_group_active" class="co_account_group_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($coAccountGroup->getActive(),'Y').
'      </select>
      <label for="co_account_group_active" class="error">'.$coAccountGroup->getAttrError("active").'</label>
      </div>

      <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function coAccountGroupRules(){
  $("#co_account_group_form").validate();
  $("#co_account_group_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#co_account_group_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  coAccountGroupRules();


})
</script>