<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8 info-panel">
            <div class="col-6 container title">Gl Area Code</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlAreaCode" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
               <a class="btn-guaramo-text" data-tf-table="#gl_area_code_dt" data-tf-file="Gl Area Code" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>
       <table id="gl_area_code_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Id Country</th>
             <th class="all">Code</th>
             <th class="all">Description</th>
             <th class="all text-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($glAreaCodeList as $row){
    $tfData["gl_area_code_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["id_country"].'</td>
            <td>'.$row["code"].'</td>
            <td>'.$row["description"].'</td>
                 <td class="text-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlAreaCode" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#gl_area_code_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>