<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

 
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coPreviousDue->getCreatedBy()).'  el '.$coPreviousDue->getCreatedDate().'</span>';
$html.= '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="co_previous_due_form" name="co_previous_due_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Deuda Previa</div>
     <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>

     <div class="col-lg-4 container">
       <label for="" class="control-label">Unidad:</label>
       <span class="form-control">'.CoUnit::description($tfs,$tfRequest->co_previous_due_id_person).'</span>
      </div>
      <div class="col-lg-4 container">
       <label for="" class="control-label">Fecha:</label>
        <span class="form-control">'.$coPreviousDue->getPaymentDate().'</span>
      </div>

      <div class="col-lg-4 container">
       <label for="" class="control-label">Monto:</label>
        <span class="form-control">'.$coPreviousDue->getAmount().'</span>
      </div>
      <div class="col-lg-6 container">
       <label for="" class="control-label">Pagado por:</label>
        <span class="form-control">'.TUser::description($tfs,$coPreviousDue->getPaymentedBy()).'</span>
      </div>
      <div class="col-lg-6 container">
       <label for="" class="control-label">Pagado el:</label>
        <span class="form-control">'.$coPreviousDue->getPaymentedDate().'</span>
      </div>
      <div class="col-lg-12 container">
       <label for="" class="control-label">Observation:</label>
        <span class="form-control">'.$coPreviousDue->getObservation().'</span>
      </div>
      
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
