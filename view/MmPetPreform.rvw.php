
<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
            <div class="mx-auto col-lg-10 card shadow mb-4">
            <div class="row">
              <div class="col-6 title">PET Preformas</div>
              <div class="col-6 text-right title-btn">
                 <a class="btn-link" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="MmPetPreform" data-tf-action="AN" onclick="TfRequest.do(this);">
                   Nuevo</a>
                 <a class="btn-link" role="button" data-tf-table="#mm_pet_preform_dt" data-tf-file="Pet Preformas" onclick="TfExport.excel(this);">
                   Excel</a>
               </div>
            </div> 
       <table id="mm_pet_preform_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Elemento</th>
             <th class="all">Color</th>
             <th class="all">Tapa</th>
             <th class="all">Peso</th>
             <th class="none">Creado por</th>
             <th class="none">Creado el</th>
             <th class="all dt-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($mmPetPreformList as $row){
    $tfData["mm_pet_preform_id"] = $row["id"];
    $tfData["fi_transaction_element_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["transaction_element"].'</td>
            <td>'.$row["color"].'</td>
            <td>'.$row["polymer_closure_type"].'</td>
            <td>'.$row["weight"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="dt-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="MmPetPreform" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#mm_pet_preform_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>