<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-6 container title">Co Bill</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBill" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
               <a class="btn-guaramo-text" data-tf-table="#co_bill_dt" data-tf-file="Co Bill" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>
       <table id="co_bill_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Id Accounting</th>
             <th class="all">Id Unit</th>
             <th class="all">Aliquot</th>
             <th class="all">Paymented By</th>
             <th class="all">Paymented Date</th>
             <th class="none">Creado por</th>
             <th class="none">Creado el</th>
             <th class="all text-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($coBillList as $row){
    $tfData["co_bill_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["id_accounting"].'</td>
            <td>'.$row["id_unit"].'</td>
            <td>'.$row["aliquot"].'</td>
            <td>'.$row["paymented_by"].'</td>
            <td>'.$row["paymented_date"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="text-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBill" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#co_bill_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>