<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#gl_person_contact_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonContact" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AU":
   case "AA":
   case "AS":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$glPersonContact->getCreatedBy()).'  el '.$glPersonContact->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#gl_person_contact_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonContact" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#gl_person_contact_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonContact" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
     
    if (TUser::notified($tfs,$glPersonContact->getAccount())=='N'){
      $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#gl_person_contact_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonContact" data-tf-action="AS" onclick="TfRequest.do(this,true);">Notificar</a>';

      $chk='checked';
    }
    break;
  }
  foreach ($glPersonContact->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($glPersonContact->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-6 form-frame shadow mb-4">
    <form id="gl_person_contact_form" name="gl_person_contact_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 title p-0 pt-2">Contacto</div>
      <div class="col-lg-12 p-0 pb-5">'.$audit.'</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_gl_person_contact" name="is_gl_person_contact" value="'.$glPersonContact->getInitialState().'">
         <input type="hidden" id="gl_person_contact_id" name="gl_person_contact_id" maxlength="22" value="'.$glPersonContact->getId().'">
         <input type="hidden" id="gl_person_contact_id_person" name="gl_person_contact_id_person" maxlength="22" value="'.$glPersonContact->getIdPerson().'">

         <input type="hidden" id="gl_person_contact_user" name="gl_person_contact_user" maxlength="22" value="'.$glPersonContact->getUser().'">

      </div>
      <div class="col-lg-12 container">
       <label for="gl_person_contact_name" class="control-label">Nombre:</label>
        <input type="text" id="gl_person_contact_name" name="gl_person_contact_name" class="gl_person_contact_name form-control"  maxlength="250"  value="'.$glPersonContact->getName().'"  tabindex="1"/>
      <label for="gl_person_contact_name" class="error">'.$glPersonContact->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_person_contact_job" class="control-label">Cargo:</label>
       <input type="text" id="gl_person_contact_job" name="gl_person_contact_job" class="gl_person_contact_job form-control"  maxlength="250"  value="'.$glPersonContact->getJob().'"  tabindex="2"/>
      <label for="gl_person_contact_job" class="error">'.$glPersonContact->getAttrError("job").'</label>
      </div>
      <div class="col-lg-6 container">
       <label for="gl_person_contact_account" class="control-label">Correo:</label>
        <input type="text" id="gl_person_contact_account" name="gl_person_contact_account" class="gl_person_contact_account form-control"  maxlength="250"  value="'.$glPersonContact->getAccount().'"  tabindex="4"/>
      <label for="gl_person_contact_account" class="error">'.$glPersonContact->getAttrError("account").'</label>
      </div>
      <div class="col-lg-6 container">
       <label for="gl_person_contact_phone_number" class="control-label">Telefono:</label>
        <input type="text" id="gl_person_contact_phone_number" name="gl_person_contact_phone_number" class="gl_person_contact_phone_number form-control"  maxlength="250"  value="'.$glPersonContact->getPhoneNumber().'"  tabindex="4"/>
      <label for="gl_person_contact_phone_number" class="error">'.$glPersonContact->getAttrError("phone_number").'</label>
      </div>';
      
if ($glPersonContact->getId()!=''){
 $html.='    <div class="col-lg-12 p-0">
  <input  type="checkbox" value="checked" data-tf-form="#gl_person_contact_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonContact" data-tf-action="AU" onclick="glPersonContactUserClick(this);" '.$glPersonContact->getUser().' '.$chk.'>
  <label  for="">Otorgar permiso a la aplicaci&oacute;n</label>
</div>';
}
$html.=' <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
  function glPersonContactRules(){
  $("#gl_person_contact_form").validate();
  $("#gl_person_contact_id_person").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#gl_person_contact_name").rules("add", {
    required:true,
    maxlength:500
  });
  $("#gl_person_contact_job").rules("add", {
    required:true,
    maxlength:500
  });
  $("#gl_person_contact_account").rules("add", {
    maxlength:250
  });

}

function glPersonContactUserClick(element) {
  if($(element).prop("checked") == true){
    $("#gl_person_contact_user").val('checked');
  }else if($(element).prop("checked") == false){
     $("#gl_person_contact_user").val('');
  }
  TfRequest.do(element,false);
}



$(document).ready(function(){
  glPersonContactRules();


})
</script>