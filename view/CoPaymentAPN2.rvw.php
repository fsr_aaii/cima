<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }
  
  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="gl_juridical_person_form" name="gl_juridical_person_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="row">
           <div class="mx-auto col-lg-12 p-0">
            <div class="col-12 container title">Condominio</div>
           </div>  

      <div class="col-lg-5 container">
       <label for="gl_juridical_person_trade_name" class="control-label">Nombre Comercial:</label>
         <span class="form-control">'.$glJuridicalPerson->getTradeName().'</span>
      </div>
      <div class="col-lg-5 container">
       <label for="gl_juridical_person_legal_name" class="control-label">Raz&oacute;n Social:</label>
       <span class="form-control">'.$glJuridicalPerson->getLegalName().'</span>
      </div>
      <div class="col-lg-2 container">
       <label for="gl_juridical_person_nin_type" class="control-label">RIF:</label>
       <span class="form-control">'.$glJuridicalPerson->getNinType().'-'.$glJuridicalPerson->getNin().'-'.$glJuridicalPerson->getNinControlDigit().'</span>
      </div>

      <div class="col-lg-12 container">
       <label for="gl_juridical_person_legal_address" class="control-label">Direcci&oacute;n Fiscal:</label>
        <span class="form-control">'.$glJuridicalPerson->getLegalAddress().'</span>
      </div>

   </fieldset>
  </form>';
  
  if ($glJuridicalPerson->getId()!=''){


   
  $html.='<div class="row">
           <div class="mx-auto col-lg-12 p-0 mt-5 mb-3">
             <div class="col-12 container title">Unidades</div>    
           </div>  
          <div class="row col-12">';
$i=0;
   foreach ($coUnitList as $row){
     $i++;
    $tfData["co_unit_id"] = $row["id"];


    $html.='  <div class="col-lg-4 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnit" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
            <div class="d-flex align-items-center card-01 p-4" >
                <div class="col-12 p-0 m-0">
                    <div class="col-lg-6 epic c-text-1 text-white m-0 p-0">'.$row["name"].'</div>
                    <div class="col-lg-6 epic g-text-5 text-white m-0 p-0 text-right">'.$row["aliquot"].'</div>
                    <div class="col-lg-12 epic c-text-3 text-white m-0 p-0">'.$row["user"].'</div>
                </div>
            </div>
          </div>';  
          if ($i%3==0){
      $html.='</div>
          <div class="row col-12">';
    }  

   }
   $html.='</div></div>';



 $html.='</div>
</div>';
  echo $html;
?>


