<style type="text/css">
  span.form-control{
    border-bottom:none !important;
  }

 .card-02 {
           background: #658E9C;
           height: 90%;
          border-radius: 5px;           

         }


</style>
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_unit_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnit" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coUnit->getCreatedBy()).'  el '.$coUnit->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_unit_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnit" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_unit_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnit" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';    
    break;
  }
  foreach ($coUnit->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coUnit->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_unit_form" name="co_unit_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Unidad</div>
      <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_unit" name="is_co_unit" value="'.$coUnit->getInitialState().'">
         <input type="hidden" id="co_unit_id" name="co_unit_id" maxlength="22" value="'.$coUnit->getId().'">
         <input type="hidden" id="co_unit_id_person" name="co_unit_id_person" co_unit_id_person="22" value="'.$coUnit->getIdPerson().'">
      </div>
      <div class="col-lg-10 container">
       <label for="co_unit_name" class="control-label">Numero:</label>
        <input type="text" id="co_unit_name" name="co_unit_name" class="co_unit_name form-control" maxlength="22"  value="'.$coUnit->getName().'"  tabindex="1"/>
      <label for="co_unit_name" class="error">'.$coUnit->getAttrError("name").'</label>
      </div>
      <div class="col-lg-2 container">
       <label for="co_unit_aliquot" class="control-label">Alicuota:</label>
        <input type="text" id="co_unit_aliquot" name="co_unit_aliquot" class="co_unit_aliquot form-control"  maxlength="22"  value="'.$coUnit->getAliquot().'"  tabindex="3"/>
      <label for="co_unit_aliquot" class="error">'.$coUnit->getAttrError("aliquot").'</label>
      </div>
     

   <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>';
if ($coUnit->getId()!=''){

 $html.= '<div class="row"> 
          <div class="col-12 container title p-0">Estado de Cuenta</div>
          <table id="co_accounting_dt" class=" display responsive" style="width:100%">
  <thead>
    <tr>
      <th class="all">Fecha</th>
      <th class="desktop">Concepto</th>
      <th class="all text-right">Credito</th>
      <th class="all text-right">Debito</th>
      <th class="all text-right">Saldo</th>      
    </tr>
  </thead>
  <tbody>   
  <tr>
      <td>'.$balancePrevious["balance_date"].'</td>
      <td>Saldo mas 120 d&iacute;as</td>';

   if ($balancePrevious["amount"]>0){
    $html.='<td class="text-right"></td>
      <td class="text-right">'.TfWidget::amount($balancePrevious["amount"]).'</td>';
   }elseif ($balancePrevious["amount"]<0){  
      $html.='<td class="text-right">'.TfWidget::amount($balancePrevious["amount"]).'</td>
      <td class="text-right"></td>';
   }else{  
      $html.='<td class="text-right"></td>
      <td class="text-right"></td>';
  }
   $html.='   <td class="text-right">'.TfWidget::amount($balancePrevious["amount"]).'</td>
    </tr>';

   $saldo=$balancePrevious["amount"];
   foreach ($accountStatus as $row){
    
    if ($row["type"]=='R'){
      $credito=0;
      $debito=$row["amount"];
      $saldo+=$row["amount"];
      $ref='Recibo '.$row["ref"];
    }elseif ($row["type"]=='D'){
      $credito=0;
      $debito=$row["amount"];
      $saldo+=$row["amount"];
      $ref='Deuda '.$row["ref"];
    }elseif ($row["type"]=='B'){
      $credito=$row["amount"];
      $debito=0;
      $saldo-=$row["amount"];
      $ref='Pronto Pago '.$row["ref"];
    }else{
      $credito=$row["amount"];
      $debito=0;
      $saldo-=$row["amount"];
      $ref='Abono '.$row["ref"];
    }

    $row["amount"];

    $html.='<tr>
      <td>'.$row["payment_date_confirmed"].'</td>
      <td>'.$ref.'</td>
      <td class="text-right">'.TfWidget::amount($credito).'</td>
      <td class="text-right">'.TfWidget::amount($debito).'</td>
      <td class="text-right">'.TfWidget::amount($saldo).'</td>
    </tr>'; 

     $total+= $row["amount"];
   }
   $html.='</tbody>
</table>
</div>';

if ($saldo>0){
  $promptPayment = CoCondominiumPromptPayment::infoByUnit($tfs,$coUnit->getId());
  $since = CoBill::since($tfs,$coUnit->getId());
  if ($since["days"]>-1){
    if ($since["days"] <= $promptPayment["days"]){

      if ($since["rate"]==0){
        $rate=CoExchange::rateUnitDate($tfs,$coUnit->getId(),date("Y-m-d",strtotime(date("d-m-Y")."- ".$since["days"] ." days")));
       }else{
        $rate=$since["rate"];
       }

       $html.='<p>Esta unidad tiene un deuda de <b>'.TfWidget::amount($saldo).' USD</b> que a la tasa de cambio por pronto pago de (<b>'.TfWidget::amount($rate).' VEB</b>) serian <b>'.TfWidget::amount($saldo*$rate).' VEB </b></p>';
    }else{
       $html.='<p>Esta unidad tiene un deuda de <b>'.TfWidget::amount($saldo).' USD</b> que a la tasa de cambio actual (<b>'.TfWidget::amount($coExchangeRate).' VEB</b>) serian <b>'.TfWidget::amount($saldo*$coExchangeRate).' VEB </b></p>';
    }
  }else{
    $html.='<p>Esta unidad tiene un deuda de <b>'.TfWidget::amount($saldo).' USD</b> que a la tasa de cambio actual (<b>'.TfWidget::amount($coExchangeRate).' VEB</b>) serian <b>'.TfWidget::amount($saldo*$coExchangeRate).' VEB </b></p>';
  }

  
}else{
  $html.='<p>Usted tiene un abono saldo a favor de <b>'.TfWidget::amount(abs($saldo)).' USD</b> que a la tasa de cambio actual (<b>'.TfWidget::amount($coExchangeRate).' VEB</b>) serian <b>'.TfWidget::amount(abs($saldo)*$coExchangeRate).' VEB </b></p>';
}
$z=date("Y");
  $html.=' <div class="row">
           <div class="col-12 container title mt-3 mb-3">Avisos de Cobros</div>
           <div class="col-12 container row justify-content-end  mb-3">
            <div class="mr-0 p-0 col-lg-2 container">
              <select  id="co_bill_year" name="co_bill_year" class="co_bill_year form-control" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBill" data-tf-fn-name="billByYear"  tabindex="1">'.
      TfWidget::selectStructure(TfWidget::years($tfs,2020),$z).
'      </select>
            </div>
           </div>
           <div id="co-bill-all" class="row col-12 p-0">
             <div  id="co-bill-y-'.$z.'" class="co-bill-list row col-12 p-0">';  
   foreach ($coBillList as $row){
    $tfData["co_bill_id"] = $row["id"];

     if ($row["status"]=='PAGADO'){
      $check='<div class="badge-container"> 
                              <span class="badge badge-pill circle-check float-right m-2"><i class="bx bx-check"></i></span>
                            </div>';
    }else{
      $check='';
    }
     
     
    //if ($row["is_rate_current"]=='Y'){
    //  $amount = round($row["amount"]/$coExchangeRate,2);
    //}else{
      $amount = round($row["amount"]/$row["rate_original"],2);  //aqui estaba rate, en lugar de rate_original

     
    //}
    $html.='   <div class="col-lg-4 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBill" data-tf-action="AC" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.$check.'
            <div class="d-flex align-items-center card-01 p-4" >
                <div class="ml-2">
                    <div class="epic col-12 c-text-1 text-white p-0 m-0">'.$row["month"].' '.$row["REF"].'</div>
                    <div class="epic col-12 c-text-6 text-white p-0 m-0">'.$row["payment_date"].'</div>
                   ';// <div class="epic col-12 c-text-3 text-white p-0 m-0">'.TfWidget::amount($row["amount"]).' VEB </div> 
                   $html.=' <div class="epic col-12  c-text-3 text-white p-0 m-0">'.TfWidget::amount($amount).'USD </div> 
                    <div class="epic col-12 c-text-5 text-white p-0 m-0">'.$row["status"].'</div> 
                    
                </div>
            </div>
          </div>';  
   }    
   $html.='</div>
   </div>

    <div class="col-12 container title mb-3">Pagos</div>

<div class="col-12 container row justify-content-end mb-3">
            <div class="mr-0 p-0 col-lg-2 container">
              <select  id="co_payment_year" name="co_payment_year" class="co_payment_year form-control" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPayment" data-tf-fn-name="paymentByYear"  tabindex="1">'.
      TfWidget::selectStructure(TfWidget::years($tfs,2020),$z).
'      </select>
            </div>
           </div>
             <div id="co-payment-all" class="row col-12 p-0">
             <div  id="co-payment-y-'.$z.'" class="co-payment-list row col-12 p-0">';
   foreach ($coPaymentList as $row){
    $tfData["co_payment_id"] = $row["id"];

    if ($row["status"]=='A'){
      $check='<div class="badge-container"> 
                              <span class="badge badge-pill circle-check float-right m-2"><i class="bx bx-check"></i></span>
                            </div>';
      $action = 'AC';           
      $status = 'Aprobado';              
    }elseif ($row["status"]=='R'){
      $check='<div class="badge-container"> 
                              <span class="badge badge-pill circle-block float-right m-2"><i class="bx bx-block"></i></span>
                            </div>';
      $action = 'AC'; 
      $status = 'Rechazado';                     
    }else{
      $check='';
      $action = 'AE';
      $status = 'Por Aprobar';
    }

    if ($row["currency"]=='VEB'){
      $VEB = $row["amount"];
      $USD = round($row["amount"]/$row["rate"],2);  
    }else{
      $USD = $row["amount"];
      $VEB = round($row["amount"]*$row["rate"],2);  
    }
  

    $html.='  <div class="col-lg-4 coaching p-1">
    <div class="col-lg-12 card-01 p-1 " data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPayment" data-tf-action="'.$action.'" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
           '.$check;

            if(substr($row["image_path"],-3)=="pdf"){
              
          //  $html.='<embed src="'.$row["image_path"].'" type="application/pdf" class="card-img-top">' ;
               $html.='<img class="card-img-top" src="../../../upload/payment/pdf.png" alt="">' ;
          }else{
            $html.='   <img class="card-img-top" src="'.$row["image_path"].'" alt="">';
          }
           $html.='  <div class="d-flex align-items-center p-4" >
                <div class="col-12 p-0 ml-2">
                   <div class="epic col-12 c-text-1 text-white p-0 m-0">'.$row["REF"].'</div>
                    <div class="epic col-12 c-text-6 text-white p-0 m-0">'.$row["payment_date"].'</div>
                    <div class="col-12 epic c-text-3 text-white p-0 m-0">'.TfWidget::amount($VEB).' VEB</div>
                    <div class="col-12 epic c-text-3 text-white p-0 m-0">'.TfWidget::amount($USD).' USD</div>
                    <div class="epic col-12 c-text-5 text-white p-0 m-0">'.$status.'</div> 
                </div>

            </div>
          </div>
          </div>'; 


  
   }
   $html.='</div>
          </div>


   </div>';

}
 $html.=' </div>
</div>';

echo $html;

?>
<script type="text/javascript">

  function coUnitRules(){
  $("#co_unit_form").validate();
  $("#co_unit_id_person").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_unit_name").rules("add", {
    required:true,
    maxlength:500
  });
  $("#co_unit_aliquot").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}


$(document).ready(function(){
  $('select').niceSelect();
  coUnitRules();
   $("#co_accounting_dt").DataTable({
   info:false,
    paging:false,
    lengthChange:false,
    dom: 'rtip'
  });

   $("#co_payment_year").on('change', function(e) {
     let year= $(this).children("option:selected").val();

     if ($("#co-payment-y-"+year).length){
      $('.co-payment-list').hide();
      $('#co-payment-y-'+year).show();
     }else{
       $('.co-payment-list').hide();
       let JDATA = new Object();
        JDATA.tfTaskId = $(this).data("tf-task-id");
        JDATA.tfController = $(this).data("tf-controller"); 
        JDATA.tfFnName = $(this).data("tf-fn-name");  
        JDATA.tfData = new Object();
        JDATA.tfData.co_payment_id_unit=$("#co_unit_id").val();
        JDATA.tfData.co_payment_year=year;

         
        let response = TfRequest.fn(JDATA);
        console.log(response);   
        var JRESP=JSON.parse(response);   
        if (JRESP.code=='OK'){
           $('#co-payment-all').append(JRESP.content);   
        }else{
          $('#co-payment-all').append('<div id="co-payment-y-'+year+'" class="co-payment-list row col-12 p-0">Error al consultar</div>');
        }

       
     } 
  });
   
   $("#co_bill_year").on('change', function(e) {
     let year= $(this).children("option:selected").val();

     if ($("#co-bill-y-"+year).length){
      $('.co-bill-list').hide();
      $('#co-bill-y-'+year).show();
     }else{
       $('.co-bill-list').hide();
       let JDATA = new Object();
        JDATA.tfTaskId = $(this).data("tf-task-id");
        JDATA.tfController = $(this).data("tf-controller"); 
        JDATA.tfFnName = $(this).data("tf-fn-name");  
        JDATA.tfData = new Object();
        JDATA.tfData.co_bill_id_unit=$("#co_unit_id").val();
        JDATA.tfData.co_bill_year=year;

         
        let response = TfRequest.fn(JDATA);
        console.log(response);   
        var JRESP=JSON.parse(response);   
        if (JRESP.code=='OK'){
           $('#co-bill-all').append(JRESP.content);   
        }else{
          $('#co-bill-all').append('<div id="co-bill-y-'+year+'" class="co-bill-list row col-12 p-0">Error al consultar</div>');
        }

       
     }
 
  });


})




</script>