<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8 info-panel">
            <div class="col-6 container title">Op Inspection Report Detail</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpInspectionReportDetail" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
               <a class="btn-guaramo-text" data-tf-table="#op_inspection_report_detail_dt" data-tf-file="Op Inspection Report Detail" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>
       <table id="op_inspection_report_detail_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Row</th>
             <th class="all">Hash</th>
             <th class="all">Id Travel</th>
             <th class="all">SICA Guide</th>
             <th class="all">Id List</th>
             <th class="all">Transport</th>
             <th class="all">Name</th>
             <th class="all">Nin</th>
             <th class="all">Chuto</th>
             <th class="all">Id Destination</th>
             <th class="all">BL</th>
             <th class="all">Admission Date</th>
             <th class="all">Admission Time</th>
             <th class="all">Tara Number</th>
             <th class="all">Status 1</th>
             <th class="all">Operation Day</th>
             <th class="all">Survey Guide</th>
             <th class="all">Ocamar Guide</th>
             <th class="all">Store</th>
             <th class="all">Gross Weight</th>
             <th class="all">Tara Weight</th>
             <th class="all">Net Weight</th>
             <th class="all">Aggregate</th>
             <th class="all">ROB</th>
             <th class="all">Status 2</th>
             <th class="all">Id Travel Ocamar</th>
             <th class="all">Departure Date</th>
             <th class="all">Departure Time</th>
             <th class="all">Seal</th>
             <th class="all">Observation</th>
             <th class="all">Active</th>
             <th class="none">Creado por</th>
             <th class="none">Creado el</th>
             <th class="all text-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($opInspectionReportDetailList as $row){
    $tfData["op_inspection_report_detail_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["row"].'</td>
            <td>'.$row["hash"].'</td>
            <td>'.$row["id_travel"].'</td>
            <td>'.$row["SICA_guide"].'</td>
            <td>'.$row["id_list"].'</td>
            <td>'.$row["transport"].'</td>
            <td>'.$row["name"].'</td>
            <td>'.$row["nin"].'</td>
            <td>'.$row["chuto"].'</td>
            <td>'.$row["id_destination"].'</td>
            <td>'.$row["BL"].'</td>
            <td>'.$row["admission_date"].'</td>
            <td>'.$row["admission_time"].'</td>
            <td>'.$row["tara_number"].'</td>
            <td>'.$row["status_1"].'</td>
            <td>'.$row["operation_day"].'</td>
            <td>'.$row["survey_guide"].'</td>
            <td>'.$row["ocamar_guide"].'</td>
            <td>'.$row["store"].'</td>
            <td>'.$row["gross_weight"].'</td>
            <td>'.$row["tara_weight"].'</td>
            <td>'.$row["net_weight"].'</td>
            <td>'.$row["aggregate"].'</td>
            <td>'.$row["ROB"].'</td>
            <td>'.$row["status_2"].'</td>
            <td>'.$row["id_travel_ocamar"].'</td>
            <td>'.$row["departure_date"].'</td>
            <td>'.$row["departure_time"].'</td>
            <td>'.$row["seal"].'</td>
            <td>'.$row["observation"].'</td>
            <td>'.$row["active"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="text-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpInspectionReportDetail" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#op_inspection_report_detail_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>