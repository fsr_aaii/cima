
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-link" role="button" data-tf-form="#mm_pet_preform_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmPetPreform" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$mmPetPreform->getCreatedBy()).'  on '.$mmPetPreform->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn-link" role="button" data-tf-form="#mm_pet_preform_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmPetPreform" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-link" role="button" data-tf-form="#mm_pet_preform_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmPetPreform" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($mmPetPreform->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($mmPetPreform->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }

  $tfData["mm_pet_preform_id"] = $row["id"];
  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="mm_pet_preform_form" name="mm_pet_preform_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">PET Preforma</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_mm_pet_preform" name="is_mm_pet_preform" value="'.$mmPetPreform->getInitialState().'">
         <input type="hidden" id="mm_pet_preform_id" name="mm_pet_preform_id" maxlength="22" value="'.$mmPetPreform->getId().'">
         <input type="hidden" id="fi_transaction_element_id" name="fi_transaction_element_id" maxlength="22" value="'.$fiTransactionElement->getId().'">
      </div>


<div class="col-lg-3 container">
       <label for="fi_transaction_element_code" class="control-label">Codigo:</label>
        <input type="text" id="fi_transaction_element_code" name="fi_transaction_element_code" class="fi_transaction_element_code form-control"  maxlength="15"  value="'.$fiTransactionElement->getCode().'"  tabindex="1"/>
      <label for="fi_transaction_element_code" class="error">'.$fiTransactionElement->getAttrError("code").'</label>
      </div>
       <div class="col-lg-9 container">
       <label for="fi_transaction_element_name" class="control-label">Descripci&oacute;n:</label>
        <input type="text" id="fi_transaction_element_name" name="fi_transaction_element_name" class="fi_transaction_element_name form-control"  maxlength="200"  value="'.$fiTransactionElement->getName().'"  tabindex="2"/>
      <label for="fi_transaction_element_name" class="error">'.$fiTransactionElement->getAttrError("name").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="mm_pet_preform_id_polymer_closure_type" class="control-label">Tapa:</label>
        <select  id="mm_pet_preform_id_polymer_closure_type" name="mm_pet_preform_id_polymer_closure_type" class="mm_pet_preform_id_polymer_closure_type form-control" tabindex="3">
      <option value="">Select a option</option>'.
      tfWidget::selectStructure(MmPolymerClosureType::selectOptions($tfs),$mmPetPreform->getIdPolymerClosureType()).

'      </select>
      <label for="mm_pet_preform_id_polymer_closure_type" class="error">'.$mmPetPreform->getAttrError("id_polymer_closure_type").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="mm_pet_preform_weight" class="control-label">Peso:</label>
        <input type="number" id="mm_pet_preform_weight" name="mm_pet_preform_weight" class="mm_pet_preform_weight form-control"  maxlength="22" min="0" step="0.1" value="'.$mmPetPreform->getWeight().'"  tabindex="4"/>
      <label for="mm_pet_preform_weight" class="error">'.$mmPetPreform->getAttrError("weight").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="mm_pet_preform_id_color" class="control-label">Color:</label>
        <select  id="mm_pet_preform_id_color" name="mm_pet_preform_id_color" class="mm_pet_preform_id_color form-control" tabindex="4">
      <option value="">Select a option</option>'.
      tfWidget::selectStructure(MmColor::selectOptions($tfs),$mmPetPreform->getIdColor()).
'      </select>
      <label for="mm_pet_preform_id_color" class="error">'.$mmPetPreform->getAttrError("id_color").'</label>
      </div>
      '.$audit.'
     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
  </div>
  </div>';

  if (!in_array($tfRequestAction,array("AN","AI"))){
  $html.='<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">

            <div class="row">
              <div class="col-6 title">Presentacion</div>
              <div class="col-6 text-right title-btn">
                 <a class="btn-link" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="FiTransactionElementUm" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt(array('fi_transaction_element_um_id_transaction_element' => $fiTransactionElement->getId())).'" onclick="TfRequest.do(this);">Nuevo</a>
               </div>
            </div>';

     if (count($fiTransactionElementUmList)>0){
       $html.='<table id="fi_transaction_element_um_dt" class="display responsive" style="width:100%">

         <thead>
          <tr>
             <th class="all">Tipo</th>
             <th class="all">U.M.</th>
             <th class="all">Cantidad</th>
             <th class="desktop">Activo</th>
             <th class="all text-right"></th>
           </tr>
         </thead>
         <tbody>';
     foreach ($fiTransactionElementUmList as $row){
      $html.='   <tr>
              <td>'.$row["id_packeting"].'</td>
              <td>'.$row["id_unit_measurement"].'</td>
              <td>'.$row["qty"].'</td>
              <td>'.$row["active"].'</td>
                   <td class="text-right">
                     <a class="btn" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="FiTransactionElementUm" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt(array('fi_transaction_element_um_id' => $row['id'])).'" onclick="TfRequest.do(this);">Editar</a>
                   </td>';  
     }
     $html.='</tr>
             </tbody>
            </table>';
   }else{
    $html.='<span class="font-weight-light font-italic small pl-3">Sin presentacion registrado</span>';
   }
   $html.='</div>
  </div>

  <div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">

            <div class="row">
              <div class="col-6 title">Precios</div>
              <div class="col-6 text-right title-btn">
                 <a class="btn-link" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="SdPricing" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt(array('sd_pricing_id_transaction_element' => $fiTransactionElement->getId())).'" onclick="TfRequest.do(this);">Nuevo</a>
               </div>
            </div>';

     if (count($sdPricingList)>0){
       $html.='<table id="sd_pricing_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Monto</th>
             <th class="desktop">Factor</th>
             <th class="all">Desde</th>
             <th class="all">Hasta</th>           
             <th class="all dt-right"></th>
           </tr>
         </thead>
         <tbody>';
     foreach ($sdPricingList as $row){
      $html.='   <tr>
              <td>'.$row["price"].' BsF.</td>
              <td>'.$row["calculation_factor"].' $</td>
              <td>'.$row["date_from"].'</td>
              <td>'.$row["date_to"].'</td>            
                   <td class="text-right">
                     <a class="btn" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="SdPricing" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt(array('sd_pricing_id' => $row['id'])).'" onclick="TfRequest.do(this);">Editar</a>
                   </td>'; 
 
     }
     $html.='</tr>
             </tbody>
            </table>';
   }else{
    $html.='<span class="font-weight-light font-italic small pl-3">Sin precio registrado</span>';
   }
   $html.='</div>
  </div>

  <div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">

            <div class="row">
              <div class="col-6 title">Descuentos</div>
              <div class="col-6 text-right title-btn">
                 <a class="btn-link" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="SdDiscount" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt(array('sd_discount_id_transaction_element' => $fiTransactionElement->getId())).'" onclick="TfRequest.do(this);">Nuevo</a>
               </div>
            </div>';

     if (count($sdDiscountList)>0){
       $html.='<table id="sd_discount_dt" class="display responsive dataTable no-footer" style="width:100%">
         <thead>
          <tr>
             <th class="all">%</th>
             <th class="all">Desde</th>
             <th class="all">Hasta</th>
             <th class="all text-right"></th>
           </tr>
         </thead>
         <tbody>';
     foreach ($sdDiscountList as $row){
      $html.='   <tr>
              <td>'.$row["percentage"].'</td>
              <td>'.$row["date_from"].'</td>
              <td>'.$row["date_to"].'</td>
                   <td class="text-right">
                     <a class="btn" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="SdDiscount" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt(array('sd_discount_id' => $row['id'])).'" onclick="TfRequest.do(this);">Editar</a>
                   </td>';  
     }
     $html.='</tr>
             </tbody>
            </table>';
   }else{
    $html.='<span class="font-weight-light font-italic small pl-3">Sin descuento registrado</span>';
   }
   $html.='</div>
  </div>

  <div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">

            <div class="row">
              <div class="col-12 title">Metricas</div>
            </div>

  </div>
</div>';
}

 $html.='</div>
</div>';
 echo $html;
?>


<script type="text/javascript">

  $('select').niceSelect();
function mmPetPreformRules(){
  $("#mm_pet_preform_form").validate();

  $("#mm_pet_preform_id_color").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#mm_pet_preform_id_polymer_closure_type").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#mm_pet_preform_weight").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}

$("#fi_transaction_element_um_dt").DataTable({
  pageLength: 8,
  info:false,
  paging:true,
  lengthChange:false,
  dom: 'rtip',
  columnDefs: [
    { width: "3em", targets: -1 },
    { orderable: false, targets: -1 }
  ],
});
$("#sd_pricing_dt").DataTable({
  pageLength: 8,
  info:false,
  paging:true,
  lengthChange:false,
  dom: 'rtip',
  columnDefs: [
    { width: "3em", targets: -1 },
    { orderable: false, targets: -1 }
  ],
});
$("#sd_discount_dt").DataTable({
  pageLength: 8,
  info:false,
  paging:true,
  lengthChange:false,
  dom: 'rtip',
  columnDefs: [
    { width: "3em", targets: -1 },
    { orderable: false, targets: -1 }
  ],
});

$(document).ready(function(){
  mmPetPreformRules();
  $(".section").on('click',function(){  
    if ($(this).hasClass('collapsed')){
      $('#'+$(this).data('form-action')).show();
    }else{
     $('#'+$(this).data('form-action')).hide();
    }
  });
})
</script>