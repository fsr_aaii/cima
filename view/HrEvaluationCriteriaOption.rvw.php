<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8 info-panel">
            <div class="col-6 container title">Hr Evaluation Criteria Option</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEvaluationCriteriaOption" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
               <a class="btn-guaramo-text" data-tf-table="#hr_evaluation_criteria_option_dt" data-tf-file="Hr Evaluation Criteria Option" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>
       <table id="hr_evaluation_criteria_option_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Id Evaluation Criteria</th>
             <th class="all">Name</th>
             <th class="all">Correct</th>
             <th class="all">Active</th>
             <th class="none">Creado por</th>
             <th class="none">Creado el</th>
             <th class="all text-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($hrEvaluationCriteriaOptionList as $row){
    $tfData["hr_evaluation_criteria_option_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["id_evaluation_criteria"].'</td>
            <td>'.$row["name"].'</td>
            <td>'.$row["correct"].'</td>
            <td>'.$row["active"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="text-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEvaluationCriteriaOption" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#hr_evaluation_criteria_option_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>