<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-12 container title">Morosos</div>
             <div class="col-8 container subtitle">Listado</div>
             <div class="col-4 container text-right action">
               <a class="btn-guaramo-text" data-tf-table="#co_bill_dt" data-tf-file="Morosos" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>

       <table id="co_bill_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Unidad</th>
             <th class="all text-right"">Por Pagar</th>
             <th class="all text-right"">Abono</th>
             <th class="all text-right"">Deuda</th>
           </tr>
         </thead>
         <tbody>';
   $usd=0;$due=0;$credit=0;$credit2=0;      
   foreach ($coBillList as $row){

    if (intval($row["usd"])>0){
      $html.='   <tr>
              <td>'.$row["name"].'</td>
              <td class="text-right">'.TfWidget::amount($row["usd"]).'</td>
              <td class="text-right">'.TfWidget::amount($row["credit"]).'</td>
              <td class="text-right">'.TfWidget::amount(($row["usd"]-$row["credit"])).'</td>
              </tr>'; 
      $usd+=$row["usd"]; 
      $credit+=$row["credit"];
      $due+=($row["usd"]-$row["credit"]);
    }else{       
      if (intval($row["credit"])>0){   
        $html2.='   <tr>
                <td>'.$row["name"].'</td>
                <td class="text-right">'.TfWidget::amount($row["credit"]).'</td>
                </tr>';
       $credit2+=$row["credit"];           
      }          
     }                 
   }
   $html.='</tbody>
          </table> 
          <div class="col-12 container p-0 mt-5">
          <div class="row">
            <div class="col-lg-6 container p-0 ">
          <div class="col-8 container subtitle">Abono Saldo a Favor</div>
             <div class="col-4 container text-right action">
               <a class="btn-guaramo-text" data-tf-table="#co_bill2_dt" data-tf-file="Saldo a Favor" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>

       <table id="co_bill2_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Unidad</th>
             <th class="all text-right"">Abono</th>
           </tr>
         </thead>
         <tbody>
          '.$html2.'
         </tbody>
          </table> 
          </div>


          <div class="col-lg-6 container p-0 ">
           <div class="col-8 container subtitle">Resumen</div>
             <div class="col-4 container text-right action">
               <a class="btn-guaramo-text" data-tf-table="#co_bill3_dt" data-tf-file="Balance" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>
             <div class="col-lg-12 container pt-2 mt-5 ">
             <table id="co_bill3_dt" class="display responsive dataTable no-footer dtr-inline" style="width:100%">
         <thead>
          <tr>
             <th class="all"> </th>
             <th class="all text-right"> </th>
           </tr>
         </thead>
         <tbody>
          <tr>
                <th>Por Pagar</th>
                <td class="text-right">'.TfWidget::amount($usd).'</td>
          </tr>
          <tr>
                <th>Abono</th>
                <td class="text-right">'.TfWidget::amount($credit).'</td>
          </tr>
          <tr>
                <th>Abono Saldo a Favor</th>
                <td class="text-right">'.TfWidget::amount($credit2).'</td>
          </tr>
          <tr>
             <th>Deuda</th>
                <td class="text-right">'.TfWidget::amount($due).'</td>
          </tr>
         </tbody>
          </table>  
          </div>
          </div>
          </div>

          <div class="col-lg-12 container p-0 ">
             <div class="col-12 container subtitle">Balance a N dias</div>
            
             <div class="col-lg-12 container pt-2 mt-5 ">
 <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif;" width="100%">
                                                   <tbody>
                                                       <tr>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="10%">
                                                              &nbsp;
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              <b>a 30 días</b>
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              <b>31-60 días</b>
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              <b>61-90 días</b>
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              <b>91-120 días</b>
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              <b>mas 120 días</b>
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                             <b> Total USD</b>
                                                         </td>
                                                      </tr> 
             <tr>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="10%">
                                                            &nbsp;
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              '.TfWidget::amount($balancexx["balance_30"]).'
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              '.TfWidget::amount($balancexx["balance_60"]).'
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              '.TfWidget::amount($balancexx["balance_90"]).'
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              '.TfWidget::amount($balancexx["balance_120"]).'
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              '.TfWidget::amount($balancexx["balance_previous"]).'
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              '.TfWidget::amount($balancexx["balance"]).'
                                                         </td>
                                                      </tr> 
                                                   </tbody>
                                                </table>
             
             </div>
          </div>



        </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">

  $(document).ready(function() {
  $("#co_bill_dt").DataTable({
    info:false,
    paging:false,
    lengthChange:false,
    dom: 'frtip',
    order: [[ 3, "desc" ]]
  });
  $("#co_bill2_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
  });
});
</script>