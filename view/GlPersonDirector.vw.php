<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#gl_person_director_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonDirector" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$glPersonDirector->getCreatedBy()).'  el '.$glPersonDirector->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#gl_person_director_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonDirector" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    if ($glPersonDirector->getActive()=='Y'){
      $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#gl_person_director_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonDirector" data-tf-action="INA" onclick="TfRequest.do(this,true);">Inactivar</a>';
    }else{
       $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#gl_person_director_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonDirector" data-tf-action="ACT" onclick="TfRequest.do(this,true);">Activar</a>';
    }

    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#gl_person_director_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonDirector" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($glPersonDirector->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($glPersonDirector->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="gl_person_director_form" name="gl_person_director_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Miembro Junta</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_gl_person_director" name="is_gl_person_director" value="'.$glPersonDirector->getInitialState().'">
         <input type="hidden" id="gl_person_director_id" name="gl_person_director_id" maxlength="22" value="'.$glPersonDirector->getId().'">
         <input type="hidden" id="gl_person_director_id_person" name="gl_person_director_id_person" maxlength="22" value="'.$glPersonDirector->getIdPerson().'">

      </div>
    
       <div class="col-lg-12 container">
       <label for="gl_person_director_id_user" class="control-label">Unidad:</label>
        <input type="text"
                   placeholder="Escribe nombre del  directivo"
                   value="'.$glPersonDirector->getIdUser().'"
                    class="flexdatalist-json form-control"
                     data-data=\''.$all.'\'
                     data-visible-properties=\'["name","email_account","unit"]\'
                     data-selection-required="true"
                     data-focus-first-result="true"
                     data-min-length="0"
                     data-value-property="id"
                     data-text-property="{name},{email_account}, {unit}"
                     data-search-contain="true"
                   id="gl_person_director_id_user"  name="gl_person_director_id_user" >
       <label for="gl_person_director_id_user" class="error">'.$glPersonDirector->getAttrError("id_user").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_person_director_job" class="control-label">Cargo:</label>
      <input type="text" id="gl_person_director_job" name="gl_person_director_job" class="gl_person_director_job form-control"   value="'.$glPersonDirector->getJob().'"  tabindex="2"/>

      <label for="gl_person_director_job" class="error">'.$glPersonDirector->getAttrError("job").'</label>
      </div>



      
   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>

<script type="text/javascript">
          $("#gl_person_director_id_user").flexdatalist({
             minLength: 0,
             searchByWord: true,
             searchContain: true,
             textProperty: \'{name},{email_account},{unit}\',
             valueProperty: "id",
             selectionRequired: true,
             visibleProperties: ["name","email_account","unit"],
             searchIn:["name","email_account","unit"],
             data: \''.$all.'\'
          });  
    </script>';
  echo $html;
?>
<script type="text/javascript">
  function glPersonDirectorRules(){
  $("#gl_person_director_form").validate();
  $("#gl_person_director_id_person").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#gl_person_director_id_user").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#gl_person_director_job").rules("add", {
    required:true,
    maxlength:500
  });

}


$(document).ready(function(){
  glPersonDirectorRules();


})
</script>