<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

$tfData["co_accounting_id"] = $coBill->getIdAccounting();
  
$buttons ='<a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccounting" data-tf-action="AV" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this,false);">Ver Detalles</a>';


   $tfData["co_bill_id"] = $coBill->getId();

    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBill" data-tf-action="PRT" onclick="co_bill_print(this,false); ">Imprimir</a>';


  foreach ($coBill->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coBill->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
      <div class="col-lg-12 container title">Aviso de Cobro</div>
    
    <div class="row">
    <div class="col-lg-12 p-0">

           <table id="co_bill_dt" class="display responsive" style="width:100%">

  <thead>
    <tr>
      <th class="all">Concepto</th>
      <th class="desktop text-right">Global</th>
      <th class="all text-right">Monto</th>
    </tr>
  </thead>
  <tbody>    
   
';

  $global=0;$total=0;
   foreach ($coBillDetailList as $row){
    $html.='<tr>
      <td>'.$row["account"].'</td>
      <td class="text-right">'.TfWidget::amount($row["amount_gl"]).'</td>
      <td class="text-right">'.TfWidget::amount($row["amount"]).'</td>
    </tr>'; 
     $global+= $row["amount_gl"]; 
     $total+= $row["amount"];
   }
   $html.='</tbody>
   <tfoot><tr>
      <th>Total</th>
      <th class="text-right">'.TfWidget::amount($global).'</th>
      <th class="text-right">'.TfWidget::amount($total).'</th>
    </tr></tfoot>
</table>
</div>
</div>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">




function co_bill_print(element){
     window.open(sessionStorage.getItem('TF_URL_ROOT')+"/print/"+element.dataset.tfTaskId+"/"+element.dataset.tfController+"/"+element.dataset.tfAction+"/"+element.dataset.tfData,'_blank');
}

$(document).ready(function(){
 
  $("#co_bill_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:false,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });


})
</script>