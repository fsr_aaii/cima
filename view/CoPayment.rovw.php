<style type="text/css">
.pdf-fluid {
    max-width: 100%;
    height: 500px;
    padding: 0;
}
</style>
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  if ($coPayment->getCurrency()=='VEB'){
    $currency = 'Bolivar';
    $amountOther = round($coPayment->getAmount()/$coPayment->getRate(),2);
  }else{
    $currency = 'Dolar';
    $amountOther = round($coPayment->getAmount()*$coPayment->getRate(),2);
  }



   if ($coPayment->getCurrencyConfirmed()=='VEB'){
    $currencyConfirmed = 'Bolivar';
    $amountOtherConfirmed = round($coPayment->getAmountConfirmed()/$coPayment->getRateConfirmed(),2);
  }else{
    $currencyConfirmed= 'Dolar';
    $amountOtherConfirmed = round($coPayment->getAmountConfirmed()*$coPayment->getRateConfirmed(),2);
  }
 
   $currencyOther = array('VEB'=>'USD','USD'=>'VEB');

  if ($coPayment->getImagePath()=='..' or $coPayment->getImagePath()==''){
    $photo='../../../asset/images/no-image.png';
  }else{
    $photo=$coPayment->getImagePath();
  }

  $audit='<span class="created_by">Creado por '.TUser::description($tfs,$coPayment->getCreatedBy()).'  el '.$coPayment->getCreatedDate().'</span>';

  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_payment_form" name="co_payment_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Pago</div>
      <div class="col-lg-12 container mb-5">'.$audit.'</div>
       <div class="col-lg-12 container">';

         if(substr($photo,-3)=="pdf"){
            $html.=' <embed src="'.$photo.'" type="application/pdf" class="col-12  pdf-fluid ">' ;

          }else{
            $html .=  '<img id="photo-upload" name="photo-upload" class="col-12  img-fluid " src="'.$photo.'">';
          }

       $html .=  '</div>
      <div class="col-12 container">
        <label for="" class="control-label">Datos de Registro:</label>
      </div>  
      <div class="col-lg-2 container">
       <label for="co_payment_amount" class="control-label">Moneda:</label>
       <span class="form-control">'.$currency.'</span>
      </div>
      <div class="col-lg-2 container">
       <label for="" class="control-label">Fecha Pago:</label>
       <span class="form-control">'.$coPayment->getPaymentDate().'</span>
      </div>
      <div class="col-lg-2 container">
       <label for="" class="control-label">Tasa:</label>
       <span class="form-control">'.TfWidget::amount($coPayment->getRate()).'</span>
      </div>
      <div class="col-lg-3 container">
       <label for="" class="control-label">Monto '.$coPayment->getCurrency().':</label>
        <span class="form-control">'.TfWidget::amount($coPayment->getAmount()).'</span>
      </div>
      <div class="col-lg-3 container">
       <label for="" class="control-label">Monto '.$currencyOther[$coPayment->getCurrency()].':</label>
        <span class="form-control">'.TfWidget::amount($amountOther).'</span>
      </div>

       <div class="col-12 container">
        <label for="" class="control-label">Datos de Confirmaci&oacute;n:</label>
      </div>  
      <div class="col-lg-2 container">
       <label for="co_payment_amount" class="control-label">Moneda:</label>
       <span class="form-control">'.$currencyConfirmed.'</span>
      </div>
      <div class="col-lg-2 container">
       <label for="" class="control-label">Fecha Pago:</label>
       <span class="form-control">'.$coPayment->getPaymentDateConfirmed().'</span>
      </div>
      <div class="col-lg-2 container">
       <label for="" class="control-label">Tasa:</label>
       <span class="form-control">'.TfWidget::amount($coPayment->getRateConfirmed()).'</span>
      </div>
      <div class="col-lg-3 container">
       <label for="" class="control-label">Monto '.$coPayment->getCurrency().':</label>
        <span class="form-control">'.TfWidget::amount($coPayment->getAmountConfirmed()).'</span>
      </div>
      <div class="col-lg-3 container">
       <label for="" class="control-label">Monto '.$currencyOther[$coPayment->getCurrency()].':</label>
        <span class="form-control">'.TfWidget::amount($amountOtherConfirmed).'</span>
      </div>

      <div class="col-lg-12 container">
       <label for="" class="control-label">Observacion:</label>
        <span class="form-control">'.$coPayment->getObservation().'</span>
      </div>

  
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>