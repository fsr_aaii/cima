<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-link" role="button" data-tf-form="#sd_discount_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="sdDiscount" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$sdDiscount->getCreatedBy()).'  on '.$sdDiscount->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn-link" role="button" data-tf-form="#sd_discount_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="sdDiscount" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-link" role="button" data-tf-form="#sd_discount_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="sdDiscount" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($sdDiscount->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($sdDiscount->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="sd_discount_form" name="sd_discount_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title xl mb-5">Descuento</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_sd_discount" name="is_sd_discount" value="'.$sdDiscount->getInitialState().'">
         <input type="hidden" id="sd_discount_id" name="sd_discount_id" maxlength="22" value="'.$sdDiscount->getId().'">
         <input type="hidden" id="sd_discount_id_transaction_element" name="sd_discount_id_transaction_element" maxlength="22" value="'.$sdDiscount->getIdTransactionElement().'">

      </div>
      <div class="col-lg-2 container">
       <label for="sd_discount_percentage" class="control-label">%:</label>
        <input type="number" id="sd_discount_percentage" name="sd_discount_percentage" class="sd_discount_percentage form-control"  min="0" max="100" step="0.1" maxlength="22"  value="'.$sdDiscount->getPercentage().'"  tabindex="2"/>
      <label for="sd_discount_percentage" class="error">'.$sdDiscount->getAttrError("percentage").'</label>
      </div>
      <div class="col-lg-5 container">
       <label for="sd_discount_date_from" class="control-label">Desde:</label>
        <input type="text" id="sd_discount_date_from" name="sd_discount_date_from" class="sd_discount_date_from form-control"  maxlength="22"  value="'.$sdDiscount->getDateFrom().'"  tabindex="3"/>
      <label for="sd_discount_date_from" class="error">'.$sdDiscount->getAttrError("date_from").'</label>
      </div>
      <div class="col-lg-5 container">
       <label for="sd_discount_date_to" class="control-label">Hasta:</label>
        <input type="text" id="sd_discount_date_to" name="sd_discount_date_to" class="sd_discount_date_to form-control"  maxlength="22"  value="'.$sdDiscount->getDateTo().'"  tabindex="4"/>
      <label for="sd_discount_date_to" class="error">'.$sdDiscount->getAttrError("date_to").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function sdDiscountRules(){
  $("#sd_discount_form").validate();
  $("#sd_discount_id_transaction_element").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#sd_discount_percentage").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#sd_discount_date_from").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#sd_discount_date_to").rules("add", {
    isDate:true,
    maxlength:22
  });

}

$("#sd_discount_date_from").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#sd_discount_date_to").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  sdDiscountRules();

  $("#sd_discount_date_from").css('vertical-align','top');
  $("#sd_discount_date_from").mask('y999-m9-d9');
  $("#sd_discount_date_to").css('vertical-align','top');
  $("#sd_discount_date_to").mask('y999-m9-d9');

})
</script>