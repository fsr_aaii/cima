<style type="text/css">
  .form-control.group:focus {
   background-color: #e9ecef !important;
  border: 1px solid #ced4da !important;
  border-right: none !important;
}
.input-group-text{
  font-size: .85rem;
}
</style>
<?php
   if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  if (isset($tUserAlert)){
    $objAlerts.=TfWidget::alertDangerTemplate($tUserAlert);
  }
 

  
  $html = '<div class="row">
  <div class="mx-auto card col-lg-4 form-frame shadow mb-4">
   
    <form id="t_user_form" name="t_user_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
    <div class="col-12 container title ">Copropietario</div>
    <div class="col-12 container information mb-3">
Ingrese la cuenta de correo electrónico del copropietario que desea crear o modificar
             </div> 
    <label for="" class="error"></label>         
      <div class="col-lg-12 container">
        '.$objAlerts.'
      </div>
     
  <div class="col-lg-12 container">
   <label for="t_user_email_account" class="control-label">Correo Electronico:</label>
     <div role="wrapper" class="epic-input-group input-group">
       <input type="text" id="t_user_email_account" name="t_user_email_account" class="t_user_email_account cm_post_search form-control group"  maxlength="100"  value="'.$t_user_email_account.'"  tabindex="1" style="vertical-align: top;"/>
       
       <span class="input-group-append" role="right-icon">
                            <a class="btn btn-outline-secondary border-left-0" role="button" data-tf-form="#t_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="TUser" data-tf-action="AS" onclick="TfRequest.do(this,false);"><i class="bx bx-search"></i></a>
                          </span>
     </div>
     <label for="t_user_email_account" class="error"></label>
   </div>

   
 
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;


?>



<script type="text/javascript">

function tUserRules(){

  $("#t_user_form").validate();

  $(".t_user_email_account").rules("add", {
    required:true,
    maxlength:500
  });
  
}

$(document).ready(function(){
  tUserRules();

})

</script>

