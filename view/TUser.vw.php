<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#t_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="TUser" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AM":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$tUser->getCreatedBy()).'  el '.$tUser->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#t_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="TUser" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
     $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#t_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="TUser" data-tf-action="AM" onclick="TfRequest.do(this,true);">Enviar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#t_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="TUser" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($tUser->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($tUser->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-5 form-frame shadow mb-4">
    <form id="t_user_form" name="t_user_form" method="post" onsubmit="return false" class="form-horizontal info-panel p-0">
      <fieldset>
      <div class="col-lg-12 container title">Copropietario</div>
      <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_t_user" name="is_t_user" value="'.$tUser->getInitialState().'">
         <input type="hidden" id="t_user_id" name="t_user_id" maxlength="22" value="'.$tUser->getId().'">
      </div>
       <div class="col-lg-8 container">
       <label for="t_user_name" class="control-label">Nombre:</label>
        <input type="text" id="t_user_name" name="t_user_name" class="t_user_name form-control"  maxlength="200"  value="'.$tUser->getName().'"  tabindex="1"/>
      <label for="t_user_name" class="error">'.$tUser->getAttrError("name").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="t_user_dni" class="control-label">RIF:</label>
        <input type="text" id="t_user_dni" name="t_user_dni" class="t_user_dni form-control"  maxlength="20"  value="'.$tUser->getDni().'"  tabindex="3"/>
      <label for="t_user_phone_number" class="error">'.$tUser->getAttrError("dni").'</label>
      </div>  
      <div class="col-lg-8 container">
       <label for="t_user_login" class="control-label">Email:</label>
        <input type="email" id="t_user_login" name="t_user_login" class="t_user_login form-control"  maxlength="200"  value="'.$tUser->getLogin().'"  tabindex="1"/>
      <label for="t_user_login" class="error">'.$tUser->getAttrError("login").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="t_user_phone_number" class="control-label">Telefono:</label>
        <input type="text" id="t_user_phone_number" name="t_user_phone_number" class="t_user_phone_number form-control"  maxlength="20"  value="'.$tUser->getPhoneNumber().'"  tabindex="2"/>
      <label for="t_user_phone_number" class="error">'.$tUser->getAttrError("phone_number").'</label>
      </div> 
       
      
   <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>';

if ($tUser->getId()!=''){
$tfNewData["co_unit_user_id_user"] = $tUser->getId();

$html.='<div class="row">
           <div class="mx-auto col-lg-12 p-0 mt-5 mb-3">
            <div class="col-6 container title">Unidades</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnitUser" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt($tfNewData).'" onclick="TfRequest.do(this);">
                 Nueva
               </a>
             </div>
           </div>  
      <div class="row col-12">';
$i=0;
   foreach ($coUnitUserList as $row){
     $i++;
    $tfData["co_unit_user_id"] = $row["id"];


    $html.='  <div class="col-lg-6 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnitUser" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
            <div class="d-flex align-items-center card-01 p-4" >
                <div class="col-12 p-0 m-0">
                    <div class="epic c-text-1 text-white m-0">'.$row["condominium"].'</div>
                    <div class="col-lg-6 epic g-text-2 text-white m-0 p-0">'.$row["name"].'</div>
                    <div class="col-lg-6 epic g-text-5 text-white m-0 p-0 text-right">'.$row["aliquot"].'</div>
                    <div class="col-lg-12 epic c-text-3 text-white m-0 p-0">'.$row["user"].'</div>
                </div>
            </div>
          </div>';  
          if ($i%2==0){
      $html.='</div>
          <div class="row col-12">';
    }  

     }
     $html.='</div></div>';
   }
   $html.='

 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
  function tUserRules(){
  $("#t_user_form").validate();
  $("#t_user_login").rules("add", {
    email:true,
    required:true,
    maxlength:500
  });
  $("#t_user_dni").rules("add", {
    required:true,
    maxlength:20
  });
  
  $("#t_user_name").rules("add", {
    maxlength:200
  });
  $("#t_user_phone_number").rules("add", {
    maxlength:20
  });

}


$(document).ready(function(){
  tUserRules();

})
</script>