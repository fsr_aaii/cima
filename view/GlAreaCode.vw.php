<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#gl_area_code_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlAreaCode" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#gl_area_code_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlAreaCode" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#gl_area_code_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlAreaCode" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($glAreaCode->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($glAreaCode->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="gl_area_code_form" name="gl_area_code_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Gl Area Code</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_gl_area_code" name="is_gl_area_code" value="'.$glAreaCode->getInitialState().'">
         <input type="hidden" id="gl_area_code_id" name="gl_area_code_id" maxlength="22" value="'.$glAreaCode->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="gl_area_code_id_country" class="control-label">Id Country:</label>
        <select  id="gl_area_code_id_country" name="gl_area_code_id_country" class="gl_area_code_id_country form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(GlCountry::selectOptions($tfs),$glAreaCode->getIdCountry()).
'      </select>
      <label for="gl_area_code_id_country" class="error">'.$glAreaCode->getAttrError("id_country").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_area_code_code" class="control-label">Code:</label>
        <input type="text" id="gl_area_code_code" name="gl_area_code_code" class="gl_area_code_code form-control"  maxlength="22"  value="'.$glAreaCode->getCode().'"  tabindex="2"/>
      <label for="gl_area_code_code" class="error">'.$glAreaCode->getAttrError("code").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_area_code_description" class="control-label">Description:</label>
        <input type="text" id="gl_area_code_description" name="gl_area_code_description" class="gl_area_code_description form-control"  maxlength="40"  value="'.$glAreaCode->getDescription().'"  tabindex="3"/>
      <label for="gl_area_code_description" class="error">'.$glAreaCode->getAttrError("description").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function glAreaCodeRules(){
  $("#gl_area_code_form").validate();
  $("#gl_area_code_id_country").rules("add", {
    required:true,
    maxlength:2
  });
  $("#gl_area_code_code").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#gl_area_code_description").rules("add", {
    required:true,
    maxlength:40
  });

}


$(document).ready(function(){
  glAreaCodeRules();


})
</script>