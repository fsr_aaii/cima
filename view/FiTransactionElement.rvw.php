<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-6 title">Fi Transaction Element</div>
             <div class="col-6 text-right action">
               <a class="btn" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="FiTransactionElement" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo</div>
               </a>
               <a class="btn" role="button" data-tf-table="#fi_transaction_element_dt" data-tf-file="Fi Transaction Element" onclick="TfExport.excel(this);">
                 Excel</div>
               </a>
             </div>
       <table id="fi_transaction_element_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Name</th>
             <th class="all">Code</th>
             <th class="all">Type</th>
             <th class="all">Id Transaction Element Rank</th>
             <th class="all">Id Transaction Type</th>
             <th class="all">Id Element Type</th>
             <th class="all">Active</th>
             <th class="none">Created by</th>
             <th class="none">Created date</th>
             <th class="all dt-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($fiTransactionElementList as $row){
    $tfData["fi_transaction_element_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["name"].'</td>
            <td>'.$row["code"].'</td>
            <td>'.$row["type"].'</td>
            <td>'.$row["id_transaction_element_rank"].'</td>
            <td>'.$row["id_transaction_type"].'</td>
            <td>'.$row["id_element_type"].'</td>
            <td>'.$row["active"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="dt-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="FiTransactionElement" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#fi_transaction_element_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>