<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#gl_juridical_person_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlCustomer" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$glJuridicalPerson->getCreatedBy()).'  el '.$glJuridicalPerson->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#gl_juridical_person_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlCustomer" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#gl_juridical_person_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlCustomer" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($glJuridicalPerson->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($glJuridicalPerson->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="gl_juridical_person_form" name="gl_juridical_person_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Condominio</div>
     <div class="col-lg-12 container">'.$audit.'</div>
      <div class="col-lg-12 container mt-5">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_gl_person" name="is_gl_person" value="'.$glPerson->getInitialState().'">
         <input type="hidden" id="gl_person_id" name="gl_person_id" maxlength="22" value="'.$glPerson->getId().'">
         <input type="hidden" id="gl_person_person_type" name="gl_person_person_type" maxlength="22" value="'.$glPerson->getPersonType().'">
         
         <input type="hidden" id="is_gl_juridical_person" name="is_gl_juridical_person" value="'.$glJuridicalPerson->getInitialState().'">
         <input type="hidden" id="gl_juridical_person_id" name="gl_juridical_person_id" maxlength="22" value="'.$glJuridicalPerson->getId().'">

      </div>
      <div class="col-lg-1 col-2 container">
       <label for="gl_juridical_person_nin_type" class="control-label">RIF:</label>
        <input type="text" id="gl_juridical_person_nin_type" name="gl_juridical_person_nin_type" class="gl_juridical_person_nin_type form-control"  maxlength="1"  value="'.$glJuridicalPerson->getNinType().'"  tabindex="1"/>
      <label for="gl_juridical_person_nin_type" class="error">'.$glJuridicalPerson->getAttrError("nin_type").'</label>
      </div>
      <div class="col-lg-2 col-8 container">
       <label for="gl_juridical_person_nin" class="control-label">&nbsp</label>
        <input type="text" id="gl_juridical_person_nin" name="gl_juridical_person_nin" class="gl_juridical_person_nin form-control"  maxlength="22"  value="'.$glJuridicalPerson->getNin().'"  tabindex="2"/>
      <label for="gl_juridical_person_nin" class="error">'.$glJuridicalPerson->getAttrError("nin").'</label>
      </div>
      <div class="col-lg-1 col-2 container">
       <label for="gl_juridical_person_nin_control_digit" class="control-label">&nbsp</label>
        <input type="text" id="gl_juridical_person_nin_control_digit" name="gl_juridical_person_nin_control_digit" class="gl_juridical_person_nin_control_digit form-control"  maxlength="22"  value="'.$glJuridicalPerson->getNinControlDigit().'"  tabindex="3"/>
      <label for="gl_juridical_person_nin_control_digit" class="error">'.$glJuridicalPerson->getAttrError("nin_control_digit").'</label>
      </div>
      <div class="col-lg-8 container">
       <label for="gl_juridical_person_trade_name" class="control-label">Nombre Comercial:</label>
        <input type="text" id="gl_juridical_person_trade_name" name="gl_juridical_person_trade_name" class="gl_juridical_person_trade_name form-control"  maxlength="200"  value="'.$glJuridicalPerson->getTradeName().'"  tabindex="4"/>
      <label for="gl_juridical_person_trade_name" class="error">'.$glJuridicalPerson->getAttrError("trade_name").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="gl_juridical_person_email_account" class="control-label">Email:</label>
        <input type="email" id="gl_juridical_person_email_account" name="gl_juridical_person_email_account" class="gl_juridical_person_email_account form-control"  maxlength="500"  value="'.$glJuridicalPerson->getEmailAccount().'"  tabindex="5"/>
      <label for="gl_juridical_person_email_account" class="error">'.$glJuridicalPerson->getAttrError("email_account").'</label>
      </div>
      <div class="col-lg-8 container">
       <label for="gl_juridical_person_legal_name" class="control-label">Raz&oacute;n Social:</label>
        <input type="text" id="gl_juridical_person_legal_name" name="gl_juridical_person_legal_name" class="gl_juridical_person_legal_name form-control"  maxlength="200"  value="'.$glJuridicalPerson->getLegalName().'"  tabindex="6"/>
      <label for="gl_juridical_person_legal_name" class="error">'.$glJuridicalPerson->getAttrError("legal_name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_juridical_person_legal_address" class="control-label">Direcci&oacute;n Fiscal:</label>
        <textarea id="gl_juridical_person_legal_address" name="gl_juridical_person_legal_address" class="gl_juridical_person_legal_address form-control" rows="3" tabindex="6" >'.$glJuridicalPerson->getLegalAddress().'</textarea>
      <label for="gl_juridical_person_legal_address" class="error">'.$glJuridicalPerson->getAttrError("legal_address").'</label>
      </div>

  <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>';
  
 /* if ($glJuridicalPerson->getId()!=''){

   $tfNewData["gl_person_contact_id_person"] = $glJuridicalPerson->getId();
       
   $html.='<div class="row">
           <div class="mx-auto col-lg-12 p-0">
            <div class="col-6 container title">Junta</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonContact" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt($tfNewData).'" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
             </div>
           </div>  
      ';
   foreach ($glPersonContactList as $row){
    $tfData["gl_person_contact_id"] = $row["id"];
    $html.='  <div class="col-lg-4 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlPersonContact" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
            <div class="d-flex align-items-center bg-info p-4" >
                <div class="ml-2">
                    <div class="epic h2 text-white m-0">'.$row["name"].'</div>
                    <div class="epic h4 text-white m-0">'.$row["job"].'</div>
                    <div class="epic h5 text-white m-0"><i class="bx bx-envelope"></i> '.$row["account"].'</div>
                    <div class="epic h5 text-white m-0"><i class="bx bx-phone"></i> '.$row["phone_number"].'</div>
                </div>
            </div>
          </div>';  
   }
   $html.='</div>
        </div>';
  }
*/
 $html.='</div>
</div>';
  echo $html;
?>
<script type="text/javascript">

  function glJuridicalPersonRules(){
  $("#gl_juridical_person_form").validate();
  $("#gl_juridical_person_nin_type").rules("add", {
    required:true,
    maxlength:1
  });
  $("#gl_juridical_person_nin").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#gl_juridical_person_nin_control_digit").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#gl_juridical_person_legal_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#gl_juridical_person_trade_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#gl_juridical_person_legal_address").rules("add", {
    required:true,
    maxlength:1000
  });
  $("#gl_juridical_person_email_account").rules("add", {
    email:true,
    maxlength:500
  });
  $("#gl_juridical_person_phone_number").rules("add", {
    maxlength:20
  });

}


$(document).ready(function(){
  glJuridicalPersonRules();

  
})
</script>