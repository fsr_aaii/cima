<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-link" role="button" data-tf-form="#sd_pricing_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="sdPricing" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$sdPricing->getCreatedBy()).'  on '.$sdPricing->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn-link" role="button" data-tf-form="#sd_pricing_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="sdPricing" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-link" role="button" data-tf-form="#sd_pricing_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="sdPricing" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($sdPricing->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($sdPricing->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">
    <form id="sd_pricing_form" name="sd_pricing_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title xl mb-5">Precio</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_sd_pricing" name="is_sd_pricing" value="'.$sdPricing->getInitialState().'">
         <input type="hidden" id="sd_pricing_id" name="sd_pricing_id" maxlength="22" value="'.$sdPricing->getId().'">
         <input type="hidden" id="sd_pricing_id_transaction_element" name="sd_pricing_id_transaction_element" maxlength="22" value="'.$sdPricing->getIdTransactionElement().'">

      </div>
      <div class="col-lg-3 container">
       <label for="sd_pricing_price" class="control-label">Monto (BsF.):</label>
        <input type="number" id="sd_pricing_price" name="sd_pricing_price" class="sd_pricing_price form-control"  maxlength="22"  value="'.$sdPricing->getPrice().'"  step="0.1" tabindex="2"/>
      <label for="sd_pricing_price" class="error">'.$sdPricing->getAttrError("price").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="sd_pricing_calculation_factor" class="control-label">Factor ($):</label>
        <input type="number" id="sd_pricing_calculation_factor" name="sd_pricing_calculation_factor" class="sd_pricing_calculation_factor form-control"  maxlength="22"  value="'.$sdPricing->getCalculationFactor().'"  step="0.1" tabindex="3"/>
      <label for="sd_pricing_calculation_factor" class="error">'.$sdPricing->getAttrError("calculation_factor").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="sd_pricing_date_from" class="control-label">Desde:</label>
        <input type="text" id="sd_pricing_date_from" name="sd_pricing_date_from" class="sd_pricing_date_from form-control"  maxlength="22"  value="'.$sdPricing->getDateFrom().'"  tabindex="4"/>
      <label for="sd_pricing_date_from" class="error">'.$sdPricing->getAttrError("date_from").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="sd_pricing_date_to" class="control-label">Hasta:</label>
        <input type="text" id="sd_pricing_date_to" name="sd_pricing_date_to" class="sd_pricing_date_to form-control"  maxlength="22"  value="'.$sdPricing->getDateTo().'"  tabindex="5"/>
      <label for="sd_pricing_date_to" class="error">'.$sdPricing->getAttrError("date_to").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function sdPricingRules(){
  $("#sd_pricing_form").validate();
  $("#sd_pricing_id_transaction_element").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#sd_pricing_price").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#sd_pricing_calculation_factor").rules("add", {
    number:true,
    maxlength:22
  });
  $("#sd_pricing_date_from").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#sd_pricing_date_to").rules("add", {
    isDate:true,
    maxlength:22
  });

}

$("#sd_pricing_date_from").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#sd_pricing_date_to").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  sdPricingRules();

  $("#sd_pricing_date_from").css('vertical-align','top');
  $("#sd_pricing_date_from").mask('y999-m9-d9');
  $("#sd_pricing_date_to").css('vertical-align','top');
  $("#sd_pricing_date_to").mask('y999-m9-d9');

})
</script>