<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#hr_evaluation_criteria_option_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="hrEvaluationCriteriaOption" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$hrEvaluationCriteriaOption->getCreatedBy()).'  el '.$hrEvaluationCriteriaOption->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#hr_evaluation_criteria_option_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="hrEvaluationCriteriaOption" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#hr_evaluation_criteria_option_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="hrEvaluationCriteriaOption" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrEvaluationCriteriaOption->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrEvaluationCriteriaOption->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="hr_evaluation_criteria_option_form" name="hr_evaluation_criteria_option_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr Evaluation Criteria Option</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_evaluation_criteria_option" name="is_hr_evaluation_criteria_option" value="'.$hrEvaluationCriteriaOption->getInitialState().'">
         <input type="hidden" id="hr_evaluation_criteria_option_id" name="hr_evaluation_criteria_option_id" maxlength="22" value="'.$hrEvaluationCriteriaOption->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_evaluation_criteria_option_id_evaluation_criteria" class="control-label">Id Evaluation Criteria:</label>
        <select  id="hr_evaluation_criteria_option_id_evaluation_criteria" name="hr_evaluation_criteria_option_id_evaluation_criteria" class="hr_evaluation_criteria_option_id_evaluation_criteria form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrEvaluationCriteria::selectOptions($tfs),$hrEvaluationCriteriaOption->getIdEvaluationCriteria()).
'      </select>
      <label for="hr_evaluation_criteria_option_id_evaluation_criteria" class="error">'.$hrEvaluationCriteriaOption->getAttrError("id_evaluation_criteria").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_evaluation_criteria_option_name" class="control-label">Name:</label>
        <input type="text" id="hr_evaluation_criteria_option_name" name="hr_evaluation_criteria_option_name" class="hr_evaluation_criteria_option_name form-control"  maxlength="200"  value="'.$hrEvaluationCriteriaOption->getName().'"  tabindex="2"/>
      <label for="hr_evaluation_criteria_option_name" class="error">'.$hrEvaluationCriteriaOption->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_evaluation_criteria_option_correct" class="control-label">Correct:</label>
        <input type="text" id="hr_evaluation_criteria_option_correct" name="hr_evaluation_criteria_option_correct" class="hr_evaluation_criteria_option_correct form-control"  maxlength="1"  value="'.$hrEvaluationCriteriaOption->getCorrect().'"  tabindex="3"/>
      <label for="hr_evaluation_criteria_option_correct" class="error">'.$hrEvaluationCriteriaOption->getAttrError("correct").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_evaluation_criteria_option_active" class="control-label">Active:</label>
        <select  id="hr_evaluation_criteria_option_active" name="hr_evaluation_criteria_option_active" class="hr_evaluation_criteria_option_active form-control" tabindex="4">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($hrEvaluationCriteriaOption->getActive(),'Y').
'      </select>
      <label for="hr_evaluation_criteria_option_active" class="error">'.$hrEvaluationCriteriaOption->getAttrError("active").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrEvaluationCriteriaOptionRules(){
  $("#hr_evaluation_criteria_option_form").validate();
  $("#hr_evaluation_criteria_option_id_evaluation_criteria").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_evaluation_criteria_option_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#hr_evaluation_criteria_option_correct").rules("add", {
    required:true,
    maxlength:1
  });
  $("#hr_evaluation_criteria_option_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  hrEvaluationCriteriaOptionRules();


})
</script>