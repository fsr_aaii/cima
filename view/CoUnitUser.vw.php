<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_unit_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnitUser" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
   case "ACT":
   case "INA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coUnitUser->getCreatedBy()).'  el '.$coUnitUser->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_unit_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnitUser" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    if ($coUnitUser->getActive()=='Y'){
      $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_unit_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnitUser" data-tf-action="INA" onclick="TfRequest.do(this,true);">Inactivar</a>';
    }else{
       $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_unit_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnitUser" data-tf-action="ACT" onclick="TfRequest.do(this,true);">Activar</a>';
    }
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_unit_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoUnitUser" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coUnitUser->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coUnitUser->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-6 form-frame shadow mb-4">
    <form id="co_unit_user_form" name="co_unit_user_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Copropietario</div>
      <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_unit_user" name="is_co_unit_user" value="'.$coUnitUser->getInitialState().'">
         <input type="hidden" id="co_unit_user_id" name="co_unit_user_id" maxlength="22" value="'.$coUnitUser->getId().'">
         <input type="hidden" id="co_unit_user_id_user" name="co_unit_user_id_user" maxlength="22" value="'.$coUnitUser->getIdUser().'">

      </div>
    <div class="col-lg-12 container">
       <label for="co_unit_user_id_unit" class="control-label">Unidad:</label>
        <input type="text"
                   placeholder="Escribe nombre de la unidad"
                   value="'.$coUnitUser->getIdUnit().'"
                    class="flexdatalist-json form-control"
                     data-data=\''.$all.'\'
                     data-visible-properties=\'["condominium","name","aliquot"]\'
                     data-selection-required="true"
                     data-focus-first-result="true"
                     data-min-length="0"
                     data-value-property="id"
                     data-text-property="{condominium},{name}, {aliquot}"
                     data-search-contain="true"
                   id="co_unit_user_id_unit"  name="co_unit_user_id_unit" >
       <label for="co_unit_user_id_unit" class="error">'.$coUnitUser->getAttrError("id_unit").'</label>
      </div>

   <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>

<script type="text/javascript">
          $("#co_unit_user_id_unit").flexdatalist({
             minLength: 0,
             searchByWord: true,
             searchContain: true,
             textProperty: \'{condominium},{name},{aliquot}\',
             valueProperty: "id",
             selectionRequired: true,
             visibleProperties: ["condominium","name","aliquot"],
             searchIn:["condominium","name","aliquot"],
             data: \''.$all.'\'
          });  
    </script>';
  echo $html;
?>
<script type="text/javascript">
  function coUnitUserRules(){
  $("#co_unit_user_form").validate();
  $("#co_unit_user_id_unit").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_unit_user_id_user").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}


$(document).ready(function(){
  coUnitUserRules();


})
</script>