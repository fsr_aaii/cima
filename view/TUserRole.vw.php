<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#t_user_role_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="TUserRole" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$tUserRole->getCreatedBy()).'  el '.$tUserRole->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#t_user_role_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="TUserRole" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#t_user_role_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="TUserRole" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($tUserRole->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($tUserRole->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="t_user_role_form" name="t_user_role_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">T User Role</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_t_user_role" name="is_t_user_role" value="'.$tUserRole->getInitialState().'">
         <input type="hidden" id="t_user_role_id" name="t_user_role_id" maxlength="22" value="'.$tUserRole->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="t_user_role_id_user" class="control-label">Id User:</label>
        <select  id="t_user_role_id_user" name="t_user_role_id_user" class="t_user_role_id_user form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(TUser::selectOptions($tfs),$tUserRole->getIdUser()).
'      </select>
      <label for="t_user_role_id_user" class="error">'.$tUserRole->getAttrError("id_user").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="t_user_role_id_functional_area_role" class="control-label">Id Functional Area Role:</label>
        <input type="text" id="t_user_role_id_functional_area_role" name="t_user_role_id_functional_area_role" class="t_user_role_id_functional_area_role form-control"  maxlength="22"  value="'.$tUserRole->getIdFunctionalAreaRole().'"  tabindex="2"/>
      <label for="t_user_role_id_functional_area_role" class="error">'.$tUserRole->getAttrError("id_functional_area_role").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="t_user_role_active" class="control-label">Active:</label>
        <select  id="t_user_role_active" name="t_user_role_active" class="t_user_role_active form-control" tabindex="3">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($tUserRole->getActive(),'Y').
'      </select>
      <label for="t_user_role_active" class="error">'.$tUserRole->getAttrError("active").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function tUserRoleRules(){
  $("#t_user_role_form").validate();
  $("#t_user_role_id_user").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#t_user_role_id_functional_area_role").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#t_user_role_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  tUserRoleRules();
  $('select').niceSelect();

})
</script>