<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8 info-panel">
            <div class="col-6 container title">Hr User Evaluation</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrUserEvaluation" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
               <a class="btn-guaramo-text" data-tf-table="#hr_user_evaluation_dt" data-tf-file="Hr User Evaluation" onclick="TfExport.excel(this);">
                 Excel
               </a>
             </div>
       <table id="hr_user_evaluation_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Id User</th>
             <th class="all">Date</th>
             <th class="all">Active</th>
             <th class="all">Id Evaluation Type</th>
             <th class="all">Evaluator Type</th>
             <th class="none">Creado por</th>
             <th class="none">Creado el</th>
             <th class="all text-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($hrUserEvaluationList as $row){
    $tfData["hr_user_evaluation_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["id_user"].'</td>
            <td>'.$row["date"].'</td>
            <td>'.$row["active"].'</td>
            <td>'.$row["id_evaluation_type"].'</td>
            <td>'.$row["evaluator_type"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="text-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrUserEvaluation" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#hr_user_evaluation_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>