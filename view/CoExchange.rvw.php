<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $tfData["co_exchange_id_person"] = $tfRequest->co_exchange_id_person;

  $html='<div class="row">
           <div class="mx-auto col-lg-6 tf-card shadow mb-4">
             <div class="row">
             <div class="col-6 container title">Tasas de Cambio</div>
             <div class="col-6 container text-right action">
               <a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoExchange" data-tf-action="AN" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                 Nuevo
               </a>
             </div>  
             </div> 
<main class="timeline">';
   foreach ($coExchangeList as $row){

  $html.='<p><b>'.TfWidget::amount($row["rate"]).'</b> <br>  <small>'.TfWidget::dateTimeFormat($row["created_date"]).'</small></p>';
}
   $html.='</main>


         </div>
        </div>';
 echo $html;
?>

