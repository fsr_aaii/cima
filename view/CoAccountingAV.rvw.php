<style type="text/css">
   .card.invoce {
          padding: .5rem .5rem .5rem .5rem;
           background: none;
          border-radius: 5px;  
          font-size: .75rem;         

         }
</style>
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  $html = '<div class="row">
  <div class="mx-auto card col-lg-8 form-frame shadow mb-4">';

$tfNewData["co_accounting_detail_id_accounting"] = $coAccounting->getId();

  $html.='<div class="row">
           <div class="mx-auto col-lg-12 p-0">
            <div class="col-12 container title p-4">Gastos</div>
             
           </div>  
      ';
   foreach ($coAccountingDetailList as $row){
    if ($row["rate"]!=''){
       $rate=$row["rate"];
    }else{
       $rate=$coExchangeRate;
    }
    
    $html.='  <div class="col-lg-12" >
            <div class="d-flex align-items-center invoce p-0" >
                <div class="col-12 p-0 ml-2 mb-2">
                    <div class="epic col-12 p-0 m-0"><b>'.$row["account"].'</b></div>
                    <div class="epic col-lg-5 p-0 m-0">'.$row["description"].'</div>
                    <div class="epic col-lg-4 p-0 m-0 text-right"><b>'.TfWidget::amount($row["amount"]).' VEB</b></div>
                    <div class="epic col-lg-3 p-0 m-0 text-right"><b>'.TfWidget::amount(round($row["amount"]/$rate,2)).' USD</b></div>
                </div>
            </div>
          </div>';  
   }
   $html.='</div>
<div class="row">
           <div class="mx-auto col-lg-12 p-0">
            <div class="col-12 container title p-4">Gastos Individuales</div>
             
           </div>  
      ';
   foreach ($coAccountingUnitList as $row){
    if ($row["rate"]!=''){
       $rate=$row["rate"];
    }else{
       $rate=$coExchangeRate;
    }
    
    $html.='  <div class="col-lg-12" >
            <div class="d-flex align-items-center invoce p-0" >
                <div class="col-12 p-0 ml-2 mb-2">
                    <div class="epic col-12 p-0 m-0"><b>'.$row["account"].'</b></div>
                    <div class="epic col-lg-5 p-0 m-0">'.$row["description"].'</div>
                    <div class="epic col-lg-4 p-0 m-0 text-right"><b>'.TfWidget::amount($row["amount"]).' VEB</b></div>
                    <div class="epic col-lg-3 p-0 m-0 text-right"><b>'.TfWidget::amount(round($row["amount"]/$rate,2)).' USD</b></div>
                </div>
            </div>
          </div>';  
   }
   $html.='</div>
  <div class="row">
    <div class="col-lg-12 p-0">

          <table id="co_accounting_dt" class="display responsive" style="width:100%">
  <thead>
    <tr>
      <th class="all">Concepto</th>
      <th class="all text-right">Monto VEB</th>
      <th class="all text-right">Monto USD</th>
    </tr>
  </thead>
  <tbody>    
   
';

  $total=0;
   foreach ($coAccountingSumary as $row){
    if ($row["rate"]!=''){
       $rate=$row["rate"];
    }else{
       $rate=$coExchangeRate;
    }
    $html.='<tr>
      <td>'.$row["account"].'</td>
      <td class="text-right">'.TfWidget::amount($row["amount"]).'</td>
      <td class="text-right">'.TfWidget::amount(round($row["amount"]/$rate,2)).'</td>
    </tr>'; 

     $total+= $row["amount"];
   }
   $html.='</tbody>
   <tfoot><tr>
      <th>Total</th>
      <th class="text-right">'.TfWidget::amount($total).'</th>
      <th class="text-right">'.TfWidget::amount(round($total/$rate,2)).'</th>
    </tr></tfoot>
</table>
</div>

</div>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">


$(document).ready(function(){
  
 $("#co_accounting_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:false,
    lengthChange:false,
    dom: 'rtip'
  });

});
</script>