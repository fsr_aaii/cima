<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }


//==============================================================
//==============================================================
//==============================================================

$html ='<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:0 5px;color:#999999;font-weight:200;color:#000;font-family:Helvetica,Arial,sans-serif" width="100%">
                                       <tbody>
                                          <tr>
                                             <td align="left" width="50%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="left" style="padding:0 0 8px 0;font-size:12px;" width="100%"><b>'.$coBillInfo["trade_name"].'</b></td>
                                                      </tr>
                                                      <tr>
                                                         <td align="left" style="padding:0 0 8px 0" width="100%">'.$coBillInfo["rif"].'</td>
                                                      </tr>
                                                      <tr>
                                                         <td align="left" style="padding:0 0 8px 0" width="100%">'.$coBillInfo["legal_address"].'</td>
                                                      </tr>
                                                      <tr>
                                                         <td align="left" style="padding:0 0 8px 0" width="100%">e-mail: '.$coBillInfo["email"].'</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                             <td align="right" width="50%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="right" style="padding:0 0 8px 0" width="100%">
                                                           <img style="vertical-align: top" src="asset/images/logo.jpg" width="180" />
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td align="right" style="padding:0 0 8px 0" width="100%">J-50116307-3</td>
                                                      </tr>
                                                      <tr>
                                                         <td align="right" style="padding:0 0 8px 0" width="100%">saycecconsultores@gmail.com</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                  <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:5px 5px;color:#999999;font-weight:200;color:#000;font-family:Helvetica,Arial,sans-serif" width="100%">
                                       <tbody>
                                          <tr>
                                             <td align="center" width="100%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="center" style="padding:0 0 8px 0;font-size:12px;" width="100%"><b>AVISO DE COBRO DE CONDOMINIO</b></td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                             
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:5px 5px;color:#999999;font-weight:200;color:#000;font-family:Helvetica,Arial,sans-serif" width="100%">
                                       <tbody>
                                          <tr>
                                             <td align="left" width="50%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="left" style="padding:0 0 8px 0" width="100%">Propietario: '.$coBillInfo["name"].'</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                             <td align="left" width="25%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="left" style="padding:0 0 8px 0" width="100%">RIF:'.$coBillInfo["dni"].'</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                             <td align="left" width="25%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="right" style="padding:0 0 8px 0" width="100%">AVISO DE COBRO N°:'.$coBillInfo["ref"].'</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:0 5px;color:#999999;font-weight:200;color:#000;font-family:Helvetica,Arial,sans-serif" width="100%">
                                       <tbody>
                                          <tr>
                                             <td align="left"  valign="top" width="25%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="left" style="padding:0 0 8px 0" width="100%">Inmueble:'.$coBillInfo["unit"].'</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                             <td align="left"  valign="top" width="25%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="left" style="padding:0 0 8px 0" width="100%">Alícuota General:'.$coBillInfo["aliquot"].'%</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                             <td align="left" width="25%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="left" style="padding:0 0 8px 0" width="100%">Fecha Emisión: '.TfWidget::ymdH2dmyH($coBillInfo["payment_date"]).'</td>
                                                      </tr>
                                                      <tr>
                                                         <td align="left" style="padding:0 0 8px 0" width="100%">Fecha Vencimiento: '.TfWidget::ymdH2dmyH($coBillInfo["expiration_date"]).'</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                             <td align="left"  valign="top" width="25%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="right" style="padding:0 0 8px 0" width="100%">Periodo:'.$coBillInfo["period"].'</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>

                                    

                                     <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                       <tr>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="2%">
                                                             <b>Item</b>
                                                         </td>
                                                         <td align="center" style="padding:5px 5px;border-bottom: 1px solid black;" width="48%">
                                                              <b>Descripcion</b>
                                                         </td>
                                                         <td align="right" style="padding:5px 5px;border-bottom: 1px solid black;" width="25%">
                                                              <b>Gasto Gral. Bs</b>
                                                         </td>
                                                         <td align="right" style="padding:5px 5px;border-bottom: 1px solid black;" width="25%">
                                                              <b>Cuota Prop. Bs.</b>
                                                         </td>                                                    
                                                      </tr>';

    $t1=0;$t2=0;$item=0;$group_name='';$foot=false;
    foreach ($coBillDetailInfo as $row){
      $mark = "";
    if ($row["type"]=='U'){
      $mark = "***";
      $foot=true;
    }else{
      $mark = "";
    }

   if ($group_name!=$row["group_name"]){
      $item++;
      $group_name=$row["group_name"];
      $html.=' <tr bgcolor="#d9d9d9">
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="2%">
                                                              '.$item.'
                                                         </td>
                                                         <td align="center" style="padding:5px 5px;border-bottom: 1px solid black;" width="48%">
                                                              '.$row["group_name"].'
                                                         </td>
                                                         <td align="right" style="padding:5px 5px;border-bottom: 1px solid black;" width="25%">
                                                         </td>
                                                         <td align="right" style="padding:5px 5px;border-bottom: 1px solid black;" width="25%">
                                                         </td>                                                    
                                                      </tr> '; 

   }
    $html.=' <tr>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="2%">
                                                              
                                                         </td>
                                                         <td align="center" style="padding:5px 5px;border-bottom: 1px solid black;" width="48%">
                                                              '.$row["account_name"].' '.$mark.'</td>
                                                         </td>
                                                         <td align="right" style="padding:5px 5px;border-bottom: 1px solid black;" width="25%">
                                                              '.TfWidget::amount($row["amount_gl"]).'
                                                         </td>
                                                         <td align="right" style="padding:5px 5px;border-bottom: 1px solid black;" width="25%">
                                                              '.TfWidget::amount($row["amount"]).'
                                                         </td>                                                    
                                                      </tr> '; 
     $t1+=$row["amount_gl"];
     $t2+=$row["amount"];                                                 
   }
   $html.='</tbody>
 <tfoot>
                                                   <tr>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="2%">
                                                              &nbsp;
                                                         </td>
                                                         <td align="right" style="padding:5px 5px;border-bottom: 1px solid black;" width="48%">
                                                              <b>TOTALES</b>     
                                                         </td>
                                                         <td align="right" style="padding:5px 5px;border-bottom: 1px solid black;" width="25%">
                                                              '.TfWidget::amount($t1).'
                                                         </td>
                                                         <td align="right" style="padding:5px 5px;border-bottom: 1px solid black;" width="25%">
                                                              '.TfWidget::amount($t2).'
                                                         </td>                                                    
                                                      </tr> 
                                                   </tfoot>
                                                      </table>




                                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:5px 5px;color:#999999;font-weight:200;color:#000;font-family:Helvetica,Arial,sans-serif" width="100%">
                                       <tbody>
                                          <tr>
                                             <td align="left"  valign="top" width="50%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                       <tr>
                                                         <td align="left" style="padding:0 0 8px 0" width="100%">
                                                              &nbsp;</td>
                                                      </tr>
                                                         
                                                      <tr>
                                                         <td align="left" style="padding:0 0 8px 0" width="100%">
                                                              Tasa Cambiaria día '.TfWidget::ymd2dmy($coBillInfo["created_date"]).' Bs.'.TfWidget::amount($coBillInfo["rate"]).'
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                             <td align="right"  valign="top" width="50%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                      <tr>
                                                         <td align="right" style="padding:0 0 8px 0" width="50%"><b>TOTAL A PAGAR Bs.</b></td>
                                                         <td align="right" style="padding:0 0 8px 0" width="50%">'.TfWidget::amount($coBillInfo["amount"]).'</td>
                                                      </tr>
                                                      <tr>
                                                         <td align="right" style="padding:0 0 8px 0" width="50%"><b>TOTAL A PAGAR USD.</b></td>
                                                         <td align="right" style="padding:0 0 8px 0" width="50%">'.TfWidget::amount($coBillInfo["amount"]/$coBillInfo["rate"]).' </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                            
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:5px 5px;color:#999999;font-weight:200;color:#000;font-family:Helvetica,Arial,sans-serif" width="100%">
                                       <tbody>
                                          <tr>
                                             <td align="center" width="100%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif" width="100%">
                                                   <tbody>
                                                    ';
if ($foot){
   $html.='<tr>
                                                         <td align="center" style="padding:0 0 8px 0" width="100%"><b>*** Esta imputaci&oacute;n es exclusiva para esta unidad</b></div></td>
                                                      </tr>';
}
      $html.='
                                                      <tr>
                                                         <td align="center" style="padding:0 0 8px 0" width="100%">PUEDE EFECTUAR SU PAGO A TRAVÉS DE DEPÓSITOS O TRANSFERENCIAS A LA CUENTA CORRIENTE Nº '.$coBillInfo["account"].'<br>BANCO: '.$coBillInfo["bank"].', A NOMBRE DE: '.$coBillInfo["trade_name"].', RIF '.$coBillInfo["rif"].'</td>
                                                      </tr>
                                                      <tr>
                                                         <td align="center" style="padding:0 0 8px 0" width="100%"> 
                                                      <b>RECUERDE: DE SU PAGO OPORTUNO, DEPENDE EL MANTENIMIENTO DE SU PATRIMONIO</b></td>
                                                      </tr>
                                                      <tr>
                                                         <td align="center" style="padding:0 0 8px 0" width="100%"> 
                                                      <b>ESTADO DE CUENTA INDIVIDUAL - CUOTAS PENDIENTES POR PAGAR</b></td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                             
                                          </tr>
                                       </tbody>
                                    </table>


                                    <table align="center" bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:0 5px;color:#999999;font-weight:200;color:#000;font-family:Helvetica,Arial,sans-serif" width="100%">
                                       <tbody>
                                          <tr align="center">
                                             <td align="left"  valign="top" width="100%">
                                                <table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;color:#000;font-size:10px;font-family:Helvetica,Arial,sans-serif;" width="100%">
                                                   <tbody>
                                                       <tr>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="10%">
                                                              &nbsp;
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              <b>a 30 días</b>
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              <b>31-60 días</b>
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              <b>61-90 días</b>
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              <b>91-120 días</b>
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              <b>mas 120 días</b>
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                             <b> Total USD</b>
                                                         </td>
                                                      </tr> 
                                                      <tr>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="10%">
                                                              <b>Avisos de Cobros por pagar</b>
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              '.TfWidget::amount($coBillInfo["balance_30"]).'
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              '.TfWidget::amount($coBillInfo["balance_60"]).'
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              '.TfWidget::amount($coBillInfo["balance_90"]).'
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              '.TfWidget::amount($coBillInfo["balance_120"]).'
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              '.TfWidget::amount($coBillInfo["balance_previous"]).'
                                                         </td>
                                                         <td align="left" style="padding:5px 5px;border-bottom: 1px solid black;" width="15%">
                                                              '.TfWidget::amount($coBillInfo["balance"]).'
                                                         </td>
                                                      </tr> 
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>';

error_reporting(0);
use Mpdf\Mpdf; 
$mpdf = new Mpdf();
$mpdf->showImageErrors=true;
$mpdf->WriteHTML(ob_get_clean());
$mpdf->WriteHTML($html);
$mpdf->Output();

?>
