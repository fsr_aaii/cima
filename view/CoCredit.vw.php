<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_credit_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCredit" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coCredit->getCreatedBy()).'  el '.$coCredit->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_credit_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCredit" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_credit_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoCredit" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coCredit->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coCredit->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_credit_form" name="co_credit_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Co Credit</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_credit" name="is_co_credit" value="'.$coCredit->getInitialState().'">
         <input type="hidden" id="co_credit_id" name="co_credit_id" maxlength="22" value="'.$coCredit->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="co_credit_id_unit" class="control-label">Id Unit:</label>
        <select  id="co_credit_id_unit" name="co_credit_id_unit" class="co_credit_id_unit form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CoUnit::selectOptions($tfs),$coCredit->getIdUnit()).
'      </select>
      <label for="co_credit_id_unit" class="error">'.$coCredit->getAttrError("id_unit").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_credit_amount" class="control-label">Amount:</label>
        <input type="text" id="co_credit_amount" name="co_credit_amount" class="co_credit_amount form-control"  maxlength="22"  value="'.$coCredit->getAmount().'"  tabindex="2"/>
      <label for="co_credit_amount" class="error">'.$coCredit->getAttrError("amount").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_credit_status" class="control-label">Status:</label>
        <input type="text" id="co_credit_status" name="co_credit_status" class="co_credit_status form-control"  maxlength="1"  value="'.$coCredit->getStatus().'"  tabindex="3"/>
      <label for="co_credit_status" class="error">'.$coCredit->getAttrError("status").'</label>
      </div>

   <div class="col-lg-12 container">
     <div class="col-lg-6 container mb-5 mt-2">'.$audit.'</div>
     <div class="col-lg-6 container mb-5 mt-2 text-right">'.$buttons.'</div>
  </div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
  function coCreditRules(){

  $("#co_credit_form").validate();
  $("#co_credit_id_unit").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_credit_amount").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_credit_status").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
   $('select').niceSelect();
  coCreditRules();


})
</script>