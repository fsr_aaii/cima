<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_previous_due_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPreviousDue" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coPreviousDue->getCreatedBy()).'  el '.$coPreviousDue->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_previous_due_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPreviousDue" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_previous_due_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPreviousDue" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coPreviousDue->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coPreviousDue->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_previous_due_form" name="co_previous_due_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Deuda Previa</div>
     <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_previous_due" name="is_co_previous_due" value="'.$coPreviousDue->getInitialState().'">
         <input type="hidden" id="co_previous_due_id" name="co_previous_due_id" maxlength="22" value="'.$coPreviousDue->getId().'">

      </div>
     <div class="col-lg-4 container">
       <label for="co_previous_due_id_unit" class="control-label">Unidad:</label>
        <select  id="co_previous_due_id_unit" name="co_previous_due_id_unit" class="co_previous_due_id_unit form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CoUnit::selectOptionsByPerson($tfs,$tfRequest->co_previous_due_id_person),$coPreviousDue->getIdUnit()).
'      </select>
      <label for="co_previous_due_id_unit" class="error">'.$coPreviousDue->getAttrError("id_unit").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="co_previous_due_payment_date" class="control-label">Fecha:</label>
        <input type="text" id="co_previous_due_payment_date" name="co_previous_due_payment_date" class="co_previous_due_payment_date form-control"  maxlength="22"  value="'.$coPreviousDue->getPaymentDate().'"  tabindex="2"/>
      <label for="co_previous_due_payment_date" class="error">'.$coPreviousDue->getAttrError("payment_date").'</label>
      </div>

      <div class="col-lg-4 container">
       <label for="co_previous_due_amount" class="control-label">Monto:</label>
        <input type="text" id="co_previous_due_amount" name="co_previous_due_amount" class="co_previous_due_amount form-control"  maxlength="22"  value="'.$coPreviousDue->getAmount().'"  tabindex="4"/>
      <label for="co_previous_due_amount" class="error">'.$coPreviousDue->getAttrError("amount").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="co_previous_due_observation" class="control-label">Observation:</label>
        <textarea id="co_previous_due_observation" name="co_previous_due_observation" class="co_previous_due_observation form-control" rows="3" >'.$coPreviousDue->getObservation().'</textarea>
      <label for="co_previous_due_observation" class="error">'.$coPreviousDue->getAttrError("observation").'</label>
      </div>

      <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function coPreviousDueRules(){
  $("#co_previous_due_form").validate();
  $("#co_previous_due_id_unit").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#co_previous_due_payment_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#co_previous_due_observation").rules("add", {
    maxlength:5000
  });
  $("#co_previous_due_amount").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}

$("#co_previous_due_payment_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  coPreviousDueRules();

  $("#co_previous_due_payment_date").css('vertical-align','top');
  $("#co_previous_due_payment_date").mask('y999-m9-d9');

})
</script>