<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $tfData["co_accounting_id"] = $coBill->getIdAccounting();
  $tfData["co_accounting_id_unit"] = $coBill->getIdUnit();
  
$buttons ='<a class="btn-guaramo-text" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccounting" data-tf-action="AV" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this,false);">Ver Detalles</a>';


   $tfData["co_bill_id"] = $coBill->getId();

    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBill" data-tf-action="PRT" onclick="co_bill_print(this,false); ">Imprimir</a>';

  $html='<div class="row">
           <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            <div class="col-12 container title">Aviso de Cobro</div>
            <div class="col-12 container subtitle mb-3">'.CoBill::REF($tfs,$coBill->getId()).'</div>
           
        <table id="co_bill_dt" class="display responsive" style="width:100%">

  <thead>
    <tr>
      <th class="all">Concepto</th>
      <th class="desktop text-right">Global VEB</th>
      <th class="desktop text-right">Global USD</th>
      <th class="all text-right">Monto VEB</th>
      <th class="all text-right">Monto USD</th>
    </tr>
  </thead>
  <tbody>    
   
';

  $global=0;$total=0;$foot=false;
   foreach ($coBillDetailList as $row){

    $mark = "";
    if ($row["type"]=='U'){
      $mark = "***";
      $foot=true;
    }else{
      $mark = "";
    }
    $html.='<tr>
      <td>'.$row["account"].' '.$mark.'</td>
      <td class="text-right">'.TfWidget::amount($row["amount_gl"]).'</td>
      <td class="text-right">'.TfWidget::amount(round($row["amount_gl"]/$coBill->getRate(),2)).'</td>
      <td class="text-right">'.TfWidget::amount($row["amount"]).'</td>
      <td class="text-right">'.TfWidget::amount(round($row["amount"]/$coBill->getRate(),2)).'</td>
    </tr>'; 
     $global+= $row["amount_gl"]; 
     $total+= $row["amount"];
   }
   $html.='</tbody>
   <tfoot><tr>
      <th>Total</th>
      <th class="text-right">'.TfWidget::amount($global).'</th>
      <th class="text-right">'.TfWidget::amount(round($global/$coBill->getRate(),2)).'</th>
      <th class="text-right">'.TfWidget::amount($total).'</th>
      <th class="text-right">'.TfWidget::amount(round($total/$coBill->getRate(),2)).'</th>
    </tr></tfoot>
</table>';
if ($foot){
   $html.='<div class="col-12 container"><b>*** Esta imputaci&oacute;n es exclusiva para esta unidad</b></div>';
}

$html.='<div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">


function co_bill_print(element){
  console.log(sessionStorage.getItem('TF_URL_ROOT')+"/print/"+element.dataset.tfTaskId+"/"+element.dataset.tfController+"/"+element.dataset.tfAction+"/"+element.dataset.tfData);
     window.open(sessionStorage.getItem('TF_URL_ROOT')+"/print/"+element.dataset.tfTaskId+"/"+element.dataset.tfController+"/"+element.dataset.tfAction+"/"+element.dataset.tfData,'_blank');
}

$(document).ready(function(){
 
  $("#co_bill_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:false,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });


})
</script>