
<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }


$tfDataN["gl_juridical_person_id"] = $tfRequest->co_payment_id_condominium;

$html='<div class="row">
           <div class="mx-auto col-lg-10 tf-card shadow mb-4">
            
            <div class="col-7 title">Pagos</div>
             <div class="col-5 text-right action">
               <a class="btn btn-guaramo-text" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPayment" data-tf-action="APN" data-tf-data="'.$tfResponse->encrypt($tfDataN).'" onclick="TfRequest.do(this);">
                 Nuevo</a>
             </div>


             <div class="col-12 container">
 <div class="row justify-content-end">

    <div class="col-lg-4 input-group mb-3">
  <input type="text" class="filter form-control" placeholder="Buscar..." aria-label="Username" aria-describedby="basic-addon1">
   <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1"><i class="bx bx-search"></i></span>
  </div>
</div>
  </div>
</div>

<div class="row col-12 p-0">';
$i=0;
foreach ($coPaymentList as $row){
    
    $tfData["co_payment_id"] = $row["id"];

    if ($row["status"]=='A'){
      $check='<div class="badge-container"> 
                              <span class="badge badge-pill circle-check float-right m-2"><i class="bx bx-check"></i></span>
                            </div>';
      $action = 'AC';           
      $status = 'Aprobado';              
    }elseif ($row["status"]=='R'){
      $check='<div class="badge-container"> 
                              <span class="badge badge-pill circle-block float-right m-2"><i class="bx bx-block"></i></span>
                            </div>';
      $action = 'AC'; 
      $status = 'Rechazado';                     
    }else{
      $check='';
      $action = 'CEE';
      $status = 'Por Aprobar';
    }

    if ($row["currency"]=='VEB'){
      $VEB = $row["amount"];
      $USD = round($row["amount"]/$row["rate"],2);
    }else{
      $USD = $row["amount"];
      $VEB = round($row["amount"]*$row["rate"],2);
    }

    
    
     $html.='  <div class="col-lg-4 coaching p-1">
    <div class="col-lg-12 card-01 p-1 " data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPayment" data-tf-action="'.$action.'" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);" data-string="'.$row["name"].''.$row["rif"].'">
           '.$check;

           if(substr($row["image_path"],-3)=="pdf"){
            //$html.='<embed src="'.$row["image_path"].'" type="application/pdf" class="card-img-top">' ;
             $html.='<img class="card-img-top" src="../../../upload/payment/pdf.png" alt="">' ;
          }else{
            $html.='<img class="card-img-top" src="'.$row["image_path"].'" alt="">' ;
          }
           
           $html.='   <div class="d-flex align-items-center p-4" >
                <div class="col-12 p-0 ml-2">
                   <div class="epic col-12 c-text-1 text-white p-0 m-0">'.$row["REF"].'</div>
                    <div class="epic col-12 c-text-6 text-white p-0 m-0">'.$row["payment_date"].'</div>
                    <div class="col-12 epic c-text-3 text-white p-0 m-0">'.TfWidget::amount($VEB).' VEB</div>
                    <div class="col-12 epic c-text-3 text-white p-0 m-0">'.TfWidget::amount($USD).' USD</div>
                    <div class="epic col-12 c-text-5 text-white p-0 m-0">'.$status.'</div> 
                </div>

            </div>
          </div>
          </div>'; 


       
   }
   $html.='</div>
          </div>
        </div>';
 echo $html;
?>

<script type="text/javascript">
  
  $(".filter").on("keyup", function() {
  var input = $(this).val().toUpperCase();

  $(".coaching").each(function() {
    if ($(this).data("string").toUpperCase().indexOf(input) < 0) {
      $(this).hide();
    } else {
      $(this).show();
    }
  })
});

</script>
