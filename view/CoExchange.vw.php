<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_exchange_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoExchange" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<span class="created_by">Creado por '.TUser::description($tfs,$coExchange->getCreatedBy()).'  el '.$coExchange->getCreatedDate().'</span>';
    $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_exchange_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoExchange" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_exchange_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoExchange" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($coExchange->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coExchange->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-4 form-frame shadow mb-4">
    <form id="co_exchange_form" name="co_exchange_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Tasa de Camnio</div>
      <div class="col-lg-12 container mb-5">'.$audit.'</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_exchange" name="is_co_exchange" value="'.$coExchange->getInitialState().'">
         <input type="hidden" id="co_exchange_id" name="co_exchange_id" maxlength="22" value="'.$coExchange->getId().'">
         <input type="hidden" id="co_exchange_id_person" name="co_exchange_id_person" maxlength="22" value="'.$coExchange->getIdPerson().'">

      </div>
      <div class="col-lg-12 container">
       <label for="co_exchange_rate" class="control-label">Rate:</label>
        <input type="text" id="co_exchange_rate" name="co_exchange_rate" class="co_exchange_rate form-control"  maxlength="22"  value="'.$coExchange->getRate().'"  tabindex="3"/>
      <label for="co_exchange_rate" class="error">'.$coExchange->getAttrError("rate").'</label>
      </div>
     
     
     <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
  function coExchangeRules(){
  $("#co_exchange_form").validate();
  $("#co_exchange_id_person").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  
  $("#co_exchange_rate").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}


$(document).ready(function(){
  coExchangeRules();

})
</script>