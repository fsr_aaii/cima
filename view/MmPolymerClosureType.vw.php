<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn" role="button" data-tf-form="#mm_polymer_closure_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmPolymerClosureType" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$mmPolymerClosureType->getCreatedBy()).'  on '.$mmPolymerClosureType->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn" role="button" data-tf-form="#mm_polymer_closure_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmPolymerClosureType" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn" role="button" data-tf-form="#mm_polymer_closure_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mmPolymerClosureType" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($mmPolymerClosureType->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($mmPolymerClosureType->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="mm_polymer_closure_type_form" name="mm_polymer_closure_type_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container epic-title xl mb-5">Mm Polymer Closure Type</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_mm_polymer_closure_type" name="is_mm_polymer_closure_type" value="'.$mmPolymerClosureType->getInitialState().'">
         <input type="hidden" id="mm_polymer_closure_type_id" name="mm_polymer_closure_type_id" maxlength="22" value="'.$mmPolymerClosureType->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="mm_polymer_closure_type_name" class="control-label">Name:</label>
        <input type="text" id="mm_polymer_closure_type_name" name="mm_polymer_closure_type_name" class="mm_polymer_closure_type_name form-control"  maxlength="200"  value="'.$mmPolymerClosureType->getName().'"  tabindex="1"/>
      <label for="mm_polymer_closure_type_name" class="error">'.$mmPolymerClosureType->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="mm_polymer_closure_type_active" class="control-label">Active:</label>
        <select  id="mm_polymer_closure_type_active" name="mm_polymer_closure_type_active" class="mm_polymer_closure_type_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($mmPolymerClosureType->getActive(),'Y').
'      </select>
      <label for="mm_polymer_closure_type_active" class="error">'.$mmPolymerClosureType->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function mmPolymerClosureTypeRules(){
  $("#mm_polymer_closure_type_form").validate();
  $("#mm_polymer_closure_type_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#mm_polymer_closure_type_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  mmPolymerClosureTypeRules();


})
</script>