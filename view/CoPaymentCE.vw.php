<style type="text/css">
.form-control:disabled, .form-control[readonly] {
    background-color: transparent; 
    color: #585858;
}
.pdf-fluid {
    max-width: 100%;
    height: 500px;
    padding: 0;
}

</style>
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  $currencyA = array();
  $currencyA[0]['value'] = 'VEB';
  $currencyA[0]['option'] = 'Bolivar';
  $currencyA[1]['value'] = 'USD';
  $currencyA[1]['option'] = 'Dolar';

  $currencyOther = array('VEB'=>'USD','USD'=>'VEB');

    if ($coPayment->getImagePath()=='..' or $coPayment->getImagePath()==''){
    $photo='../asset/images/no-image.png';
  }else{
    $photo=$coPayment->getImagePath();
  }

  $audit='<span class="created_by">Creado por '.TUser::description($tfs,$coPayment->getCreatedBy()).'  el '.$coPayment->getCreatedDate().'</span>';
  $buttons ='<a class="btn-guaramo-text" data-tf-form="#co_payment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPayment" data-tf-action="CEA" onclick="TfRequest.do(this,true);">Aprobar</a>';
  $buttons.='<a class="btn-guaramo-text pl-2" data-tf-form="#co_payment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPayment" data-tf-action="CER" onclick="TfRequest.do(this,true);">Rechazar</a>';
  

  foreach ($coPayment->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($coPayment->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  
  if ($coPayment->getCurrency()=='VEB'){
    $currency = 'Bolivar';
    $amountOther = round($coPayment->getAmount()/$coPayment->getRate(),2);
  }else{
    $currency = 'Dolar';
    $amountOther = round($coPayment->getAmount()*$coPayment->getRate(),2);
  }
  
  
  if ($coPayment->getCurrencyConfirmed()!=''){
    $currencyConfirmed = $coPayment->getCurrencyConfirmed();
  }else{
    $currencyConfirmed = $coPayment->getCurrency();
  }


  if ($coPayment->getPaymentDateConfirmed()!=''){
    $paymentDateConfirmed = $coPayment->getPaymentDateConfirmed();
  }else{
    $paymentDateConfirmed = $coPayment->getPaymentDate();
  }
  if ($coPayment->getRateConfirmed()!=''){
    $rateConfirmed = $coPayment->getRateConfirmed();
  }else{
    $rateConfirmed = $coPayment->getRate();
  }
  if ($coPayment->getAmountConfirmed()!=''){
    $amountConfirmed = $coPayment->getAmountConfirmed();
  }else{
    $amountConfirmed = $coPayment->getAmount();
  }
   
  if ($currencyConfirmed =='VEB'){
     $amountOtherConfirmed = round($amountConfirmed/$rateConfirmed,2);
  }else{
     $amountOtherConfirmed = round($amountConfirmed*$rateConfirmed,2);
  } 

  
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="co_payment_form" name="co_payment_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Pago</div>
       <div class="col-lg-12 container mb-5 mt-2">'.$audit.'</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_co_payment" name="is_co_payment" value="'.$coPayment->getInitialState().'">
         <input type="hidden" id="co_payment_id" name="co_payment_id" maxlength="22" value="'.$coPayment->getId().'">
      </div>

       <div class="col-lg-12 container" width="100%" height="600px" >
       
     
         <div class="col-12">';           
          if(substr($photo,-3)=="pdf"){
              
            $html.=' <label for=""  class="control-label">PDF:</label>
                    <embed src="'.$photo.'" type="application/pdf" class="col-12  pdf-fluid ">' ;
          }else{
              
           $html.=' <label for=""  class="control-label">Image:</label>
                <img id="photo-upload" name="photo-upload" class="col-12  img-fluid " src="'.$photo.'">';
          }
        $html.=' </div>
         <label for="co_payment_image_path" class="error">'.$coPayment->getAttrError("image_path").'</label> 
      </div>
       <div class="col-12 container">
        <label for="" class="control-label" style="font-size: .8rem !important; margin-bottom: 3;">Datos de Registro:</label>
      </div> 
         <div class="col-12 "> </div>
      <br> 
      <div class="col-12 container">
      <label for=""  class="control-label" style="font-size: .8rem !important; margin-bottom: 3;">Unidad: '.$desUnit.'</label>
      </div>
      <div class="col-lg-2 container">
       <label for="co_payment_amount" class="control-label">Moneda:</label>
       <span class="form-control">'.$currency.'</span>
      </div>
      <div class="col-lg-2 container">
       <label for="" class="control-label">Fecha Pago:</label>
       <span class="form-control">'.$coPayment->getPaymentDate().'</span>
      </div>
      <div class="col-lg-2 container">
       <label for="" class="control-label">Tasa:</label>
       <span class="form-control">'.TfWidget::amount($coPayment->getRate()).'</span>
      </div>
      <div class="col-lg-3 container">
       <label for="" class="control-label">Monto '.$coPayment->getCurrency().':</label>
        <span class="form-control">'.TfWidget::amount($coPayment->getAmount()).'</span>
      </div>
      <div class="col-lg-3 container">
       <label for="" class="control-label">Monto '.$currencyOther[$coPayment->getCurrency()].':</label>
        <span class="form-control">'.TfWidget::amount($amountOther).'</span>
      </div>

      <div class="col-12 container">
        <label for="" class="control-label">Datos de Confirmaci&oacute;n:</label>
      </div>  
      <div class="col-lg-2 container">
       <label for="co_payment_currency_confirmed" class="control-label">Moneda:</label>
               <select  id="co_payment_currency_confirmed" name="co_payment_currency_confirmed" class="co_payment_currency_confirmed form-control" tabindex="2">
      <option value="">Selecciona</option>'.
      TfWidget::selectStructure($currencyA,$currencyConfirmed).
'      </select>
      <label for="co_payment_currency_confirmed" class="error">'.$coPayment->getAttrError("currency_confirmed").'</label>
      </div>
      <div class="col-lg-2 container">
       <label for="co_payment_payment_date_confirmed" class="control-label">Fecha Pago:</label>
        <input type="text" id="co_payment_payment_date_confirmed" name="co_payment_payment_date_confirmed" class="co_payment_payment_date_confirmed form-control"  maxlength="22"  value="'.$paymentDateConfirmed.'"  tabindex="2" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPayment" data-tf-fn-name="paymentDateConfirmed" data-id-person="'.CoUnit::idPerson($tfs,$coPayment->getIdUnit()).'"/>


      <label for="co_payment_payment_date_confirmed" class="error">'.$coPayment->getAttrError("payment_date_confirmed").'</label>
      </div>
      <div class="col-lg-2 container">
       <label for="co_payment_rate_confirmed" class="control-label">Tasa:</label>
        <input type="text" id="co_payment_rate_confirmed" name="co_payment_rate_confirmed" class="co_payment_rate_confirmed form-control"  maxlength="22"  value="'.$rateConfirmed.'"  tabindex="2"/>
      <label for="co_payment_rate_confirmed" class="error">'.$coPayment->getAttrError("rate_confirmed").'</label>
      </div>
      <div class="col-lg-3 container">
       <label id="co_payment_amount_confirmed_l1" for="co_payment_amount_confirmed" class="control-label">Monto '.$currencyConfirmed.':</label>
        <input type="text" id="co_payment_amount_confirmed" name="co_payment_amount_confirmed" class="co_payment_amount_confirmed form-control"  maxlength="22"  value="'.$amountConfirmed.'"  tabindex="2"/>
      <label for="co_payment_amount_confirmed" class="error">'.$coPayment->getAttrError("amount_confirmed").'</label>
      </div>
      <div class="col-lg-3 container">
       <label id="co_payment_amount_confirmed_l2" for="co_payment_amount_other_confirmed" class="control-label">Monto '.$currencyOther[$currencyConfirmed].':</label>
        <span  id="co_payment_amount_other_confirmed" name="co_payment_amount_other_confirmed class="form-control">
        <input  id="co_payment_amount_other_confirmed" name="co_payment_amount_other_confirmedtype="text" id="co_payment_amount_other_confirmed" name="co_payment_amount_other_confirmed" class="co_payment_amount_other_confirmed form-control"  maxlength="22"  value="'.$amountOtherConfirmed.'"  tabindex="2" disabled/>
      <label for="co_payment_amount_other_confirmed" class="error"></label>
      </div> 

      <div class="col-lg-12 container">
       <label for="co_payment_observation" class="control-label">Observacion:</label>
        <textarea id="co_payment_observation" name="co_payment_observation" class="co_payment_observation form-control" rows="3" >'.$coPayment->getObservation().'</textarea>
      <label for="co_payment_observation" class="error">'.$coPayment->getAttrError("observation").'</label>
      </div>

  <div class="col-lg-12 container mb-5 mt-2 text-right">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">

function coPaymentRules(){
  $("#co_payment_form").validate();

  $("#co_payment_currency_confirmed").rules("add", {
    required:true
  });
  $("#co_payment_payment_date_confirmed").rules("add", {
    required:true
  });
  $("#co_payment_rate_confirmed").rules("add", {
    required:true,
    number:true
  });
  $("#co_payment_amount_confirmed").rules("add", {
    required:true,
    number:true
  });
  $("#co_payment_observation").rules("add", {
    maxlength:4294967295
  });

}

$("#co_payment_payment_date_confirmed").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$("#co_payment_currency_confirmed").on('change',function(){ 

   var currency = $(this).val();
   var co_payment_amount_other_confirmed;

   if (currency=='VEB'){
      $("#co_payment_amount_confirmed_l1").html("Monto VEB");
      $("#co_payment_amount_confirmed_l2").html("Monto USD");

      co_payment_amount_other_confirmed=($("#co_payment_amount_confirmed").val()/$("#co_payment_rate_confirmed").val());

   }else{
     $("#co_payment_amount_confirmed_l2").html("Monto VEB");
     $("#co_payment_amount_confirmed_l1").html("Monto USD");

     co_payment_amount_other_confirmed=($("#co_payment_amount_confirmed").val()*$("#co_payment_rate_confirmed").val());
   }
   $("#co_payment_amount_other_confirmed").val(co_payment_amount_other_confirmed.toFixed(2));

})


$("#co_payment_rate_confirmed").on('change',function(){ 

   var currency = $("#co_payment_currency_confirmed").val();
   var co_payment_amount_other_confirmed;

   if (currency=='VEB'){
     
      co_payment_amount_other_confirmed=($("#co_payment_amount_confirmed").val()/$("#co_payment_rate_confirmed").val());

   }else{
     
     co_payment_amount_other_confirmed=($("#co_payment_amount_confirmed").val()*$("#co_payment_rate_confirmed").val());
   }
   $("#co_payment_amount_other_confirmed").val(co_payment_amount_other_confirmed.toFixed(2));

})


$("#co_payment_amount_confirmed").on('change',function(){ 

   var currency = $("#co_payment_currency_confirmed").val();
   var co_payment_amount_other_confirmed;

   if (currency=='VEB'){
     
      co_payment_amount_other_confirmed=($("#co_payment_amount_confirmed").val()/$("#co_payment_rate_confirmed").val());

   }else{
     
     co_payment_amount_other_confirmed=($("#co_payment_amount_confirmed").val()*$("#co_payment_rate_confirmed").val());
   }
   $("#co_payment_amount_other_confirmed").val(co_payment_amount_other_confirmed.toFixed(2));

})


$("#co_payment_payment_date_confirmed").on('change',function(){ 
   
    let JDATA = new Object();
        JDATA.tfTaskId = $("#co_payment_payment_date_confirmed").data("tf-task-id");
        JDATA.tfController = $("#co_payment_payment_date_confirmed").data("tf-controller"); 
        JDATA.tfFnName = $("#co_payment_payment_date_confirmed").data("tf-fn-name");  
        JDATA.tfData = new Object();
        JDATA.tfData.date=$("#co_payment_payment_date_confirmed").val();
        JDATA.tfData.id_person=$("#co_payment_payment_date_confirmed").data("id-person");

        let response = TfRequest.fn(JDATA);
             console.log(response);   
        var JRESP=JSON.parse(response);   
        if (JRESP.code=='OK'){
         var currency = $("#co_payment_currency_confirmed").val();
         var co_payment_amount_other_confirmed;
         $("#co_payment_rate_confirmed").val(JRESP.rate);
         if (currency=='VEB'){
           
            co_payment_amount_other_confirmed=($("#co_payment_amount_confirmed").val()/JRESP.rate);

         }else{
           
           co_payment_amount_other_confirmed=($("#co_payment_amount_confirmed").val()*$("#co_payment_rate_confirmed").val());
         }
         $("#co_payment_amount_other_confirmed").val(co_payment_amount_other_confirmed.toFixed(2));
       
          
        }


})

       

    


$(document).ready(function(){
  $('select').niceSelect();
  $("#co_payment_payment_date_confirmed").css('vertical-align','top');
  $("#co_payment_payment_date_confirmed").mask('y999-m9-d9');

  coPaymentRules();


})

  
</script>