<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='    <div class="row">
           <div class="mx-auto col-lg-8 tf-card shadow mb-4">
            
            <div class="col-8 title">Puerto - Muelle</div>
             <div class="col-4 text-right action">
               <a class="btn btn-guaramo-text" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpPortPier" data-tf-action="AN" onclick="TfRequest.do(this);">
                 Nuevo</a>
             </div>




       <table id="op_port_pier_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Nombre</th>
             <th class="all">Habilitado</th>
             <th class="none">Creado por</th>
             <th class="none">Creado el</th>
             <th class="all text-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($opPortPierList as $row){
    $tfData["op_port_pier_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["name"].'</td>
            <td>'.$row["active"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="text-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="OpPortPier" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     Editar
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#op_port_pier_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>