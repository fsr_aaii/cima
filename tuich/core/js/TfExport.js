class TfExport{
	
	static excel(element,deleteLastCell=false){
    let dt = new Date();
    let table = document.querySelector(element.dataset.tfTable);

    
    if (deleteLastCell){
      let clone = table.cloneNode(true);	
      let rows = clone.rows;

	    for (var j = 0; j < rows.length; j++) {
	            rows[j].deleteCell(-1);
	    }
	  table = clone;
    }
    

    TableToExcel.convert(table,element.dataset.tfFile+" "+dt.getFullYear()+dt.getMonth()+dt.getDate()+dt.getHours()+dt.getMinutes());
  }  
}  