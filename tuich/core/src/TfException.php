<?php
class TfException extends Exception
{
    private $http_code;

    public function __construct($message, $code = 0,$httpCode) { 
        parent::__construct($message, $code, $previous);
        $this->http_code=$httpCode;
    }

    public function getHttpCode(){
        return $this->http_code;
    }
    public static function format($t){
        return '<p>'.$t->getCode().'</p><p>'.$t->getMessage().'</p><p>'.pathinfo($t->getFile(), PATHINFO_FILENAME).'</p><p>'.$t->getLine().'</p>';
    }

}
