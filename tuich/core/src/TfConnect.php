<?php

class TfConnect{

  protected $connection;  
  protected $host;
  protected $service;
  protected $user;
  protected $type;
  protected $database;
  protected $profile;
  protected $password;
  protected $state;
  
  public function __construct($database){      
		$this->host=$database["host"];
    $this->user=$database["user"];
    $this->password=$database["password"];
    $this->type=$database["type"];
    $this->database=$database["name"];
    $this->profile=$database["profile"];
    $this->state='Initial';     
    $this->connection=NewADOConnection("mysqli");
    $this->connection->SetFetchMode(ADODB_FETCH_ASSOC);  
  } 
  
  public function setHost($value){
    $this->host=$value;
  } 
  public function getHost(){
    return $this->host;
  }  

  public function setService($value){
    $this->service=$value;
  } 
  public function getService(){
    return $this->service;
  } 

  public function setUser($value){
    $this->user=$value;
  } 
  public function getUser(){
    return $this->user;
  } 
  
  public function setDatabase($value){
    $this->database=$value;
  } 
  public function getDatabase(){
    return $this->database;
  } 

  public function setProfile($value){
    $this->profile=$value;
  } 
  public function getProfile(){
    return $this->profile;
  } 

  public function setState($value){
    $this->state=$value;
  } 
  public function getState(){
    return $this->state;
  } 

  public function setPassword($value){
    $this->password=$value;
  } 

  public function connect(){ 

    if (!isset($this->host)||$this->host==''){
      $this->state='Failed'; 
      $this->exception(901,"The database host can not be empty");
    }
    if (!isset($this->user)||$this->user==''){
      $this->state='Failed';
      $this->exception(902,"The database user can not be empty");
    }
    if (!isset($this->password)||$this->password==''){
      $this->state='Failed';
      $this->exception(903,"The database password can not be empty");
    }
    if (!isset($this->database)||$this->database==''){
      $this->state='Failed'; 
      $this->exception(904,"The database name can not be empty");
    }
 
    if ($this->state=='Closed'||$this->state=='Initial'){
      $this->connection->Connect($this->host,$this->user,$this->password,$this->database);
      $this->connection->execute("SET NAMES 'utf8'");
      $this->connection->execute("SET CHARACTER SET utf8");
      $this->connection->execute("SET COLLATION_CONNECTION='utf8_spanish_ci'");
      $this->connection->startTrans();
      $this->state='Start';
    }else{
      $this->state='Failed'; 
      $this->exception(906,"Unable to start a connection that is in ".$this->state." state ");
    }  
  }  
            
  public function execute($sql,$param=array()){
    if ($this->state=='Start'||$this->state=='Ok'){
      try{
        $this->connection->Execute($sql,$param);
      }catch (exception $e){  
        $this->connection->FailTrans();  
        $this->connection->CompleteTrans(); 
        $this->exception(999,$e->getMessage(),$sql,$param);           
      }
    }else{
      $this->exception(905,"Can not execute a query with a connection that is in  ".$this->state." state ",$sql,$param); 
    }
  }
  
  public function executeQuery($sql,$param=array()){
    if ($this->state=='Start'||$this->state=='Ok'){
      try{
        $recordSet = $this->connection->Execute($sql,$param);
        $recordArray = array();
        $recordArray = $recordSet->GetRows(); 
      }catch (exception $e){  
        $this->connection->FailTrans();  
        $this->connection->CompleteTrans(); 
        $this->exception(999,$e,$sql,$param); 
      }
    }else{
      $this->exception(905,"Can not execute a query with a connection that is in  ".$this->state." state ",$sql,$param);
    }
    return array_change_key_case($recordArray, CASE_LOWER);
  }

  public function checkTrans(){  
    if ($this->state=='Failed'){
      $this->connection->FailTrans();
    }

    $this->connection->completeTrans();
    $this->connection->startTrans();
    $this->state='Ok'; 
  }

  public function close(){ 
    $this->connection->completeTrans(); 
    $this->connection->close();
    $this->state='Closed';
  }  
  
  public function affectedRows(){ 
    return $this->connection->affected_rows(); 
  }  

  public function exception($code,$msg,$q="",$param=""){
   if ($this->profile=="dev"){
      $this->state='Failed';
      $exception.='<p>'.$this->connection->errorMsg().'</p>';
      $exception.='<p>'.$msg.'</p>';
      if ($q!=""){
        $exception.='<p>'.SqlFormatter::format($q).'</p>';
      } 
      if ($param!=""){
        $exception.='<p> values ('. implode( ",", $param ).')</p>';
      }      

      throw new Exception($exception,$code); 
    }else{
      $this->state='Failed';
      throw new Exception("The execution in the database failure. host ".$this->host,900); 
    }  
  } 

}