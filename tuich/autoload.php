<?php

function tfCoreAutoload($class){
  
	$path = TF_REQUIRE_PATH."/tuich/core/src/{$class}.php";
	
	if (is_file($path)){
      require $path;
    }  
}

function tfModelBaseAutoload($class){
    $path = TF_REQUIRE_PATH."/modelBase/{$class}.mdl.php";
    if (is_file($path)){
      require $path;

    } 
}

function tfModelAutoload($class){
    $path = TF_REQUIRE_PATH."/model/{$class}.mdl.php";
    if (is_file($path)){
      require $path;

    } 
}


spl_autoload_register("tfCoreAutoload");
spl_autoload_register("tfModelBaseAutoload");
spl_autoload_register("tfModelAutoload");