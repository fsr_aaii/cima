<?php
switch ($tfFnName){
  case "paymentDateConfirmed": 
    $response["code"]="OK";
    $response["rate"]=CoExchange::rateByDate($tfs,$tfRequest->id_person,$tfRequest->date);
    echo json_encode($response,true);
  break;
  case "paymentByYear": 
    $response["code"]="OK";
    $response["year"]=$tfRequest->co_payment_year;

    $coExchangeRate=CoExchange::rateUnit($tfs,$tfRequest->co_payment_id_unit); 
    
    $coPaymentList=CoPayment::dLByUnitYear($tfs,$tfRequest->co_payment_id_unit,$tfRequest->co_payment_year);

    $html= '<div  id="co-payment-y-'.$tfRequest->co_payment_year.'" class="co-payment-list row col-12 p-0">';
    if (count($coPaymentList)>0){
     foreach ($coPaymentList as $row){
      $tfData["co_payment_id"] = $row["id"];

      if ($row["status"]=='A'){
        $check='<div class="badge-container"> 
                                <span class="badge badge-pill circle-check float-right m-2"><i class="bx bx-check"></i></span>
                              </div>';
        $action = 'AC';           
        $status = 'Aprobado';              
      }elseif ($row["status"]=='R'){
        $check='<div class="badge-container"> 
                                <span class="badge badge-pill circle-block float-right m-2"><i class="bx bx-block"></i></span>
                              </div>';
        $action = 'AC'; 
        $status = 'Rechado';                     
      }else{
        $check='';
        $action = 'AE';
        $status = 'Por Aprobar';
      }

      if ($row["currency"]=='VEB'){
        $VEB = $row["amount"];
        $USD = round($row["amount"]/$row["rate"],2);
      }else{
        $USD = $row["amount"];
        $VEB = round($row["amount"]*$row["rate"],2);
      }
      $html.='  <div class="col-lg-4 coaching p-1">
      <div class="col-lg-12 card-01 p-1 " data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoPayment" data-tf-action="'.$action.'" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
             '.$check.'
             <img class="card-img-top" src="'.$row["image_path"].'" alt="">
              <div class="d-flex align-items-center p-4" >
                  <div class="col-12 p-0 ml-2">
                     <div class="epic col-12 c-text-1 text-white p-0 m-0">'.$row["REF"].'</div>
                      <div class="epic col-12 c-text-6 text-white p-0 m-0">'.$row["payment_date"].'</div>
                      <div class="col-12 epic c-text-3 text-white p-0 m-0">'.TfWidget::amount($VEB).' VEB</div>
                      <div class="col-12 epic c-text-3 text-white p-0 m-0">'.TfWidget::amount($USD).' USD</div>
                      <div class="epic col-12 c-text-5 text-white p-0 m-0">'.$status.'</div> 
                  </div>

              </div>
            </div>
            </div>'; 


    
     }
   }else{
    $html.=TfWidget::alertInfoTemplate("No se encontraron pagos");
   }  
   $html.='</div>';
   $response["content"]=$html;
    echo json_encode($response,true);
  break;
}  