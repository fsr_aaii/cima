<?php
switch ($tfFnName){

  case "billByYear": 
    $response["code"]="OK";
    $response["year"]=$tfRequest->co_bill_year;

    $coExchangeRate=CoExchange::rateUnit($tfs,$tfRequest->co_bill_id_unit); 
    
    $coBillList=CoBill::dLByUnitYear($tfs,$tfRequest->co_bill_id_unit,$tfRequest->co_bill_year);

    $html= '<div  id="co-bill-y-'.$tfRequest->co_bill_year.'" class="co-bill-list row col-12 p-0">';
    if (count($coBillList)>0){
      foreach ($coBillList as $row){
    $tfData["co_bill_id"] = $row["id"];

     if ($row["status"]=='PAGADO'){
      $check='<div class="badge-container"> 
                              <span class="badge badge-pill circle-check float-right m-2"><i class="bx bx-check"></i></span>
                            </div>';
    }else{
      $check='';
    }
     
    if ($row["is_rate_current"]=='Y'){
    //  $amount = round($row["amount"]/$coExchangeRate,2); 
      $amount = round($row["amount"]/$row["rate_original"],2);
    }else{
     // $amount = round($row["amount"]/$row["rate"],2);
      $amount = round($row["amount"]/$row["rate_original"],2);
    }
    $html.='   <div class="col-lg-4 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoBill" data-tf-action="AC" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">'.$check.'
            <div class="d-flex align-items-center card-01 p-4" >
                <div class="ml-2">
                    <div class="epic col-12 c-text-1 text-white p-0 m-0">'.$row["month"].' '.$row["REF"].'</div>
                    <div class="epic col-12 c-text-6 text-white p-0 m-0">'.$row["payment_date"].'</div>
                  '; //  <div class="epic col-12 c-text-3 text-white p-0 m-0">'.TfWidget::amount($row["amount"]).' VEB </div> 
                  $html.='<div class="epic col-12  c-text-3 text-white p-0 m-0">'.TfWidget::amount($amount).' USD'.'</div> 
                    <div class="epic col-12 c-text-5 text-white p-0 m-0">'.$row["status"].'</div> 
                    
                </div>
            </div>
          </div>';  
   }          


   $html.='</div>
          </div>'; 


  
    }else{
      $html.=TfWidget::alertInfoTemplate("No se encontraron recibos para el a&ntilde;o ".$tfRequest->co_bill_year);
    }  
   $html.='</div>';
   $response["content"]=$html;
    echo json_encode($response,true);
  break;
}  