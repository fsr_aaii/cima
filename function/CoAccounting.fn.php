<?php
switch ($tfFnName){
    case "accountingByYear": 
    $response["code"]="OK";
    $response["year"]=$tfRequest->co_accounting_year;

    $coAccountingList=CoAccounting::dLByPersonYear($tfs,$tfRequest->co_accounting_id_person,$tfRequest->co_accounting_year);

    $html= '<div  id="co-accounting-y-'.$tfRequest->co_accounting_year.'" class="co-accounting-list row col-12 p-0">';
    if (count($coAccountingList)>0){
     foreach ($coAccountingList as $row){
        $tfData["co_accounting_id"] = $row["id"];

        if ($row["processed_date"]!=''){
          $check='<div class="badge-container"> 
                                  <span class="badge badge-pill circle-check float-right m-2"><i class="bx bx-check"></i></span>
                                </div>';
          $status = 'PROCESADO';
          $action='ACC';
        }else{
          $check='';
          $action='AE';
          $status = 'POR PROCESADO';
        }

        $html.='  <div class="col-lg-4 coaching" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CoAccounting" data-tf-action="'.$action.'" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                <div class="d-flex align-items-center card-01 p-4" >
                '.$check.'
                    <div class="ml-2">
                        <div class="epic c-text-1 text-white m-0">'.$row["month"].'</div>
                        <div class="epic c-text-3 text-white m-0">'.$row["payment_date"].'</div>
                        <div class="epic c-text-5 text-white m-0">'.$status.'</div>
                    </div>
                </div>
              </div>';  
       }
   }else{
    $html.=TfWidget::alertInfoTemplate("No se encontraron gastos");
   }  
   $html.='</div>';
   $response["content"]=$html;
    echo json_encode($response,true);
  break;
}  