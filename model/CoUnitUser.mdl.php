<?php
  class CoUnitUser extends CoUnitUserBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_unit"=>true,
                              "id_user"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }

   public function create(){
    parent::create();
    if ($this->isValid()){ 
       $q = "SELECT 1 exist 
                FROM t_user_role
               WHERE id = ?
                AND id_functional_area_role = 3";
       
       $param = array($this->id_user);
       list($rs) = $this->tfs->executeQuery($q,$param); 

       if ($rs["exist"]!=1){
         $q = "INSERT INTO t_user_role (id_user,id_functional_area_role) VALUES(?,3)";
         
         $param = array($this->id_user);
         $this->tfs->execute($q,$param); 
       } 
    }  
  }

  public function delete(){
    parent::delete();
    if ($this->isValid()){ 
       $q = "SELECT count(*) exist 
                FROM co_unit_user
               WHERE id_user = ?
                AND active = 'Y'";
       
       $param = array($this->id_user);
       list($rs) = $this->tfs->executeQuery($q,$param); 

       if ($rs["exist"]=='0'){
         $q = "UPDATE t_user_role SET active = 'N' WHERE id_user = ? AND id_functional_area_role = 3";
         
         $param = array($this->id_user);
         $this->tfs->execute($q,$param); 
       } 
    }  
  }

  public function inactivate(){
    $this->active = 'N';
    $this->update();
    if ($this->isValid()){ 
       $q = "SELECT count(*) exist 
                FROM co_unit_user
               WHERE id_user = ?
                AND active = 'Y'";
       
       $param = array($this->id_user);
       list($rs) = $this->tfs->executeQuery($q,$param); 

       if ($rs["exist"]=='0'){
         $q = "UPDATE t_user_role SET active = 'N' WHERE id_user = ? AND id_functional_area_role = 3";
         
         $param = array($this->id_user);
         $this->tfs->execute($q,$param); 
       } 
    }  
  }


   public function activate(){
    $this->active = 'Y';
    $this->update();
    if ($this->isValid()){ 
      $q = "UPDATE t_user_role SET active = 'Y' WHERE id_user = ? AND id_functional_area_role = 3";     
      $param = array($this->id_user);
         $this->tfs->execute($q,$param);   
    }  
  }

  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM co_unit_user
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM co_unit_user
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name id_unit,
                 c.name id_user,
                 a.active,
                 d.name created_by,
                 a.created_date
            FROM co_unit_user a,
                 co_unit b,
                 t_user c,
                 t_user d
           WHERE b.id = a.id_unit
           AND c.id = a.id_user
           AND d.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function dlByUser(TfSession $tfs,$idUser){
    $q = "SELECT c.id,
                 b.id id_person,
                 b.legal_name condominium,
                 c.id_unit,
                 a.name,
                 CONCAT(a.aliquot,'%') aliquot,
                 c.active
            FROM co_unit a,
                 gl_juridical_person b,
                 co_unit_user c
           WHERE b.id = a.id_person
             AND a.active = 'Y'
             AND a.id = c.id_unit
             AND c.id_user = ?";

    $param = array($idUser);         
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }


   public static function userByCondominium(TfSession $tfs,$idPerson){
    $q = "SELECT u.id,
                 u.name,
                 u.email_account,
                 a.name unit                 
            FROM co_unit a,  
                 t_user u,
                 co_unit_user uu  ,
                 gl_juridical_person b
           WHERE u.id = uu.id_user 
             AND uu.active = 'Y'
             AND uu.id_unit = a.id
             AND b.id = a.id_person
             AND a.active = 'Y'
             AND a.id_person = ?";

    $param=array($idPerson);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
