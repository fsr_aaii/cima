<?php
  class CoPromptPayment extends CoPromptPaymentBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_unit"=>true,
                              "rate"=>true,
                              "amount"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM co_prompt_payment
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM co_prompt_payment
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_unit,
                 a.rate,
                 a.amount,
                 b.name created_by,
                 a.created_date
            FROM co_prompt_payment a,
                 t_user b
           WHERE b.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
