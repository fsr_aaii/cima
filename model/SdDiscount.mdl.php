<?php
  class SdDiscount extends SdDiscountBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_transaction_element"=>true,
                              "percentage"=>true,
                              "date_from"=>true,
                              "date_to"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM sd_discount
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM sd_discount
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name id_transaction_element,
                 a.percentage,
                 a.date_from,
                 a.date_to,
                 c.name created_by,
                 a.created_date
            FROM sd_discount a,
                 fi_transaction_element b,
                 t_user c
           WHERE b.id = a.id_transaction_element
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function dataListByTE(TfSession $tfs,$idTransactionElement){
    $q = "SELECT a.id,
                 b.name id_transaction_element,
                 a.percentage,
                 a.date_from,
                 a.date_to,
                 c.name created_by,
                 a.created_date
            FROM sd_discount a,
                 fi_transaction_element b,
                 t_user c
           WHERE b.id = a.id_transaction_element
           AND c.id = a.created_by
           AND a.id_transaction_element = ?";

    $param = array($idTransactionElement);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
