<?php
  class HrUserEvaluationCriteria extends HrUserEvaluationCriteriaBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_user_evaluation"=>true,
                              "id_evaluation_criteria"=>true,
                              "value"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM hr_user_evaluation_criteria
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM hr_user_evaluation_criteria
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_user_evaluation,
                 c.description id_evaluation_criteria,
                 a.value,
                 d.name created_by,
                 a.created_date
            FROM hr_user_evaluation_criteria a,
                 hr_user_evaluation b,
                 hr_evaluation_criteria c,
                 t_user d
           WHERE b.id = a.id_user_evaluation
           AND c.id = a.id_evaluation_criteria
           AND d.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function dLbyEvaluation(TfSession $tfs,$id_user_evaluation){
    $q = "SELECT a.id,a.id_evaluation_criteria,
                 c.description evaluation_criteria
            FROM hr_user_evaluation_criteria a,
                 hr_evaluation_criteria c
           WHERE c.id = a.id_evaluation_criteria
           AND a.id_user_evaluation = ?";
    
    $param = array($id_user_evaluation);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
