<?php
  class CoAccountingDetail extends CoAccountingDetailBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_accounting"=>true,
                              "id_account"=>true,
                              "description"=>true,
                              "observation"=>true,
                              "amount"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",description \"option\"
            FROM co_accounting_detail
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT description description
            FROM co_accounting_detail
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_accounting,
                 c.name id_account,
                 a.description,
                 a.observation,
                 a.amount,
                 d.name created_by,
                 a.created_date
            FROM co_accounting_detail a,
                 co_accounting b,
                 co_account c,
                 t_user d
           WHERE b.id = a.id_accounting
           AND c.id = a.id_account
           AND d.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function dLByAccounting(TfSession $tfs,$idAccounting){
    $q = "SELECT a.id,b.rate,
                 a.id_accounting,
                 c.name account,
                 a.description,
                 a.observation,
                 (a.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END) amount,
                 d.name created_by,
                 a.created_date
            FROM co_accounting_detail a,
                 co_accounting b,
                 co_account c,
                 t_user d
           WHERE b.id = a.id_accounting
           AND c.id = a.id_account
           AND d.id = a.created_by
           AND a.id_accounting = ?";

    $param = array($idAccounting);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function sumaryByAccounting(TfSession $tfs,$idAccounting){
    $q = "SELECT c.name account,b.rate,
     SUM((a.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END)) amount
            FROM co_accounting_detail a,
                 co_accounting b,
                 co_account c
           WHERE b.id = a.id_accounting
           AND c.id = a.id_account
           AND a.id_accounting = ?
           GROUP BY c.name,b.rate";

    $param = array($idAccounting);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
