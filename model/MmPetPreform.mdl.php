<?php
  class MmPetPreform extends MmPetPreformBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_transaction_element"=>true,
                              "id_color"=>true,
                              "id_polymer_closure_type"=>true,
                              "weight"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM mm_pet_preform
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM mm_pet_preform
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name transaction_element,
                 c.name color,
                 d.name polymer_closure_type,
                 a.weight,
                 a.active,
                 e.name created_by,
                 a.created_date,
                 b.id id_transaction_element,
                 c.id id_color,
                 d.id id_polymer_closure_type
            FROM mm_pet_preform a,
                 fi_transaction_element b,
                 mm_color c,
                 mm_polymer_closure_type d,
                 t_user e
           WHERE b.id = a.id_transaction_element
           AND c.id = a.id_color
           AND d.id = a.id_polymer_closure_type
           AND e.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
