<?php
  class CoCondominiumExchangeType extends CoCondominiumExchangeTypeBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_person"=>true,
                              "exchange_type"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }

  public function create(){
     $q = "UPDATE co_condominium_exchange_type
              SET active = 'N'
           WHERE active = 'Y'
             AND id_person=?";
    $param = array($this->id_person);

    $this->tfs->execute($q,$param);
    parent::create();
  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM co_condominium_exchange_type
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM co_condominium_exchange_type
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.legal_name id_person,
                 a.exchange_type,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM co_condominium_exchange_type a,
                 gl_juridical_person b,
                 t_user c
           WHERE b.id = a.id_person
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  
   public static function dLByPerson(TfSession $tfs,$idPerson){
    $q = "SELECT a.id,
                 CASE WHEN a.exchange_type = 'O' THEN 'BCV' ELSE 'LIBRE' END exchange_type,
                 a.created_date
            FROM co_condominium_exchange_type a
           WHERE a.id_person = ?
           ORDER BY a.created_date DESC";

    $param = array($idPerson);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
