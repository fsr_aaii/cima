<?php
  class HrUserHomework extends HrUserHomeworkBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_user"=>true,
                              "date"=>true,
                              "active"=>true,
                              "id_evaluation_type"=>true,
                              "evaluator_type"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM hr_user_homework
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM hr_user_homework
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_user,
                 a.date,
                 a.active,
                 b.name id_evaluation_type,
                 a.evaluator_type,
                 c.name created_by,
                 a.created_date
            FROM hr_user_homework a,
                 hr_evaluation_type b,
                 t_user c
           WHERE b.id = a.id_evaluation_type
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public function populate4ThisMonth(){
    $q = "SELECT id
            FROM hr_user_homework
           WHERE id_user=?
             AND MONTH(date)=?
             AND YEAR(date)=?";

    $param = array($this->tfs->getUserId(),date("m"),date("Y"));

    list($rs) = $this->tfs->executeQuery($q,$param);

    if ($rs["id"]!=''){
      $this->populatePk($rs["id"]);
    }else{
      $this->id_user=$this->tfs->getUserId();
      $this->date=date("Y-m-d");
      $this->active="Y";
      $this->id_evaluation_type="1";
      $this->evaluator_type="U";
      $this->created_by=$this->tfs->getUserId();
      $this->created_date=date("Y-m-d H:i:s");
      $this->setValidations();
      $this->create();

      $q="INSERT INTO hr_user_homework_criteria
          (id_user_homework,id_evaluation_criteria,created_by,created_date)
          SELECT ?,id,?,? FROM hr_evaluation_criteria
          ORDER BY RAND()
          LIMIT ?;";

      $param = array($this->id,$this->created_by,$this->created_date,APP_HOMEWORK_QTY);    

      $this->tfs->execute($q,$param);

      $this->tfs->checkTrans();

    }

  }  

}
?>
