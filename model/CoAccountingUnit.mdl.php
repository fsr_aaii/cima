<?php
  class CoAccountingUnit extends CoAccountingUnitBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_accounting"=>true,
                              "id_unit"=>true,
                              "id_account"=>true,
                              "description"=>true,
                              "observation"=>true,
                              "amount"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",description \"option\"
            FROM co_accounting_unit
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT description description
            FROM co_accounting_unit
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public function getIdPerson(){ 
    $q = "SELECT id_person
            FROM co_accounting
           WHERE id=?";

    $param = array($this->id_accounting);
    list($rs) = $this->tfs->executeQuery($q,$param);

    return $rs["id_person"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_accounting,
                 a.id_unit,
                 a.id_account,
                 a.description,
                 a.observation,
                 a.amount,
                 e.name created_by,
                 a.created_date
            FROM co_accounting_unit a,
                 co_accounting b,
                 co_unit c,
                 co_account d,
                 t_user e
           WHERE b.id = a.id_accounting
           AND c.id = a.id_unit
           AND d.id = a.id_account
           AND e.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function dLByAccounting(TfSession $tfs,$idAccounting){
    $q = "SELECT a.id,b.rate,
                 a.id_accounting,
                 c.name account,
                 a.description,
                 a.observation,
                 (a.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END) amount,
                 d.name unit
            FROM co_accounting_unit a,
                 co_accounting b,
                 co_account c,
                 co_unit d
           WHERE b.id = a.id_accounting
           AND c.id = a.id_account
           AND d.id = a.id_unit
           AND a.id_accounting = ?";

    $param = array($idAccounting);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  public static function dLByAccountingU(TfSession $tfs,$idAccounting,$idUnit){
    $q = "SELECT a.id,b.rate,
                 a.id_accounting,
                 c.name account,
                 a.description,
                 a.observation,
                 (a.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END) amount,
                 d.name unit
            FROM co_accounting_unit a,
                 co_accounting b,
                 co_account c,
                 co_unit d
           WHERE b.id = a.id_accounting
           AND c.id = a.id_account
           AND d.id = a.id_unit
           AND a.id_unit = ?
           AND a.id_accounting = ?";

    $param = array($idUnit,$idAccounting);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
