<?php
  class OpInspectionReportDetail extends OpInspectionReportDetailBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "row"=>true,
                              "hash"=>true,
                              "id_travel"=>true,
                              "SICA_guide"=>true,
                              "id_list"=>true,
                              "transport"=>true,
                              "name"=>true,
                              "nin"=>true,
                              "chuto"=>true,
                              "id_destination"=>true,
                              "BL"=>true,
                              "admission_date"=>true,
                              "admission_time"=>true,
                              "tara_number"=>true,
                              "status_1"=>true,
                              "operation_day"=>true,
                              "survey_guide"=>true,
                              "ocamar_guide"=>true,
                              "store"=>true,
                              "gross_weight"=>true,
                              "tara_weight"=>true,
                              "net_weight"=>true,
                              "aggregate"=>true,
                              "ROB"=>true,
                              "status_2"=>true,
                              "id_travel_ocamar"=>true,
                              "departure_date"=>true,
                              "departure_time"=>true,
                              "seal"=>true,
                              "observation"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",name \"option\"
            FROM op_inspection_report_detail
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT name description
            FROM op_inspection_report_detail
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.row,
                 a.hash,
                 a.id_travel,
                 a.SICA_guide,
                 a.id_list,
                 a.transport,
                 a.name,
                 a.nin,
                 a.chuto,
                 b.name id_destination,
                 a.BL,
                 a.admission_date,
                 a.admission_time,
                 a.tara_number,
                 a.status_1,
                 a.operation_day,
                 a.survey_guide,
                 a.ocamar_guide,
                 a.store,
                 a.gross_weight,
                 a.tara_weight,
                 a.net_weight,
                 a.aggregate,
                 a.ROB,
                 a.status_2,
                 a.id_travel_ocamar,
                 a.departure_date,
                 a.departure_time,
                 a.seal,
                 a.observation,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM op_inspection_report_detail a,
                 op_destination b,
                 t_user c
           WHERE b.id = a.id_destination
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }


  public static function inactive(TfSession $tfs,$idFile){ 
    $q = "UPDATE op_inspection_report_detail a,
                 op_destination b,
                 op_inspection_report c
             SET a.active = 'N'
           WHERE a.active = 'Y'
             AND a.id_destination = b.id
             AND b.id_inspection_report = c.id
             AND c.id_file=?";

    $param = array($idFile);
    $tfs->execute($q,$param);

  }


   public static function listHash(TfSession $tfs,$idFile){ 
    $q = "SELECT a.id,a.row,a.hash
            FROM op_inspection_report_detail a,
                 op_destination b,
                 op_inspection_report c
            WHERE a.active = 'Y'
              AND a.id_destination = b.id
              AND b.id_inspection_report = c.id
              AND c.id_file=?
            ORDER BY a.row";
    
    $param = array($idFile);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
