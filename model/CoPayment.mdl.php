<?php
  class CoPayment extends CoPaymentBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_unit"=>true,
                              "currency"=>true,
                              "currency_confirmed"=>true,
                              "payment_date"=>true,
                              "payment_date_confirmed"=>true,
                              "rate"=>true,
                              "rate_confirmed"=>true,
                              "amount"=>true,
                              "amount_confirmed"=>true,
                              "image_path"=>true,
                              "observation"=>true,
                              "status"=>true,
                              "confirmed_by"=>true,
                              "confirmed_date"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }

  public function confirm(){ 
    
    if ($this->status=="I"){
      $this->status = "A";
      $this->confirmed_by = $this->tfs->getUserId();
      $this->confirmed_date = date("Y-m-d H:i:s");
      $this->setValidations();
      $this->update();
      
     
      $coBillList=CoBill::dueByUnit($this->tfs,$this->id_unit);
      $coExchangeRate=CoExchange::rateUnit($this->tfs,$this->id_unit); 

      $promptPayment = CoCondominiumPromptPayment::infoByUnit($this->tfs,$this->id_unit);
      $since = CoBill::since($this->tfs,$this->id_unit,$this->payment_date_confirmed);
      $prompt = false;
      $promptAmount = 0;
      if ($since["days"] <= $promptPayment["days"]){
          $prompt = true;
        }
      
      
      //echo "<br>tasa: ".$this->id_unit.":".$coExchangeRate; 
      //echo "<br>Deuda:";
      //print_r($coBillList);    

      if ($this->currency_confirmed =='VEB'){
         $amountConfirmed = round($this->amount_confirmed/$this->rate_confirmed,2);
      }else{
         $amountConfirmed = $this->amount_confirmed;
      } 
      
      //echo "<br>Monto Confirmado USD:".$amountConfirmed;


      $credit = CoCredit::balanceByUnit($this->tfs,$this->id_unit) + $amountConfirmed;

      //echo "<br>Credito Inicial USD:".$credit;

      $debit = true;
      
      foreach ($coBillList as $row){

        if (round($credit,2)<round($row["amount"],2)){
          //echo "<br>break:";
          break;
        }else{

          //echo "<br>Bill USD:".$row["amount"];

          //$credit = $credit-$row["amount"];

          $credit = round($credit,2)-round($row["amount"],2);
          if ($row["type"]=='B'){
            $q = "UPDATE co_bill 
                     SET paymented_by = ?, paymented_date=?, rate_payment=?
                   WHERE id = ?";
            $param = array($this->tfs->getUserId(),date("Y-m-d H:i:s"),$coExchangeRate,$row["id"]);
            $this->tfs->execute($q,$param);       
          }elseif ($row["type"]=='D'){
            $q = "UPDATE co_previous_due 
                     SET paymented_by = ?, paymented_date=?
                   WHERE id = ?";
            $param = array($this->tfs->getUserId(),date("Y-m-d H:i:s"),$row["id"]);
          $this->tfs->execute($q,$param);       
          }
       

          if ($prompt){
            $promptAmount+= round(round($row["amount"],2)*($promptPayment["credit_percentage"]/100),2);
            echo "PRONTO ".$promptAmount;
          }
          if ($debit){
            //echo "<br>Credito Cero";  

            $q = "INSERT INTO co_credit_exchange_fix (id_credit,amount,rate,created_by,created_date)
                  SELECT id,balance,?,?,?
                    FROM co_credit
                   WHERE id_unit = ?
                     AND status = 'E'";

            $param = array($coExchangeRate,$this->created_by,$this->created_date,$this->id_unit);
            $this->tfs->execute($q,$param);
                   
            $q = "UPDATE co_credit 
                     SET balance=0,status = 'D'
                   WHERE id_unit = ?
                     AND status = 'E'";

            $param = array($this->id_unit);
            $this->tfs->execute($q,$param);

            $debit=false;

          }


        }
      }  
      
      //echo "<br>Credito Final USD:".$credit;
      if (round($credit+$promptAmount,2)>0){
        
        if ($promptAmount>0){
          $coPromptPayment = new CoPromptPayment($this->tfs);
          $coPromptPayment->setIdUnit($this->id_unit);
          $coPromptPayment->setAmount($promptAmount);
          $coPromptPayment->setRate($coExchangeRate);
          $coPromptPayment->setCreatedBy($this->tfs->getUserId());
          $coPromptPayment->setCreatedDate(date("Y-m-d H:i:s"));
          $coPromptPayment->setValidations();
          $coPromptPayment->create();  
        }

        $q = "UPDATE co_credit 
                     SET balance=0,status = 'D'
                   WHERE id_unit = ?
                     AND status = 'E'";

            $param = array($this->id_unit);
            $this->tfs->execute($q,$param);
      
        //echo "<br>Nuevo Credito USD:".$credit;
        $coCredit = new CoCredit($this->tfs);
        $coCredit->setIdUnit($this->id_unit);
        $coCredit->setAmount(($credit+$promptAmount));
        $coCredit->setBalance(($credit+$promptAmount));
        $coCredit->setRate($coExchangeRate);
        $coCredit->setStatus('E');
        $coCredit->setCreatedBy($this->tfs->getUserId());
        $coCredit->setCreatedDate(date("Y-m-d H:i:s"));
        $coCredit->setValidations();
        $coCredit->create();
      } 
    }

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM co_payment
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM co_payment
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name id_unit,
                 a.amount,
                 a.amount_confirmed,
                 a.image_path,
                 a.observation,
                 a.confirmed_by,
                 a.confirmed_date,
                 c.name created_by,
                 a.created_date
            FROM co_payment a,
                 co_unit b,
                 t_user c
           WHERE b.id = a.id_unit
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function dLByUnit(TfSession $tfs,$idUnit){
    $q = "SELECT a.id,
                 b.name unit,
                 a.currency,
                 a.rate,
                 a.amount,
                 a.amount_confirmed,
                 a.image_path,
                 a.observation,
                 a.confirmed_by,
                 a.confirmed_date,
                 a.status,
                 a.payment_date,
                 CONCAT(m.name,' P',YEAR(a.payment_date),LPAD(MONTH(a.payment_date),2,0),'#',a.id) REF
            FROM co_payment a,
                 co_unit b,
                 gl_month m
           WHERE m.id = MONTH(a.payment_date)
           AND b.id = a.id_unit
           AND a.id_unit = ?";

    $param = array($idUnit);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function dLByUnitYear(TfSession $tfs,$idUnit,$year){
    $q = "SELECT a.id,
                 b.name unit,
                 a.currency,
                 a.rate,
                 a.amount,
                 a.amount_confirmed,
                 a.image_path,
                 a.observation,
                 a.confirmed_by,
                 a.confirmed_date,
                 a.status,
                 a.payment_date,
                 CONCAT(m.name,' P',YEAR(a.payment_date),LPAD(MONTH(a.payment_date),2,0),'#',a.id) REF
            FROM co_payment a,
                 co_unit b,
                 gl_month m
           WHERE m.id = MONTH(a.payment_date)
           AND b.id = a.id_unit
           AND YEAR(coalesce(a.payment_date_confirmed,a.payment_date)) = ?
           AND a.id_unit = ?";

    $param = array($year,$idUnit);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  public static function dLByCondominium(TfSession $tfs,$idCondominium){
    $q = "SELECT a.id,
                 b.name unit,
                 a.currency,
                 a.rate,
                 a.amount,
                 a.amount_confirmed,
                 a.image_path,
                 a.observation,
                 a.confirmed_by,
                 a.confirmed_date,
                 c.name created_by,
                 a.status,
                 a.payment_date,
                 CONCAT(m.name,' P',YEAR(a.payment_date),LPAD(MONTH(a.payment_date),2,0),'#',a.id) REF
            FROM co_payment a,
                 co_unit b,
                 t_user c,
                 gl_month m
           WHERE  m.id = MONTH(CASE WHEN a.payment_date = '0000-00-00' then a.created_date else a.payment_date end )
           AND a.status = 'I'
           AND a.id_unit =  b.id
           AND c.id = a.created_by
           AND b.id_person = ?";

    $param = array($idCondominium);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

   public static function qty2ApproveByCondominium(TfSession $tfs,$idCondominium){
    $q = "SELECT count(*) qty
            FROM co_payment a,
                 co_unit b
           WHERE  a.id_unit =  b.id
             AND a.status = 'I'
             AND b.id_person = ?";

    $param = array($idCondominium);       
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["qty"];
  }


  public static function customerList(TfSession $tfs){
    $q = "SELECT b.id,b.trade_name name,CONCAT(b.nin_type,b.nin,'-',b.nin_control_digit) rif,
                 a.date_from,a.id id_relationship,b.phone_number, p.qty payments
            FROM gl_person_relationship a,
                 gl_juridical_person b LEFT JOIN (SELECT y.id_person,COUNT(x.id) qty 
                                                    FROM co_payment x ,
                                                         co_unit y
                                                   WHERE y.id = x.id_unit
                                                     AND x.status = 'I'
                                                    GROUP BY y.id_person ) p ON p.id_person = b.id
           WHERE b.id = a.id_person
             AND a.id_relationship = 1";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
