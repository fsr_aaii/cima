<?php
  class CoAccountingPrevious extends CoAccountingPreviousBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_accounting"=>true,
                              "id_unit"=>true,
                              "id_account"=>true,
                              "description"=>true,
                              "observation"=>true,
                              "amount"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",description \"option\"
            FROM co_accounting_previous
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT description description
            FROM co_accounting_previous
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_accounting,
                 a.id_unit,
                 a.id_account,
                 a.description,
                 a.observation,
                 a.amount,
                 e.name created_by,
                 a.created_date
            FROM co_accounting_previous a,
                 co_accounting b,
                 co_unit c,
                 co_account d,
                 t_user e
           WHERE b.id = a.id_accounting
           AND c.id = a.id_unit
           AND d.id = a.id_account
           AND e.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
