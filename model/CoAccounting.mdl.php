<?php
  class CoAccounting extends CoAccountingBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_person"=>true,
                              "rate"=>true,
                              "payment_date"=>true,
                              "processed_by"=>true,
                              "processed_date"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }

  public function generate(){

    $coExchangeRate=CoExchange::rateByDate($this->tfs,$this->id_person,$this->payment_date); 

    
    $this->rate =$coExchangeRate;
    $this->processed_by =$this->tfs->getUserId();
    $this->processed_date =date("Y-m-d H:i:s");

      
    /*$q = "INSERT INTO co_bill (id_accounting,id_unit,aliquot,rate,balance_previous,balance_120,
                               balance_90,balance_60,balance_30,created_by,created_date)
            SELECT ?,u.id,u.aliquot,?,
                   SUM(coalesce(x.balance_previous,0)) balance_previous,SUM(coalesce(x.balance_120,0)) balance_120,
                   SUM(coalesce(x.balance_90,0)) balance_90,SUM(coalesce(x.balance_60,0)) balance_60,
                   SUM(coalesce(x.balance_30,0)) balance_30,?,CURRENT_TIMESTAMP()
            FROM co_unit u LEFT JOIN ( SELECT u.id,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) > 120 THEN ROUND((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate,2) ELSE 0 END) balance_previous,          
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) BETWEEN 91 AND 120 THEN ROUND((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate,2) ELSE 0 END) balance_120,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) BETWEEN 61 AND 90 THEN ROUND((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate,2) ELSE 0 END) balance_90,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) BETWEEN 31 AND 60 THEN ROUND((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate,2) ELSE 0 END) balance_60,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) < 31 THEN ROUND((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate,2) ELSE 0 END) balance_30
            FROM co_bill a,
                 co_bill_detail b,
                 co_accounting c,
                 co_accounting_detail d,
                 co_account e,
                 co_unit u
          WHERE e.id = d.id_account
            AND d.id_accounting = c.id
            AND c.id = a.id_accounting
            AND b.id_bill = a.id
            AND a.paymented_date IS NULL
            AND a.id_unit=u.id
            AND u.active = 'Y'
            AND u.id_person = ?
            GROUP BY u.id
UNION ALL   
SELECT u.id,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) > 120 THEN a.amount  END) balance_previous,          
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) BETWEEN 91 AND 120 THEN a.amount  END) balance_120,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) BETWEEN 61 AND 90 THEN a.amount  END) balance_90,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) BETWEEN 31 AND 60 THEN a.amount  END) balance_60,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) < 31 THEN a.amount  END) balance_30
            FROM co_previous_due a,
                 co_unit u
          WHERE a.paymented_date IS NULL
            AND a.id_unit=u.id
            AND u.active = 'Y'
            AND u.id_person = ?
            GROUP BY u.id) x ON u.id=x.id
          WHERE u.id_person = ?
        GROUP BY u.id,u.aliquot";*/


       $q = "INSERT INTO co_bill (id_accounting,id_unit,aliquot,rate,balance_previous,balance_120,
                               balance_90,balance_60,balance_30,created_by,created_date)
            SELECT ?,u.id,u.aliquot,?,0,0,0,0,0,?,CURRENT_TIMESTAMP()
            FROM co_unit u 
          WHERE u.id_person = ?
            AND u.active = 'Y'"; 




    $param = array($this->id,$coExchangeRate,$this->tfs->getUserId(),$this->id_person);
    
    $this->tfs->execute($q,$param);


    $q = "INSERT INTO co_bill_detail(id_bill,id_account,amount,type,created_by,created_date)
         SELECT a.id,b.id_account,ROUND((b.amount*a.aliquot)/100,2) amount,'G',a.created_by,a.created_date
                  FROM co_bill a,
                       (SELECT id_accounting,id_account,SUM(amount) amount
                           FROM co_accounting_detail
                    GROUP BY id_accounting,id_account) b
                WHERE b.id_accounting = a.id_accounting
                  AND a.id_accounting =?
UNION ALL                  
SELECT a.id,b.id_account,b.amount,'U',a.created_by,a.created_date
                  FROM co_bill a,
                       (SELECT id_accounting,id_unit,id_account,SUM(amount) amount
                           FROM co_accounting_unit
                    GROUP BY id_accounting,id_unit,id_account) b
                WHERE a.id_unit = b.id_unit
                  AND b.id_accounting = a.id_accounting
                  AND a.id_accounting =?";

    $param = array($this->id,$this->id);
    $this->tfs->execute($q,$param);

    $this->update();

    //verificar creditos
    /*$q = "SELECT  a.id_unit,
                  round(SUM(CASE WHEN c.id <> ? THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END),2) total,
                  round(SUM(CASE WHEN c.id = ? THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END),2) new
            FROM co_bill a,
                 co_bill_detail b,
                 co_accounting c,
                 co_accounting_detail d,
                 co_account e
          WHERE e.id = d.id_account
            AND d.id_accounting = c.id
            AND c.id_person = ?
            AND c.id = a.id_accounting
            AND b.id_bill = a.id
            AND a.paymented_date IS NULL
            GROUP BY a.id_unit,c.payment_date
            ORDER BY c.payment_date ASC";

    $param = array($this->id,$this->id,$this->id_person);
    $dues = $tfs->executeQuery($q,$param); */

    $credits = CoCredit::UnitBalanceByCondominium($this->tfs,$this->id_person);

    foreach ($credits as $row){
      //echo "<br>Unidad:".$row["id_unit"];
      //echo "<br>Credito Inicial USD:".$row["amount"];
      //echo "<br>Credito Final USD:".$row["amount"]-$totalDueByUnit;
      $totalDueByUnit = CoBill::totalDueByUnit2($this->tfs,$row["id_unit"]);

      //echo "<br>Bill USD:".$totalDueByUnit;

        if (($row["amount"]>=$totalDueByUnit) && ($totalDueByUnit>0)){         
        
          $q = "UPDATE co_bill 
                   SET paymented_by = ?, paymented_date=?, rate_payment=?
                 WHERE paymented_date IS NULL
                   AND id_unit = ?";

          $param = array($this->created_by,$this->created_date,$coExchangeRate,$row["id_unit"]);
          $this->tfs->execute($q,$param);

          $uCredits = CoCredit::dLBalanceByUnit($this->tfs,$row["id_unit"]);
              
          $auxDue = $totalDueByUnit;
             
          $fix=0;
          foreach ($uCredits as $c){
            unset($fix);
            if ($auxDue>0){
              if ($c["amount"]==$auxDue){

                $q = "INSERT INTO co_credit_exchange_fix (id_credit,amount,rate,created_by,created_date)
                      VALUES (?,?,?,?,?)";

                $param = array($c["id"],$c["amount"],$coExchangeRate,$this->created_by,$this->created_date);
                $this->tfs->execute($q,$param);

                $q = "UPDATE co_credit 
                         SET balance=0,status = 'D'
                       WHERE id = ?";

                $param = array($c["id"]);
                $this->tfs->execute($q,$param);

                $fix=$auxDue;
                $auxDue =0;

              }elseif ($c["amount"]<$auxDue) {
                  
                $q = "INSERT INTO co_credit_exchange_fix (id_credit,amount,rate,created_by,created_date)
                      VALUES (?,?,?,?,?)";

                $param = array($c["id"],$c["amount"],$coExchangeRate,$this->created_by,$this->created_date);
                $this->tfs->execute($q,$param);

                $q = "UPDATE co_credit 
                         SET balance=0,status = 'D'
                       WHERE id = ?";

                $param = array($c["id"]);
                $this->tfs->execute($q,$param);

                $fix=$c["amount"];
                $auxDue-=$c["amount"];

              }else{
                $q = "INSERT INTO co_credit_exchange_fix (id_credit,amount,rate,created_by,created_date)
                      VALUES (?,?,?,?,?)";

                $param = array($c["id"],$auxDue,$coExchangeRate,$this->created_by,$this->created_date);
                $this->tfs->execute($q,$param);

                $q = "UPDATE co_credit 
                        SET balance=?
                      WHERE id = ?";

                $param = array($c["amount"]-$auxDue,$c["id"]);
                $this->tfs->execute($q,$param);

                $fix=$auxDue;
                $auxDue=0;
              }
            }
          }//end foreach

          /////insertar pronto pago $totalDueByUnit * % pronto pago

          $promptPayment = CoCondominiumPromptPayment::infoByPerson($this->tfs,$this->id_person);

          if ($promptPayment["credit_percentage"]>0){
            $amount  = round($totalDueByUnit*($promptPayment["credit_percentage"]/100),2); 
                
            $coPromptPayment = new CoPromptPayment($this->tfs);
            $coPromptPayment->setIdUnit($row["id_unit"]);
            $coPromptPayment->setAmount($amount);
            $coPromptPayment->setRate($coExchangeRate);
            $coPromptPayment->setCreatedBy($this->tfs->getUserId());
            $coPromptPayment->setCreatedDate(date("Y-m-d H:i:s"));
            $coPromptPayment->setValidations();
            $coPromptPayment->create();  
                

            $coCredit = new CoCredit($this->tfs);
            $coCredit->setIdUnit($row["id_unit"]);
            $coCredit->setAmount($amount);
            $coCredit->setBalance($amount);
            $coCredit->setRate($coExchangeRate);
            $coCredit->setStatus('E');
            $coCredit->setCreatedBy($this->tfs->getUserId());
            $coCredit->setCreatedDate(date("Y-m-d H:i:s"));
            $coCredit->setValidations();
            $coCredit->create();  
          }             
        }  
    }  
    //end foreach    

    $q="UPDATE  co_bill y INNER JOIN 
    (SELECT id,ROUND(SUM(balance_previous),2) balance_previous,ROUND(SUM(balance_120),2) balance_120,ROUND(SUM(balance_90),2) balance_90,ROUND(SUM(balance_60),2) balance_60,ROUND(SUM(balance_30),2) balance_30,ROUND(SUM(balance),2) balance
        FROM (SELECT u.id, 
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) > 120 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_previous,          
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) BETWEEN 91 AND 120 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_120,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) BETWEEN 61 AND 90 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_90,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) BETWEEN 31 AND 60 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_60,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) < 31 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_30,
                  SUM((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ) balance
            FROM co_bill a,
                 co_bill_detail b,
                 co_accounting c,
                 co_account e,
                 co_unit u
          WHERE e.id = b.id_account
            AND c.id = a.id_accounting
            AND b.id_bill = a.id
            AND a.paymented_date IS NULL
            AND a.id_unit=u.id
            AND u.active = 'Y'
            AND u.id_person = ?
            GROUP BY u.id
        UNION ALL   
        SELECT   u.id,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) > 120 THEN a.amount  END) balance_previous,          
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) BETWEEN 91 AND 120 THEN a.amount  END) balance_120,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) BETWEEN 61 AND 90 THEN a.amount  END) balance_90,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) BETWEEN 31 AND 60 THEN a.amount  END) balance_60,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) < 31 THEN a.amount  END) balance_30,
                  SUM(a.amount) balance
            FROM co_previous_due a,
                 co_unit u
          WHERE a.paymented_date IS NULL
            AND a.id_unit=u.id
            AND u.active = 'Y'
            AND u.id_person = ?
            GROUP BY u.id
        UNION ALL
        SELECT   u.id,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) > 120 THEN a.balance  END)*-1 balance_previous,          
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) BETWEEN 91 AND 120 THEN a.balance  END)*-1 balance_120,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) BETWEEN 61 AND 90 THEN a.balance  END) *-1 balance_90,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) BETWEEN 31 AND 60 THEN a.balance  END)*-1 balance_60,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) < 31 THEN a.balance  END)*-1 balance_30,
                  SUM(a.balance)*-1 balance
          FROM co_credit a,
               co_unit u
        WHERE a.status='E'
        AND a.id_unit = u.id
        AND u.active = 'Y'
        AND u.id_person = ?
        GROUP BY u.id) x
    GROUP BY x.id) z
 ON  z.id = y.id_unit
   AND y.id_accounting = ?
SET y.balance_previous = coalesce(z.balance_previous,0), y.balance_120= coalesce(z.balance_120,0),
    y.balance_90 = coalesce(z.balance_90,0), y.balance_60 = coalesce(z.balance_60,0), y.balance_30 = coalesce(z.balance_30,0)";

$param = array($this->id_person,$this->id_person,$this->id_person,$this->id);

$this->tfs->execute($q,$param);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM co_accounting
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM co_accounting
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_person,
                 a.payment_date,
                 a.processed_by,
                 a.processed_date,
                 c.name created_by,
                 a.created_date
            FROM co_accounting a,
                 gl_person b,
                 t_user c
           WHERE b.id = a.id_person
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  
  public static function dLByPerson(TfSession $tfs,$id){
    $q = "SELECT a.id,
                 a.payment_date,
                 a.processed_by,
                 a.processed_date,
                 m.name month
            FROM co_accounting a,
                 gl_month m
          WHERE m.id = MONTH(a.payment_date)
            AND a.id_person = ?";
   
   $param = array($id);
   $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function dLByPersonYear(TfSession $tfs,$id,$year){
    $q = "SELECT a.id,
                 a.payment_date,
                 a.processed_by,
                 a.processed_date,
                 m.name month
            FROM co_accounting a,
                 gl_month m
          WHERE m.id = MONTH(a.payment_date)
            AND YEAR(a.payment_date) = ?
            AND a.id_person = ?";
   
   $param = array($year,$id);
   $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public function sumaryByAccounting(){
    $q = "SELECT x.account,x.rate,SUM(x.amount) amount
  FROM (SELECT c.name account,b.rate,
         SUM((a.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END)) amount
            FROM co_accounting_detail a,
               co_accounting b,
               co_account c
             WHERE b.id = a.id_accounting
             AND c.id = a.id_account
             AND a.id_accounting =?
             GROUP BY c.name,b.rate
      UNION ALL
      SELECT c.name account,b.rate,
         SUM((a.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END)) amount
            FROM co_accounting_unit a,
               co_accounting b,
               co_account c
             WHERE b.id = a.id_accounting
             AND c.id = a.id_account
             AND a.id_accounting =?
             GROUP BY c.name,b.rate) x
GROUP BY x.account,x.rate";

    $param = array($this->id,$this->id);       
    $rs = $this->tfs->executeQuery($q,$param);

    return $rs;
  }

  public function sumaryByAccountingU($idUnit){
    $q = "SELECT x.account,x.rate,SUM(x.amount) amount
  FROM (SELECT c.name account,b.rate,
         SUM((a.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END)) amount
            FROM co_accounting_detail a,
               co_accounting b,
               co_account c
             WHERE b.id = a.id_accounting
             AND c.id = a.id_account
             AND a.id_accounting =?
             GROUP BY c.name,b.rate
      UNION ALL
      SELECT c.name account,b.rate,
         SUM((a.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END)) amount
            FROM co_accounting_unit a,
               co_accounting b,
               co_account c
             WHERE b.id = a.id_accounting
             AND c.id = a.id_account
              AND a.id_unit =?
             AND a.id_accounting =?
             GROUP BY c.name,b.rate) x
GROUP BY x.account,x.rate";

    $param = array($this->id,$idUnit,$this->id);       
    $rs = $this->tfs->executeQuery($q,$param);

    return $rs;
  }

  
}
?>
