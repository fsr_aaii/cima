<?php
  class GlPersonBanking extends GlPersonBankingBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_person"=>true,
                              "bank"=>true,
                              "account"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public function create(){
     $q = "UPDATE gl_person_banking
              SET active = 'N'
           WHERE active = 'Y'
             AND id_person=?";
    $param = array($this->id_person);

    $this->tfs->execute($q,$param);
    parent::create();
  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM gl_person_banking
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM gl_person_banking
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_person,
                 a.bank,
                 a.account,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM gl_person_banking a,
                 gl_person b,
                 t_user c
           WHERE b.id = a.id_person
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  
  public static function dLByPerson(TfSession $tfs,$idPerson){
    $q = "SELECT a.id,
                 a.bank,
                 a.account,
                 a.active
            FROM gl_person_banking a
           WHERE a.id_person = ?
            ORDER BY a.created_date DESC";

    $param = array($idPerson);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

}
?>
