<?php
  class CoCondominiumPromptPayment extends CoCondominiumPromptPaymentBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_person"=>true,
                              "days"=>true,
                              "credit_percentage"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }

  public function create(){
     $q = "UPDATE co_condominium_prompt_payment
              SET active = 'N'
           WHERE active = 'Y'
             AND id_person=?";
    $param = array($this->id_person);

    $this->tfs->execute($q,$param);
    parent::create();
  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM co_condominium_prompt_payment
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM co_condominium_prompt_payment
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_person,
                 a.days,
                 a.active,
                 b.name created_by,
                 a.created_date
            FROM co_condominium_prompt_payment a,
                 t_user b
           WHERE b.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function dLByPerson(TfSession $tfs,$idPerson){
    $q = "SELECT a.id,
                 a.days,
                 a.credit_percentage,
                 a.created_date
            FROM co_condominium_prompt_payment a
           WHERE a.id_person = ?
           ORDER BY a.created_date DESC";

    $param = array($idPerson);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function infoByPerson(TfSession $tfs,$idPerson){
    $q = "SELECT a.id,
                 a.days,
                 a.credit_percentage,
                 a.created_date
            FROM co_condominium_prompt_payment a
           WHERE a.id_person = ?
             AND a.active = 'Y'
           ORDER BY a.created_date DESC";

    $param = array($idPerson);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs;
  }
   public static function infoByUnit(TfSession $tfs,$idUnit){
    $q = "SELECT a.id,
                 a.days,
                 a.credit_percentage,
                 a.created_date
            FROM co_condominium_prompt_payment a,
                 co_unit u
           WHERE a.active = 'Y'
             AND a.id_person = u.id_person
             AND u.id = ? 
           ORDER BY a.created_date DESC";

    $param = array($idUnit);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
