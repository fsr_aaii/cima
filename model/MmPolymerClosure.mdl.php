<?php
  class MmPolymerClosure extends MmPolymerClosureBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_transaction_element"=>true,
                              "id_polymer_closure_type"=>true,
                              "id_color"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM mm_polymer_closure
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM mm_polymer_closure
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name id_transaction_element,
                 c.name id_polymer_closure_type,
                 d.name id_color,
                 a.active,
                 e.name created_by,
                 a.created_date
            FROM mm_polymer_closure a,
                 fi_transaction_element b,
                 mm_polymer_closure_type c,
                 mm_color d,
                 t_user e
           WHERE b.id = a.id_transaction_element
           AND c.id = a.id_polymer_closure_type
           AND d.id = a.id_color
           AND e.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
