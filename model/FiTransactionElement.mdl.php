<?php
  class FiTransactionElement extends FiTransactionElementBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "name"=>true,
                              "code"=>true,
                              "type"=>true,
                              "id_transaction_element_rank"=>true,
                              "id_transaction_type"=>true,
                              "id_element_type"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",name \"option\"
            FROM fi_transaction_element
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT name description
            FROM fi_transaction_element
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.name,
                 a.code,
                 a.type,
                 b.name id_transaction_element_rank,
                 c.name id_transaction_type,
                 d.name id_element_type,
                 a.active,
                 e.name created_by,
                 a.created_date
            FROM fi_transaction_element a,
                 fi_transaction_element_rank b,
                 fi_transaction_type c,
                 fi_element_type d,
                 t_user e
           WHERE b.id = a.id_transaction_element_rank
           AND c.id = a.id_transaction_type
           AND d.id = a.id_element_type
           AND e.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
