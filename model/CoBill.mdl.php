<?php
  class CoBill extends CoBillBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_accounting"=>true,
                              "id_unit"=>true,
                              "aliquot"=>true,
                              "rate"=>true,
                              "rate_payment"=>true,
                              "balance_30"=>true,
                              "balance_60"=>true,
                              "balance_90"=>true,
                              "balance_120"=>true,
                              "balance_previous"=>true,
                              "paymented_by"=>true,
                              "paymented_date"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public function getPrintInfo(){
      
  }
  public static function REF(tfSession $tfs,$id){

    $q = "SELECT  CONCAT('R',YEAR(c.payment_date),LPAD(MONTH(c.payment_date),2,0),'#',a.id)  REF
            FROM co_bill a,
                 co_accounting c,
                 gl_month m
          WHERE m.id = MONTH(c.payment_date)
            AND c.id = a.id_accounting
            AND a.id=?";

    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

     return $rs["REF"];

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM co_bill
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM co_bill
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_accounting,
                 c.name id_unit,
                 a.aliquot,
                 a.paymented_by,
                 a.paymented_date,
                 d.name created_by,
                 a.created_date
            FROM co_bill a,
                 co_accounting b,
                 co_unit c,
                 t_user d
           WHERE b.id = a.id_accounting
           AND c.id = a.id_unit
           AND d.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }


  public static function dlByUnit(TfSession $tfs,$idUnit){
    $q = "SELECT  a.id,CONCAT('R',YEAR(c.payment_date),LPAD(MONTH(c.payment_date),2,0),'#',a.id)  REF,
                  c.payment_date,a.paymented_date,m.name month,a.aliquot,
                  CASE WHEN rate_payment IS NULL THEN a.rate ELSE rate_payment END rate,
                  SUM((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)) amount,
                  CASE WHEN a.paymented_date IS NULL THEN 'POR PAGAR' ELSE 'PAGADO' END status,
                  CASE WHEN a.paymented_date IS NOT NULL THEN 'N' 
                       ELSE CASE WHEN TIMESTAMPDIFF(DAY, c.payment_date, curdate())<=pp.days THEN 'N' ELSE 'Y' END  END is_rate_current  
            FROM co_bill a ,
                 co_bill_detail b,
                 co_accounting c,
                 co_account e,
                 gl_month m,
                 co_unit u LEFT JOIN (SELECT id_person,days 
                                        FROM co_condominium_prompt_payment 
                                       WHERE active = 'Y') pp ON pp.id_person = u.id_person 
          WHERE u.id = a.id_unit
            AND m.id = MONTH(c.payment_date)
            AND e.id = b.id_account
            AND c.id = a.id_accounting
            AND b.id_bill = a.id
            AND a.id_unit=?
            GROUP BY a.id,CONCAT('R',YEAR(c.payment_date),LPAD(MONTH(c.payment_date),2,0),'#',a.id)  ,a.paymented_date,c.payment_date,m.name,a.aliquot,CASE WHEN rate_payment IS NULL THEN a.rate ELSE rate_payment END,
                    CASE WHEN a.paymented_date IS NULL THEN 'POR PAGAR' ELSE 'PAGADO' END,
                    CASE WHEN a.paymented_date IS NOT NULL THEN 'N' 
                       ELSE CASE WHEN TIMESTAMPDIFF(DAY, c.payment_date, curdate())<=pp.days THEN 'N' ELSE 'Y' END  END";

    $param = array($idUnit);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

   public static function dLByUnitYear(TfSession $tfs,$idUnit,$year){
    $q = "SELECT  a.id,CONCAT('R',YEAR(c.payment_date),LPAD(MONTH(c.payment_date),2,0),'#',a.id)  REF,
                  c.payment_date,a.paymented_date,m.name month,a.aliquot,
                  CASE WHEN rate_payment IS NULL THEN a.rate ELSE rate_payment END rate,
                  SUM((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)) amount,
                  CASE WHEN a.paymented_date IS NULL THEN 'POR PAGAR' ELSE 'PAGADO' END status,
                  CASE WHEN a.paymented_date IS NOT NULL THEN 'N' 
                       ELSE CASE WHEN TIMESTAMPDIFF(DAY, c.payment_date, curdate())<=pp.days THEN 'N' ELSE 'Y' END  END is_rate_current  ,
                  a.rate rate_original
            FROM co_bill a ,
                 co_bill_detail b,
                 co_accounting c,
                 co_account e,
                 gl_month m,
                 co_unit u LEFT JOIN (SELECT id_person,days 
                                        FROM co_condominium_prompt_payment 
                                       WHERE active = 'Y') pp ON pp.id_person = u.id_person 
          WHERE u.id = a.id_unit
            AND m.id = MONTH(c.payment_date)
            AND YEAR(c.payment_date) = ?
            AND e.id = b.id_account
            AND c.id = a.id_accounting
            AND b.id_bill = a.id
            AND a.id_unit=?
            GROUP BY a.id,CONCAT('R',YEAR(c.payment_date),LPAD(MONTH(c.payment_date),2,0),'#',a.id)  ,a.paymented_date,c.payment_date,m.name,a.aliquot,CASE WHEN rate_payment IS NULL THEN a.rate ELSE rate_payment END,
                    CASE WHEN a.paymented_date IS NULL THEN 'POR PAGAR' ELSE 'PAGADO' END,
                    CASE WHEN a.paymented_date IS NOT NULL THEN 'N' 
                       ELSE CASE WHEN TIMESTAMPDIFF(DAY, c.payment_date, curdate())<=pp.days THEN 'N' ELSE 'Y' END  END";

    $param = array($year,$idUnit);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function dueByUnit(TfSession $tfs,$idUnit){
    $q = "SELECT  a.id,c.payment_date,round(SUM((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate),2) amount, 'B' type
            FROM co_bill a,
                 co_bill_detail b,
                 co_accounting c,
                 co_account e,
                 gl_month m
          WHERE m.id = MONTH(c.payment_date)
            AND e.id = b.id_account
            AND c.id = a.id_accounting
            AND b.id_bill = a.id
            AND a.paymented_date IS NULL
            AND a.id_unit=?
            GROUP BY a.id,c.payment_date
          UNION ALL   
          SELECT x.id,x.payment_date,x.amount, 'D' type
            FROM co_previous_due x
           WHERE x.paymented_date IS NULL
             AND x.id_unit=?
            GROUP BY x.id,x.payment_date
            ORDER BY 2 ASC";

    $param = array($idUnit,$idUnit);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  
  public static function totalDueByUnit(TfSession $tfs,$idUnit){
    $q = "SELECT SUM(x.amount) amount
            FROM (SELECT round(SUM((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate),2) amount
                    FROM co_bill a,
                         co_bill_detail b,
                         co_accounting c,
                         co_account e,
                         gl_month m
                  WHERE m.id = MONTH(c.payment_date)
                    AND e.id = b.id_account
                    AND c.id = a.id_accounting
                    AND b.id_bill = a.id
                    AND a.paymented_date IS NULL
                    AND a.id_unit=?
                  UNION ALL
                  SELECT SUM(amount) amount
                    FROM co_previous_due
                   WHERE paymented_date IS NULL
                     AND id_unit=?
                   UNION ALL
                   SELECT SUM(a.balance)*-1  amount
                    FROM co_credit a,
                         co_unit u
                  WHERE a.status='E'
                  AND a.id_unit = u.id
                  AND u.id = ?
                ) x";

    $param = array($idUnit,$idUnit,$idUnit);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["amount"];
  }

  public static function totalDueByUnit2(TfSession $tfs,$idUnit){
    $q = "SELECT SUM(x.amount) amount
            FROM (SELECT round(SUM((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate),2) amount
                    FROM co_bill a,
                         co_bill_detail b,
                         co_accounting c,
                         co_account e,
                         gl_month m
                  WHERE m.id = MONTH(c.payment_date)
                    AND e.id = b.id_account
                    AND c.id = a.id_accounting
                    AND b.id_bill = a.id
                    AND a.paymented_date IS NULL
                    AND a.id_unit=?
                  UNION ALL
                  SELECT SUM(amount) amount
                    FROM co_previous_due
                   WHERE paymented_date IS NULL
                     AND id_unit=?) x";

    $param = array($idUnit,$idUnit);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["amount"];
  }

  public static function since(TfSession $tfs,$idUnit,$date=""){
    $q = "SELECT MAX(x.days) days,round(SUM(x.rate)/count(*),2) rate
           FROM (SELECT coalesce(TIMESTAMPDIFF(DAY, MIN(c.payment_date), ?),-1)  days,round(SUM(a.rate)/count(*),2) rate
                FROM co_bill a,
                     co_accounting c
              WHERE c.id = a.id_accounting
                AND a.paymented_date IS NULL
                AND a.id_unit=?
             UNION ALL
                SELECT coalesce(TIMESTAMPDIFF(DAY, MIN(a.payment_date), ?),-1) days,0 rate
                  FROM co_previous_due a
                  WHERE a.paymented_date IS NULL
                  AND a.id_unit=?) x
                   WHERE days> 0";

    $param = array($date==""?date("Y-m-d"):$date,$idUnit,$date==""?date("Y-m-d"):$date,$idUnit);
    list($rs) = $tfs->executeQuery($q,$param);
  
    return $rs;
  }


  
  public static function balancePrevious(TfSession $tfs,$idUnit){
    $q = "SELECT DATE_SUB(CURDATE(),INTERVAL 120 DAY) balance_date,ROUND(SUM(coalesce(x.amount,0)),2) amount
            FROM (SELECT SUM((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate) amount
                    FROM co_bill a,
                       co_bill_detail b,
                       co_accounting c,
                       co_account e,
                       gl_month m
                    WHERE m.id = MONTH(c.payment_date)
                    AND e.id = b.id_account
                    AND DATEDIFF(CURDATE(),c.payment_date) > 120 
                    AND c.id = a.id_accounting
                    AND b.id_bill = a.id
                    AND a.id_unit=?
          UNION ALL
          SELECT SUM(CASE WHEN a.currency_confirmed = 'VEB' THEN a.amount_confirmed/a.rate_confirmed ELSE a.amount_confirmed END)*-1  amount
            FROM co_payment a
           WHERE DATEDIFF(CURDATE(),a.payment_date_confirmed) > 120 
             AND a.id_unit=?
             AND a.status = 'A'
          UNION ALL
          SELECT SUM(a.amount)*-1 amount  
            FROM co_prompt_payment a
           WHERE DATEDIFF(CURDATE(),a.created_date) > 120 
             AND a.id_unit=?   
          UNION ALL
                  SELECT SUM(a.amount)amount
              FROM co_previous_due a
              WHERE DATEDIFF(CURDATE(),a.payment_date) > 120 
              AND a.id_unit=?) x";

    $param = array($idUnit,$idUnit,$idUnit,$idUnit);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs;
  }
   public static function accountStatus(TfSession $tfs,$idUnit){
    $q = "SELECT a.id,CONCAT('P',YEAR(a.payment_date_confirmed),LPAD(MONTH(a.payment_date_confirmed),2,0),'#',a.id)  ref,a.payment_date_confirmed,CASE WHEN a.currency_confirmed = 'VEB' THEN ROUND(a.amount_confirmed/a.rate_confirmed,2) ELSE a.amount_confirmed END amount, 'P' type
            FROM co_payment a
            WHERE DATEDIFF(CURDATE(),a.payment_date_confirmed) <= 120 
            AND a.id_unit=?
            AND a.status = 'A'
            UNION ALL
            SELECT a.id,CONCAT('B',YEAR(a.created_date),LPAD(MONTH(a.created_date),2,0),'#',a.id)  ref,a.created_date,a.amount, 'B' type
            FROM co_prompt_payment a
            WHERE DATEDIFF(CURDATE(),a.created_date) <= 120 
            AND a.id_unit=?
            UNION ALL
            SELECT a.id,CONCAT('R',YEAR(c.payment_date),LPAD(MONTH(c.payment_date),2,0),'#',a.id)  ref,c.payment_date,round(SUM((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate),2) amount, 'R' type
                        FROM co_bill a,
                             co_bill_detail b,
                             co_accounting c,
                             co_account e,
                             gl_month m
                      WHERE m.id = MONTH(c.payment_date)
                        AND e.id = b.id_account
                        AND DATEDIFF(CURDATE(),c.payment_date) <= 120 
                        AND c.id = a.id_accounting
                        AND b.id_bill = a.id
                        AND a.id_unit=?
                        GROUP BY a.id,c.payment_date
             UNION ALL
                  SELECT a.id,CONCAT('D',YEAR(a.payment_date),LPAD(MONTH(a.payment_date),2,0),'#',a.id)  ref,a.payment_date,round(SUM(a.amount),2) amount, 'D' type
              FROM co_previous_due a
              WHERE DATEDIFF(CURDATE(),a.payment_date) <= 120 
              AND a.id_unit=? 
              GROUP BY a.id,a.payment_date          
            ORDER BY 3 ASC";

    $param = array($idUnit,$idUnit,$idUnit,$idUnit);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  public function info(){
    $q = "SELECT u.id,u.name,u.dni,u.email_account,un.name unit,a.aliquot,
                CONCAT(j.nin_type,j.nin,'-',j.nin_control_digit) rif,
                j.trade_name,j.legal_address,j.email_account email,a.balance_30,a.balance_60,
                a.balance_90,a.balance_120,a.balance_previous,
                a.balance_30+a.balance_60+a.balance_90+a.balance_120+a.balance_previous balance,
                a.rate,a.created_date,
                CONCAT(LPAD(MONTH(c.payment_date),2,0),'-',YEAR(c.payment_date)) period,
                CONCAT('R',YEAR(c.payment_date),LPAD(MONTH(c.payment_date),2,0),'#',a.id) ref, 
                 c.payment_date,DATE_ADD(c.payment_date, INTERVAL coalesce(p.days,39) DAY) expiration_date,
                 a.paymented_date,m.name month,SUM((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)) amount,
                 k.bank,k.account
       FROM  co_bill a,
                 co_bill_detail b,
                 co_accounting c,
                 co_account e,
                 gl_month m,
                 gl_juridical_person j LEFT JOIN (SELECT id_person,days FROM co_condominium_prompt_payment WHERE active='Y') p ON p.id_person = j.id 
                                       LEFT JOIN (SELECT id_person,bank,account FROM gl_person_banking WHERE active='Y') k ON k.id_person = j.id,
                 co_unit un LEFT JOIN (SELECT uu.id_unit,y.id,y.name,y.dni,y.email_account 
                                         FROM t_user y,
                                              co_unit_user uu,
                                              (SELECT id_unit,min(id) id
                                                  FROM co_unit_user 
                                                 GROUP BY id_unit) x
                                        WHERE y.id = uu.id_user 
                                          AND x.id = uu.id
                                          AND uu.active = 'Y') u ON u.id_unit = un.id
      WHERE j.id = un.id_person
            AND m.id = MONTH(c.payment_date)
            AND un.id = a.id_unit
            AND e.id = b.id_account
            AND c.id = a.id_accounting
            AND b.id_bill = a.id
            AND a.id=?
      GROUP BY u.id,u.name,u.dni,u.email_account,un.name,a.aliquot,
                CONCAT(j.nin_type,j.nin,'-',j.nin_control_digit),
                j.trade_name,j.legal_address,j.email_account,a.balance_30,a.balance_60,
                a.balance_90,a.balance_120,a.balance_previous,
                a.balance_30+a.balance_60+a.balance_90+a.balance_120+a.balance_previous,
                a.rate,a.created_date,
                CONCAT(LPAD(MONTH(c.payment_date),2,0),'-',YEAR(c.payment_date)),
                CONCAT('R',YEAR(c.payment_date),LPAD(MONTH(c.payment_date),2,0),'#',a.id),
                a.paymented_date,c.payment_date,DATE_ADD(c.payment_date, INTERVAL coalesce(p.days,39) DAY),
                m.name,k.bank,k.account";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);

    return $rs;
  }
  public static function BalanceDetailByCondominium(TfSession $tfs,$id){
   /* $q = "SELECT y.id,y.name,SUM(coalesce(x.credit,0)) credit,round(SUM(coalesce(x.usd,0)),2) usd
          FROM co_unit y LEFT JOIN 
               (SELECT  u.id,u.name,0 credit,
                        SUM( CASE WHEN a.paymented_date IS NULL THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) usd
                            FROM co_unit u,
                                 co_bill a,
                                 co_bill_detail b,
                                 co_accounting c,
                                 co_account e
                          WHERE e.id = b.id_account
                            AND c.id = a.id_accounting
                            AND b.id_bill = a.id
                            AND a.paymented_date IS NULL
                            AND a.id_unit=u.id
                            AND u.id_person = ?
                            GROUP BY u.id,u.name
                UNION ALL
                 SELECT u.id,u.name,0 credit,SUM(a.amount) usd
                    FROM co_previous_due a,
                         co_unit u
                   WHERE a.paymented_date IS NULL
                     AND a.id_unit = u.id
                     AND u.id_person = ?
                GROUP BY u.id,u.name
                UNION ALL
                SELECT u.id,u.name,SUM(a.balance) credit, 0 usd
                  FROM co_credit a,
                       co_unit u
                WHERE a.status='E'
                AND a.id_unit = u.id
                AND u.id_person = ?
                GROUP BY u.id,u.name) x ON x.id = y.id 
        WHERE y.id_person = ?
         GROUP BY y.id,y.name
          ORDER BY y.name";*/

        $q="SELECT y.id,y.name,SUM(coalesce(x.credit,0)) credit,SUM(coalesce(x.usd,0)) usd
          FROM co_unit y LEFT JOIN 
               ( SELECT  br.id,br.name,0 credit,SUM(br.usd) usd
                    FROM (SELECT  u.id,u.name,a.id bill,
                        round(SUM( CASE WHEN a.paymented_date IS NULL THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END),2) usd
                            FROM co_unit u,
                                 co_bill a,
                                 co_bill_detail b,
                                 co_accounting c,
                                 co_account e
                          WHERE e.id = b.id_account
                            AND c.id = a.id_accounting
                            AND b.id_bill = a.id
                            AND a.paymented_date IS NULL
                            AND a.id_unit=u.id
                            AND u.id_person = ?
                            GROUP BY u.id,u.name,a.id) br
                 GROUP BY br.id,br.name                  
                   
                UNION ALL
                 SELECT u.id,u.name,0 credit,SUM(a.amount) usd
                    FROM co_previous_due a,
                         co_unit u
                   WHERE a.paymented_date IS NULL
                     AND a.id_unit = u.id
                     AND u.id_person = ?
                GROUP BY u.id,u.name
                UNION ALL
                SELECT u.id,u.name,SUM(a.balance) credit, 0 usd
                  FROM co_credit a,
                       co_unit u
                WHERE a.status='E'
                AND a.id_unit = u.id
                AND u.id_person = ?
                GROUP BY u.id,u.name) x ON x.id = y.id 
        WHERE y.id_person = ?
         GROUP BY y.id,y.name
          ORDER BY y.name";

    $param = array($id,$id,$id,$id);
   
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function DefaulterDetailByCondominium(TfSession $tfs,$id){
    $q = "SELECT x.id,x.name,SUM(x.credit) credit,SUM(x.usd) usd
  FROM (SELECT  u.id,u.name,0 credit,
                SUM( CASE WHEN a.paymented_date IS NULL THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) usd
                    FROM co_unit u,
                         co_bill a,
                         co_bill_detail b,
                         co_accounting c,
                         co_account e
                  WHERE e.id = b.id_account
                    AND c.id = a.id_accounting
                    AND b.id_bill = a.id
                    AND a.paymented_date IS NULL
                    AND a.id_unit=u.id
                    AND u.id_person = ?
                    GROUP BY u.id,u.name
        UNION ALL
         SELECT u.id,u.name,0 credit,SUM(a.amount) usd
            FROM co_previous_due a,
                 co_unit u
           WHERE a.paymented_date IS NULL
             AND a.id_unit = u.id
             AND u.id_person = ?
        GROUP BY u.id,u.name
        UNION ALL
        SELECT u.id,u.name,SUM(a.balance) credit, 0 usd
          FROM co_credit a,
               co_unit u
        WHERE a.status='E'
        AND a.id_unit = u.id
        AND u.id_person = ?
        GROUP BY u.id,u.name) x
        GROUP BY x.id,x.name
    ORDER BY 4 DESC";

    $param = array($id,$id,$id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }


   public static function BalanceXX(TfSession $tfs,$id_person){

   $q = "SELECT ROUND(SUM(balance_previous),2) balance_previous,ROUND(SUM(balance_120),2) balance_120,ROUND(SUM(balance_90),2) balance_90,ROUND(SUM(balance_60),2) balance_60,ROUND(SUM(balance_30),2) balance_30,ROUND(SUM(balance),2) balance
        FROM (SELECT SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) > 120 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_previous,          
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) BETWEEN 91 AND 120 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_120,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) BETWEEN 61 AND 90 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_90,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) BETWEEN 31 AND 60 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_60,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) < 31 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_30,
                  SUM((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ) balance
            FROM co_bill a,
                 co_bill_detail b,
                 co_accounting c,
                 co_account e,
                 co_unit u
          WHERE e.id = b.id_account
            AND c.id = a.id_accounting
            AND b.id_bill = a.id
            AND a.paymented_date IS NULL
            AND a.id_unit=u.id
            AND u.active = 'Y'
            AND u.id_person = ?
        UNION ALL   
        SELECT 
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) > 120 THEN a.amount  END) balance_previous,          
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) BETWEEN 91 AND 120 THEN a.amount  END) balance_120,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) BETWEEN 61 AND 90 THEN a.amount  END) balance_90,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) BETWEEN 31 AND 60 THEN a.amount  END) balance_60,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) < 31 THEN a.amount  END) balance_30,
                  SUM(a.amount) balance
            FROM co_previous_due a,
                 co_unit u
          WHERE a.paymented_date IS NULL
            AND a.id_unit=u.id
            AND u.active = 'Y'
            AND u.id_person = ?
        UNION ALL
        SELECT   SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) > 120 THEN a.balance  END)*-1 balance_previous,          
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) BETWEEN 91 AND 120 THEN a.balance  END)*-1 balance_120,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) BETWEEN 61 AND 90 THEN a.balance  END) *-1 balance_90,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) BETWEEN 31 AND 60 THEN a.balance  END)*-1 balance_60,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) < 31 THEN a.balance  END)*-1 balance_30,
                  SUM(a.balance)*-1 balance
          FROM co_credit a,
               co_unit u
        WHERE a.status='E'
        AND a.id_unit = u.id
        AND u.active = 'Y'
        AND u.id_person = ?) x";

    $param = array($id_person,$id_person,$id_person);
    
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs;

   } 


   public static function BalanceYY(TfSession $tfs,$id_unit){

   $q = "SELECT ROUND(SUM(balance_previous),2) balance_previous,ROUND(SUM(balance_120),2) balance_120,ROUND(SUM(balance_90),2) balance_90,ROUND(SUM(balance_60),2) balance_60,ROUND(SUM(balance_30),2) balance_30,ROUND(SUM(balance),2) balance
        FROM (SELECT SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) > 120 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_previous,          
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) BETWEEN 91 AND 120 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_120,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) BETWEEN 61 AND 90 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_90,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) BETWEEN 31 AND 60 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_60,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),c.payment_date) < 31 THEN (b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ELSE 0 END) balance_30,
                  SUM((b.amount*CASE WHEN e.seat = 'C' THEN -1 ELSE 1 END)/a.rate ) balance
            FROM co_bill a,
                 co_bill_detail b,
                 co_accounting c,
                 co_account e,
                 co_unit u
          WHERE e.id = b.id_account
            AND c.id = a.id_accounting
            AND b.id_bill = a.id
            AND a.paymented_date IS NULL
            AND a.id_unit=u.id
            AND u.active = 'Y'
            AND u.id = ?
        UNION ALL   
        SELECT 
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) > 120 THEN a.amount  END) balance_previous,          
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) BETWEEN 91 AND 120 THEN a.amount  END) balance_120,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) BETWEEN 61 AND 90 THEN a.amount  END) balance_90,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) BETWEEN 31 AND 60 THEN a.amount  END) balance_60,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.payment_date) < 31 THEN a.amount  END) balance_30,
                  SUM(a.amount) balance
            FROM co_previous_due a,
                 co_unit u
          WHERE a.paymented_date IS NULL
            AND a.id_unit=u.id
            AND u.active = 'Y'
            AND u.id = ?
        UNION ALL
        SELECT   SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) > 120 THEN a.balance  END)*-1 balance_previous,          
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) BETWEEN 91 AND 120 THEN a.balance  END)*-1 balance_120,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) BETWEEN 61 AND 90 THEN a.balance  END) *-1 balance_90,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) BETWEEN 31 AND 60 THEN a.balance  END)*-1 balance_60,
                  SUM(CASE WHEN DATEDIFF(CURDATE(),a.created_date) < 31 THEN a.balance  END)*-1 balance_30,
                  SUM(a.balance)*-1 balance
          FROM co_credit a,
               co_unit u
        WHERE a.status='E'
        AND a.id_unit = u.id
        AND u.active = 'Y'
        AND u.id = ?) x";

    $param = array($id_unit,$id_unit,$id_unit);
    
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs;

   } 
}
?>
