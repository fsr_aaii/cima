<?php
  class GlPersonDirector extends GlPersonDirectorBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_person"=>true,
                              "id_user"=>true,
                              "job"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }


   public function create(){
    parent::create();
    if ($this->isValid()){ 
       $q = "SELECT 1 exist 
                FROM t_user_role
               WHERE id = ?
                AND id_functional_area_role = 5";
       
       $param = array($this->id_user);
       list($rs) = $this->tfs->executeQuery($q,$param); 

       if ($rs["exist"]!=1){
         $q = "INSERT INTO t_user_role (id_user,id_functional_area_role) VALUES(?,5)";
         
         $param = array($this->id_user);
         $this->tfs->execute($q,$param); 
       } 
    }  
  }

  public function delete(){
    parent::delete();
    if ($this->isValid()){ 
       $q = "SELECT count(*) exist 
                FROM gl_person_director
               WHERE id_user = ?
                AND active = 'Y'";
       
       $param = array($this->id_user);
       list($rs) = $this->tfs->executeQuery($q,$param); 

       if ($rs["exist"]=='0'){
         $q = "UPDATE t_user_role SET active = 'N' WHERE id_user = ? AND id_functional_area_role = 5";
         
         $param = array($this->id_user);
         $this->tfs->execute($q,$param); 
       } 
    }  
  }

  public function inactivate(){
    $this->active = 'N';
    $this->update();
    if ($this->isValid()){ 
       $q = "SELECT count(*) exist 
                FROM gl_person_director
               WHERE id_user = ?
                AND active = 'Y'";
       
       $param = array($this->id_user);
       list($rs) = $this->tfs->executeQuery($q,$param); 

       if ($rs["exist"]=='0'){
         $q = "UPDATE t_user_role SET active = 'N' WHERE id_user = ? AND id_functional_area_role = 5";
         
         $param = array($this->id_user);
         $this->tfs->execute($q,$param); 
       } 
    }  
  }


   public function activate(){
    $this->active = 'Y';
    $this->update();
    if ($this->isValid()){ 
      $q = "UPDATE t_user_role SET active = 'Y' WHERE id_user = ? AND id_functional_area_role = 5";       
      $param = array($this->id_user);
         $this->tfs->execute($q,$param); 
    }  
  }

  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM gl_person_director
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM gl_person_director
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_person,
                 c.name id_user,
                 a.job,
                 a.active,
                 d.name created_by,
                 a.created_date
            FROM gl_person_director a,
                 gl_person b,
                 t_user c,
                 t_user d
           WHERE b.id = a.id_person
           AND c.id = a.id_user
           AND d.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }


  public static function dLByPerson(TfSession $tfs,$idPerson){
    $q = "SELECT a.id,
                 a.id_person,
                 CASE WHEN LENGTH(c.name) > 24 THEN CONCAT(SUBSTR(c.name,1,24),'...') ELSE c.name END name,
                  CASE WHEN LENGTH(c.email_account) > 24 THEN CONCAT(SUBSTR(c.email_account,1,24),'...') ELSE c.email_account END account,
                 CASE WHEN c.phone_number IS NULL THEN '&nbsp' ELSE c.phone_number END phone_number,
                 a.job,
                 a.active,
                 d.name created_by,
                 a.created_date
            FROM gl_person_director a,
                 gl_person b,
                 t_user c,
                 t_user d
           WHERE b.id = a.id_person
           AND c.id = a.id_user
           AND d.id = a.created_by
           AND a.id_person = ?";

    $param=array($idPerson);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
