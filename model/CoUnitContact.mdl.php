<?php
  class CoUnitContact extends CoUnitContactBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_unit"=>true,
                              "name"=>true,
                              "account"=>true,
                              "phone_number"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",name \"option\"
            FROM co_unit_contact
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT name description
            FROM co_unit_contact
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name id_unit,
                 a.name,
                 a.account,
                 a.phone_number,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM co_unit_contact a,
                 co_unit b,
                 t_user c
           WHERE b.id = a.id_unit
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
