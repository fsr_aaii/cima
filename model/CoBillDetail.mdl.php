<?php
  class CoBillDetail extends CoBillDetailBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_bill"=>true,
                              "id_account"=>true,
                              "amount"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM co_bill_detail
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM co_bill_detail
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }


 public function sumaryByAccounting(){
   

    $param = array($this->id,$this->id);       
    $rs = $this->tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function infoByBill(TfSession $tfs,$idBill){ 
    $q = "SELECT g.name group_name,
                 c.name account_name,
                 SUM(a.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END) amount_gl,
                 SUM(e.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END) amount,
                 e.type
            FROM (SELECT id_accounting,id_account,SUM(amount) amount
                    FROM co_accounting_detail
                    GROUP BY id_accounting,id_account) a,
                 co_accounting b,
                 co_account c,
                 co_account_group g,
                 co_bill d,
                 co_bill_detail e
           WHERE b.id = a.id_accounting
           AND g.id = c.id_account_group 
           AND c.id = a.id_account
           AND a.id_account = e.id_account
           AND a.id_accounting = d.id_accounting
           AND e.type = 'G'
           AND e.id_bill =d.id
           AND d.id = ?
           GROUP BY g.name, c.name,e.type
           UNION ALL
           SELECT g.name group_name,
                 c.name account_name,SUM(e.amount) amount_gl,
                 SUM(e.amount) amount,e.type
            FROM co_account c,
                 co_account_group g,
                 co_bill d,
                 co_bill_detail e
           WHERE g.id = c.id_account_group 
           AND c.id = e.id_account
           AND e.type = 'U'
           AND e.id_bill =d.id
           AND d.id = ?   
           GROUP BY g.name, c.name,e.type
           ORDER BY 1 ASC,2 ASC";
    $param = array($idBill,$idBill);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_bill,
                 c.name id_account,
                 (a.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END) amount,
                 d.name created_by,
                 a.created_date
            FROM co_bill_detail a,
                 co_bill b,
                 co_account c,
                 t_user d
           WHERE b.id = a.id_bill
           AND c.id = a.id_account
           AND d.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function sumaryByBill(TfSession $tfs,$idBill){
    $q = "SELECT c.name account,SUM((a.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END)) amount_gl,
                    e.amount*CASE WHEN c.seat = 'C' THEN -1 ELSE 1 END amount,e.type
            FROM co_accounting_detail a,
                 co_accounting b,
                 co_account c,
                 co_bill d,
                 co_bill_detail e
           WHERE b.id = a.id_accounting
           AND c.id = a.id_account
           AND a.id_account = e.id_account
           AND a.id_accounting = d.id_accounting
           AND e.id_bill =d.id
           AND e.type = 'G'
           AND d.id = ?
           GROUP BY c.name,e.amount,e.type
        UNION ALL
        SELECT c.name account,e.amount amount_gl,e.amount amount,e.type
            FROM co_account c,
                 co_bill d,
                 co_bill_detail e
           WHERE c.id = e.id_account
           AND e.type = 'U'
           AND e.id_bill =d.id
           AND d.id = ?";

    $param = array($idBill,$idBill);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
