<?php
  class OpNotification extends OpNotificationBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_inspection_report"=>true,
                              "subject"=>true,
                              "body"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM op_notification
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM op_notification
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name id_inspection_report,
                 a.subject,
                 a.body,
                 c.name created_by,
                 a.created_date
            FROM op_notification a,
                 op_inspection_report b,
                 t_user c
           WHERE b.id = a.id_inspection_report
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function dLByReport(TfSession $tfs,$id){
    $q = "SELECT a.id,
                 a.subject,
                 a.body,
                 c.name created_by,
                 a.created_date
            FROM op_notification a,
                 op_inspection_report b,
                 t_user c
           WHERE b.id = a.id_inspection_report
           AND c.id = a.created_by
           AND a.id_inspection_report = ?
           ORDER BY a.created_date";

    $param = array($id);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
