<?php
  class OpInspectionReport2 extends OpInspectionReportBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_customer"=>true,
                              "vessel"=>true,
                              "id_port_pier"=>true,
                              "product"=>true,
                              "quantity"=>true,
                              "inspection_date"=>true,
                              "issued_date"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM op_inspection_report
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM op_inspection_report
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_customer,
                 a.vessel,
                 c.name id_port_pier,
                 a.product,
                 a.quantity,
                 a.inspection_date,
                 a.issued_date,
                 d.name created_by,
                 a.created_date
            FROM op_inspection_report a,
                 gl_person b,
                 op_port_pier c,
                 t_user d
           WHERE b.id = a.id_customer
           AND c.id = a.id_port_pier
           AND d.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }


  public static function getClient(){
    $client = new Google_Client();
    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes(array(Google_Service_Sheets::SPREADSHEETS_READONLY,Google_Service_Drive::DRIVE));

    $client->setAuthConfig('environment/credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'environment/google.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        }
    }
    return $client;
  }

  public function createFile($service,$client) {
    $file = new Google_Service_Drive_DriveFile();
    $file->setName('Test2ss1.xlsx');
    $file->setDescription('Reporte de Inspecci&oacute;n ');
    $file->setMimeType('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  

  $sheets = new \Google_Service_Sheets($client);

    //$parentId='1oRWDG8sZOP27o9SozcvPzVfyOQpS7qcC';
    // Set the parent folder.
    // if ($parentId != null) {
    // $parent = new Google_Service_Drive_ParentReference();
    // $parent->setId($parentId);
    $file->setParents(['1llssrd48gOZ_pKYAX0xlvcggK9m3fq_R']);
    //}

  try {
    $data = file_get_contents("template/InspectionReport.xlsx");

    $createdFile = $service->files->create($file, array(
      'data' => $data,
      'mimeType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    ));

    // Uncomment the following line to print the File ID
    echo 'File ID:'.$createdFile->getId();


$sheets = new Google_Service_Sheets($client);

// The ID of the spreadsheet to update.
$spreadsheetId = $createdFile->getId();// TODO: Update placeholder value.

// The A1 notation of the values to update.
$range = 'Portada!B39:C39';  // TODO: Update placeholder value.

// TODO: Assign values to desired properties of `requestBody`. All existing
// properties will be replaced:
$requestBody = new Google_Service_Sheets_ValueRange();

$response = $sheets->spreadsheets_values->update($spreadsheetId, $range, $requestBody);

// TODO: Change code below to process the `response` object:
echo '<pre>', var_export($response, true), '</pre>', "\n";

  /*$spreadsheetId = $createdFile->getId();

$sheet_name = 'Portada';

$range = "Portada!B39:G39";


$updateBody = new \Google_Service_Sheets_ValueRange([
            'range' => $range,
            'majorDimension' => 'ROWS',
            'values' => ['values' => date('c')],
        ]);
        $sheets->spreadsheets_values->update(
            $spreadsheetId,
            $updateRange,
            $updateBody,
            ['valueInputOption' => 'USER_ENTERED']
        );


*/
    return $createdFile;
  } catch (Exception $e) {
    print "An error occurred: " . $e->getMessage();
  }
}



  public function leer($client) {
  
$sheets = new Google_Service_Sheets($client);

// The ID of the spreadsheet to update.
$spreadsheetId = '1ItjIuV_X9yfB2Tf97xtLM30fH1Utq8pT';// TODO: Update placeholder value.

// The A1 notation of the values to update.
$range = 'Portada!B39';  // TODO: Update placeholder value.

// TODO: Assign values to desired properties of `requestBody`. All existing
// properties will be replaced:
$requestBody = new Google_Service_Sheets_ValueRange();

//$response = $sheets->spreadsheets_values->update($spreadsheetId, $range, $requestBody);

$results = $sheets->spreadsheets_values->get($spreadsheetId, $range );

    var_dump($results->getValues());

}



static public function sync(TfSession $tfs,$service, $client,$folderId) {

$optParams = array(
  'pageSize' => 100,
  'fields' => 'nextPageToken, files(id, name,headRevisionId,md5Checksum,modifiedTime,version,size)',
  'q' => "'".$folderId."' in parents",
);


$service->files->emptyTrash();

$results = $service->files->listFiles($optParams);

foreach ($results->getFiles() as $file) {
  //print_r($file);
  echo $file["name"]." - ".$file["id"]." - ".$file["size"]." - ".$file["md5Checksum"]." - ".$file["modifiedTime"]." - ".$file["version"]."<br>";
  

  INSERT INTO `grainca`.`op_inspection_report` (`id`,`name`,`version`,`process`,`created_by`,`created_date`)
  VALUES
  (?,?,?,'Y',$tfs->getUserId(),date("Y-m-d H:i:s"));


  /*$http = $client->authorize();

    // Open a file for writing
    $fp = fopen('upload/xlsx/'.$file["id"].'.xlsx', 'w');

    // Download in 1 MB chunks
    $chunkSizeBytes = 1 * 1024 * 1024;
    $chunkStart = 0;

    // Iterate over each chunk and write it to our file
    while ($chunkStart < $file["size"]) {
      $chunkEnd = $chunkStart + $chunkSizeBytes;
      $response = $http->request(
        'GET',
        sprintf('/drive/v3/files/%s', $file["id"]),
        [
          'query' => ['alt' => 'media'],
          'headers' => [
            'Range' => sprintf('bytes=%s-%s', $chunkStart, $chunkEnd)
          ]
        ]
      );
      $chunkStart = $chunkEnd + 1;
      fwrite($fp, $response->getBody()->getContents());
    }
    // close the file pointer
    fclose($fp);

  */
  }
}



/**
 * Print a file's metadata.
 *
 * @param Google_Service_Drive $service Drive API service instance.
 * @param string $fileId ID of the file to print metadata for.
 */
function printFile($service, $fileId) {
  try {
    $file = $service->files->get($fileId);

    print "Title: " . $file->getTitle();
    print "Description: " . $file->getDescription();
    print "MIME type: " . $file->getMimeType();
  } catch (Exception $e) {
    print "An error occurred: " . $e->getMessage();
  }
}

/**
 * Download a file's content.
 *
 * @param Google_Service_Drive $service Drive API service instance.
 * @param File $file Drive File instance.
 * @return String The file's content if successful, null otherwise.
 */
function downloadFile($client) {

 $httpClient = $client->authorize();
$request = new GuzzleHttp\Psr7\Request('GET', "https://www.googleapis.com/drive/v2/files/".'1ItjIuV_X9yfB2Tf97xtLM30fH1Utq8pT');

$response = $httpClient->send($request, ['save_to' => 'upload/xlsx/utest.xlsx']);

echo $response->getStatusCode(); // 200
echo $response->getProtocolVersion(); // 1.1

if ($response->getBody()) {
    
/*$file="utest.xlsx";
header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header("Content-Disposition: attachment; filename=$file");


$resource = fopen('upload/xlsx/utest.xlsx', 'w');

$stream = GuzzleHttp\Psr7\stream_for($resource);

//$response = $client->request('GET', 'https://test.com/download', ['sink' => $stream]);

echo stream_get_contents($stream);*/



}


}


}
?>
