<?php
  class CoUnit extends CoUnitBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_person"=>true,
                              "name"=>true,
                              "aliquot"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }

 
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",name \"option\"
            FROM co_unit
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function selectOptionsByPerson(TfSession $tfs,$idPerson){ 
    $q = "SELECT id \"value\",name \"option\"
            FROM co_unit
          WHERE id_person = ?
           ORDER BY 2";
    
    $param=array($idPerson);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT name description
            FROM co_unit
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function idPerson(TfSession $tfs,$id){ 
    $q = "SELECT id_person
            FROM co_unit
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["id_person"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_person,
                 a.name,
                 a.aliquot,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM co_unit a,
                 gl_person b,
                 t_user c
           WHERE b.id = a.id_person
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }


  public static function all(TfSession $tfs){
    $q = "SELECT a.id,
                 b.legal_name condominium,
                 a.name,
                 CONCAT(a.aliquot,'%') aliquot
            FROM co_unit a,
                 gl_juridical_person b
           WHERE b.id = a.id_person
             AND a.active = 'Y'";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function dLByPerson(TfSession $tfs,$idPerson){
    $q = "SELECT a.id,
                 b.legal_name condominium,
                 a.name,
                 CONCAT(a.aliquot,'%') aliquot,
                 CASE WHEN LENGTH(x.name) > 24 THEN CONCAT(SUBSTR(x.name,1,24),'...') ELSE x.name END user
            FROM co_unit a LEFT JOIN (SELECT uu.id_unit,MAX(u.name) name 
                                        FROM t_user u,
                                             co_unit_user uu 
                                       WHERE u.id = uu.id_user 
                                         AND uu.active = 'Y'
                                        GROUP BY uu.id_unit) x ON x.id_unit = a.id  ,
                 gl_juridical_person b
           WHERE b.id = a.id_person
             AND a.active = 'Y'
             AND a.id_person = ?";

    $param=array($idPerson);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function infoByPerson(TfSession $tfs,$idPerson){
    $q = "SELECT a.id,
                 a.aliquot
            FROM co_unit a 
           WHERE a.active = 'Y'
             AND a.id_person = ?";

    $param=array($idPerson);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

}
?>
