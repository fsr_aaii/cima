<?php
  class HrUserEvaluation extends HrUserEvaluationBase {
  
  public $set_timer = false;
  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_user"=>true,
                              "date"=>true,
                              "active"=>true,
                              "id_evaluation_type"=>true,
                              "start_timestamp"=>true,
                              "end_timestamp"=>true,
                              "are_right"=>true,
                              "are_wrong"=>true,
                              "are_empty"=>true,
                              "evaluator_type"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM hr_user_evaluation
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM hr_user_evaluation
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_user,
                 a.date,
                 a.active,
                 b.name id_evaluation_type,
                 a.evaluator_type,
                 c.name created_by,
                 a.created_date
            FROM hr_user_evaluation a,
                 hr_evaluation_type b,
                 t_user c
           WHERE b.id = a.id_evaluation_type
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public function populate4ThisMonth(){

    $q = "SELECT id
            FROM hr_user_evaluation
           WHERE id_user=?
             AND (active = 'Y' OR end_timestamp IS NOT NULL)
             AND MONTH(date)=?
             AND YEAR(date)=?";

    $param = array($this->tfs->getUserId(),date("m"),date("Y"));

    list($rs) = $this->tfs->executeQuery($q,$param);

    if ($rs["id"]!=''){
      $this->populatePk($rs["id"]);
    }else{
      if (date("d")==APP_EVALUATION_DAY){
        $q = "SELECT count(id) qty
              FROM hr_user_evaluation
             WHERE id_user=?
               AND active = 'N'
               AND MONTH(date)=?
               AND YEAR(date)=?";

        $param = array($this->tfs->getUserId(),date("m"),date("Y"));

        list($rs) = $this->tfs->executeQuery($q,$param);
        
        if ($rs["qty"]< 2){
          $q = "SELECT id
                FROM hr_user_homework
               WHERE id_user=?
                 AND MONTH(date)=?
                 AND YEAR(date)=?";

          $param = array($this->tfs->getUserId(),date("m"),date("Y"));

          list($rs) = $this->tfs->executeQuery($q,$param);

          if ($rs["id"]!=''){
            $this->id_user=$this->tfs->getUserId();
            $this->date=date("Y-m-d");
            $this->active="Y";
            $this->id_evaluation_type="1";
            $this->evaluator_type="U";
            $this->created_by=$this->tfs->getUserId();
            $this->created_date=date("Y-m-d H:i:s");
            $this->setValidations();
            $this->create();

            $q="INSERT INTO hr_user_evaluation_criteria
                (id_user_evaluation,id_evaluation_criteria,created_by,created_date)
                SELECT DISTINCT ?,c.id,?,?
                  FROM hr_user_homework_criteria a,
                       hr_evaluation_criteria c,
                       hr_evaluation_criteria_option d
                 WHERE d.id_evaluation_criteria = c.id
                   AND c.id = a.id_evaluation_criteria
                   AND a.id_user_homework = ?
                   ORDER BY RAND()
                LIMIT ?";

            $param = array($this->id,$this->created_by,$this->created_date,$rs["id"],APP_EVALUATION_QTY);    

            $this->tfs->execute($q,$param);
            $this->tfs->checkTrans();
            $this->set_timer = true;
          }else{
            $this->valid = false;
            $this->objError[]="No puede realizar la prueba ya que no se le asigno una guia anteriormente";
          }
        }else{
          $this->valid = false;
          $this->objError[]="No se puede realizar mas de  2 pruebas en un mes";
        }    
      }else{
        $this->valid = false;
        $this->objError[]="No puede realizar la prueba ya que el dia de presentaci&oacute;n ya paso";
      }  
    }

  }  
  
  public function setSetTimer($value){
    $this->set_timer=$value;
  }
  public function getSetTimer(){
    return $this->set_timer;
  }

  public static function dashboard01(TfSession $tfs,$month){
    $q = " SELECT COALESCE(a.source,'NINGUNA')  institucion,COUNT(a.id) inscritos ,COUNT(b.id_user) participantes,
              SUM(COALESCE(are_right,0)) are_right,SUM(COALESCE(are_wrong,0)) are_wrong,SUM(COALESCE(are_empty,0)) are_empty ,
              SUM(TIMESTAMPDIFF(MINUTE,COALESCE(b.start_timestamp,0),COALESCE(b.end_timestamp,0)))  minutes
        FROM t_user a LEFT JOIN (SELECT id_user,are_right,are_wrong,are_empty,start_timestamp,end_timestamp
                    FROM hr_user_evaluation
                                  WHERE end_timestamp IS NOT NULL
                                   AND DATE_FORMAT(date,'%Y-%m')=?) b ON b.id_user = a.id
      GROUP BY 1";

    $param = array($month);  
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function dashboard02a(TfSession $tfs,$month){
    $q = " SELECT COALESCE(b.source,'NINGUNA') institucion,a.are_right,count(*) qty
              FROM hr_user_evaluation a,
                   t_user b 
              WHERE b.id = a.id_user 
               AND a.end_timestamp IS NOT NULL
               AND DATE_FORMAT(a.date,'%Y-%m')=?
      GROUP BY 1,2";

    $param = array($month);  
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function dashboard02b(TfSession $tfs,$month){
    $q = " SELECT are_right,count(*) qty
              FROM hr_user_evaluation
              WHERE end_timestamp IS NOT NULL
               AND DATE_FORMAT(date,'%Y-%m')=?
      GROUP BY 1";

    $param = array($month);  
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function dashboard03(TfSession $tfs,$month){
    $q = " SELECT c.description question,
                     SUM(CASE WHEN b.correct IS NULL THEN 1 ELSE 0 END) are_empty,
                     SUM(CASE WHEN b.correct = 'Y' THEN 1 ELSE 0 END) are_right,
                     SUM(CASE WHEN b.correct = 'N' THEN 1 ELSE 0 END) are_wrong
                   FROM hr_user_evaluation e,
                                          hr_evaluation_criteria c,
                                          hr_user_evaluation_criteria a LEFT JOIN hr_evaluation_criteria_option b
                      ON  b.id = a.value
                  WHERE c.id = a.id_evaluation_criteria
                     AND a.id_user_evaluation = e.id
                     AND e.end_timestamp IS NOT NULL
                     AND DATE_FORMAT(e.date,'%Y-%m')=?
                   GROUP BY c.description";

    $param = array($month);  
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }



}
?>
