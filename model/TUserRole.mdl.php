<?php
  class TUserRole extends TUserRoleBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_user"=>true,
                              "id_functional_area_role"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM t_user_role
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM t_user_role
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name id_user,
                 a.id_functional_area_role,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM t_user_role a,
                 t_user b,
                 t_user c
           WHERE b.id = a.id_user
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
