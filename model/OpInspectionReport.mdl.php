<?php

  use PhpOffice\PhpSpreadsheet\Spreadsheet;
  use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
  use PhpOffice\PhpSpreadsheet\IOFactory;

  class OpInspectionReport extends OpInspectionReportBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_customer"=>true,
                              "vessel"=>true,
                              "id_port_pier"=>true,
                              "id_product_1"=>true,
                              "id_product_2"=>true,
                              "BL"=>true,
                              "quantity"=>true,
                              "inspection_date"=>true,
                              "issued_date"=>true,
                              "created_by"=>true,
                              "created_date"=>true,
                               "inspection_report_no"=>true,
                               "id_file"=>true,
                               "name"=>true,
                               "version"=>true,
                               "process"=>true,
                               "last_processing"=>true);



               

  }
  public function create(){

    $client = OpInspectionReport::getClient();
    $drive = new Google_Service_Drive($client); 
    $this->copyBaseFile($client,$drive);

    parent::create();  
  }


  public function update(){

    $client = OpInspectionReport::getClient();
    $drive = new Google_Service_Drive($client); 
    $this->updateBaseFile($client,$drive);

    parent::update();  
  }


  public function delete(){

    $client = OpInspectionReport::getClient();
    $drive = new Google_Service_Drive($client); 
    $response = $drive->files->delete($this->id_file);

    parent::delete();  
  }


private function copyBaseFile($client,$service){
  $origin_file_id = "1xecmHQRI1q2awSZcgsxcLs-9mO7DmbV2eLvZmxqptgw";
  $copiedFile = new Google_Service_Drive_DriveFile();
  $copiedFile->setParents(["1llssrd48gOZ_pKYAX0xlvcggK9m3fq_R"]);

   
  $copiedFile->setName($this->name);


  try {
      
      $response = $service->files->copy($origin_file_id, $copiedFile);


      $params = ['valueInputOption' => 'RAW'];

      $sheet = new Google_Service_Sheets($client);



      $update_range ="Base de Datos!B1" ;
      $values = [[$this->vessel]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($response["id"], $update_range, $body, $params);

      $update_range ="Base de Datos!F1" ;
      $values = [[OpPortPier::description($this->tfs,$this->id_port_pier)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($response["id"], $update_range, $body, $params);

      $update_range ="Base de Datos!H1" ;
      $values = [[$this->quantity]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($response["id"], $update_range, $body, $params);


      $update_range ="Base de Datos!B2" ;
      $values = [[OpProduct::description($this->tfs,$this->id_product_1)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($response["id"], $update_range, $body, $params);


      $update_range ="Base de Datos!B3" ;
      if ($this->id_product_2!=''){
         $values = [[OpProduct::description($this->tfs,$this->id_product_2)]];  
      }else{
         $values = [[""]];  
      }   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($response["id"], $update_range, $body, $params);

      $update_range ="Base de Datos!B4" ;
      $values = [[GlJuridicalPerson::description($this->tfs,$this->id_customer)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($response["id"], $update_range, $body, $params);

     
      //$ownerPermission = new Google_Service_Drive_Permission();
      //$ownerPermission->setEmailAddress("{myemailhere}");
      //$ownerPermission->setType('user');
      //$ownerPermission->setRole('owner');
      //$service->permissions->create("{sheet_id_here}", $ownerPermission, 
       //   ['emailMessage' => 'You added a file to ' .
       //   static::$applicationName . ': ' . "Does this work"]);
       //   
       $this->version=$response["version"];
       $this->id_file = $response["id"];


  } catch (Exception $e) {
      print "An error occurred: " . $e->getMessage();
  }
}


private function updateBaseFile($client,$service){
  
  try {
      
     
      /*$pos = strpos($this->product,' ');

     if ($pos === false) {
        $name = $this->getVessel().'-'.$this->getProduct().'-'.OpPortPier::description($tfs,$this->getIdPortPier()).'-'.date("YmdHis"));
    } else {
        $opInspectionReport->setName($opInspectionReport->getVessel().'-'.strstr($opInspectionReport->getProduct(), ' ', true).'-'.OpPortPier::description($tfs,$opInspectionReport->getIdPortPier()).'-'.date("YmdHis"));
    }

     $file = new Google_Service_Drive_DriveFile();
    $file->setTitle($newTitle);

    $updatedFile = $service->files->patch($fileId, $file, array(
      'fields' => 'title'
    ));
*/

      $params = ['valueInputOption' => 'RAW'];

      $sheet = new Google_Service_Sheets($client);



      $update_range ="Base de Datos!B1" ;
      $values = [[$this->vessel]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($this->id_file, $update_range, $body, $params);

      $update_range ="Base de Datos!F1" ;
      $values = [[OpPortPier::description($this->tfs,$this->id_port_pier)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($this->id_file, $update_range, $body, $params);

      $update_range ="Base de Datos!H1" ;
      $values = [[$this->quantity]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($this->id_file, $update_range, $body, $params);


       $update_range ="Base de Datos!B2" ;
      $values = [[OpProduct::description($this->tfs,$this->id_product_1)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($this->id_file, $update_range, $body, $params);


      $update_range ="Base de Datos!B3" ;
      if ($this->id_product_2!=''){
         $values = [[OpProduct::description($this->tfs,$this->id_product_2)]];  
      }else{
         $values = [[""]];  
      }   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($this->id_file, $update_range, $body, $params);

      $update_range ="Base de Datos!B4" ;
      $values = [[GlJuridicalPerson::description($this->tfs,$this->id_customer)]];   
      $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

      
      $update_sheet = $sheet->spreadsheets_values->update($this->id_file, $update_range, $body, $params);


  } catch (Exception $e) {
      print "An error occurred: " . $e->getMessage();
  }
}
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM op_inspection_report
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM op_inspection_report
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }

  public static function version(TfSession $tfs,$id){ 
    $q = "SELECT  version
            FROM op_inspection_report
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["version"];
  }

   public static function idFile(TfSession $tfs,$id){ 
    $q = "SELECT id_file
            FROM op_inspection_report
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["id_file"];
  }


  public static function info(TfSession $tfs,$id){ 
    $q = "SELECT a.id,
                 a.id_customer,
                 a.vessel,
                 a.BL,
                 c.name port_pier,
                 b.trade_name customer,
                 e.name product_1,
                 f.name product_2,
                 a.quantity
            FROM op_inspection_report a LEFT JOIN op_product e ON e.id = a.id_product_1
                                        LEFT JOIN op_product f ON f.id = a.id_product_2,
                 gl_juridical_person b,
                 op_port_pier c
           WHERE b.id = a.id_customer 
           AND b.id = a.id_customer
           AND c.id = a.id_port_pier
           AND a.id=?";
    

     $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function DetailByReport(TfSession $tfs,$id_inspection_report){
    $q = " SELECT cantidad_kgr qty,round(cantidad_kgr/round(x.KG_APROX_PESADO/x.VEH_PESADO,0),0) cant_veh_estim,round(x.KG_APROX_PESADO/x.VEH_PESADO,0) promedio_kgs,
      VEH_SICA, VEH_SICA*round(x.KG_APROX_PESADO/x.VEH_PESADO,0) KG_APROX_SICA,x.VEH_PESADO,x.KG_APROX_PESADO, cantidad_kgr-x.KG_APROX_PESADO remanente_kgs,round((cantidad_kgr-x.KG_APROX_PESADO)/round(x.KG_APROX_PESADO/x.VEH_PESADO,0),0) remanente_unidades      
  FROM (SELECT c.id,SUM(DISTINCT b.qty) cantidad_kgr,COUNT(a.net_weight) VEH_SICA,SUM(CASE WHEN coalesce(a.net_weight,0) > 0 THEN 1 ELSE 0 END) VEH_PESADO,SUM(REPLACE(a.net_weight, \".\", \"\")) KG_APROX_PESADO 
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id_inspection_report = ?
      AND b.id = a.id_destination
      AND a.active = 'Y'      
    GROUP BY c.id) x";
    
    $param = array($id_inspection_report);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs;
  }


  public static function detailTransport(TfSession $tfs,$id){
    $q = " SELECT TRIM(a.transport) transport,COUNT(a.net_weight) VEH_SICA,
          SUM(CASE WHEN coalesce(a.net_weight,0) > 0 THEN 1 ELSE 0 END) VEH_PESADO,SUM(REPLACE(a.net_weight, \".\", \"\")) KG_APROX_PESADO 
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id_inspection_report = ?
      AND b.id = a.id_destination
      AND a.active = 'Y'      
    GROUP BY TRIM(a.transport)
     ORDER BY 2 DESC,3 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }


  

  public static function detailTransport2(TfSession $tfs,$id){
    $q = " SELECT TRIM(a.transport) transport,a.SICA_guide,UPPER(a.name) name,b.name destination
      FROM op_inspection_report_detail a,
        op_destination b,
        op_inspection_report c 
      WHERE c.id = b.id_inspection_report
      AND b.active = 'Y'
      AND b.id_inspection_report = ?
      AND b.id = a.id_destination
      AND coalesce(a.net_weight,0) = 0
      AND a.active = 'Y'    
     ORDER BY 2 DESC,3 DESC";
    
    $param = array($id);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_customer,
                 a.vessel,
                 a.BL,
                 c.name port_pier,
                 b.trade_name customer,
                 e.name product_1,
                 f.name product_2,
                 a.quantity
            FROM op_inspection_report a LEFT JOIN op_product e ON e.id = a.id_product_1
                                        LEFT JOIN op_product f ON f.id = a.id_product_2,
                 gl_juridical_person b,
                 op_port_pier c
           WHERE b.id = a.id_customer 
           AND b.id = a.id_customer
           AND c.id = a.id_port_pier";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }


  public static function dLByUser(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_customer,
                 a.vessel,
                 a.BL,
                 c.name port_pier,
                 b.trade_name customer,
                 e.name product_1,
                 f.name product_2,
                 a.quantity
            FROM op_inspection_report a LEFT JOIN op_product e ON e.id = a.id_product_1
                                        LEFT JOIN op_product f ON f.id = a.id_product_2,
                 gl_juridical_person b,
                 op_port_pier c,
                 gl_person_contact d
           WHERE b.id = a.id_customer 
           AND b.id = a.id_customer
           AND c.id = a.id_port_pier
           AND a.id_customer = d.id_person
           AND d.account=?";
    $param = array($tfs->getUserLogin());       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  
  public static function process(TfSession $tfs){ 
    $q = "SELECT  id
            FROM op_inspection_report
           WHERE process='Y'";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function getClient(){
    $client = new Google_Client();
    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes(array(Google_Service_Sheets::SPREADSHEETS_READONLY,Google_Service_Drive::DRIVE));

    $client->setAuthConfig('environment/credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = 'environment/google.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        }
    }
    return $client;
  }

  

static public function sync2(TfSession $tfs,$service, $client,$folderId) {

$optParams = array(
  'pageSize' => 100,
  'fields' => 'nextPageToken, files(id, name,headRevisionId,md5Checksum,modifiedTime,version,size)',
  'q' => "'".$folderId."' in parents",
);


$service->files->emptyTrash();

$results = $service->files->listFiles($optParams);

foreach ($results->getFiles() as $file) {
 //echo $file["name"]." - ".$file["id"]." - ".$file["size"]." - ".$file["md5Checksum"]." - ".$file["modifiedTime"]." - ".$file["version"]."<br>";
  

  $version=OpInspectionReport::version($tfs,$file["id"]);
  $process = FALSE;
  if ($version!=''){
    if ($version!=$file["version"]){
      $q = "UPDATE op_inspection_report 
               SET name=?,version=?,process='Y',last_processing=?
             WHERE id = ?";

      $param=array($file["name"],$file["version"],date("Y-m-d H:i:s"),$file["id"]);

      $rs = $tfs->execute($q,$param);
      $process = TRUE;
    }
  }else{
     $q = "INSERT INTO op_inspection_report (id,name,version,process,last_processing,created_by,created_date)
        VALUES (?,?,?,'Y',?,?,?)";


     $param=array($file["id"],$file["name"],$file["version"],date("Y-m-d H:i:s"),$tfs->getUserId(),date("Y-m-d H:i:s"));

     $rs = $tfs->execute($q,$param);
     $process = TRUE;
  }



 
  if ($process){
    $http = $client->authorize();
    $fp = fopen('upload/xlsx/'.$file["id"].'.xlsx', 'w');
    $chunkSizeBytes = 1 * 1024 * 1024;
    $chunkStart = 0;

    while ($chunkStart < $file["size"]) {
      $chunkEnd = $chunkStart + $chunkSizeBytes;
      $response = $http->request(
        'GET',
        sprintf('/drive/v3/files/%s', $file["id"]),
        [
          'query' => ['alt' => 'media'],
          'headers' => [
            'Range' => sprintf('bytes=%s-%s', $chunkStart, $chunkEnd)
          ]
        ]
      );
      $chunkStart = $chunkEnd + 1;
      fwrite($fp, $response->getBody()->getContents());
    }
    fclose($fp);

    }


    $opInspectionReportList = OpInspectionReport::process($tfs);

    foreach ($opInspectionReportList as $row){
     $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
      $spreadsheet = $reader->load('upload/xlsx/'.$file["id"].'.xlsx');
      
      $opInspectionReport = new OpInspectionReport($tfs);
      $opInspectionReport->populatePk($row["id"]);
      
      $GlPersonRelationshipId = GlPersonRelationship::customerId($tfs,$spreadsheet->getSheetByName("Portada")->getCell('F8'));
      
    
      if ($GlPersonRelationshipId!=''){
         $opInspectionReport->setIdCustomer($GlPersonRelationshipId);
      }
      $opInspectionReport->setProduct($spreadsheet->getSheetByName("Portada")->getCell('F11'));
      $opInspectionReport->setQuantity($spreadsheet->getSheetByName("Portada")->getCell('F14'));
      $opPortPierId = OpPortPier::id($tfs,$spreadsheet->getSheetByName("Portada")->getCell('F17'));
      if ($opPortPierId!=''){
         $opInspectionReport->setIdPortPier($opPortPierId);
      }
      $opInspectionReport->setVessel($spreadsheet->getSheetByName("Portada")->getCell('F20'));
      $opInspectionReport->setInspectionDate(TfWidget::dmy2ymd($spreadsheet->getSheetByName("Portada")->getCell('F23')));
      $opInspectionReport->setInspectionReportNo($spreadsheet->getSheetByName("Portada")->getCell('F26'));
      
      $opInspectionReport->setIssuedDate(TfWidget::dmy2ymd($spreadsheet->getSheetByName("Portada")->getCell('F29')));
      $opInspectionReport->setProcess('N');
      $opInspectionReport->setLastProcessing(date("Y-m-d H:i:s"));
      $opInspectionReport->setValidations();
      $opInspectionReport->update();
      if ($opInspectionReport->isValid()){ 
        $tfs->checkTrans();
      }
      
     
    }


    


//echo $spreadsheet->getSheetByName("Portada")->getCell('C9')->getValue();

  


  
  /*$http = $client->authorize();

    // Open a file for writing
    $fp = fopen('upload/xlsx/'.$file["id"].'.xlsx', 'w');

    // Download in 1 MB chunks
    $chunkSizeBytes = 1 * 1024 * 1024;
    $chunkStart = 0;

    // Iterate over each chunk and write it to our file
    while ($chunkStart < $file["size"]) {
      $chunkEnd = $chunkStart + $chunkSizeBytes;
      $response = $http->request(
        'GET',
        sprintf('/drive/v3/files/%s', $file["id"]),
        [
          'query' => ['alt' => 'media'],
          'headers' => [
            'Range' => sprintf('bytes=%s-%s', $chunkStart, $chunkEnd)
          ]
        ]
      );
      $chunkStart = $chunkEnd + 1;
      fwrite($fp, $response->getBody()->getContents());
    }
    // close the file pointer
    fclose($fp);

  */
  }
}



static public function sync(TfSession $tfs,$service, $client,$folderId) {

$optParams = array(
  'pageSize' => 100,
  'fields' => 'nextPageToken, files(id, name,headRevisionId,md5Checksum,modifiedTime,version,size)',
  'q' => "'".$folderId."' in parents",
);


$service->files->emptyTrash();

$results = $service->files->listFiles($optParams);

foreach ($results->getFiles() as $file) {
// echo $file["name"]." - ".$file["id"]." - ".$file["size"]." - ".$file["md5Checksum"]." - ".$file["modifiedTime"]." - ".$file["version"]."<br>";
  

  $version=OpInspectionReport::version($tfs,$file["id"]);

  $process = FALSE;
  if ($version!=''){
    if ($version!=$file["version"]){
      $q = "UPDATE op_inspection_report 
               SET name=?,version=?,process='Y',last_processing=?
             WHERE id = ?";

      $param=array($file["name"],$file["version"],date("Y-m-d H:i:s"),$file["id"]);

      $rs = $tfs->execute($q,$param);
      $process = TRUE;
    }
  }

  if (!$process){
    $listHash= OpInspectionReportDetail::listHash($tfs,$file["id"]);

    OpInspectionReportDetail::inactive($tfs,$file["id"]);

    $sheets = new Google_Service_Sheets($client); // <--- This is from your script.
    $spreadsheetId = $file["id"];
    $range = 'A8:AB';
    
    $rows = $sheets->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'ROWS']);
    
    if (isset($rows['values'])) {
      $i="0"; 
      foreach ($rows['values'] as $row) {
        
        if (empty($row[4])) {
            break;
        }
        $opInspectionReportDetail = new OpInspectionReportDetail($tfs);

       if (md5(json_encode($row))==$listHash[$i]["hash"]){
        //echo "SIN CAMBIOS<br>";
        $opInspectionReportDetail->populatePK($listHash[$i]["id"]);
        $opInspectionReportDetail->setActive('Y');

        $opInspectionReportDetail->update();
        if ($opInspectionReportDetail->isValid()){ 
          $tfs->checkTrans();
        }
       }else{
        //echo "CAMBIO<br>";

        $opInspectionReportDetail->setRow($i);
        $opInspectionReportDetail->setHash(md5(json_encode($row)));
        $opInspectionReportDetail->setIdTravel($row[0]);
        $opInspectionReportDetail->setSICAGuide($row[1]);
        $opInspectionReportDetail->setIdList($row[2]);
        $opInspectionReportDetail->setTransport($row[3]);
        $opInspectionReportDetail->setName($row[4]);
        $opInspectionReportDetail->setNin($row[5]);
        $opInspectionReportDetail->setChuto($row[6]);
        $opInspectionReportDetail->setIdDestination(OpDestination::id($tfs,$file["id"],$row[7]));
        $opInspectionReportDetail->setBL($row[8]);
        $opInspectionReportDetail->setAdmissionDate($row[9]);
        $opInspectionReportDetail->setAdmissionTime($row[10]);
        $opInspectionReportDetail->setTaraNumber($row[11]);
        $opInspectionReportDetail->setStatus1($row[12]);
        $opInspectionReportDetail->setOperationDay($row[13]);
        $opInspectionReportDetail->setSurveyGuide($row[14]);
        $opInspectionReportDetail->setOcamarGuide($row[15]);
        $opInspectionReportDetail->setStore($row[16]);
        $opInspectionReportDetail->setGrossWeight($row[17]);
        $opInspectionReportDetail->setTaraWeight($row[18]);
        $opInspectionReportDetail->setNetWeight($row[19]);
        $opInspectionReportDetail->setAggregate($row[20]);
        $opInspectionReportDetail->setROB($row[21]);
        $opInspectionReportDetail->setStatus2($row[22]);
        $opInspectionReportDetail->setIdTravelOcamar($row[23]);
        $opInspectionReportDetail->setDepartureDate($row[24]);
        $opInspectionReportDetail->setDepartureTime($row[25]);
        $opInspectionReportDetail->setSeal($row[26]);
        $opInspectionReportDetail->setObservation($row[27]);
        $opInspectionReportDetail->setActive('Y');
        $opInspectionReportDetail->setCreatedBy($tfs->getUserId());
        $opInspectionReportDetail->setCreatedDate(date("Y-m-d H:i:s"));
        
        $opInspectionReportDetail->setValidations();
        $opInspectionReportDetail->create();
        if ($opInspectionReportDetail->isValid()){ 
          $tfs->checkTrans();
        }else{
          print_r($opInspectionReportDetail->getAttrErrors());
          print_r($row);
          break;
        }
       }         
       $i++;
       //echo json_encode($row)."<br>";
      }
    }
  }

}
}
}
?>
