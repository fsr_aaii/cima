<?php
  class FiTransactionElementUm extends FiTransactionElementUmBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_transaction_element"=>true,
                              "id_packeting"=>true,
                              "id_unit_measurement"=>true,
                              "qty"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM fi_transaction_element_um
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM fi_transaction_element_um
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name id_transaction_element,
                 c.name id_packeting,
                 d.name id_unit_measurement,
                 a.qty,
                 a.active,
                 e.name created_by,
                 a.created_date
            FROM fi_transaction_element_um a,
                 fi_transaction_element b,
                 mm_packeting c,
                 mm_unit_measurement d,
                 t_user e
           WHERE b.id = a.id_transaction_element
           AND c.id = a.id_packeting
           AND d.id = a.id_unit_measurement
           AND e.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function dataListByTE(TfSession $tfs,$idTransactionElement){
    $q = "SELECT a.id,
                 b.name id_transaction_element,
                 c.name id_packeting,
                 d.name id_unit_measurement,
                 a.qty,
                 a.active,
                 e.name created_by,
                 a.created_date
            FROM fi_transaction_element_um a,
                 fi_transaction_element b,
                 mm_packeting c,
                 mm_unit_measurement d,
                 t_user e
           WHERE b.id = a.id_transaction_element
           AND c.id = a.id_packeting
           AND d.id = a.id_unit_measurement
           AND e.id = a.created_by
           AND a.id_transaction_element = ?";

    $param = array($idTransactionElement);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
