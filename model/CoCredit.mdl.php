<?php
  class CoCredit extends CoCreditBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_unit"=>true,
                              "amount"=>true,
                              "balance"=>true,
                              "status"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM co_credit
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM co_credit
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name id_unit,
                 a.amount,
                 a.balance,
                 a.status,
                 c.name created_by,
                 a.created_date
            FROM co_credit a,
                 co_unit b,
                 t_user c
           WHERE b.id = a.id_unit
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
   

  public static function balanceByUnit(TfSession $tfs,$idUnit){
    $q = "SELECT SUM(a.balance) amount
            FROM co_credit a
           WHERE a.id_unit = ?
           AND a.status = 'E'";

    $param = array($idUnit);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["amount"];
  } 

  public static function dLBalanceByUnit(TfSession $tfs,$idUnit){
    $q = "SELECT a.id,a.balance amount
            FROM co_credit a
           WHERE a.id_unit = ?
           AND a.status = 'E'
           ORDER BY a.created_date DESC";

    $param = array($idUnit);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  } 
   public static function UnitBalanceByCondominium(TfSession $tfs,$idCondominium){
    $q = "SELECT a.id_unit,SUM(a.balance) amount
            FROM co_credit a,
                 co_unit b
           WHERE b.id_person = ?
             AND b.id = a.id_unit 
             AND a.status = 'E'
           GROUP BY a.id_unit";

    $param = array($idCondominium);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  } 



  
}
?>
