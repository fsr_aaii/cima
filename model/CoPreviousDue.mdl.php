<?php
  class CoPreviousDue extends CoPreviousDueBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_unit"=>true,
                              "payment_date"=>true,
                              "observation"=>true,
                              "amount"=>true,
                              "paymented_by"=>true,
                              "paymented_date"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public function create(){
    $coExchangeRate=CoExchange::rateUnit($this->tfs,$this->id_unit); 
    $balanceByUnit = CoCredit::balanceByUnit($this->tfs,$this->id_unit);

     if ($balanceByUnit>=$this->amount){         
        $this->paymented_by = $this->created_by;
        $this->paymented_date = $this->created_date;
        //////////
         $uCredits = CoCredit::dLBalanceByUnit($this->tfs,$this->id_unit);
              
            $auxDue = $this->amount;
             
            $fix=0;
            foreach ($uCredits as $c){
              unset($fix);
              if ($auxDue>0){
                if ($c["amount"]==$auxDue){

                  $q = "INSERT INTO co_credit_exchange_fix (id_credit,amount,rate,created_by,created_date)
                        VALUES (?,?,?,?,?)";

                  $param = array($c["id"],$c["amount"],$coExchangeRate,$this->created_by,$this->created_date);
                  $this->tfs->execute($q,$param);

                  $q = "UPDATE co_credit 
                         SET balance=0,status = 'D'
                       WHERE id = ?";

                  $param = array($c["id"]);
                  $this->tfs->execute($q,$param);

                  $fix=$auxDue;
                  $auxDue =0;

                }elseif ($c["amount"]<$auxDue){
                  
                  $q = "INSERT INTO co_credit_exchange_fix (id_credit,amount,rate,created_by,created_date)
                        VALUES (?,?,?,?,?)";

                  $param = array($c["id"],$c["amount"],$coExchangeRate,$this->created_by,$this->created_date);
                  $this->tfs->execute($q,$param);

                  $q = "UPDATE co_credit 
                         SET balance=0,status = 'D'
                       WHERE id = ?";

                  $param = array($c["id"]);
                  $this->tfs->execute($q,$param);

                  $fix=$c["amount"];
                  $auxDue-=$c["amount"];

                }else{
                  $q = "INSERT INTO co_credit_exchange_fix (id_credit,amount,rate,created_by,created_date)
                        VALUES (?,?,?,?,?)";

                  $param = array($c["id"],$auxDue,$coExchangeRate,$this->created_by,$this->created_date);
                  $this->tfs->execute($q,$param);

                  $q = "UPDATE co_credit 
                          SET balance=?
                        WHERE id = ?";

                  $param = array($c["amount"]-$auxDue,$c["id"]);
                  $this->tfs->execute($q,$param);

                  $fix=$auxDue;
                  $auxDue=0;
                }
              }
            }

    }    

    parent::create();             
    
  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM co_previous_due
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM co_previous_due
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_unit,
                 a.payment_date,
                 a.observation,
                 a.amount,
                 a.paymented_by,
                 a.paymented_date,
                 c.name created_by,
                 a.created_date
            FROM co_previous_due a,
                 co_unit b,
                 t_user c
           WHERE b.id = a.id_unit
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
    public static function dLByPerson(TfSession $tfs,$idPerson){
    $q = "SELECT a.id,
                 b.name unit,
                 a.payment_date,
                 a.paymented_date,
                 a.amount
            FROM co_previous_due a,
                 co_unit b
           WHERE a.id_unit = b.id
           AND  b.id_person = ?";

    $param = array($idPerson);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
