<?php
  class CoExchange extends CoExchangeBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_person"=>true,
                              "date"=>true,
                              "rate"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }

  public function create(){
     $q = "UPDATE co_exchange
              SET active = 'N'
           WHERE active = 'Y'
             AND id_person=?";
    $param = array($this->id_person);

    $this->tfs->execute($q,$param);
    parent::create();
  }
  
   public static function getSequence(TfSession $tfs,$entity){
        $q="SELECT MAX(id) current_value
              FROM co_exchange";

        list($rs) = $tfs->executeQuery($q);
        
        if ($rs["current_value"]!=''){
          return $rs["current_value"]+1; 
        }else{
          return 1;
        }
    }

  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM co_exchange
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM co_exchange
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }

  public static function rate(TfSession $tfs,$idPerson){ 
    $q = "SELECT  rate
            FROM co_exchange
           WHERE active = 'Y'
             AND id_person=?";
             
    $param = array($idPerson);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["rate"];
  }

  public static function rateUnit(TfSession $tfs,$idUnit){ 
    $q = "SELECT a.rate
            FROM co_exchange a,
                 co_unit u
           WHERE a.active = 'Y'
             AND a.id_person=u.id_person
             AND u.id = ?";
             
    $param = array($idUnit);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["rate"];
  }


  public static function rateUnitDate(TfSession $tfs,$idUnit,$date){ 
     return CoExchange::rateByDate($tfs,CoUnit::idPerson($tfs,$idUnit),$date);
  }
  

  public static function rateByDate(TfSession $tfs,$idPerson,$date){ 
    $q = "SELECT rate
            FROM co_exchange
           WHERE id = (SELECT MAX(a.id) id
                         FROM co_exchange a
                        WHERE a.id_person=?
                          AND DATE(a.date)=?)";
             
    $param = array($idPerson,$date);
    list($rs) = $tfs->executeQuery($q,$param);

    if ($rs["rate"]!=''){
      return $rs["rate"];
    }else{
       $q = "SELECT rate
                FROM co_exchange
               WHERE id = (SELECT MAX(a.id) id
                             FROM co_exchange a
                            WHERE a.id_person=?
                              AND DATE(a.date)<?)";
                 
        $param = array($idPerson,$date);
        list($rs) = $tfs->executeQuery($q,$param);
        if ($rs["rate"]!=''){
          return $rs["rate"];
        }else{
           $q = "SELECT rate
                    FROM co_exchange
                   WHERE id = (SELECT MIN(a.id) id
                                 FROM co_exchange a
                                WHERE a.id_person=?
                                  AND DATE(a.date)>?)";
                     
            $param = array($idPerson,$date);
            list($rs) = $tfs->executeQuery($q,$param);

            return $rs["rate"];
        }
    }
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.legal_name id_person,
                 a.date,
                 a.rate,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM co_exchange a,
                 gl_juridical_person b,
                 t_user c
           WHERE b.id = a.id_person
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function dLByPerson(TfSession $tfs,$idPerson){
    $q = "SELECT a.id,
                 a.date,
                 a.rate,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM co_exchange a,
                 gl_juridical_person b,
                 t_user c
           WHERE b.id = a.id_person
           AND c.id = a.created_by
           AND a.id_person = ?
           ORDER BY a.created_date DESC";

    $param = array($idPerson);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function customerList(TfSession $tfs){
    $q = "SELECT b.id,b.trade_name name,CONCAT(b.nin_type,b.nin,'-',b.nin_control_digit) rif,
                 a.date_from,a.id id_relationship,b.phone_number, p.qty exchanges
            FROM gl_person_relationship a,
                 gl_juridical_person b LEFT JOIN (SELECT x.id_person,COUNT(x.id) qty 
                                                    FROM co_exchange x 
                                                   WHERE x.active = 'Y'
                                                     AND date(x.created_date) = CURDATE()
                                                    GROUP BY x.id_person ) p ON p.id_person = b.id
           WHERE b.id = a.id_person
             AND a.id_relationship = 1";
             
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
