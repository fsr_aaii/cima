<?php
  class GlAreaCode extends GlAreaCodeBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_country"=>true,
                              "code"=>true,
                              "description"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",description \"option\"
            FROM gl_area_code
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT description description
            FROM gl_area_code
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name id_country,
                 a.code,
                 a.description
            FROM gl_area_code a,
                 gl_country b
           WHERE b.id = a.id_country";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
